export default class Water {

constructor(scene, player){
    scene.waters = scene.physics.add.group();
    this.scene = scene
    this.player = player

    this.player_water = scene.physics.add.collider(this.player.player, scene.waters, function(player, water){
        this.player.player.data.values.time_at_collision = this.scene.time.now
        this.player.player.data.values.hit_from = water.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 18)
        water.setVelocityX(0)
        player.body.immovable = false;
        water.body.immovable = true;
      }, function(player,water){
          return (player.data.values.health > 0 && player.data.values.post_hit_invincibility == false)
      }, this);
}

addWater(scene, x, y) {
		var water = scene.waters.create(x, y, 'water');
		water.setActive(true).setVisible(true)
		water.setData({
                'layer_collisions': [],
              });

    for (var i=0;i<scene.layers.length;i++){
        water.data.values.layer_collisions.push(scene.physics.add.collider(water, scene.layers[i]));}

    water.setScale(2)
    water.setOffset(0,18)
    water.setSize(30,45,false)
    water.setDepth(800)

		scene.waters.add(water)
    water.body.setAllowGravity(false)
    scene.anims.play('water', water);
    water.body.immovable = true;
}

}
