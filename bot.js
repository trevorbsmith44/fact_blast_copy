import MathStuff from  "./math_stuff.js";
import Bs from  "./bs.js";

export default class Bot {

constructor(scene, player){
    scene.bots = scene.physics.add.group();
    this.scene = scene
    this.player = player

    this.player_bot = scene.physics.add.overlap(this.player.player, scene.bots, function(player, bot){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = bot.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 6)
        bot.body.immovable = true;
        }, function(player, bot){
            return (bot.data.values.health > 0
              && this.player.player.data.values.post_hit_invincibility == false)
        }, this);

}

update(player){
  this.scene.bots.children.each(function(bot) {
      if (bot.active){
        // console.log(bot.state)
        bot.wheel1.setPosition(bot.x-50, bot.y+98)
        bot.wheel2.setPosition(bot.x-17, bot.y+98)
        bot.wheel3.setPosition(bot.x+17, bot.y+98)
        bot.wheel4.setPosition(bot.x+50, bot.y+98)
        bot.head.setPosition(bot.x-34, bot.y-106)
        bot.chest.setPosition(bot.x-34, bot.y-48)
        bot.pipe.setPosition(bot.x-32, bot.y+36)

        if (bot.state == 'roll_right'){
            if (bot.x < player.x-10){
                bot.anims.play('bot_roll_right', true);
                bot.setVelocityX(bot.data.values.speed+30);
                bot.buzz_saw.setPosition(bot.x+200, bot.y+35)
                bot.wheel1.setAngularVelocity(100);
                bot.wheel2.setAngularVelocity(100);
                bot.wheel3.setAngularVelocity(100);
                bot.wheel4.setAngularVelocity(100);
              }
            else if (bot.x >= player.x+10) {
                bot.state = 'roll_left_throw_left'}
            else if ((player.x+10) > bot.x && bot.x > (player.x-10)){
                bot.state = 'stand_right'}}

        else if (bot.state == 'roll_left_throw_left'){
            // if (bot.x >= player.x+10){
                bot.anims.play('bot_roll_left', true);
                bot.setVelocityX(-bot.data.values.speed);
                bot.buzz_saw.setPosition(bot.x+185, bot.y-60)
                bot.wheel1.setAngularVelocity(-100);
                bot.wheel2.setAngularVelocity(-100);
                bot.wheel3.setAngularVelocity(-100);
                bot.wheel4.setAngularVelocity(-100);
              // }
            if (bot.x < player.x-10){
                bot.state = 'roll_right'}
            else if ((player.x+10) > bot.x && bot.x > (player.x-10)){
                bot.state = 'stand_left'}
            else if (bot.x < player.x+400 && bot.x > player.x-10){
                bot.last_right_turn = this.scene.manual_time
                bot.state = 'roll_right_throw_left'}
            if (this.scene.manual_time > bot.data.values.last_throw + bot.data.values.throw_wait){
                bot.time_at_throw_start = Math.trunc(this.scene.manual_time)
                bot.previous_state = bot.state
                bot.setData('throw_wait', Phaser.Math.Between(1500, 3500))
                new MathStuff(this.scene, bot, this.player)
                bot.state = 'throw_left'}
            }

        else if (bot.state == 'roll_right_throw_left'){
            // if (bot.x >= player.x+10){
                bot.anims.play('bot_roll_left', true);
                bot.setVelocityX(bot.data.values.speed);
                bot.buzz_saw.setPosition(bot.x+185, bot.y-60)
                bot.wheel1.setAngularVelocity(100);
                bot.wheel2.setAngularVelocity(100);
                bot.wheel3.setAngularVelocity(100);
                bot.wheel4.setAngularVelocity(100);
              // }
              if (this.scene.manual_time >= (bot.last_right_turn + 4000)){
                  bot.state = 'roll_left_throw_left'}
            if (this.scene.manual_time > bot.data.values.last_throw + bot.data.values.throw_wait){
                bot.time_at_throw_start = Math.trunc(this.scene.manual_time)
                bot.previous_state = bot.state
                bot.setData('throw_wait', Phaser.Math.Between(1500, 3500))
                new MathStuff(this.scene, bot, this.player)
                bot.state = 'throw_left'}
              }

        else if (bot.state == 'stand_right'){
            if ((player.x+10) > bot.x && bot.x > (player.x-10)){
            bot.setVelocityX(0);
            bot.anims.play('bot_stand_right', true)
            bot.buzz_saw.setPosition(bot.x+200, bot.y+35)
            bot.wheel1.setAngularVelocity(0);
            bot.wheel2.setAngularVelocity(0);
            bot.wheel3.setAngularVelocity(0);
            bot.wheel4.setAngularVelocity(0);
          }
            else if (bot.x < player.x-10){
                bot.state = 'roll_right'}
            else if (bot.x >= player.x+10){
                bot.state = 'roll_left_throw_left'}}

        else if (bot.state == 'stand_left'){
            if ((player.x+10) > bot.x && bot.x > (player.x-10)){
            bot.setVelocityX(0);
            bot.anims.play('bot_stand_left', true)
            bot.buzz_saw.setPosition(bot.x+185, bot.y-60)
            bot.wheel1.setAngularVelocity(0);
            bot.wheel2.setAngularVelocity(0);
            bot.wheel3.setAngularVelocity(0);
            bot.wheel4.setAngularVelocity(0);
          }
            else if (bot.x < player.x-10){
                bot.state = 'roll_right'}
            else if (bot.x >= player.x+10){
                bot.state = 'roll_left_throw_left'}}

        else if (bot.state == 'throw_left'){
            bot.data.values.last_throw = this.scene.manual_time;
            bot.buzz_saw.setPosition(bot.x+185, bot.y-60)
            bot.setVelocityX(0);
            bot.wheel1.setAngularVelocity(0);
            bot.wheel2.setAngularVelocity(0);
            bot.wheel3.setAngularVelocity(0);
            bot.wheel4.setAngularVelocity(0);
            bot.anims.play('bot_throw', true)
            if (this.scene.manual_time >= (bot.time_at_throw_start + 300)){ // gives 250 miliseconds so it plays the animation once
                bot.state = bot.previous_state}
        }

        else if (bot.state == 'explode'){
            bot.setVelocityX(0);
            bot.buzz_saw.setVisible(false);
            bot.wheel1.setAngularVelocity(0);
            bot.wheel2.setAngularVelocity(0);
            bot.wheel3.setAngularVelocity(0);
            bot.wheel4.setAngularVelocity(0);
            if (bot.previous_state.includes('left')){
                bot.anims.play('bot_explode_left', true)}
            else if (bot.previous_state.includes('right')){
                bot.anims.play('bot_explode_right', true)}
            this.scene.time.delayedCall(1000, function() {
                bot.wheel1.setVisible(false);
                bot.wheel2.setVisible(false);
                bot.wheel3.setVisible(false);
                bot.wheel4.setVisible(false);
                }, [], this.scene);
            this.scene.time.delayedCall(1400, function() {
                bot.destroy()
                bot.head.destroy()
                bot.chest.destroy()
                bot.pipe.destroy()
                bot.wheel1.destroy()
                bot.wheel2.destroy()
                bot.wheel3.destroy()
                bot.wheel4.destroy()
                bot.buzz_saw.destroy()
                }, [], this.scene);
            }

          if (bot.data.values.health <= 0){
              bot.previous_state = bot.state
              this.scene.time.delayedCall(1000, function() {
              try{
              if (!bot.data.values.explosion_played){
                this.scene.scene.explosion.play()
                bot.data.values.explosion_played = true}}
              catch (TypeError){
                console.log('Caught Bot TypeError')}
                }, [], this.scene);
              bot.state = 'explode'}

          bot.data.values.health = Math.min(bot.data.values.health, bot.head.health, bot.chest.health, bot.pipe.health)
          bot.chest.health = Math.min(bot.data.values.health, bot.head.health, bot.chest.health, bot.pipe.health)
          bot.head.health = Math.min(bot.data.values.health, bot.head.health, bot.chest.health, bot.pipe.health)
          bot.pipe.health = Math.min(bot.data.values.health, bot.head.health, bot.chest.health, bot.pipe.health)
          bot.buzz_saw.health = bot.data.values.health;

          if ((bot.data.values.just_damaged || bot.head.just_damaged || bot.chest.just_damaged || bot.pipe.just_damaged) && bot.head.health > 0){
            bot.setTint(0xff0000)
            bot.data.values.time_at_hit = this.scene.manual_time
            bot.head.just_damaged = false
            bot.chest.just_damaged = false
            bot.pipe.just_damaged = false
            bot.data.values.just_damaged = false
          }
          if (this.scene.manual_time > bot.data.values.time_at_hit + 15){
            bot.setTint(0xffffff)
          }

      } // if bot active
    }, this); // looping through all bots
} // update

addBot(scene, x, y, speed) {
		var bot = scene.bots.create(x, y, 'bot');
		bot.setActive(true).setVisible(true)
		bot.setData({'health': 12, //140
								 'time_at_collision':null,
								 'knock_back_time': 150,
								 'bs_thrown': 0,
								 'layer_collisions': [],
						 		 'last_throw': 0,
                 'throw_wait': Phaser.Math.Between(1500, 3500),
                 'just_damaged': false,
                 'explosion_played': false
							 });

     for (var i=0;i<scene.layers.length;i++){
         bot.data.values.layer_collisions.push(scene.physics.add.collider(bot, scene.layers[i]));}

    if (speed == 'stand'){
        bot.state = 'stand_left'
        bot.setData('speed', 0)}
    else if (speed == 'roll'){
      bot.state = 'roll_left_throw_left'
        bot.setData('speed', 50)}

    bot.buzz_saw = scene.physics.add.sprite(x, y, 'buzz_saw');
    bot.buzz_saw.setScale(1.5);
    bot.buzz_saw.setCircle(30, 3, 3)
    bot.buzz_saw.setFrame(0);
    bot.buzz_saw.body.setAllowGravity(false);
    bot.buzz_saw.setAngularVelocity(200)
    bot.buzz_saw.setDepth(750);
    bot.buzz_saw.health = bot.data.values.health;

    scene.physics.add.overlap(this.player.player, bot.buzz_saw, function(player, saw){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = saw.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 6)
        saw.body.immovable = true;
        }, function(player, saw){
            return (saw.health > 0
            && this.player.player.data.values.post_hit_invincibility == false)
        }, this);

    bot.wheel1 = scene.physics.add.sprite(x, y, 'wheel');
    bot.wheel1.setScale(1.2)
    bot.wheel1.body.setAllowGravity(false);
    bot.wheel1.setDepth(750);

    bot.wheel2 = scene.physics.add.sprite(x, y, 'wheel');
    bot.wheel2.setScale(1.2)
    bot.wheel2.body.setAllowGravity(false);
    bot.wheel2.setDepth(750);

    bot.wheel3 = scene.physics.add.sprite(x, y, 'wheel');
    bot.wheel3.setScale(1.2)
    bot.wheel3.body.setAllowGravity(false);
    bot.wheel3.setDepth(750);

    bot.wheel4 = scene.physics.add.sprite(x, y, 'wheel');
    bot.wheel4.setScale(1.2)
    bot.wheel4.body.setAllowGravity(false);
    bot.wheel4.setDepth(750);

    bot.body.immovable = true;
    bot.setScale(1.7)
    bot.setOffset(93, 146)
		bot.setSize(90, 24, false)
    bot.setDepth(740);
		scene.bots.add(bot)
    scene.enemies_added += 1;

    bot.head = scene.physics.add.sprite(x, y, 'car_wheel');
    bot.head.setScale(2);
    bot.head.body.setAllowGravity(false);
    bot.head.setSize(38,26)
    bot.head.setDepth(750);
    bot.head.setVisible(false)
    bot.head.health = 12; //16
    bot.head.just_damaged = false

    bot.chest = scene.physics.add.sprite(x, y, 'car_wheel');
    bot.chest.setScale(2);
    bot.chest.body.setAllowGravity(false);
    bot.chest.setSize(84,32)
    bot.chest.setDepth(750);
    bot.chest.setVisible(false)
    bot.chest.health = 12
    bot.chest.just_damaged = false

    bot.pipe = scene.physics.add.sprite(x, y, 'car_wheel');
    bot.pipe.setScale(2);
    bot.pipe.body.setAllowGravity(false);
    bot.pipe.setSize(18,50)
    bot.pipe.setDepth(750);
    bot.pipe.setVisible(false)
    bot.pipe.health = 12
    bot.pipe.just_damaged = false

    scene.physics.add.overlap(this.player.player, bot.chest, function(player, chest){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = chest.x
        this.player.damagePlayer(this.player, scene, 3)
        chest.body.immovable = true;
        }, function(player, chest){
            return (this.player.player.data.values.post_hit_invincibility == false)
        }, this);

    scene.physics.add.overlap(this.player.player, bot.head, function(player, head){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = head.x
        this.player.damagePlayer(this.player, scene, 3)
        head.body.immovable = true;
        }, function(player, head){
            return (this.player.player.data.values.post_hit_invincibility == false)
        }, this);

    scene.physics.add.overlap(this.player.player, bot.pipe, function(player, pipe){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = pipe.x
        this.player.damagePlayer(this.player, scene, 3)
        pipe.body.immovable = true;
        }, function(player, pipe){
            return (this.player.player.data.values.post_hit_invincibility == false)
        }, this);
}

throw(bot, player){
    new MathStuff(this.scene, bot, this.player)}

}
