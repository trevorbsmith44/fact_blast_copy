// import 'phaser';
import Preload from "./preload.js";
import PreloadOld from "./preload_old.js";
import Opening from "./opening.js"
import TitleScreen from "./title_screen.js"
import Options from "./options.js"
import CutScene1 from "./cutscene1.js"
import CutScene1Options from "./cutscene1_options.js"
import CutScene2 from "./cutscene2.js"
import CutScene3 from "./cutscene3.js"
import CutScene4 from "./cutscene4.js"
import CutScene5 from "./cutscene5.js"
import CutScene6P1 from "./cutscene6p1.js"
import CutScene6 from "./cutscene6.js"
import CutScene7 from "./cutscene7.js"
import Level1 from "./level1.js";
import Level2 from "./level2.js";
import Level3 from "./level3.js";
import Level4 from "./level4.js";
import Level5 from "./level5.js";
import Level6 from "./level6.js";
import Credits from "./credits.js";
import PauseScene from "./pause_scene.js"
import Cover from "./cover.js"
import CutSceneCustom from "./cutscene_custom.js"

var config = {
		type: Phaser.AUTO,
		width: 1200,
		height: 600,
		// fps: 60,
		// fps: {
		// 	min: 30,
		// 	target: 60,
		// 	forceSetTimeOut: false
		// },
		roundPixels : true,
    // backgroundColor: '#24e0e7',
		input: {
				gamepad: true
		},
		physics: {
				default: 'arcade',
				arcade: {
						gravity: { y: 800 },
						debug: false,
						overlapBias: 32,
						tileBias: 32,
						// fps: 60
				}
		},
		scene: [
						Preload,
						// PreloadOld,
						Opening,
						TitleScreen,
						Options,
						CutScene1,
						CutScene1Options,
						CutScene2,
						CutScene3,
						CutScene4,
						CutScene5,
						CutScene6P1,
						CutScene6,
						CutScene7,
						Level1,
						Level2,
						Level3,
						Level4,
						Level5,
						Level6,
						Credits,
						PauseScene,
						Cover,
						CutSceneCustom
					]
};

var game = new Phaser.Game(config);
