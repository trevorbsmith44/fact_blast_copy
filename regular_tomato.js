export default class RegularTomato {

constructor(scene, boss_tomato, player, direction){
  this.scene = scene
  this.player = player

var x_spawn
var x_velocity
// var y_velocity = Phaser.Math.Between(-500,300)
// var y_velocity = -300
var angular
var angle

var y_velocity = Phaser.Math.Between(-400,-700)

  if (direction == 'left'){
      x_spawn = -160
      x_velocity = Phaser.Math.Between(-50,-350)
      angle = 30
      angular = -50}
  else if (direction == 'right'){
      x_spawn = 160
      x_velocity = Phaser.Math.Between(50,350)
      angle = -30
      angular = 50}

  this.regular_tomato = scene.regular_tomato.create(boss_tomato.x+x_spawn, boss_tomato.y-15, 'regular_tomato');
  // regular_tomato.setScale(2.2)
  this.regular_tomato.setDepth(910)
  this.regular_tomato.setFrame(0)
  this.regular_tomato.setCircle(33, 57, 35)
  this.regular_tomato.setAngle(angle)
  this.regular_tomato.setAngularVelocity(angular);
  // regular_tomato.body.setAllowGravity(false);
  this.regular_tomato.setVelocity(x_velocity, y_velocity);
  this.regular_tomato.collided_with_fact = false

  this.scene.player_tomato = this.scene.physics.add.overlap(this.regular_tomato, this.player.player, function(tomato, player){
      tomato.setFrame(1)
      this.scene.time.delayedCall(150, function() {
        tomato.destroy()
        tomato.setVisible(false)
      }, [], this.scene);
      this.player.player.data.values.time_at_collision = scene.manual_time
      this.player.player.data.values.hit_from = tomato.x
      scene.splat.play();
      this.player.damagePlayer(this.player, scene, 5)
    },
    // this.player.checkPostHitInvincibility
    function(tomato, player){
        return (this.player.player.data.values.post_hit_invincibility == false && tomato.collided_with_fact == false)
    }, this);

}

update(){
  if (this.regular_tomato.y > (((this.scene.layers.layer.height)*2)+100)){
      this.regular_tomato.destroy()
  }

}

}
