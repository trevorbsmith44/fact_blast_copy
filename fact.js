export default class Fact {

constructor(scene, player, x_spawn, y_spawn, x_velocity, y_velocity){
    scene.facts = scene.physics.add.group();
    this.scene = scene

    var fact = scene.facts.create((player.player.x + x_spawn), y_spawn, 'fact');
    fact.setData({'time_thrown': scene.manual_time,
                 'can_collide': true}
                );
    fact.setDepth(800);
    fact.setSize(35,20);
    fact.setBounce(1);
    fact.setMaxVelocity(1500,1000);
    this.collided_with_tomato = false
    fact.name = 'fact'
    if (Math.floor(this.scene.time.now) % 5 == 0){
        fact.setAngularVelocity(300)
      }
    try {
        if (!player.gamepad.up && !player.gamepad.R2 && !player.gamepad.R1 && !player.gamepad.L2 && !player.gamepad.L1 && !player.key_up.isDown) {
            fact.setVelocity(x_velocity, y_velocity);}
        else{  // up button is being held down
            if (player.player.state.includes('right')){
                fact.setVelocity(x_velocity-250, y_velocity-400);}
            else if (player.player.state.includes('left')){
                fact.setVelocity(x_velocity+250, y_velocity-400);}}}
    catch(TypeError){
        if (!player.key_up.isDown){
            fact.setVelocity(x_velocity, y_velocity);}
        else{  // up button is being held down
            if (player.player.state.includes('right')){
                fact.setVelocity(x_velocity-250, y_velocity-400);}
            else if (player.player.state.includes('left')){
                fact.setVelocity(x_velocity+250, y_velocity-400);}}}

    scene.physics.add.overlap(scene.facts, scene.bs_statements, this.facts_destroy_bs, null, scene);
    // scene.physics.add.overlap(scene.facts, scene.censorships, this.facts_destroy_bs, null, scene);

    for (var i=0;i<scene.layers.length;i++){
        scene.physics.add.collider(scene.layers[i], scene.facts, this.dissolveFact, null, scene);}

if (scene.npc_enemies) {
    scene.physics.add.overlap(scene.npc_enemies, scene.facts, function(npc, fact){
    fact.destroy()
    npc.data.values.time_at_collision = this.manual_time
    npc.data.values.hit_from = fact.x
    var whack_num = Math.floor(Math.random() * scene.whacks.length)
    scene.whacks[whack_num].play();
    npc.data.values.health -= 1;
    if (npc.data.values.health <= 0){
        if (this.manual_time < (npc.data.values.time_at_collision + 5000)) {
            npc.data.values.knock_back_time = 5000
            if (player.player.x <= npc.x){
                npc.setAngularVelocity(400)}
            else{
                npc.setAngularVelocity(-400)}}
        for (var i=0;i<scene.layers.length;i++){
            this.physics.world.removeCollider(npc.data.values.layer_collisions[i]);
            // this.physics.world.removeCollider(npc.platform_collider);
          }
        return}
    }, null, scene);
}

if (scene.birds) {
    scene.physics.add.overlap(scene.facts, scene.birds, function(fact, bird){
        fact.destroy()
        var whack_num = Math.floor(Math.random() * scene.whacks.length)
        var bird_sound_num = Math.floor(Math.random() * scene.bird_sounds.length)

        scene.whacks[whack_num].play();
        scene.bird_sounds[bird_sound_num].play();
        bird.data.values.health -= 1;
        if (bird.data.values.health <= 0){
            bird.body.setAllowGravity(true);
            if (player.player.x <= bird.x){
                bird.setAngularVelocity(400)}
            else{
                bird.setAngularVelocity(-400)}
            return}
    }, null, scene);
}

if (scene.tomatoes) {
    scene.physics.add.overlap(scene.facts, scene.tomatoes, function(fact, tomato){
        if (!tomato.hit_player) {
        fact.destroy()
        tomato.data.values.time_at_collision = this.manual_time
        tomato.data.values.health -= 1;
        var whack_num = Math.floor(Math.random() * scene.whacks.length)
        scene.whacks[whack_num].play();
        if (tomato.data.values.health <= 0){
            if (this.manual_time < (tomato.data.values.time_at_collision + 5000)) {
                tomato.data.values.knock_back_time = 5000}
            // this.physics.world.removeCollider(tomato.data.values.layer_collision);
            for (var i=0;i<scene.layers.length;i++){
                this.physics.world.removeCollider(tomato.data.values.layer_collisions[i]);}
            return}}
    }, null, scene);
}

if (scene.bots) {
    for (var i=0;i<scene.bots.children.entries.length;i++){
        var saw = scene.bots.children.entries[i].buzz_saw
          scene.physics.add.overlap(scene.facts, saw, function(saw, fact){
            scene.clang2.play()
            fact.stop_collision_bot_saw = true
            if (saw.x <= fact.x){
                fact.setVelocityX(100)}
            else if (saw.x > fact.x){
                fact.setVelocityX(-100)}
              }, function(saw, fact){
                    return (!fact.stop_collision_bot_saw)
                  }, scene);

        var head = scene.bots.children.entries[i].head
            scene.physics.add.overlap(scene.facts, head, function(head, fact){
                try {fact.destroy();
                  var whack_num = Math.floor(Math.random() * scene.whacks.length)
                  scene.whacks[whack_num].play();
                    head.health -= 1;
                    head.just_damaged = true}
                catch (TypeError){
                    console.log('caught bot TypeError')}
                }, null, scene);

        var chest = scene.bots.children.entries[i].chest
            scene.physics.add.overlap(scene.facts, chest, function(chest, fact){
              try {fact.destroy();
                var whack_num = Math.floor(Math.random() * scene.whacks.length)
                scene.whacks[whack_num].play();
                  chest.health -= 1;
                  chest.just_damaged = true}
              catch (TypeError){
                  console.log('caught bot TypeError')}
                    }, null, scene);

        var pipe = scene.bots.children.entries[i].pipe
            scene.physics.add.overlap(scene.facts, pipe, function(pipe, fact){
              try {fact.destroy();
                var whack_num = Math.floor(Math.random() * scene.whacks.length)
                scene.whacks[whack_num].play();
                  pipe.health -= 1;
                  pipe.just_damaged = true}
              catch (TypeError){
                  console.log('caught bot TypeError')}
                }, null, scene);
      }

    scene.physics.add.overlap(scene.facts, scene.bots, function(fact, bot){
      try {fact.destroy();
          var whack_num = Math.floor(Math.random() * scene.whacks.length)
          scene.whacks[whack_num].play();
          bot.data.values.health -= 1;
          bot.data.values.just_damaged = true
        }
      catch (TypeError){
          console.log('caught bot TypeError')}
        }, null, scene);

} // if scene.bots

if (scene.boss_bots) {
    for (var i=0;i<scene.boss_bots.children.entries.length;i++){

        var saw = scene.boss_bots.children.entries[i].buzz_saw
            scene.physics.add.overlap(scene.facts, saw, function(saw, fact){
              scene.clang2.play()
              fact.stop_collision_bbot = true
              if (saw.x <= fact.x){
                  fact.setVelocityX(100)}
              else if (saw.x > fact.x){
                  fact.setVelocityX(-100)}
              }, function(boss_bot, fact){
                  return (!fact.stop_collision_bbot)
                }, scene);

        var head = scene.boss_bots.children.entries[i].head
            scene.physics.add.overlap(scene.facts, head, function(head, fact){
                try {fact.destroy();
                    var whack_num = Math.floor(Math.random() * scene.whacks.length)
                    scene.whacks[whack_num].play();
                    head.health -= 1;
                    head.just_damaged = true
                    var bb = scene.boss_bots.children.entries[0]
                    bb.time_at_hit = scene.manual_time
                    bb.state = 'hit'}
                catch (TypeError){
                    console.log('caught boss bot TypeError')}
                }, null, scene);

        var chest = scene.boss_bots.children.entries[i].chest
            scene.physics.add.overlap(scene.facts, chest, function(chest, fact){
              scene.clang2.play()
              fact.stop_collision_bbot = true
                if (chest.x <= fact.x){
                    fact.setVelocityX(100)}
                else if (chest.x > fact.x){
                    fact.setVelocityX(-100)}
                  }, function(boss_bot, fact){
                        return (!fact.stop_collision_bbot)
                      }, scene);

        var pipe = scene.boss_bots.children.entries[i].pipe
            scene.physics.add.overlap(scene.facts, pipe, function(pipe, fact){
              scene.clang2.play()
              fact.stop_collision_bbot = true
                if (pipe.x <= fact.x){
                    fact.setVelocityX(100)}
                else if (pipe.x > fact.x){
                    fact.setVelocityX(-100)}
                }, function(boss_bot, fact){
                    return (!fact.stop_collision_bbot)
                  }, scene);
      }

    scene.physics.add.overlap(scene.facts, scene.boss_bots, function(fact, boss_bot){
      scene.clang2.play()
      fact.stop_collision_bbot = true
        if (boss_bot.x <= fact.x){
            fact.setVelocityX(100)}
        else if (boss_bot.x > fact.x){
            fact.setVelocityX(-100)}
        }, function(fact, boss_bot){
            return (!fact.stop_collision_bbot)
          }, scene);

} // if scene.boss_bots

if (scene.boss_npcs){
    scene.physics.add.overlap(scene.facts, scene.boss_npcs, function(fact, boss_npc){
      scene.clang2.play()
      fact.stop_collision_boss_npc = true
      if (boss_npc.x <= fact.x){
          fact.setVelocityX(300)}
      else if (boss_npc.x > fact.x){
          fact.setVelocityX(-300)
        }
    }, function(fact, boss_tomato){
      return (!fact.stop_collision_boss_npc)
    }, scene);

    for (var i=0;i<scene.boss_npcs.children.entries.length;i++){
        var head = scene.boss_npcs.children.entries[i].head
            scene.physics.add.collider(scene.facts, head, function(head, fact){
                fact.destroy();
                var whack_num = Math.floor(Math.random() * scene.whacks.length)
                scene.whacks[whack_num].play();
                head.health -= 1;
                head.just_damaged = true
        }, null, scene);
      }
}

if (scene.boss_tomatoes){
    scene.physics.add.overlap(scene.facts, scene.boss_tomatoes, function(fact, boss_tomato){
        scene.clang2.play()
        fact.stop_collision_bt = true
        if (boss_tomato.x <= fact.x){
            fact.setVelocityX(300)}
        else if (boss_tomato.x > fact.x){
            fact.setVelocityX(-300)
          }
    }, function(fact, boss_tomato){
      return (!fact.stop_collision_bt)
    }, scene);}

if (scene.boss_birds){
    scene.physics.add.overlap(scene.facts, scene.boss_birds, function(fact, boss_bird){
        scene.clang2.play()
        fact.stop_collision_bbird = true
        if (boss_bird.x <= fact.x){
            fact.setVelocityX(300)}
        else if (boss_bird.x > fact.x){
            fact.setVelocityX(-300)
          }
    }, function(fact, boss_bird){
      return (!fact.stop_collision_bbird)
    }, scene);
    for (var i=0;i<scene.boss_birds.children.entries.length;i++){
        var head = scene.boss_birds.children.entries[i].head
            scene.physics.add.collider(scene.facts, head, function(head, fact){
                fact.destroy();
                var whack_num = Math.floor(Math.random() * scene.whacks.length)
                scene.whacks[whack_num].play();
                head.health -= 1;
                head.just_damaged = true
        }, null, scene);
      }
  }

if (scene.regular_tomato){
    scene.physics.add.collider(scene.facts, scene.regular_tomato, function(fact, tomato){
        tomato.can_hurt_boss = true;
        tomato.collided_with_fact = true;
        fact.collided_with_tomato = true;
        scene.soft_hit.play()
        if (tomato.x <= fact.x){
            fact.setVelocity(300, 200)
            tomato.setVelocity(-300, -200)
          }
        else if (tomato.x > fact.x){
            fact.setVelocity(-300, 200)
            tomato.setVelocity(300, -200)
          }
    },
    // function(fact, tomato){
    //     return (((Math.abs(tomato.x - scene.boss_tomatoes.children.entries[0].x)) > 200) || tomato.collided_with_fact == true)},
    function(fact, tomato){
      return (!fact.collided_with_tomato)
    },
    scene);}

if (scene.trash_can){
    scene.physics.add.overlap(scene.facts, scene.trash_can, function(fact, trash){
        scene.clang2.play()
        fact.stop_collision_trash = true
        if (trash.x <= fact.x){
            fact.setVelocityX(300)
          }
        else if (trash.x > fact.x){
            fact.setVelocityX(-300)
          }
    }, function(fact, trash){
      return (!fact.stop_collision_trash)
    }, scene);}

if (scene.small_bots) {
    scene.physics.add.overlap(scene.small_bots, scene.facts, function(small_bot, fact){
    fact.destroy()
    var whack_num = Math.floor(Math.random() * scene.whacks.length)
    scene.whacks[whack_num].play();
    small_bot.data.values.health-=1;
    }, null, scene);
}

if (scene.moving_saws) {
    scene.physics.add.overlap(scene.facts, scene.moving_saws, function(fact, saw){
      scene.clang2.play()
      fact.stop_collision_saw = true
        if (saw.x <= fact.x){
            fact.setVelocityX(50)}
        else if (saw.x > fact.x){
            fact.setVelocityX(-50)}
    }, function(fact, saw){
        return (!fact.stop_collision_saw)
    }, scene);}

if (scene.machines) {
    scene.physics.add.overlap(scene.machines, scene.facts, function(machine, fact){
    fact.destroy()
    if (machine.data.values.health > 0){
      var whack_num = Math.floor(Math.random() * scene.whacks.length)
      scene.whacks[whack_num].play();
        machine.data.values.health -= 1;}
    }, null, scene);
}

if (scene.platforms) {
    this.scene.platform_array.forEach(function(platform) {
        scene.platform_fact = scene.physics.add.overlap(platform, scene.facts, function(platform, fact){
            fact.destroy()
          }, null, scene);}) }

if (scene.barriers) {
    this.scene.barrier_array.forEach(function(barrier) {
        scene.barrier_fact = scene.physics.add.overlap(barrier, scene.facts, function(barrier, fact){
            fact.destroy()
          }, null, scene);}) }

if (scene.boss_npc_mds){
    scene.physics.add.collider(scene.facts, scene.boss_npc_mds, function(fact, npc_md){
        npc_md.body.immovable = true
        scene.clang2.play()
        if (npc_md.x <= fact.x){
            fact.setVelocityX(300)}
        else if (npc_md.x > fact.x){
            fact.setVelocityX(-300)}
    }, function(fact, npc_md){
      return (fact.can_collide && fact.can_collide == true) && scene.boss_transparent == false
    }, scene);
    for (var i=0;i<scene.boss_npc_mds.children.entries.length;i++){
        var glass = scene.boss_npc_mds.children.entries[i].glass
            scene.physics.add.collider(scene.facts, glass, function(glass, fact){
              scene.clang2.play()
            if (glass.x <= fact.x){
                fact.setVelocity(400, -200)}
            else {
                fact.setVelocity(-400, -200)}
            if (fact.y > glass.y){
              fact.setVelocityY(300)}
            fact.can_collide = false
        }, function(fact, npc_md){
          return (fact.can_collide && fact.can_collide == true) && scene.boss_transparent == false
        }, scene);
      }
  }

if (scene.dark_rocks){
    scene.physics.add.overlap(scene.facts, scene.dark_rocks, function(fact, dark_rock){
        dark_rock.data.values.collided_with_fact = true;
        scene.soft_hit.play()
        if (dark_rock.x <= fact.x){
            fact.setVelocity(300, 200)
            dark_rock.setVelocity(-300, -350)
          }
        else if (dark_rock.x > fact.x){
            fact.setVelocity(-300, 200)
            dark_rock.setVelocity(300, -350)
          }
        }, null, scene);}

} // fact constructor


dissolveFact (fact, layer){
		try{  // so facts cannot be blocked by stairs
			if (this.manual_time < (fact.data.values.time_thrown + 500)){
					return}}
		catch(TypeError){
				console.log('Caught TypeError for fact')}
		fact.destroy()
		fact.setVisible(false)
}

facts_destroy_bs(fact, bs){
    this.pop.play()
		this.time.delayedCall(100, function() {
				fact.destroy();}, [], this.scene);
		bs.destroy();
	}

}
