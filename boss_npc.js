import Milkshake from  "./milkshake.js";

export default class BossNpc {

constructor(scene, player){
    scene.boss_npcs = scene.physics.add.group();
    this.scene = scene
    this.player = player

    this.player_boss_npc = scene.physics.add.overlap(this.player.player, scene.boss_npcs, function(player, boss_npc){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = boss_npc.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 6)
        boss_npc.body.immovable = true;
        }, function(player, boss_npc){
            return (boss_npc.head.health > 0 && this.player.player.data.values.post_hit_invincibility == false)
        }, this);



}

update(player){
  this.scene.boss_npcs.children.each(function(boss_npc) {
      if (boss_npc.active){

          boss_npc.head.setPosition(boss_npc.x-11, boss_npc.y-90)

        if (boss_npc.state == 'walk_left_throw_left'){
                boss_npc.anims.play('boss_npc_walk_left', true);
                boss_npc.setVelocityX(-50);
            if (boss_npc.x < player.x){
                boss_npc.state = 'walk_right_throw_right'}
            if (boss_npc.x < boss_npc.data.values.origin_x-100 && boss_npc.x > player.x){
                boss_npc.state = 'walk_right_throw_left'}
            if (this.scene.manual_time > boss_npc.data.values.last_throw + boss_npc.data.values.throw_wait){
                boss_npc.time_at_throw_start = Math.trunc(this.scene.manual_time)
                boss_npc.previous_state = boss_npc.state
                boss_npc.setData('throw_wait', Phaser.Math.Between(1500, 3500))
                this.scene.time.delayedCall(250, function() {
                    var diff = boss_npc.x - player.x
                    new Milkshake(this, boss_npc, this.player, 'left', diff)
                    }, [], this.scene);
                boss_npc.state = 'throw_left'}
            }

        else if (boss_npc.state == 'walk_right_throw_left'){
                boss_npc.anims.play('boss_npc_walk_left', true);
                boss_npc.setVelocityX(50);
            if (boss_npc.x < player.x){
                boss_npc.state = 'walk_right_throw_right'}
            if (boss_npc.x > boss_npc.data.values.origin_x && boss_npc.x>player.x){
                boss_npc.state = 'walk_left_throw_left'}
            if (this.scene.manual_time > boss_npc.data.values.last_throw + boss_npc.data.values.throw_wait){
                boss_npc.time_at_throw_start = Math.trunc(this.scene.manual_time)
                boss_npc.previous_state = boss_npc.state
                boss_npc.setData('throw_wait', Phaser.Math.Between(1500, 3500))
                this.scene.time.delayedCall(250, function() {
                    var diff = boss_npc.x - player.x
                    new Milkshake(this, boss_npc, this.player, 'left', diff)
                    }, [], this.scene);
                boss_npc.state = 'throw_left'}
              }
        else if (boss_npc.state == 'walk_right_throw_right'){
            boss_npc.anims.play('boss_npc_walk_right', true);
            boss_npc.setVelocityX(50);
            if (boss_npc.x > player.x){
                boss_npc.state = 'walk_left_throw_left'}
            if (boss_npc.x > boss_npc.data.values.origin_x+100 && boss_npc.x < player.x){
                boss_npc.state = 'walk_left_throw_right'}
            if (this.scene.manual_time > boss_npc.data.values.last_throw + boss_npc.data.values.throw_wait){
                boss_npc.time_at_throw_start = Math.trunc(this.scene.manual_time)
                boss_npc.previous_state = boss_npc.state
                boss_npc.setData('throw_wait', Phaser.Math.Between(1500, 3500))
                this.scene.time.delayedCall(250, function() {
                    var diff = player.x - boss_npc.x
                    new Milkshake(this, boss_npc, this.player, 'right', diff)
                    }, [], this.scene);
                boss_npc.state = 'throw_right'}
            }

        else if (boss_npc.state == 'walk_left_throw_right'){
            boss_npc.anims.play('boss_npc_walk_right', true);
            boss_npc.setVelocityX(-50);
            if (boss_npc.x > player.x){
                boss_npc.state = 'walk_left_throw_left'}
            if (boss_npc.x < boss_npc.data.values.origin_x && boss_npc.x<player.x){
                boss_npc.state = 'walk_right_throw_right'}
            if (this.scene.manual_time > boss_npc.data.values.last_throw + boss_npc.data.values.throw_wait){
                boss_npc.time_at_throw_start = Math.trunc(this.scene.manual_time)
                boss_npc.previous_state = boss_npc.state
                boss_npc.setData('throw_wait', Phaser.Math.Between(1500, 3500))
                this.scene.time.delayedCall(250, function() {
                    var diff = player.x - boss_npc.x
                    new Milkshake(this, boss_npc, this.player, 'right', diff)
                    }, [], this.scene);
                boss_npc.state = 'throw_right'}
              }

        else if (boss_npc.state == 'throw_left'){
            boss_npc.data.values.last_throw = this.scene.manual_time;
            boss_npc.setVelocityX(0);
            boss_npc.anims.play('boss_npc_throw_left', true)
            if (this.scene.manual_time >= (boss_npc.time_at_throw_start + 575)){
                boss_npc.state = boss_npc.previous_state}
            }

        else if (boss_npc.state == 'throw_right'){
            boss_npc.data.values.last_throw = this.scene.manual_time;
            boss_npc.setVelocityX(0);
            boss_npc.anims.play('boss_npc_throw_right', true)
            if (this.scene.manual_time >= (boss_npc.time_at_throw_start + 575)){
                boss_npc.state = boss_npc.previous_state}
            }

        else if (boss_npc.state == 'defeated_right'){
            boss_npc.setVelocityX(0);
            boss_npc.anims.play('boss_npc_defeated_right')

            this.scene.time.delayedCall(500, function() {
              try {
                  for (var i=0;i<this.scene.scene.layers.length;i++){
                      this.physics.world.removeCollider(boss_npc.data.values.layer_collisions[i]);}
                }
              catch (TypeError){
                  console.log('Caught Boss NPC TypeError')}
                }, [], this.scene);
            }

        else if (boss_npc.state == 'defeated_left'){
            boss_npc.setVelocityX(0);
            boss_npc.anims.play('boss_npc_defeated_left')
            this.scene.time.delayedCall(500, function() {
              try {
                  for (var i=0;i<this.scene.scene.layers.length;i++){
                      this.physics.world.removeCollider(boss_npc.data.values.layer_collisions[i]);}
                }
              catch (TypeError){
                  console.log('Caught Boss NPC TypeError')}
                }, [], this.scene);
            }

        else if (boss_npc.state == 'no_right'){
            boss_npc.setVelocityX(0);
            boss_npc.anims.play('boss_npc_no_right')
            if (!boss_npc.data.values.no_played){
              this.scene.nooo_sound.play()
              boss_npc.data.values.no_played = true}
            this.scene.time.delayedCall(2000, function() {
                boss_npc.state = 'defeated_right'
                }, [], this.scene);
            }

        else if (boss_npc.state == 'no_left'){
            boss_npc.setVelocityX(0);
            boss_npc.anims.play('boss_npc_no_left')
            if (!boss_npc.data.values.no_played){
              this.scene.nooo_sound.play()
              boss_npc.data.values.no_played = true}
            this.scene.time.delayedCall(2000, function() {
                boss_npc.state = 'defeated_left'
                }, [], this.scene);
            }

        else if (boss_npc.state == 'fall_left'){
            boss_npc.setVelocityX(0);
            boss_npc.anims.play('boss_npc_fall_left')
            if (boss_npc.body.onFloor()){
                boss_npc.state = 'walk_left_throw_left'}
            }

        else if (boss_npc.state == 'fall_right'){
            boss_npc.setVelocityX(0);
            boss_npc.anims.play('boss_npc_fall_right')
            if (boss_npc.body.onFloor()){
                boss_npc.state = 'walk_right_throw_right'}
            }

        if (boss_npc.head.health <= 0){
            boss_npc.life_bar.setFrame(0)
            if (boss_npc.state.includes('throw_right')){
                boss_npc.state = 'no_right'}
            else if (boss_npc.state.includes('throw_left')){
                boss_npc.state = 'no_left'}
            }

        if (boss_npc.y > ((this.scene.layers.layer.height*2)+1000)){
          boss_npc.life_bar.setVisible(false)
            if (boss_npc.head.health <= 0){
                if (!this.crash_npc_played && boss_npc.data.values.boss == true){
                  this.scene.crash.play()
                  this.crash_npc_played = true}
                this.scene.cameras.main.shake(100, 0.01);}
            this.scene.time.delayedCall(400, function() {
            try {
            if (boss_npc.data.values.boss == true){
                this.scene.data.values.boss_defeated = true}}
            catch (TypeError){
                console.log('Caught Boss NPC TypeError')}
            boss_npc.destroy()
            boss_npc.setVisible(false)
            }, [], this);
          }

        if(boss_npc.state.includes('no') && boss_npc.data.values.boss == false){
          for (var i=0;i<this.scene.barrier_array.length;i++){
              if (this.scene.barrier_array[i].x < 6000){
                this.scene.barrier_array[i].body.setAllowGravity(true)}}}

        if (boss_npc.head.just_damaged && boss_npc.head.health > 0){
          boss_npc.setTint(0xff0000)
          boss_npc.data.values.time_at_hit = this.scene.manual_time
          var boss_life_frame = Phaser.Math.CeilTo((boss_npc.head.health/boss_npc.data.values.max_health) * 18)
          boss_npc.life_bar.setFrame(boss_life_frame)
          boss_npc.head.just_damaged = false
        }

        if (this.scene.manual_time > boss_npc.data.values.time_at_hit + 10){
          boss_npc.setTint(0xffffff)
        }

      } // if boss_npc active
    }, this); // looping through all boss_npcs
} // update

addBossNpc(scene, x, y, level_boss) {
    var boss_health = 16
    if (!level_boss){
      boss_health = 10
    }
		var boss_npc = scene.boss_npcs.create(x, y, 'boss_npc');
		boss_npc.setActive(true).setVisible(true)
		boss_npc.setData({'health': 1, //12
								 'time_at_collision':null,
								 'knock_back_time': 150,
								 'bs_thrown': 0,
								 'layer_collisions': [],
						 		 'last_throw': 0,
                 'throw_wait': Phaser.Math.Between(1500, 3500),
                 'boss': level_boss,
                 'origin_x': x,
                 'no_played': false,
                 'max_health': boss_health
							 });

     for (var i=0;i<scene.layers.length;i++){
         boss_npc.data.values.layer_collisions.push(scene.physics.add.collider(boss_npc, scene.layers[i]));}

    boss_npc.body.immovable = true;
    boss_npc.setScale(1.8)
    boss_npc.setOffset(92,67) //y=25
		boss_npc.setSize(72, 114, false) //h=156
    boss_npc.setDepth(740);
    boss_npc.setMaxVelocity(300, 800)

    if (boss_npc.x > this.player.player.x){
        boss_npc.state = 'fall_left'}
    else if (boss_npc.x <= this.player.player.x){
        boss_npc.state = 'fall_right'}
		scene.boss_npcs.add(boss_npc)
    scene.enemies_added += 1;

    boss_npc.head = scene.physics.add.sprite(x, y, 'wheel');
    boss_npc.head.setVisible(false)
    boss_npc.head.setScale(1.5);
    boss_npc.head.setSize(42,50)
    boss_npc.head.body.immovable = true;
    boss_npc.head.body.setAllowGravity(false);
    boss_npc.head.setDepth(750);
    boss_npc.head.health = boss_health; // 16
    boss_npc.head.just_damaged = false

    boss_npc.life_bar = scene.physics.add.sprite(1060, 30, 'boss_life_bar').setDepth(1000);
    boss_npc.life_bar.body.setAllowGravity(false);
    boss_npc.life_bar.setScrollFactor(0,0)
    boss_npc.life_bar.setFrame(18)
}

}
