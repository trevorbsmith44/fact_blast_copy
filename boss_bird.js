import Egg from  "./egg.js";

export default class BossBird {

constructor(scene, player){
    scene.boss_birds = scene.physics.add.group();
    this.scene = scene
    this.player = player

    this.player_boss_bird = scene.physics.add.overlap(this.player.player, scene.boss_birds,
      function(player, boss_bird){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = boss_bird.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 6)
        boss_bird.body.immovable = true;
        }, function(player, boss_bird){
            return (boss_bird.head.health > 0
                    && this.player.player.data.values.post_hit_invincibility == false
                    && this.player.player.data.values.health > 0)
        }, this);

}

update(player){
  this.scene.boss_birds.children.each(function(boss_bird) {
      if (boss_bird.active){
        boss_bird.head.setPosition(boss_bird.x-11, boss_bird.y-80)

        for (var i=0;i<boss_bird.projectiles.length;i++){
            boss_bird.projectiles[i].update()
        }

        if (this.scene.manual_time >= boss_bird.data.values.last_switch_time + 1000){
            boss_bird.data.values.last_switch_time = this.scene.manual_time
            if (boss_bird.data.values.hover == 'up'){
                boss_bird.data.values.hover = 'down'}
            else {boss_bird.data.values.hover = 'up'}}

        if (boss_bird.state == 'enter_right'){
            boss_bird.setVelocityX(0);
            boss_bird.setVelocityY(100);
            boss_bird.anims.play('boss_bird_right_closed', true)
            if (boss_bird.y >= boss_bird.data.values.y_spawn+400){
                boss_bird.state = 'hover_right'}}

        else if (boss_bird.state == 'enter_left'){
            boss_bird.setVelocityX(0);
            boss_bird.setVelocityY(100);
            boss_bird.anims.play('boss_bird_left_closed', true)
            if (boss_bird.y >= boss_bird.data.values.y_spawn+400){
                boss_bird.state = 'hover_left'}}

        else if (boss_bird.state == 'hover_right'){
          boss_bird.anims.play('boss_bird_right_closed', true)
          boss_bird.setVelocityX(0);
            if (boss_bird.y > boss_bird.data.values.y_spawn+400){
                boss_bird.setVelocityY(-50)}
            else{
                if (boss_bird.data.values.hover == 'up'){
                    boss_bird.setVelocityY(-10);}
                else if (boss_bird.data.values.hover == 'down'){
                    boss_bird.setVelocityY(10);}
                if (this.scene.manual_time > boss_bird.data.values.last_attack + boss_bird.data.values.attack_wait){
                    boss_bird.data.values.attack_wait = Phaser.Math.Between(3000, 6000)
                    var rand = Phaser.Math.Between(0, 1)
                    // var rand = 0
                    if (rand == 1){
                        this.scene.bird_swoop.play()
                        boss_bird.state = 'swoop_right'}
                    else {
                      boss_bird.data.values.last_attack = this.scene.manual_time
                      boss_bird.projectiles.push(new Egg(this.scene, boss_bird, this.player, 'right'))
                      this.scene.bird_egg.play()
                      boss_bird.state = 'drop_right'}
                  }}
          }

          else if (boss_bird.state == 'hover_left'){
            boss_bird.anims.play('boss_bird_left_closed', true)
            boss_bird.setVelocityX(0);
              if (boss_bird.y > boss_bird.data.values.y_spawn+400){
                  boss_bird.setVelocityY(-50)}
              else{
                  if (boss_bird.data.values.hover == 'up'){
                      boss_bird.setVelocityY(-10);}
                  else if (boss_bird.data.values.hover == 'down'){
                      boss_bird.setVelocityY(10);}
                  if (this.scene.manual_time > boss_bird.data.values.last_attack + boss_bird.data.values.attack_wait){
                      boss_bird.data.values.attack_wait = Phaser.Math.Between(3000, 6000)
                      var rand = Phaser.Math.Between(0, 1)
                      // var rand = 0
                      if (rand == 1){
                          this.scene.bird_swoop.play()
                          boss_bird.state = 'swoop_left'}
                      else {
                        boss_bird.data.values.last_attack = this.scene.manual_time
                        boss_bird.projectiles.push(new Egg(this.scene, boss_bird, this.player, 'left'))
                        this.scene.bird_egg.play()
                        boss_bird.state = 'drop_left'}
                    }}
            }

        else if (boss_bird.state == 'swoop_left'){
            boss_bird.data.values.last_attack = this.scene.manual_time
            boss_bird.setVelocityX(-700);
            boss_bird.setVelocityY(50);
            boss_bird.setFrame(12)
            if (boss_bird.x < boss_bird.data.values.x_spawn-800){
                boss_bird.state = 'hover_right'}
            }

        else if (boss_bird.state == 'swoop_right'){
            boss_bird.data.values.last_attack = this.scene.manual_time
            boss_bird.setVelocityX(700);
            boss_bird.setVelocityY(50);
            boss_bird.setFrame(3)
            if (boss_bird.x > boss_bird.data.values.x_spawn){
                boss_bird.state = 'hover_left'}
            }

        else if (boss_bird.state == 'drop_left'){
            boss_bird.setVelocityX(0);
            if (boss_bird.data.values.hover == 'up'){
                boss_bird.setVelocityY(-10);}
            else if (boss_bird.data.values.hover == 'down'){
                boss_bird.setVelocityY(10);}
            boss_bird.setFrame(12)
            if (this.scene.manual_time > boss_bird.data.values.last_attack + 500){
                boss_bird.state = 'hover_left'}
            }

        else if (boss_bird.state == 'drop_right'){
          boss_bird.setVelocityX(0);
          if (boss_bird.data.values.hover == 'up'){
              boss_bird.setVelocityY(-10);}
          else if (boss_bird.data.values.hover == 'down'){
              boss_bird.setVelocityY(10);}
          boss_bird.setFrame(3)
          if (this.scene.manual_time > boss_bird.data.values.last_attack + 500){
              boss_bird.state = 'hover_right'}
          }

        else if (boss_bird.state == 'hit_right'){
            boss_bird.setVelocityX(0);
            boss_bird.setFrame(11)
            if (this.scene.manual_time > boss_bird.time_at_hit + 1000){
                boss_bird.state = boss_bird.state_before_hit}}

        else if (boss_bird.state == 'hit_left'){
            boss_bird.setVelocityX(0);
            boss_bird.setFrame(5)
            if (this.scene.manual_time > boss_bird.time_at_hit + 1000){
                boss_bird.state = boss_bird.state_before_hit}}

        else if (boss_bird.state == 'defeated_right'){
            boss_bird.setFrame(6);
            if (this.scene.manual_time <= this.boss_bird_defeated + 2000){
                boss_bird.setVelocityX(0);
                boss_bird.setVelocityY(0);}
            else {
              try {
                    boss_bird.setVelocityY(800)
                    }
              catch (TypeError){
                  console.log('Caught Boss Bird TypeError')}
                }
            }

        else if (boss_bird.state == 'defeated_left'){
            boss_bird.setFrame(15);
            if (this.scene.manual_time <= this.boss_bird_defeated + 2000){
                boss_bird.setVelocityX(0);
                boss_bird.setVelocityY(0);}
            else {
              try {
                    boss_bird.setVelocityY(800)
                    }
              catch (TypeError){
                  console.log('Caught Boss Bird TypeError')}
                }
            }

        if (boss_bird.head.health <= 0 && (!boss_bird.state.includes('defeated'))) {
          boss_bird.life_bar.setFrame(0)
          this.scene.bird_defeated.play()
            if (boss_bird.state.includes('right')){
                this.boss_bird_defeated = this.scene.manual_time
                boss_bird.state = 'defeated_right'}
            else if (boss_bird.state.includes('left')){
                this.boss_bird_defeated = this.scene.manual_time
                boss_bird.state = 'defeated_left'}
            }

        if (boss_bird.y > ((this.scene.layers.layer.height*2)+1000)){
          boss_bird.life_bar.setVisible(false)
            if (!this.crash_bird_played){
              this.scene.crash.play()
              this.crash_bird_played = true}
            if (boss_bird.head.health <= 0){
              this.scene.cameras.main.shake(100, 0.01);}
            this.scene.time.delayedCall(400, function() {
            try {
              if (boss_bird.data.values.boss == true){
                this.scene.data.values.boss_defeated = true}}
            catch (TypeError){
                console.log('Caught Boss Bird TypeError')}
            boss_bird.destroy()
            boss_bird.setVisible(false)
            }, [], this);
          }

      if(boss_bird.state.includes('defeated') && boss_bird.data.values.boss == false){
        for (var i=0;i<this.scene.barrier_array.length;i++){
            if (this.scene.barrier_array[i].x < 18000){
              this.scene.barrier_array[i].body.setAllowGravity(true)}}}

      if (boss_bird.head.just_damaged && boss_bird.head.health > 0){
        var boss_life_frame = Phaser.Math.CeilTo((boss_bird.head.health/boss_bird.data.values.max_health) * 18)
        boss_bird.life_bar.setFrame(boss_life_frame)
        boss_bird.setTint(0x440000)
        boss_bird.data.values.time_at_hit = this.scene.manual_time
        boss_bird.head.just_damaged = false
      }

      if (this.scene.manual_time > boss_bird.data.values.time_at_hit + 50){
        boss_bird.setTint(0xffffff)
      }

      } // if boss_bird active
    }, this); // looping through all boss_birds
} // update

addBossBird(scene, x, y, level_boss) {
    var boss_health = 30
    if (!level_boss){
      boss_health = 20
    }
		var boss_bird = scene.boss_birds.create(x, y, 'boss_bird');
		boss_bird.setActive(true).setVisible(true)
		boss_bird.setData({'health': 7, //10
								 'time_at_collision':null,
								 'knock_back_time': 150,
								 'bs_thrown': 0,
                 'x_spawn': x,
                 'y_spawn': y,
								 'layer_collisions': [],
						 		 'last_attack': 0,
                 'attack_wait': Phaser.Math.Between(10000, 12000),
                 'boss': level_boss,
                 'origin_x': x,
                 'hover': 'up',
                 'last_switch_time': scene.manual_time,
                 'max_health': boss_health
							 });

     // for (var i=0;i<scene.layers.length;i++){
     //     boss_bird.data.values.layer_collisions.push(scene.physics.add.collider(boss_bird, scene.layers[i]));}

    boss_bird.body.immovable = true;
    boss_bird.setScale(2.3)
    // boss_bird.setOffset(92,67) //y=25
		// boss_bird.setSize(72, 114, false) //h=156
    boss_bird.setCircle(36,60,92)
    boss_bird.setDepth(740);
    boss_bird.projectiles = []
    if (boss_bird.x > this.player.player.x){
        boss_bird.state = 'enter_left'}
    else if (boss_bird.x <= this.player.player.x){
        boss_bird.state = 'enter_right'}
		scene.boss_birds.add(boss_bird)
    boss_bird.body.setAllowGravity(false);
    scene.enemies_added += 1;

    boss_bird.head = scene.physics.add.sprite(x, y, 'wheel');
    boss_bird.head.setVisible(false)
    boss_bird.head.setScale(1.5);
    boss_bird.head.setSize(42,50)
    boss_bird.head.body.immovable = true;
    boss_bird.head.body.setAllowGravity(false);
    boss_bird.head.setDepth(750);
    boss_bird.head.health = boss_health; // 30
    boss_bird.head.just_damaged = false

    boss_bird.life_bar = scene.physics.add.sprite(1060, 30, 'boss_life_bar').setDepth(1000);
    boss_bird.life_bar.body.setAllowGravity(false);
    boss_bird.life_bar.setScrollFactor(0,0)
    boss_bird.life_bar.setFrame(18)
}

}
