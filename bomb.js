export default class Bomb {

constructor(scene, boss, player, direction){
  this.scene = scene
  this.player = player

var diff = Math.abs(boss.x - player.player.x)
var x_velocity
// var y_velocity = -300
var angular
var angle

var y_velocity = Phaser.Math.Between(-500,-700)

  if (direction == 'left'){
      x_velocity = (-(this.get_x_velocity(diff)))
      angle = 15
      angular = -7}
  else if (direction == 'right'){
      x_velocity = this.get_x_velocity(diff)
      angle = -15
      angular = 7}

  this.bomb = scene.bombs.create(boss.x, boss.y-15, 'canceled');
  this.bomb.setScale(0.175)
  this.bomb.setDepth(100)
  this.bomb.setOffset(150,50)
  // bomb.setFrame(0)
  this.bomb.setSize(500, 230, false)
  this.bomb.setAngle(Phaser.Math.Between(-20,20))
  this.bomb.setAngularVelocity(angular);
  // bomb.body.setAllowGravity(false);
  this.bomb.setVelocity(x_velocity, y_velocity);

  this.scene.player_bomb = this.scene.physics.add.collider(this.bomb, this.player.player, function(bomb, player){
      bomb.destroy()
      bomb.setVisible(false)
      this.player.player.data.values.time_at_collision = scene.manual_time
      this.player.player.data.values.hit_from = bomb.x
      scene.player_hit.play()
      this.player.damagePlayer(this.player, scene, 3)
    },
    function(bomb, player){
        return (this.player.player.data.values.post_hit_invincibility == false)
    }, this);

}

update(){
    if (this.bomb.y > (((this.scene.layers.layer.height)*2)+100)){
        this.bomb.destroy()
    }
}

get_x_velocity(diff){
    if (diff <= 200){
        return Phaser.Math.Between(0,50)}
    else if (diff > 200 && diff < 300) {
        return Phaser.Math.Between(50,100)}
    else if (diff > 300 && diff < 400) {
        return Phaser.Math.Between(100,170)}
    else if (diff > 400 && diff < 500) {
        return Phaser.Math.Between(170,240)}
    else if (diff > 500 && diff < 600) {
        return Phaser.Math.Between(210,270)}
    else if (diff > 600 && diff < 700) {
        return Phaser.Math.Between(310,380)}
    else if (diff > 700 && diff < 800) {
        return Phaser.Math.Between(380,450)}
    else if (diff > 800) {
        return Phaser.Math.Between(450,500)}
}

}
