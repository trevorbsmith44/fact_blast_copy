export default class Gear {

constructor(scene){
    scene.gears = scene.physics.add.group();
}

addGear(scene, x, y, frame, size, angular_velocity) {
		var gear = scene.gears.create(x, y, 'gear');
		gear.setActive(true).setVisible(true);
    gear.setFrame(frame) // 1 or 0
		gear.setScale(size);
    gear.setDepth(15)
    gear.setTint('0xff747474')

    gear.body.setAllowGravity(false);
    gear.body.allowGravity = false;
		scene.gears.add(gear);
    gear.body.setAllowGravity(false);
    gear.body.setAngularVelocity(angular_velocity)
}

}
