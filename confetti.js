export default class Confetti {

constructor(scene){
    scene.confetti_dots = scene.physics.add.group();
    this.scene = scene
}

update(){
  this.scene.confetti_dots.children.each(function(confetti) {
      if (confetti.active){

          if (confetti.y > (1300)) {
              confetti.setPosition(Phaser.Math.Between(-500, 1700), -100)
            }

          if (confetti.data){ // prevents TypeError
            confetti.setAngle(325)
            confetti.setVelocity(-Phaser.Math.Between(50, 100), confetti.data.values.speed) // down left
            } // if confetti.data exists

          }  // if confetti active
    }, this); // looping through all confetti_dots
}

addConfetti(scene) {
    var x = Phaser.Math.Between(-500, 1700)
    var y = -100
    var confetti = this.scene.confetti_dots.create(x, y, 'confetti');
		confetti.setActive(true).setVisible(true)
		confetti.setData({'health': 1,
      								 'speed': Phaser.Math.Between(150, 225),
                       'x_origin': x,
                       'y_origin': y
      							 });
		confetti.setSize(80, 50, false)
		confetti.setOffset(15,5)
    confetti.setScale(1.5)

    confetti.setDepth(999)
    confetti.name = 'confetti'
		this.scene.confetti_dots.add(confetti)
		confetti.body.setAllowGravity(false);
    confetti.setFrame(Phaser.Math.Between(0,6))

}

}
