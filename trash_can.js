export default class TrashCan {

constructor(scene, boss_tomato, player, direction){
  this.scene = scene
  this.player = player

var diff = Math.abs(boss_tomato.x - player.player.x)
var x_spawn
var x_velocity
// var y_velocity = -300
var angular
var angle

var y_velocity = Phaser.Math.Between(-500,-700)

  if (direction == 'left'){
      x_spawn = -160
      // x_velocity = Phaser.Math.Between(-50,-350)
      x_velocity = (-(this.get_x_velocity(diff)))
      angle = 30
      angular = -50}
  else if (direction == 'right'){
      x_spawn = 160
      x_velocity = this.get_x_velocity(diff)
      angle = -30
      angular = 50}

  this.trash_can = scene.trash_can.create(boss_tomato.x+x_spawn, boss_tomato.y-15, 'trash_can');
  this.trash_can.setScale(2.2)
  this.trash_can.setDepth(100)
  this.trash_can.setOffset(5,10)
  // trash_can.setFrame(0)
  this.trash_can.setSize(20,20, false)
  this.trash_can.setAngle(angle)
  this.trash_can.setAngularVelocity(angular);
  // trash_can.body.setAllowGravity(false);
  this.trash_can.setVelocity(x_velocity, y_velocity);

  this.scene.player_trash = this.scene.physics.add.collider(this.trash_can, this.player.player, function(trash, player){
      trash.destroy()
      trash.setVisible(false)
      this.player.player.data.values.time_at_collision = scene.manual_time
      this.player.player.data.values.hit_from = trash.x
      scene.clang.play()
      this.player.damagePlayer(this.player, scene, 5)
    },
    // this.player.checkPostHitInvincibility
    function(trash, player){
        return (this.player.player.data.values.post_hit_invincibility == false)
    }, this);

}

update(){
    if (this.trash_can.y > (((this.scene.layers.layer.height)*2)+100)){
        this.trash_can.destroy()
    }
}

get_x_velocity(diff){
    if (diff <= 200){
        return Phaser.Math.Between(0,50)}
    else if (diff > 200 && diff < 300) {
        return Phaser.Math.Between(50,100)}
    else if (diff > 300 && diff < 400) {
        return Phaser.Math.Between(100,170)}
    else if (diff > 400 && diff < 500) {
        return Phaser.Math.Between(170,240)}
    else if (diff > 500 && diff < 600) {
        return Phaser.Math.Between(210,270)}
    else if (diff > 600 && diff < 700) {
        return Phaser.Math.Between(310,380)}
    else if (diff > 700 && diff < 800) {
        return Phaser.Math.Between(380,450)}
    else if (diff > 800) {
        return Phaser.Math.Between(450,500)}
}

}
