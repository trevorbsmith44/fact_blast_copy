export default class CutScene1Options extends Phaser.Scene {

constructor() {
  	super({key: "cutscene1_options"});
}

create(){
  this.current_image = 1
  this.c1i1 = this.add.image(600, 300, 'c1i1')
  this.add.text(980, 450, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
  this.may_skip = true
  this.input.gamepad.once('down', function (pad, button, index) {
			this.gamepad = pad;
    }, this);

  this.key_skip = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);

  this.music = this.sound.add('cutscene_soundtrack');
  var music_config = {
    mute: false,
    volume: 0.3,
    rate: 1,
    detune: 0,
    seek: 0,
    loop: true,
    delay: 0
  }
  this.music.play(music_config)
}

update(){
  if (Phaser.Input.Keyboard.JustDown(this.key_skip)) { //  || (this.gamepad.A)
    if (this.current_image == 1){
      this.add.image(600, 300, 'c1i2')
      this.add.text(980, 450, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 2}
    else if (this.current_image == 2){
      this.add.image(600, 300, 'c1i3')
      this.add.text(980, 450, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 3}
    else if (this.current_image == 3){
      this.music.stop()
      this.scene.start('level1')}
      }

  if (this.gamepad){
    if (this.gamepad.A && this.may_skip) {
      if (this.current_image == 1){
        this.add.image(600, 300, 'c1i2')
        this.add.text(980, 450, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 2}
      else if (this.current_image == 2){
        this.add.image(600, 300, 'c1i3')
        this.add.text(980, 450, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 3}
      else if (this.current_image == 3){
        this.music.stop()
        this.scene.start('level1')}}
    this.may_skip = ((this.gamepad.A) ? false : true);
  }
}

} // class
