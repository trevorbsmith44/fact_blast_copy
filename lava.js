export default class Lava {

constructor(scene, player){
    scene.lavas = scene.physics.add.group();
    this.scene = scene
    this.player = player

    this.player_lava = scene.physics.add.collider(this.player.player, scene.lavas, function(player, lava){
        this.player.player.data.values.time_at_collision = this.scene.time.now
        this.player.player.data.values.hit_from = lava.x
        this.player.player.data.values.post_hit_invincibility = false
        scene.sizzle.play()
        this.player.damagePlayer(this.player, scene, 18)
        lava.setVelocityX(0)
        this.player.player.setTint(0xf02222)
        player.body.immovable = false;
        lava.body.immovable = true;
      }, function(player,lava){
          return (player.data.values.health > 0)
      }, this);
}

addLava(scene, x, y) {
		var lava = scene.lavas.create(x, y, 'lava');
		lava.setActive(true).setVisible(true)
		lava.setData({
                'layer_collisions': [],
              });

    for (var i=0;i<scene.layers.length;i++){
        lava.data.values.layer_collisions.push(scene.physics.add.collider(lava, scene.layers[i]));}

    lava.setScale(2)
    lava.setOffset(0,18)
    lava.setSize(30,45,false)
    lava.setDepth(800)

		scene.lavas.add(lava)
    lava.body.setAllowGravity(false)
    scene.anims.play('lava_bubble', lava);
    lava.body.immovable = true;
}

}
