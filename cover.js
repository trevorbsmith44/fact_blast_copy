export default class Cover extends Phaser.Scene {

constructor() {
  	super({key: "cover"});
}

create (){
  this.background = this.physics.add.sprite(600, 300, "starburst");
  // this.background.setOrigin(0,0)
  this.background.body.setAllowGravity(false)
  // this.background.setAngularVelocity(20)
  // this.background.displayOriginY = 200

  this.title_text = this.add.image(600, 280 - 170, 'title')
  this.title_text.setScale(1.4)

  // this.logo = this.add.image(800, 300, 'logo2')
  // this.logo.setScale(0.26)

  this.player = this.physics.add.sprite(420,480, 'jeremy').setScale(6);
  this.player.body.setAllowGravity(false)
  this.player.setFrame(0)

  this.npc = this.physics.add.sprite(780,480, 'npc').setScale(6);
  this.npc.body.setAllowGravity(false)
  this.npc.setFrame(5)

}


} // class
