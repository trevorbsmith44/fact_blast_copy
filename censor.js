export default class Censor {

constructor(scene, bird, player){
    this.scene = scene
    this.player = player

    var censor = scene.censorships.create(bird.x, bird.y, 'censored');
		bird.setData({'throw_wait': Phaser.Math.Between(2500, 5000),
								 'last_throw': this.scene.manual_time,
								 'may_throw': false})
    censor.setDepth(500)

    this.scene.time.delayedCall(10000, function() {
        censor.destroy()
    }, [], this);

		this.scene.player_censor = this.scene.physics.add.collider(censor, this.player, function(censor, player){  // ONLY RUNS DURING COLLISION, DOES NOT LOOP
				censor.destroy()
				censor.setVisible(false)
				player.data.values.time_at_collision = this.manual_time
				player.data.values.hit_from = censor.x
        scene.player_hit.play()
				this.player.damagePlayer(this.player, scene, 4)
			}, function(bs, player){
          return (this.player.player.data.values.post_hit_invincibility == false)
      }, scene);

      for (var i=0;i<scene.layers.length;i++){
          scene.physics.add.collider(scene.layers[i], scene.censorships, this.dissolveCensorship, null, scene);
          }

}

update(){

}

dissolveCensorship(censor, target){
		censor.destroy()
		censor.setVisible(false)}

}
