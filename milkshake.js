export default class Milkshake {

constructor(scene, boss_npc, player, direction, diff){
  this.scene = scene
  this.player = player

var x_spawn
var x_velocity
var angular
var angle

diff = ((diff<=1000) ? (diff) : (1000));
var y_velocity = -300

  if (direction == 'left'){
      x_spawn = -145
      x_velocity = -diff+100
      angle = 30
      angular = -50}
  else if (direction == 'right'){
      x_spawn = 145
      x_velocity = diff-100
      angle = -30
      angular = 50}

  var altitudes = [-15, 65]
  var alt = altitudes[Math.floor(Math.random() * altitudes.length)];
  var frame_array = [0,1,2];
  var random_frame = frame_array[Math.floor(Math.random() * frame_array.length)];

  var milkshake = scene.milkshake.create(boss_npc.x+x_spawn, boss_npc.y-15, 'milkshake');
  milkshake.setScale(2.2)
  milkshake.setDepth(100)
  milkshake.setOffset(5,6)
  milkshake.setFrame(random_frame)
  milkshake.setSize(20,20, false)
  milkshake.setAngle(angle)
  milkshake.setAngularVelocity(angular);
  milkshake.setVelocity(x_velocity, y_velocity);

  this.scene.player_math = this.scene.physics.add.collider(milkshake, this.player.player, function(math, player){
      math.destroy()
      math.setVisible(false)
      this.player.player.data.values.time_at_collision = scene.manual_time
      this.player.player.data.values.hit_from = math.x
      scene.player_hit.play()
      this.player.damagePlayer(this.player, scene, 6)
    },
    function(math, player){
        return (this.player.player.data.values.post_hit_invincibility == false)
    }, this);

}

}
