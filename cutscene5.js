export default class CutScene5 extends Phaser.Scene {

constructor() {
  	super({key: "cutscene5"});
}

create(){
  this.current_image = 1
  this.c4i1 = this.add.image(600, 300, 'c5i1')
  this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
  this.may_skip = true
  this.input.gamepad.once('down', function (pad, button, index) {
			this.gamepad = pad;
    }, this);

  this.key_skip = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);

  this.music = this.sound.add('cutscene5_soundtrack');
  var music_config = {
    mute: false,
    volume: 0.5,
    rate: 1,
    detune: 0,
    seek: 0,
    loop: true,
    delay: 0
  }
  this.music.play(music_config)

}

update(){
  if (Phaser.Input.Keyboard.JustDown(this.key_skip)) { //  || (this.gamepad.A)
    if (this.current_image == 1){
      this.add.image(600, 300, 'c5i2')
      this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 2}
    else if (this.current_image == 2){
      this.add.image(600, 300, 'c5i3')
      this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 3}
    else if (this.current_image == 3){
      this.add.image(600, 300, 'c5i4')
      this.current_image = 4}
    else if (this.current_image == 4){
      this.music.stop()
      this.scene.start('level5')}
      }

  if (this.gamepad){
    if (this.gamepad.A && this.may_skip) {
      if (this.current_image == 1){
        this.add.image(600, 300, 'c5i2')
        this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 2}
      else if (this.current_image == 2){
        this.add.image(600, 300, 'c5i3')
        this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 3}
      else if (this.current_image == 3){
        this.add.image(600, 300, 'c5i4')
        this.current_image = 4}
      else if (this.current_image == 4){
        this.music.stop()
        this.scene.start('level5')}}
    this.may_skip = ((this.gamepad.A) ? false : true);
  }
}

} // class










// i1
// this.add.image(600, 100, 'level2_sky')
// this.add.image(600, 250, 'i1_bg')
// this.add.image(1000, 480, 'hill')
//
//
// this.physics.add.sprite(1000, 330, 'jeremy_straight_face').setScale(3).setFrame(1).setFlip(true,false).body.setAllowGravity(false)
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
// this.text = this.add.text(115, 510, "The desert has been purged of those evil bots,", { fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(100 , 545, "and Jeremy has finally reached the Golden State.", { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)















// i3
// this.add.image(600, 100, 'level2_sky')
// this.add.image(550, 125, 'i3_bg').setScale(1.2)
//
// this.physics.add.sprite(200, 305, 'jeremy').setScale(4).setFrame(8).body.setAllowGravity(false)
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
// this.text = this.add.text(90, 515, "The front door is open as if they dare Jeremy to", {fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(83 , 545, "enter. Let's put an end to this hostile takeover!", { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)
