export default class Demonetization {

constructor(scene, bot, player, x_spawn, x_velocity, angular){
  this.scene = scene
  this.player = player

  var demonetization = scene.demonetization.create(bot.x+x_spawn, bot.y-15, 'demonetization');
  demonetization.setScale(0.6)
  demonetization.setDepth(550)
  // demonetization.setOffset(20,15)
  // demonetization.setSize(90,100, false)
  demonetization.setCircle(42, 20, 20)
  demonetization.setAngle(Phaser.Math.Between(0,360))
  demonetization.setAngularVelocity(angular);
  demonetization.body.setAllowGravity(false);
  demonetization.setVelocityX(x_velocity);
  // demonetization.time_thrown = scene.time.now
  this.scene.time.delayedCall(10000, function() {
      demonetization.destroy()
  }, [], this);

  this.scene.player_math = this.scene.physics.add.overlap(demonetization, this.player.player, function(math, player){
      math.destroy()
      math.setVisible(false)
      scene.player_hit.play()
      this.player.player.data.values.time_at_collision = scene.manual_time
      this.player.player.data.values.hit_from = math.x
      this.player.damagePlayer(this.player, scene, 3)
    },
    function(math, player){
        return (this.player.player.data.values.post_hit_invincibility == false)
    }, this);

  this.demonetization = demonetization
  this.i = 0
}

update(){

}


}
