export default class PauseScene extends Phaser.Scene {

constructor() {
  	super({key: "pause_scene"});
}

init(data){
  this.data = data
}

create(){
  this.input.gamepad.once('down', function (pad, button, index) {
      this.gamepad = pad;
    }, this);

  this.key_pause = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.P);
  this.add.text(400, 50, "pause", {fill: '#FFF'}).setFontSize(120).setStroke('#000', 10)

  if (this.data.prev != 'level1'){
    this.add.text(150, 280, "To return to this level, hold   ", {fill: '#FFF'}).setFontSize(50).setStroke('#000', 6)
    this.add.text(550, 380, "and", {fill: '#FFF'}).setFontSize(50).setStroke('#000', 6)
    this.add.text(200, 480, "while clicking 'play game'", {fill: '#FFF'}).setFontSize(50).setStroke('#000', 6)
    this.add.image(470, 400, 'keyboard').setScale(1.5)
    this.add.image(720, 400, 'keyboard').setScale(1.5)}

  if (this.data.prev == 'level1'){
    this.add.text(190, 180, 'Level 1: Orlando Theme Park', {fill: '#FFF'}).setFontSize(50).setStroke('#000', 4)
  }
  if (this.data.prev == 'level2'){
    this.add.text(300, 180, 'Level 2: The Interstate', {fill: '#FFF'}).setFontSize(40).setStroke('#000', 4)
    this.add.text(450, 370, "I", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
    this.add.text(700, 370, "N", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
  }
  if (this.data.prev == 'level3'){
    this.add.text(390, 180, 'Level 3: Chicago', {fill: '#FFF'}).setFontSize(40).setStroke('#000', 4)
    this.add.text(450, 370, "I", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
    this.add.text(700, 370, "L", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
  }
  if (this.data.prev == 'level4'){
    this.add.text(340, 180, 'Level 4: The Desert', {fill: '#FFF'}).setFontSize(40).setStroke('#000', 4)
    this.add.text(450, 370, "N", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
    this.add.text(700, 370, "V", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
  }
  if (this.data.prev == 'level5'){
    this.add.text(390, 180, 'Level 5: HPC HQ', {fill: '#FFF'}).setFontSize(40).setStroke('#000', 4)
    this.add.text(450, 370, "G", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
    this.add.text(700, 370, "S", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
  }
  if (this.data.prev == 'level6'){
    this.add.text(280, 180, 'Level 6: The Final Battle', {fill: '#FFF'}).setFontSize(40).setStroke('#000', 4)
    this.add.text(450, 370, "F", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
    this.add.text(700, 370, "B", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
  }

  this.key_left = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
  this.key_right = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
  this.key_up = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
  this.key_down = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);

  this.key_left.isDown = this.data.left_key_pressed
  this.key_right.isDown = this.data.right_key_pressed
  this.key_up.isDown = this.data.up_key_pressed
  this.key_down.isDown = this.data.down_key_pressed

  this.mute = this.physics.add.sprite(1080, 566, 'mute')
  this.mute.setScale(0.5)
  this.mute.setInteractive().on('pointerup', () => this.muteAll())
  if (this.game.sound.mute == false){
    this.mute.setFrame(1)}
  else{
    this.mute.setFrame(0)}
  this.mute.body.setAllowGravity(false)

  this.main_menu = this.physics.add.sprite(150, 566, 'main_menu')
  this.main_menu.setScale(0.5)
  this.main_menu.setInteractive().on('pointerup', () => this.startTitleScreen())
  this.main_menu.setInteractive().on('pointerover', () => this.main_menu.setFrame(1))
  this.main_menu.setInteractive().on('pointerout', () => this.main_menu.setFrame(0))
  this.main_menu.body.setAllowGravity(false)
}

update(){

    if (this.key_left.isDown){
      this.game.left_key_pressed = true
      this.game.right_key_pressed = false}
    else if (this.key_right.isDown){
      this.game.right_key_pressed = true
      this.game.left_key_pressed = false}
    else {
      this.game.right_key_pressed = false
      this.game.left_key_pressed = false}
    if (this.key_up.isDown){
      this.game.up_key_pressed = true}
    else{
      this.game.up_key_pressed = false}
    if (this.key_down.isDown){
      this.game.down_key_pressed = true}
    else{
      this.game.down_key_pressed = false}

  if (Phaser.Input.Keyboard.JustDown(this.key_pause)){
      this.game.use_game_properties = true
      this.game.cur_time = this.data.cur_time
      this.scene.resume(this.data.prev)
      this.scene.stop()
  }
}

muteAll(){
  if (this.game.sound.mute == false){
    this.game.sound.mute = true
    this.mute.setFrame(0)}
  else{
    this.game.sound.mute = false
    this.mute.setFrame(1)}
}

startTitleScreen(){
  location.reload()
}

} // class
