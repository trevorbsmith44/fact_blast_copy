export default class Liquid {

constructor(scene, player){
    scene.liquid_tanks = scene.physics.add.group();
    this.scene = scene
    this.player = player
}

addLiquid(scene, x, y, color) {
		var liquid = scene.liquid_tanks.create(x, y, 'liquid_'+color);
		liquid.setActive(true).setVisible(true)
		liquid.setData({
                'layer_collisions': [],
              });

    for (var i=0;i<scene.layers.length;i++){
        liquid.data.values.layer_collisions.push(scene.physics.add.collider(liquid, scene.layers[i]));}

    liquid.setScale(3)
    liquid.setSize(30,65,false)
    liquid.setDepth(2)
		scene.liquid_tanks.add(liquid)
    scene.anims.play('liquid_'+color, liquid);
}

}
