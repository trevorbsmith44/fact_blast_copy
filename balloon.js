export default class Balloon {

constructor(scene){
    scene.balloons = scene.physics.add.group();
}

addBalloon(scene, x, y, color) {
		var balloon = scene.balloons.create(x, y, 'balloon_'+color);
		balloon.setActive(true).setVisible(true);
		balloon.setScale(2);
    balloon.setDepth(2)

    balloon.body.setAllowGravity(false);
    balloon.body.allowGravity = false;
		scene.balloons.add(balloon);
    balloon.body.setAllowGravity(false);
    scene.anims.play('balloon_'+color, balloon);
}

}
