export default class Flag {

constructor(scene){
    scene.flags = scene.physics.add.group();
}

addFlag(scene, x, y, color) {
		var flag = scene.flags.create(x, y, 'flag');
		flag.setActive(true).setVisible(true);
		flag.setScale(2);
    flag.setDepth(1)

    flag.body.setAllowGravity(false);
    flag.body.allowGravity = false;
		scene.flags.add(flag);
    flag.body.setAllowGravity(false);
    scene.anims.play('flag_'+color, flag);
}

}
