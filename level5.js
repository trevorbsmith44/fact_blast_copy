import Preload from "./preload.js";
import Player from "./player.js";
import Fact from  "./fact.js";
import Npc from  "./npc.js";
import Bs from  "./bs.js";
import Bird from  "./bird.js";
import Censor from "./censor.js"
import Tomato from  "./tomato.js";
import Juice from "./juice.js";
import Balloon from "./balloon.js";
import Flag from "./flag.js";
import Bot from "./bot.js";
import BossBot from "./boss_bot.js";
import SmallBot from "./small_bot.js";
import SpikeyBall from  "./spikey_ball.js";
import MovingSaw from "./moving_saw.js";
import BossNpc from "./boss_npc.js"
import BossTomato from "./boss_tomato.js"
import BossBird from "./boss_bird.js"
import Machine from "./machine.js"
import Gear from "./gear.js"
import Platform from "./platform.js"
import Mocker from "./mocker.js"
import Explosion from "./explosion.js"
import Liquid from "./liquid.js"
import Barrier from "./barrier.js"

export default class level5 extends Phaser.Scene {

constructor() {
  	super({key: "level5"});
}

create (){
    this.scene_name = 'level5'
    this.bg_1 = this.add.tileSprite(0, 1000, 1200, 800, "level5_bg");
    this.bg_1.setOrigin(0, 0);
    this.bg_1.setScrollFactor(0); // repeats background
    this.bg_1.setPosition(0, -125)
    this.bg_1.setTileScale(2,2);
    this.myCam = this.cameras.main;

    this.map = this.add.tilemap("level5_map");
    var tileset = this.map.addTilesetImage("tileset", "level5_tileset", 32, 32, 1, 2);  //margin=1, spacing=2, this is needed because of using the frame extruder program
    this.layers = []
    this.layers.layer = this.map.createStaticLayer(0, tileset, 0, 0);
    this.layers.push(this.layers.layer)
    this.layers.layer.setCollisionByProperty({ collides: true });
    this.layers.layer.setScale(2)

    this.layers.layer2 = this.map.createStaticLayer(1, tileset, 0, 0);
    this.layers.push(this.layers.layer2)
    this.layers.layer2.setCollisionByProperty({ collides: true });
    this.layers.layer2.setScale(2)

    for (var i=0;i<this.layers.length;i++){
        // this.layers[i].setTileLocationCallback(20, 49, 9, 1, this.topCollisionOnly, this);
    }

    this.player_x_spawn = 150 // 150
    this.player_y_spawn = 1000 // 1000

    if (this.cp == 1){
        this.player_x_spawn = 4670
        this.player_y_spawn = 300}
    else if (this.cp == 2){
        this.player_x_spawn = 9500
        this.player_y_spawn = 420}
    else if (this.cp == 3){
        this.player_x_spawn = 16179
        this.player_y_spawn = 420}

		this.player = new Player(this, this.player_x_spawn, this.player_y_spawn);
    this.cameras.main.setBounds(0, 0, (this.map.widthInPixels*2), (this.map.heightInPixels*2));
    this.player_y_at_jump = this.player.player.y
    this.time_at_landing = 0
    this.bs_statements = this.physics.add.group(); // Must have this here, otherwise facts go through BS randomly
    this.censorships = this.physics.add.group();
    this.math_stuff = this.physics.add.group();
    this.milkshake = this.physics.add.group();
    this.egg = this.physics.add.group();
    this.trash_can = this.physics.add.group();
    this.demonetization = this.physics.add.group();
    this.stop_follow = false
    this.regular_tomato = this.physics.add.group();
    this.trash_can = this.physics.add.group();
    this.egg = this.physics.add.group();
    this.platform_sprites = new Platform(this, this.player)
    this.barrier_sprites = new Barrier(this, this.player)

    this.juice_sprites = new Juice(this, this.player)
    this.juice_sprites.addJuice(this, 5183, 1135, 'big')
    if (!this.cp || this.cp < 3){
      this.juice_sprites.addJuice(this, 16704, 1100, 'big')}

    this.platform_sprites.addPlatform(this, 800, 1185, 'horizontal', 150, 260, 1, 1, 4) // direction, speed, distance, x_scale, y_scale, frame
    this.platform_sprites.addPlatform(this, 1445, 1185, 'horizontal', -150, 260, 1, 1, 4) // direction, speed, distance, x_scale, y_scale, frame
    this.platform_sprites.addPlatform(this, 2400, 1185, 'horizontal', 150, 200, 1, 1, 4) // direction, speed, distance, x_scale, y_scale, frame
    this.platform_sprites.addPlatform(this, 11040, 520, 'vertical', 200, 701, 1, 1, 4) // direction, speed, distance, x_scale, y_scale, frame
    this.platform_sprites.addPlatform(this, 11290, 1250, 'horizontal', -100, 50, 1, 1, 4) // direction, speed, distance, x_scale, y_scale, frame
    this.platform_sprites.addPlatform(this, 11485, 1220, 'vertical', -200, 700, 1, 1, 4) // direction, speed, distance, x_scale, y_scale, frame
    this.platform_sprites.addPlatform(this, 11170, 300, 'horizontal', -75, 140, 3, 1, 4) // direction, speed, distance, x_scale, y_scale, frame
    this.platform_sprites.addPlatform(this, 11000, 90, 'horizontal', 100, 300, 1, 1, 4) // direction, speed, distance, x_scale, y_scale, frame
    this.platform_sprites.addPlatform(this, 11681, 580, 'horizontal', 100, 130, 1, 1, 4) // direction, speed, distance, x_scale, y_scale, frame
    this.platform_sprites.addPlatform(this, 12320, 580, 'horizontal', 100, 130, 1, 1, 4) // direction, speed, distance, x_scale, y_scale, frame
    this.platform_sprites.addPlatform(this, 11616, 500, 'vertical', 0, 0, 1, 8, 0) // direction, speed, distance, x_scale, y_scale, frame
    this.platform_sprites.addPlatform(this, 20000, 1225, 'horizontal', 150, 210, 1, 1, 4) // direction, speed, distance, x_scale, y_scale, frame
    this.platform_sprites.addPlatform(this, 20400, 1225, 'horizontal', 150, 210, 1, 1, 4) // direction, speed, distance, x_scale, y_scale, frame

    this.mocker_sprites = new Mocker(this);
    this.mocker_sprites.addMocker(this, 12190, 1020, 'right')
    this.mocker_sprites.addMocker(this, 14440, 1150, 'right')
    this.mocker_sprites.addMocker(this, 21830, 1088, 'right')

    this.gear_sprites = new Gear(this)
    // this.gear_sprites.addGear(this, 340, 1250, 0, 4, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 460, 1275, 0, 4, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 540, 1250, 1, 4, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 590, 1260, 1, 4, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 3230, 1250, 1, 6, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 3310, 1275, 1, 6, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 3450, 1300, 0, 6, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 3550, 820, 0, 5, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 3680, 710, 0, 6, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 1110, 370, 0, 4, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 1220, 400, 0, 3.5, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 1330, 370, 0, 4, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 1415, 400, 1, 5, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 200, 720, 0, 5, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 120, 650, 1, 5, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 3930, 700, 0, 8, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 4480, 650, 1, 7, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 5350, 475, 0, 10, 70) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 5150, 620, 0, 6, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 6220, 360, 0, 4, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 6330, 390, 0, 3.5, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 6440, 360, 0, 4, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 6535, 390, 1, 5, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 7770, 240, 0, 10, -70) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 7990, 150, 0, 6, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 10250, 740, 0, 5, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 10340, 670, 1, 5, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 15595, 780, 1, 6, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 15655, 740, 1, 5, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 15760, 780, 0, 5, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 15925, 800, 0, 5, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 16090, 760, 0, 5.75, -100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 13280, 35, 0, 5.75, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 13380, 600, 0, 5.75, -80) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 13420, 780, 0, 6.25, 80) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 13560, 920, 0, 5.75, -80) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 13670, 850, 1, 5.75, 100) // frame, size, angular_velocity
    // this.gear_sprites.addGear(this, 13795, 900, 0, 6.25, -60) // frame, size, angular_velocity

    this.enemies = []
    for (var i=0;i<this.enemies.length;i++){
        this.enemies[i] = false}

    this.npc_sprites = new Npc(this, this.player)
    this.bird_sprites = new Bird(this, this.player)
    this.bot_sprites = new Bot(this, this.player)
    this.boss_bot_sprites = new BossBot(this, this.player)
    this.small_bot_sprites = new SmallBot(this, this.player)
    this.spikey_ball_sprites = new SpikeyBall(this, this.player)
    this.tomato_sprites = new Tomato(this, this.player)
    this.moving_saw_sprites = new MovingSaw(this, this.player)
    this.boss_npc_sprites = new BossNpc(this, this.player)
    this.boss_tomato_sprites = new BossTomato(this, this.player)
    this.boss_bird_sprites = new BossBird(this, this.player)
    this.machine_sprites = new Machine(this, this.player)

    this.npc_sprites.addNPC(this, 3500, 1050, 'stand', 'none')
    this.npc_sprites.addNPC(this, 5240, 850, 'stand', 'none')
    this.npc_sprites.addNPC(this, 13500, 50, 'stand', 'none')
    this.npc_sprites.addNPC(this, 20100, 650, 'stand', 'none')
    this.npc_sprites.addNPC(this, 20490, 650, 'stand', 'none')
    this.npc_sprites.addNPC(this, 11045, 400, 'stand', 'none')
    this.npc_sprites.addNPC(this, 11495, 100, 'stand', 'none')
    this.npc_sprites.addNPC(this, 12335, 470, 'stand', 'none')

    this.ms1 = this.moving_saw_sprites.addMovingSaw(this, 2080, 930, 'vertical', 350, 450, 1.75) // speed, distance, size
    this.ms2 = this.moving_saw_sprites.addMovingSaw(this, 2910, 930, 'vertical', 350, 450, 1.75) // speed, distance, size
    this.ms3 = this.moving_saw_sprites.addMovingSaw(this, 560, 90, 'horizontal', 350, 1080, 1.75) // speed, distance, size
    this.ms4 = this.moving_saw_sprites.addMovingSaw(this, 1880, 190, 'horizontal', 350, 1270, 1.75) // speed, distance, size

    this.ms5 = this.moving_saw_sprites.addMovingSaw(this, 5545, 190, 'horizontal', 430, 1860, 2.5) // speed, distance, size
    this.ms6 = this.moving_saw_sprites.addMovingSaw(this, 5870, 980, 'horizontal', 400, 700, 2.5) // speed, distance, size
    this.ms7 = this.moving_saw_sprites.addMovingSaw(this, 6570, 980, 'horizontal', 400, 700, 2.5) // speed, distance, size
    this.ms8 = this.moving_saw_sprites.addMovingSaw(this, 7270, 980, 'horizontal', 400, 700, 2.5) // speed, distance, size

    this.ms9 = this.moving_saw_sprites.addMovingSaw(this, 10050, 400, 'vertical', 400, 500, 1.8) // speed, distance, size
    this.ms10 = this.moving_saw_sprites.addMovingSaw(this, 10495, 400, 'vertical', 400, 500, 1.8) // speed, distance, size
    this.ms11 = this.moving_saw_sprites.addMovingSaw(this, 10790, 400, 'vertical', 400, 500, 1.8) // speed, distance, size
    this.ms12 = this.moving_saw_sprites.addMovingSaw(this, 14020, 600, 'vertical', 400, 325, 1.8) // speed, distance, size
    this.ms13 = this.moving_saw_sprites.addMovingSaw(this, 14430, 600, 'vertical', 400, 475, 1.8) // speed, distance, size
    this.ms14 = this.moving_saw_sprites.addMovingSaw(this, 14690, 600, 'vertical', 400, 475, 1.8) // speed, distance, size
    this.ms15 = this.moving_saw_sprites.addMovingSaw(this, 14390, 540, 'horizontal', 375, 360, 1.8) // speed, distance, size
    this.ms16 = this.moving_saw_sprites.addMovingSaw(this, 15090, 500, 'vertical', 500, 475, 1.6) // speed, distance, size
    this.ms17 = this.moving_saw_sprites.addMovingSaw(this, 15180, 500, 'vertical', 500, 475, 1.6) // speed, distance, size

    this.ms18 = this.moving_saw_sprites.addMovingSaw(this, 21340, 420, 'vertical', 275, 300, 1.8) // speed, distance, size
    this.ms19 = this.moving_saw_sprites.addMovingSaw(this, 21600, 600, 'vertical', 300, 300, 1.8) // speed, distance, size
    this.ms20 = this.moving_saw_sprites.addMovingSaw(this, 21600, 200, 'vertical', 300, 300, 1.8) // speed, distance, size
    this.ms21 = this.moving_saw_sprites.addMovingSaw(this, 21920, 450, 'vertical', 0, 0, 3) // speed, distance, size
    this.ms22 = this.moving_saw_sprites.addMovingSaw(this, 22220, 350, 'vertical', 800, 450, 1.2) // speed, distance, size
    this.ms23 = this.moving_saw_sprites.addMovingSaw(this, 22440, 270, 'vertical', 280, 300, 2.3) // speed, distance, size
    this.ms24 = this.moving_saw_sprites.addMovingSaw(this, 22440, 720, 'vertical', 0, 0, 2.3) // speed, distance, size

    this.explosion_sprites = new Explosion(this)

    this.liquid_sprites = new Liquid(this)
    this.liquid_sprites.addLiquid(this, 3450, 900, 'green')
    this.liquid_sprites.addLiquid(this, 515, 600, 'green')
    this.liquid_sprites.addLiquid(this, 620, 600, 'purple')
    this.liquid_sprites.addLiquid(this, 5900, 650, 'blue')
    this.liquid_sprites.addLiquid(this, 6020, 650, 'purple')
    this.liquid_sprites.addLiquid(this, 5450, 700, 'purple')
    this.liquid_sprites.addLiquid(this, 5450, 1000, 'blue')
    this.liquid_sprites.addLiquid(this, 6200, 1000, 'green')
    this.liquid_sprites.addLiquid(this, 10270, 450, 'blue')
    this.liquid_sprites.addLiquid(this, 13750, 100, 'blue')
    this.liquid_sprites.addLiquid(this, 20550, 700, 'purple')

    if (!this.cp){
      this.barrier_sprites.addBarrier(this, 5025, 256, 1, 6)} // x, y, x_scale, y_scale
    if (!this.cp || this.cp < 2){
      this.barrier_sprites.addBarrier(this, 9887, 417, 1, 7)} // x, y, x_scale, y_scale
    if (!this.cp || this.cp < 3){
      this.barrier_sprites.addBarrier(this, 16544, 480, 1, 5)} // x, y, x_scale, y_scale
    if (!this.cp || this.cp < 4){
      this.barrier_sprites.addBarrier(this, 23840, 352, 1, 7)} // x, y, x_scale, y_scale

      this.music = this.sound.add('level_5_soundtrack');
      var music_config = {
        mute: false,
        volume: 0.4,
        rate: 1,
        detune: 0,
        seek: 0,
        loop: true,
        delay: 0
      }
      this.music.play(music_config)
      // this.game.sound.mute = true;

      // this.fps_display = this.add.text(600, 5, "FPS: " + this.sys.game.loop.actualFps, {fill: '#fff'}).setFontSize(40).setStroke('#000', 4).setDepth(1000)
      // this.fps_display.fixedToCamera = true;
      // this.fps_display.cameraOffset.setTo(200, 500);
      // console.log(this.myCam)

}

update(){
    this.player.update();
    this.npc_sprites.update(this.player.player);
    this.bird_sprites.update(this.player.player);
    this.small_bot_sprites.update(this.player.player);
    this.bot_sprites.update(this.player.player);
    this.boss_bot_sprites.update(this.player.player);
    this.spikey_ball_sprites.update(this.player.player);
    this.moving_saw_sprites.update(this.player.player)
    this.tomato_sprites.update(this.player.player);
    this.boss_npc_sprites.update(this.player.player);
    this.boss_tomato_sprites.update(this.player.player);
    this.boss_bird_sprites.update(this.player.player);
    this.machine_sprites.update(this.player.player);
    this.platform_sprites.update(this);

    var px = this.player.player.x
    var py = this.player.player.y

    // this.fps_display.setText("FPS: " + this.sys.game.loop.actualFps)
    // this.fps_display.setPosition(this.myCam.midPoint.x+380, this.myCam.midPoint.y-270)

    if (this.playerInBox(px, py, 730, 1080) && !this.vl62_played){
      this.vl62.play()
      this.vl62_played = true}

    if (this.playerInBox(px, py, 5150, 185) && !this.vl30_played){
      this.vl30.play()
      this.vl30_played = true}

    if (this.playerInBox(px, py, 9130, 570) && !this.vl23_played){
      this.vl23.play()
      this.vl23_played = true}

    if (this.playerInBox(px, py, 11278, 1147) && !this.vl65_played){
      this.vl65.play()
      this.vl65_played = true}

    if (this.playerInBox(px, py, 13036, 440) && !this.vl66_played){
      this.vl66.play()
      this.vl66_played = true}

    if (this.playerInBox(px, py, 20680, 1120) && !this.vl24_played){
      this.vl24.play()
      this.vl24_played = true}

    if (this.playerInBox(px, py, 18600, 600) && !this.vl67_played){
      this.vl67.play()
      this.vl67_played = true}

    if (this.playerInBox(px, py, 24500, 370) && !this.vl2_played){
      this.vl2.play()
      this.vl2_played = true}

    ///////////////////////////////////////////////

    if (this.playerInBox(px, py, 260, 1080) && (!this.enemies.enemy0) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 50, 550, 'walk', 'none')
        this.enemies.enemy0 = true}

    if (this.playerInBox(px, py, 260, 1080) && (!this.enemies.enemy4) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 400, 500, 'run', 'none')
        this.enemies.enemy4 = true}

    if (this.playerInBox(px, py, 1330, 1080) && (!this.enemies.enemy5) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 2200, 1080, 'stand', 'none')
        this.enemies.enemy5 = true}

    if (this.playerInBox(px, py, 1330, 1080) && (!this.enemies.enemy5_1) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 1950, 1080, 'stand', 'none')
        this.enemies.enemy5_1 = true}

    if (this.playerInBox(px, py, 2105, 1080) && (!this.enemies.enemy7) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 2780, 1080, 'stand', 'low')
        this.enemies.enemy7 = true}

    if (this.playerInBox(px, py, 2370, 1080) && (!this.enemies.enemy8) && this.cp < 1 ) {
        this.bird_sprites.addBird(this, 1700, 1000, 'wave')
        this.enemies.enemy8 = true}

    if (this.playerInBox(px, py, 2430, 1080) && (!this.enemies.enemy9) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 3322, 820, 'stand', 'none')
        this.enemies.enemy9 = true}

    if (this.playerInBox(px, py, 2430, 1080) && (!this.enemies.enemy10) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 3073, 690, 'stand', 'low')
        this.enemies.enemy10 = true}

    if (this.playerInBox(px, py, 3070, 700) && (!this.enemies.enemy11) && this.cp < 1 ) {
        this.bird_sprites.addBird(this, 3300, 0, 'dive')
        this.enemies.enemy11 = true}

    if (this.playerInBox(px, py, 2320, 600) && (!this.enemies.enemy12) && this.cp < 1 ) {
        this.tomato_sprites.addTomato(this, 1300, 550, 'red')
        this.enemies.enemy12 = true}

    if (this.playerInBox(px, py, 1450, 630) && (!this.enemies.enemy13) && this.cp < 1 ) {
        this.spikey_ball_sprites.addSpikeyBall(this, 570, 580, 'right', false, 18)
        this.enemies.enemy13 = true}

    if (this.playerInBox(px, py, 1200, 630) && (!this.enemies.enemy13_1) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 500, 620, 'run', 'low')
        this.enemies.enemy13_1 = true}

    if (this.playerInBox(px, py, 1200, 630) && (!this.enemies.enemy13_2) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 350, 620, 'run', 'none')
        this.enemies.enemy13_2 = true}

    if (this.playerInBox(px, py, 2000, 630) && (!this.enemies.enemy14) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 1180, 620, 'stand', 'none')
        this.enemies.enemy14 = true}

    if (this.playerInBox(px, py, 2000, 630) && (!this.enemies.enemy15) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 1130, 620, 'stand', 'none')
        this.enemies.enemy15 = true}

    if (this.playerInBox(px, py, 2000, 630) && (!this.enemies.enemy16) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 1080, 620, 'stand', 'none')
        this.enemies.enemy16 = true}

    if (this.playerInBox(px, py, 2000, 630) && (!this.enemies.enemy17) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 1030, 620, 'stand', 'none')
        this.enemies.enemy17 = true}

    if (this.playerInBox(px, py, 2000, 630) && (!this.enemies.enemy18) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 980, 620, 'stand', 'none')
        this.enemies.enemy18 = true}

    if (this.playerInBox(px, py, 2000, 630) && (!this.enemies.enemy19) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 930, 620, 'stand', 'none')
        this.enemies.enemy19 = true}

    if (this.playerInBox(px, py, 2000, 630) && (!this.enemies.enemy20) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 880, 620, 'stand', 'none')
        this.enemies.enemy20 = true}

    if (this.playerInBox(px, py, 2000, 630) && (!this.enemies.enemy21) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 830, 620, 'stand', 'none')
        this.enemies.enemy21 = true}

    if (this.playerInBox(px, py, 2000, 630) && (!this.enemies.enemy22) && this.cp < 1 ) { // end from last day
        this.npc_sprites.addNPC(this, 780, 620, 'stand', 'none')
        this.enemies.enemy22 = true}

    if (this.playerInBox(px, py, 1330, 185) && (!this.enemies.enemy25) && this.cp < 1 ) {
        this.tomato_sprites.addTomato(this, 300, 0, 'red')
        this.enemies.enemy25 = true}

    if (this.playerInBox(px, py, 1850, 180) && (!this.enemies.enemy26) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 2750, 50, 'stand', 'low')
        this.enemies.enemy26 = true}

    if (this.playerInBox(px, py, 3160, 185) && (!this.enemies.enemy27) && this.cp < 1 ) {
        this.tomato_sprites.addTomato(this, 4030, 440, 'green')
        this.enemies.enemy27 = true}

    if (this.playerInBox(px, py, 4390, 400) && (!this.enemies.enemy28) && this.cp < 1 ) {
        this.boss_npc_sprites.addBossNpc(this, 4200, -300, false)
        this.enemies.enemy28 = true}

    // if (this.playerInBox(px, py, 3160, 185) && (!this.transition_1_2) && this.cp < 1 ) { // SECTION 1 -> 2 TRANSITION
    //     this.ms1.destroy()
    //     this.ms2.destroy()
    //     this.ms3.destroy()
    //     this.ms4.destroy()
    //     this.ms5 = this.moving_saw_sprites.addMovingSaw(this, 5545, 190, 'horizontal', 422, 1860, 2.5) // speed, distance, size
    //     this.ms6 = this.moving_saw_sprites.addMovingSaw(this, 5870, 980, 'horizontal', 400, 700, 2.5) // speed, distance, size
    //     this.ms7 = this.moving_saw_sprites.addMovingSaw(this, 6570, 980, 'horizontal', 400, 700, 2.5) // speed, distance, size
    //     this.ms8 = this.moving_saw_sprites.addMovingSaw(this, 7270, 980, 'horizontal', 400, 700, 2.5) // speed, distance, size
    //     this.transition_1_2 = true
    // }

    // ============================= SECTION 2 =================================

    if (this.playerInBox(px, py, 6780, 185) && (!this.enemies.enemy30) && this.cp < 2 ) {
        this.small_bot_sprites.addSmallBot(this, 7820, 635, false)
        this.enemies.enemy30 = true}

    if (this.playerInBox(px, py, 7230, 700) && (!this.enemies.enemy31) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 6540, 690, 'walk', 'low')
        this.enemies.enemy31 = true}

    if (this.playerInBox(px, py, 7230, 700) && (!this.enemies.enemy32) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 6280, 690, 'walk', 'none')
        this.enemies.enemy32 = true}

    if (this.playerInBox(px, py, 7230, 700) && (!this.enemies.enemy33) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 6000, 690, 'walk', 'low')
        this.enemies.enemy33 = true}

    if (this.playerInBox(px, py, 7230, 700) && (!this.enemies.enemy33_1) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 5730, 690, 'stand', 'low')
        this.enemies.enemy33_1 = true}

    if (this.playerInBox(px, py, 6600, 700) && (!this.enemies.enemy34) && this.cp < 2 ) {
        this.tomato_sprites.addTomato(this, 7400, 690, 'red')
        this.enemies.enemy34 = true}

    if (this.playerInBox(px, py, 8650, 600) && (!this.enemies.enemy38) && this.cp < 2 ) {
        this.boss_tomato_sprites.addBossTomato(this, 9500, 400, false)
        this.enemies.enemy38 = true}

    // ============================= SECTION 3 =================================

    if (this.playerInBox(px, py, 9650, 570) && (!this.enemies.enemy42) && this.cp < 3 ) {
        this.npc_sprites.addNPC(this, 10800, 0, 'stand', 'low')
        this.enemies.enemy42 = true}

    if (this.playerInBox(px, py, 11730, 400) && (!this.enemies.enemy45) && this.cp < 3 ) {
        this.bird_sprites.addBird(this, 10950, 450, 'wave')
        this.enemies.enemy45 = true}

    if (this.playerInBox(px, py, 12300, 500) && (!this.enemies.enemy50) && this.cp < 3 ) {
        this.bot_sprites.addBot(this, 12990, 350, 'stand')
        this.enemies.enemy50 = true}

    if (this.playerInBox(px, py, 13020, 500) && (!this.enemies.enemy52) && this.cp < 3 ) {
        this.bot_sprites.addBot(this, 14220, 650, 'stand')
        this.enemies.enemy52 = true}

    if (this.playerInBox(px, py, 15680, 570) && (!this.enemies.enemy58) && this.cp < 3 ) {
        this.boss_bird_sprites.addBossBird(this, 16180, -65, false)
        this.enemies.enemy58 = true}

    // ============================= SECTION 4 =================================

    if (this.playerInBox(px, py, 16800, 570) && (!this.enemies.enemy59)) {
        this.npc_sprites.addNPC(this, 17567, 1080, 'stand', 'low')
        this.enemies.enemy59 = true}

    if (this.playerInBox(px, py, 16800, 570) && (!this.enemies.enemy60)) {
        this.npc_sprites.addNPC(this, 17635, 1015, 'stand', 'low')
        this.enemies.enemy60 = true}

    if (this.playerInBox(px, py, 16800, 570) && (!this.enemies.enemy61)) {
        this.npc_sprites.addNPC(this, 17435, 880, 'stand', 'low')
        this.enemies.enemy61 = true}

    if (this.playerInBox(px, py, 16800, 570) && (!this.enemies.enemy62)) {
        this.npc_sprites.addNPC(this, 17371, 755, 'stand', 'low')
        this.enemies.enemy62 = true}

    if (this.playerInBox(px, py, 16800, 570) && (!this.enemies.enemy63)) {
        this.npc_sprites.addNPC(this, 17507, 630, 'stand', 'low')
        this.enemies.enemy63 = true}

    if (this.playerInBox(px, py, 16800, 570) && (!this.enemies.enemy64)) {
        this.npc_sprites.addNPC(this, 17571, 560, 'stand', 'low')
        this.enemies.enemy64 = true}

    if (this.playerInBox(px, py, 16800, 570) && (!this.enemies.enemy65)) {
        this.npc_sprites.addNPC(this, 17371, 435, 'stand', 'low')
        this.enemies.enemy65 = true}

    if (this.playerInBox(px, py, 16800, 570) && (!this.enemies.enemy66)) {
        this.npc_sprites.addNPC(this, 17307, 310, 'stand', 'low')
        this.enemies.enemy66 = true}

    if (this.playerInBox(px, py, 16800, 570) && (!this.enemies.enemy67)) {
        this.npc_sprites.addNPC(this, 17494, 180, 'stand', 'low')
        this.enemies.enemy67 = true}

    if (this.playerInBox(px, py, 17730, 185) && (!this.enemies.enemy68)) {
        this.spikey_ball_sprites.addSpikeyBall(this, 17575, -50, 'right', false, 18)
        this.enemies.enemy68 = true}

    if (this.playerInBox(px, py, 17730, 185) && (!this.enemies.enemy69)) {
        this.spikey_ball_sprites.addSpikeyBall(this, 17575, -300, 'right', false, 18)
        this.enemies.enemy69 = true}

    if (this.playerInBox(px, py, 17730, 185) && (!this.enemies.enemy70)) {
        this.spikey_ball_sprites.addSpikeyBall(this, 17575, -550, 'right', false, 18)
        this.enemies.enemy70 = true}

    if (this.playerInBox(px, py, 17730, 185) && (!this.enemies.enemy71)) {
        this.spikey_ball_sprites.addSpikeyBall(this, 17575, -800, 'right', false, 18)
        this.enemies.enemy71 = true}

    if (this.playerInBox(px, py, 17730, 185) && (!this.enemies.enemy72)) {
        this.spikey_ball_sprites.addSpikeyBall(this, 17575, -1050, 'right', false, 18)
        this.enemies.enemy72 = true}

    if (this.playerInBox(px, py, 20400, 1080) && (!this.enemies.enemy73)) {
        this.npc_sprites.addNPC(this, 21472, 560, 'stand', 'none')
        this.enemies.enemy73 = true}

    if (this.playerInBox(px, py, 20400, 1080) && (!this.enemies.enemy74)) {
        this.npc_sprites.addNPC(this, 21724, 560, 'stand', 'none')
        this.enemies.enemy74 = true}

    if (this.playerInBox(px, py, 20400, 1080) && (!this.enemies.enemy75)) {
        this.npc_sprites.addNPC(this, 21921, 560, 'stand', 'none')
        this.enemies.enemy75 = true}

    if (this.playerInBox(px, py, 20400, 1080) && (!this.enemies.enemy76)) {
        this.npc_sprites.addNPC(this, 22112, 560, 'stand', 'none')
        this.enemies.enemy76 = true}

    if (this.playerInBox(px, py, 20400, 1080) && (!this.enemies.enemy76_1)) {
        this.npc_sprites.addNPC(this, 22309, 560, 'stand', 'none')
        this.enemies.enemy76_1 = true}

    if (this.playerInBox(px, py, 22160, 570) && (!this.enemies.enemy83_1)) {
        this.tomato_sprites.addTomato(this, 23050, 500, 'green')
        this.enemies.enemy83_1 = true}

    if (this.playerInBox(px, py, 21820, 570) && (!this.enemies.enemy83_2)) {
        this.bird_sprites.addBird(this, 21000, 650, 'wave')
        this.enemies.enemy83_2 = true}

    if (this.playerInBox(px, py, 22730, 570) && (!this.enemies.enemy84)) {
        this.boss_bot_sprites.addBossBot(this, 23600, 300, false)
        this.enemies.enemy84 = true}

    if (this.playerInBox(px, py, 23950, 370) && (!this.enemies.enemy85)) {
        this.machine_sprites.addMachine(this, 25020, 100)
        this.enemies.enemy85 = true}

    // CHECKPOINTS
    if (!this.cp) {
        this.cp = 0}
    if (this.playerInBox(px, py, 5130, 315) && this.cp <= 0) {
        this.cp = 1}
    if (this.playerInBox(px, py, 10020, 570) && this.cp <= 1) {
        this.cp = 2}
    if (this.playerInBox(px, py, 16760, 570) && this.cp <= 2) {
        this.cp = 3}

    if (!this.stop_follow){
    this.myCam.centerOnX(this.player.player.x)
    if (this.player.player.body.onFloor() || this.player.player.body.touching.down) {
            this.myCam.pan(this.player.player.x, this.player.player.y-90, 50, 'Linear', true);
        this.player_y_at_jump = this.player.player.y}
    else {
        this.y_offset = this.player.player.y - this.player_y_at_jump
        if (this.y_offset < 0){ // player staying above original jump point
                this.myCam.centerOnY(this.player_y_at_jump-90)}
        else {
            this.myCam.centerOn(this.player.player.x, this.player.player.y-90)}
        this.time_at_landing = this.time.now}}

    this.bg_1.tilePositionX = this.myCam.scrollX * .425;

    // END LEVEL code for this level is in Player class

} // update

playerInBox(px, py, x, y){
    if (px > x-100 && px < x+100 && py > y-100 && py < y+100){
        return true}
    else{
        return false}}

topCollisionOnly(sprite, tile){
    tile.setCollision(false, false, true, false)} //left, right, top, bottom

} // class
