import Egg from  "./egg.js";
import DarkRock from "./dark_rock.js"
import Bomb from  "./bomb.js";

export default class BossNpcMd {

constructor(scene, player){
    scene.boss_npc_mds = scene.physics.add.group();
    this.scene = scene
    this.player = player

    scene.physics.add.overlap(this.player.player, scene.boss_npc_mds,
      function(player, boss_npc_md){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = boss_npc_md.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 5) //6
        boss_npc_md.body.immovable = true;
        }, function(player, boss_npc_md){
            return (boss_npc_md.glass.health > 0 &&
                    this.player.player.data.values.post_hit_invincibility == false &&
                    scene.boss_transparent == false)
        }, this);
}

update(player){
  this.scene.boss_npc_mds.children.each(function(boss_npc_md) {
      if (boss_npc_md.active){

        for (var i=0;i<boss_npc_md.projectiles.length;i++){
            boss_npc_md.projectiles[i].update()
        }

        if (this.scene.boss_transparent == true){ // is transparent
          if (this.scene.manual_time < this.scene.boss_transparent_time+4000){ // still transparent
            boss_npc_md.setAlpha(0.5);
            boss_npc_md.drill1.setAlpha(0.5);
            boss_npc_md.drill2.setAlpha(0.5);
            boss_npc_md.torch.setAlpha(0.5);
          }
          else{
            this.scene.boss_transparent = false
            boss_npc_md.setAlpha(1);
            boss_npc_md.drill1.setAlpha(1);
            boss_npc_md.drill2.setAlpha(1);
            boss_npc_md.torch.setAlpha(1);
          }
        }

        if (boss_npc_md.glass.health == 7){
          if (!this.scene.vl68_played){
            this.scene.time.delayedCall(1000, function() {
              this.scene.vl68.play()
              }, [], this)
          this.scene.vl68_played = true}
          boss_npc_md.data.values.left_boundry = 2200
          boss_npc_md.data.values.right_boundry = 2400
        }
        if (boss_npc_md.glass.health == 6){
          boss_npc_md.data.values.left_boundry = 2030
          boss_npc_md.data.values.right_boundry = 2230
          boss_npc_md.data.values.upper_boundry = 2850
        }
        if (boss_npc_md.glass.health == 5){
          boss_npc_md.data.values.left_boundry = 2970
          boss_npc_md.data.values.right_boundry = 3170
        }
        if (boss_npc_md.glass.health == 4){
          boss_npc_md.data.values.left_boundry = 2200
          boss_npc_md.data.values.right_boundry = 2380
          boss_npc_md.data.values.upper_boundry = 2750
          boss_npc_md.data.values.lower_boundry = 2840
          this.scene.rock_x_spawn = 2060
        }
        // if (boss_npc_md.glass.health == 5){
        //   if (!this.scene.vl63_played){
        //     this.scene.time.delayedCall(500, function() {
        //       this.scene.vl63.play()
        //       }, [], this)
        //   this.scene.vl63_played = true}
        //   boss_npc_md.data.values.left_boundry = 2800
        //   boss_npc_md.data.values.right_boundry = 2980
        //   this.scene.rock_x_spawn = 3130
        // }
        if (boss_npc_md.glass.health == 3){
          if (!this.scene.vl63_played){
              this.scene.time.delayedCall(500, function() {
                this.scene.vl63.play()
                }, [], this)
                this.scene.vl63_played = true}
          boss_npc_md.data.values.left_boundry = 2200
          boss_npc_md.data.values.right_boundry = 2980
          this.scene.rock_x_spawn = 3130
          // boss_npc_md.data.values.lower_boundry = 2950
        }
        // if (boss_npc_md.glass.health == 3){
        //   this.scene.rock_x_spawn = 2060
        // }
        if (boss_npc_md.glass.health == 2){
          if (!this.scene.vl69_played){
            this.scene.time.delayedCall(500, function() {
              this.scene.vl69.play()
              }, [], this)
          this.scene.vl69_played = true}
          boss_npc_md.data.values.left_boundry = 2130
          boss_npc_md.data.values.right_boundry = 2330
          boss_npc_md.data.values.upper_boundry = 2850
          boss_npc_md.data.values.lower_boundry = 3000
          this.scene.rock_x_spawn = 3130
        }
        if (boss_npc_md.glass.health == 1){
          boss_npc_md.data.values.left_boundry = 2900
          boss_npc_md.data.values.right_boundry = 3100
          this.scene.rock_x_spawn = 2070
        }

        if (boss_npc_md.glass.time_at_hit && this.scene.manual_time > boss_npc_md.glass.time_at_hit + 1200){
          boss_npc_md.glass.hit = false}

        boss_npc_md.glass.setPosition(boss_npc_md.x-11, boss_npc_md.y-80)

        if (boss_npc_md.state.includes('hover') || boss_npc_md.state.includes('enter')){
          boss_npc_md.eye1.setAngularVelocity(200)
          boss_npc_md.eye2.setAngularVelocity(-190)
          if (boss_npc_md.x > player.player.x){
            if (boss_npc_md.glass.health <= 0){
              boss_npc_md.state = 'defeated_left'}
            boss_npc_md.eye1.setPosition(boss_npc_md.x-14, boss_npc_md.y-75)
            boss_npc_md.eye2.setPosition(boss_npc_md.x+8, boss_npc_md.y-75)
            if (boss_npc_md.glass.hit == false){
              boss_npc_md.setFrame(0)
              boss_npc_md.eye1.setFrame(0)
              boss_npc_md.eye2.setFrame(0)}
            else {
              boss_npc_md.setFrame(2)
              boss_npc_md.eye1.setFrame(1)
              boss_npc_md.eye2.setFrame(1)}
            }
          else {
            if (boss_npc_md.glass.health <= 0){
              boss_npc_md.state = 'defeated_right'}
            boss_npc_md.eye1.setPosition(boss_npc_md.x-8, boss_npc_md.y-75)
            boss_npc_md.eye2.setPosition(boss_npc_md.x+14, boss_npc_md.y-75)
            if (boss_npc_md.glass.hit == false){
              boss_npc_md.setFrame(1)
              boss_npc_md.eye1.setFrame(0)
              boss_npc_md.eye2.setFrame(0)}
            else {
              boss_npc_md.setFrame(3)
              boss_npc_md.eye1.setFrame(1)
              boss_npc_md.eye2.setFrame(1)}
          }}

        boss_npc_md.torch.setPosition(boss_npc_md.x, boss_npc_md.y+210)
        boss_npc_md.drill1.setPosition(boss_npc_md.x+100, boss_npc_md.y+50)
        boss_npc_md.drill2.setPosition(boss_npc_md.x-100, boss_npc_md.y+50)
        boss_npc_md.torch.anims.play('burn', true)
        boss_npc_md.drill1.anims.play('drill_right', true)
        boss_npc_md.drill2.anims.play('drill_left', true)

        if (boss_npc_md.data.values.hover == 'up' && boss_npc_md.y < boss_npc_md.data.values.upper_boundry){
          boss_npc_md.data.values.hover = 'down'}
        if (boss_npc_md.data.values.hover == 'down' && boss_npc_md.y > boss_npc_md.data.values.lower_boundry){
          boss_npc_md.data.values.hover = 'up'}

        if (boss_npc_md.state.includes('hover')){
          if (boss_npc_md.data.values.hover == 'up'){
            boss_npc_md.setVelocityY(-120);}
          else if (boss_npc_md.data.values.hover == 'down'){
            boss_npc_md.setVelocityY(120);}}

        if (boss_npc_md.state == 'enter'){
            boss_npc_md.setVelocityX(0);
            boss_npc_md.setVelocityY(100);
            if (boss_npc_md.y >= boss_npc_md.data.values.y_spawn+420){
                boss_npc_md.state = 'hover_left'}}

        else if (boss_npc_md.state == 'hover_left'){
          boss_npc_md.setVelocityX(-(boss_npc_md.data.values.speed));
          if (boss_npc_md.x < boss_npc_md.data.values.left_boundry){
            boss_npc_md.state = 'hover_right'
          }}

        else if (boss_npc_md.state == 'hover_right'){
          boss_npc_md.setVelocityX(boss_npc_md.data.values.speed);
          if (boss_npc_md.x > boss_npc_md.data.values.right_boundry){
            boss_npc_md.state = 'hover_left'
          }}

        else if (boss_npc_md.state == 'defeated_left'){
          boss_npc_md.eye1.setAngularVelocity(0)
          boss_npc_md.eye2.setAngularVelocity(0)
          boss_npc_md.setFrame(2);
          boss_npc_md.setVelocityX(0);
          boss_npc_md.setVelocityY(0);
          }

        else if (boss_npc_md.state == 'defeated_right'){
          boss_npc_md.eye1.setAngularVelocity(0)
          boss_npc_md.eye2.setAngularVelocity(0)
          boss_npc_md.setFrame(3);
          boss_npc_md.setVelocityX(0);
          boss_npc_md.setVelocityY(0);
          }

        if (boss_npc_md.state.includes('hover') && this.scene.boss_transparent == false){
          if (this.scene.manual_time > boss_npc_md.data.values.last_throw + boss_npc_md.data.values.throw_wait){
            boss_npc_md.setData('throw_wait', Phaser.Math.Between(2000, 5000))
              var diff = boss_npc_md.x - this.player.player.x
                boss_npc_md.data.values.last_throw = this.scene.manual_time
                var direction = ((diff<0) ? 'right' : 'left')
                boss_npc_md.projectiles.push(new Bomb(this.scene, boss_npc_md, this.player, direction))}
            }

        if (boss_npc_md.glass.health <= 0 && boss_npc_md.data.values.destroyed == false){
          boss_npc_md.glass.life_bar.setFrame(0)
          if (boss_npc_md.data.values.explosion_added == false){
            this.scene.explosion_sprites.addExplosion(this.scene, boss_npc_md.x, boss_npc_md.y)
            boss_npc_md.data.values.explosion_added = true}
          if (this.scene.explosions.children['entries'][0].frame.name == 10){
            if (!boss_npc_md.data.values.explosion_played){
              this.scene.explosion.play()
              boss_npc_md.data.values.explosion_played = true}
            boss_npc_md.data.values.destroyed = true
            this.destroyBossNpcMd(boss_npc_md)
            this.scene.time.delayedCall(2000, function() {
              this.scene.cameras.main.fade(3050, 255, 255, 255);
            }, [], this)
            this.scene.time.delayedCall(5000, function() {
              this.scene.boss_defeated = true
            }, [], this)}
          }



      } // if boss_npc_md active
    }, this); // looping through all boss_npc_mds
} // update

addBossNpcMd(scene, x, y, level_boss) {
		var boss_npc_md = scene.boss_npc_mds.create(x, y, 'boss_npc_md');
		boss_npc_md.setActive(true).setVisible(true)
		boss_npc_md.setData({'health': 8, //8
								 'time_at_collision':null,
								 'knock_back_time': 150,
								 'bs_thrown': 0,
                 'x_spawn': x,
                 'y_spawn': y,
								 'layer_collisions': [],
						 		 'last_attack': 0,
                 'attack_wait': Phaser.Math.Between(10000, 12000),
                 'boss': level_boss,
                 'origin_x': x,
                 'hit': false,
                 'last_switch_time': scene.manual_time,
                 'speed': 150,
                 'hover': 'up',
                 'left_boundry': 2750, // x - 800
                 'right_boundry': 2950, // x
                 'upper_boundry': 2850, // 2750
                 'lower_boundry': 2950, //2950
                 'destroyed': false,
                 'explosion_added': false,
                 'end_scene': false,
                 'last_throw': 0,
                 'throw_wait': Phaser.Math.Between(1500, 3500),
                 'explosion_played': false
                });

    boss_npc_md.body.immovable = true;
    boss_npc_md.setDepth(740);
    boss_npc_md.state = 'enter'
		scene.boss_npc_mds.add(boss_npc_md)
    boss_npc_md.setOffset(45, 85)
    boss_npc_md.setSize(70, 75, false)
    boss_npc_md.setScale(1.75)
    boss_npc_md.body.setAllowGravity(false);
    boss_npc_md.projectiles = []
    scene.enemies_added += 1;

    boss_npc_md.glass = scene.physics.add.sprite(x, y, 'wheel');
    boss_npc_md.glass.setVisible(false)
    boss_npc_md.glass.setScale(1.5);
    boss_npc_md.glass.setCircle(54,-30,-10)
    boss_npc_md.glass.body.immovable = true;
    boss_npc_md.glass.body.setAllowGravity(false);
    boss_npc_md.glass.setDepth(750);
    boss_npc_md.glass.health = 8; //
    boss_npc_md.glass.max_health = 8; //
    boss_npc_md.glass.hit = false
    // boss_npc_md.body.checkCollision.up = false  // how to set collisions from certain directions

    boss_npc_md.eye1 = scene.physics.add.sprite(x, y, 'eye');
    boss_npc_md.eye1.body.setAllowGravity(false);
    boss_npc_md.eye1.setScale(0.75)
    boss_npc_md.eye1.setDepth(760);

    boss_npc_md.eye2 = scene.physics.add.sprite(x, y, 'eye');
    boss_npc_md.eye2.body.setAllowGravity(false);
    boss_npc_md.eye2.setScale(0.75)
    boss_npc_md.eye2.setDepth(760);

    boss_npc_md.torch = scene.physics.add.sprite(x, y, 'torch');
    boss_npc_md.torch.setScale(5);
    boss_npc_md.torch.setSize(12,10, false)
    boss_npc_md.torch.setOffset(10,0)
    boss_npc_md.torch.body.setAllowGravity(false);
    boss_npc_md.torch.setDepth(730);
    boss_npc_md.torch.health = boss_npc_md.data.values.health

    scene.physics.add.collider(scene.player.player, boss_npc_md.torch,
    function(player, torch){
      this.player.player.data.values.time_at_collision = this.scene.manual_time
      this.player.player.data.values.hit_from = torch.x
      scene.player_hit.play()
      this.player.damagePlayer(this.player, scene, 5) //6
      torch.body.immovable = true;
    }, function(player, torch){
          return (
            torch.health > 0 &&
            this.player.player.data.values.post_hit_invincibility == false
            && scene.boss_transparent == false)
      }, this);

    boss_npc_md.drill1 = scene.physics.add.sprite(x, y, 'drill');
    boss_npc_md.drill1.setScale(2.5);
    boss_npc_md.drill1.body.setAllowGravity(false);
    boss_npc_md.drill1.setDepth(780);
    boss_npc_md.drill1.setOffset(0,6);
    boss_npc_md.drill1.setSize(30,20,false)
    boss_npc_md.drill1.health = boss_npc_md.data.values.health

    scene.physics.add.collider(scene.player.player, boss_npc_md.drill1,
    function(player, drill1){
      this.player.player.data.values.time_at_collision = this.scene.manual_time
      this.player.player.data.values.hit_from = drill1.x
      scene.player_hit.play()
      this.player.damagePlayer(this.player, scene, 5) //6
      drill1.body.immovable = true;
      }, function(player, drill1){
          return (
            drill1.health > 0 &&
            this.player.player.data.values.post_hit_invincibility == false
            && scene.boss_transparent == false)
      }, this);

    boss_npc_md.drill2 = scene.physics.add.sprite(x, y, 'drill');
    boss_npc_md.drill2.setScale(2.5);
    boss_npc_md.drill2.body.setAllowGravity(false);
    boss_npc_md.drill2.setDepth(780);
    boss_npc_md.drill2.setOffset(8,6);
    boss_npc_md.drill2.setSize(30,20,false)
    boss_npc_md.drill2.health = boss_npc_md.data.values.health

    scene.physics.add.collider(scene.player.player, boss_npc_md.drill2,
    function(player, drill2){
      this.player.player.data.values.time_at_collision = this.scene.manual_time
      this.player.player.data.values.hit_from = drill2.x
      scene.player_hit.play()
      this.player.damagePlayer(this.player, scene, 5) //6
      drill2.body.immovable = true;
      }, function(player, drill2){
          return (
            drill2.health > 0 &&
            this.player.player.data.values.post_hit_invincibility == false
            && scene.boss_transparent == false)
      }, this);

      boss_npc_md.glass.life_bar = scene.physics.add.sprite(1060, 30, 'boss_life_bar').setDepth(1000);
      boss_npc_md.glass.life_bar.body.setAllowGravity(false);
      boss_npc_md.glass.life_bar.setScrollFactor(0,0)
      boss_npc_md.glass.life_bar.setFrame(18)
}

destroyBossNpcMd(boss_npc_md){
    boss_npc_md.destroy()
    boss_npc_md.setVisible(false)
    boss_npc_md.eye1.destroy()
    boss_npc_md.eye2.destroy()
    boss_npc_md.drill1.destroy()
    boss_npc_md.drill2.destroy()
    boss_npc_md.torch.destroy()
    }

}
