export default class TitleScreen extends Phaser.Scene {

constructor() {
  	super({key: "title_screen"});
}

create (){
  this.background = this.physics.add.sprite(600, 300, "title_screen_background");
  // this.background.setOrigin(0,0)
  this.background.body.setAllowGravity(false)
  this.background.setAngularVelocity(20)
  // this.background.displayOriginY = 200

  this.title_text = this.add.image(600, 300 - 170, 'title')
  this.title_text.setScale(1.6)

  this.logo = this.add.image(600, 300, 'logo2')
  this.logo.setScale(0.26)

  this.play_game = this.physics.add.sprite(600, 300+130, 'play_game')
  this.play_game.setScale(0.6)
  this.play_game.setInteractive().on('pointerup', () => this.startCutscene())
  this.play_game.setInteractive().on('pointerover', () => this.play_game.setFrame(1))
  this.play_game.setInteractive().on('pointerout', () => this.play_game.setFrame(0))
  this.play_game.body.setAllowGravity(false)

  this.options = this.physics.add.sprite(600, 300+230, 'options')
  this.options.setScale(0.6)
  this.options.setInteractive().on('pointerup', () => this.startOptions())
  this.options.setInteractive().on('pointerover', () => this.options.setFrame(1))
  this.options.setInteractive().on('pointerout', () => this.options.setFrame(0))
  this.options.body.setAllowGravity(false)

  this.player = this.physics.add.sprite(225,500, 'jeremy').setScale(5);
  this.player.body.setAllowGravity(false)
  this.player.setFrame(0)

  this.npc = this.physics.add.sprite(975,500, 'npc').setScale(5);
  this.npc.body.setAllowGravity(false)
  this.npc.setFrame(4)

  this.add.text(1070, 575, "Ver. 1.1.0", {fill: '#fff'}).setFontSize(20).setDepth(1000)

  this.music = this.sound.add('title_screen_soundtrack');
  var music_config = {
    mute: false,
    volume: 0.3,
    rate: 1,
    detune: 0,
    seek: 0,
    loop: true,
    delay: 0
  }
  this.music.play(music_config)
  this.key_i = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.I);
  this.key_n = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.N);
  this.key_l = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.L);
  this.key_v = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.V);
  this.key_s = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
  this.key_f = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.F);
  this.key_b = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.B);
  this.key_g = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.G);

}

startCutscene(){
  if (this.key_i.isDown && this.key_n.isDown){
    this.scene.start('cutscene2')
  }
  else if (this.key_i.isDown && this.key_l.isDown){
    this.scene.start('cutscene3')
  }
  else if (this.key_n.isDown && this.key_v.isDown){
    this.scene.start('cutscene4')
  }
  else if (this.key_g.isDown && this.key_s.isDown){
    this.scene.start('cutscene5')
  }
  else if (this.key_f.isDown && this.key_b.isDown){
    this.scene.start('cutscene6p1')
  }
  else {
    this.scene.start('cutscene1')
  }
  this.music.stop()
}

startOptions(){
  this.scene.start('options')
  this.music.stop()
}

} // class
