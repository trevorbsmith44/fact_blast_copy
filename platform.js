export default class Platform {

constructor(scene, player){
    scene.platforms = scene.physics.add.group();
    this.scene = scene
    this.player = player
    this.scene.platform_array = []
    this.scene.platform_count = 0

    // scene.fact_platform = scene.physics.add.collider(scene.platforms, scene.facts, function(fact, platform){
    //   }, null, this);
}

update(scene){
      this.scene.platform_array.forEach(function(platform) {
          if (platform.active){

              if (platform.state == 'forward') {
                  if (platform.data.values.direction == 'horizontal'){
                      platform.body.setVelocityX(platform.data.values.x_velocity)
                      if ((platform.data.values.speed > 0 && platform.x > (platform.data.values.x_spawn + platform.data.values.distance)) ||
                          (platform.data.values.speed < 0 && platform.x < (platform.data.values.x_spawn - platform.data.values.distance))){
                          platform.state = 'backward'}}
                  else {
                      platform.body.setVelocityY(platform.data.values.y_velocity)
                      if ((platform.data.values.speed > 0 && platform.y >= (platform.data.values.y_spawn + platform.data.values.distance)) ||
                          (platform.data.values.speed < 0 && platform.y < (platform.data.values.y_spawn - platform.data.values.distance))){
                          platform.state = 'backward'}}}
              else if (platform.state == 'backward') {
                  if (platform.data.values.direction == 'horizontal'){
                      platform.body.setVelocityX(-(platform.data.values.x_velocity))
                      if ((platform.data.values.speed < 0 && platform.x > (platform.data.values.x_spawn)) ||
                          (platform.data.values.speed > 0 && platform.x < (platform.data.values.x_spawn))){
                          platform.state = 'forward'}}
                  else {
                      platform.body.setVelocityY(-(platform.data.values.y_velocity))
                      if ((platform.data.values.speed < 0 && platform.y > (platform.data.values.y_spawn)) ||
                          (platform.data.values.speed > 0 && platform.y <= (platform.data.values.y_spawn))){
                          platform.state = 'forward'}}}
              if (platform.x == platform.data.values.x_spawn && platform.y == platform.data.values.y_spawn
                  && platform.scene.time.now > platform.data.values.time_at_last_lock + 100){
                  platform.data.values.locked = true}

        }  // if platform active
      }) // looping through all platforms
} // update

addPlatform(scene, x, y, direction, speed, distance, x_scale, y_scale, frame) {
    var platform = scene.add.tileSprite(x, y, 32 * x_scale, 32 * y_scale, 'platform');
    scene.platform_array.push(platform)
    this.scene.physics.add.existing(platform, false);
    scene.physics.add.collider(platform, this.player.player, function(platform, player){
        platform.body.immovable = true
        player.body.immovable = false
    });


    var x_velocity
    var y_velocity
    if (direction == 'horizontal'){
        x_velocity = speed
        y_velocity = 0}
    else {
        x_velocity = 0
        y_velocity = speed}

		platform.setActive(true).setVisible(true);
		platform.setScale(2);
    platform.setDepth(10);
    platform.setFrame(frame)
    platform.state = 'forward'
    platform.body.setAllowGravity(false)
    platform.body.immovable = true

    platform.setData({'id': this.scene.platform_count,
                      'x_spawn': x,
                      'y_spawn': y,
                      'direction': direction,
                      'speed': speed,
                      'distance': distance,
                      'x_velocity': x_velocity,
                      'y_velocity': y_velocity,
                      'locked': true,
                      'obj': this,
                      'time_at_last_lock': this.scene.time.now,
                    });

    platform.body.setVelocityX(platform.data.values.x_velocity);
    platform.body.setVelocityY(platform.data.values.y_velocity);
    this.scene.platform_count += 1
}

}
