import Player from "./player.js";
import Fact from  "./fact.js";
import Npc from  "./npc.js";
import Bs from  "./bs.js";
import Bird from  "./bird.js";
import Censor from "./censor.js"
import Tomato from  "./tomato.js";

export default class Preload extends Phaser.Scene {
constructor() {
  	super("preload");
}

preload (){

  // this.game.scale.pageAlignHorizontally = true;
  // this.game.scale.pageAlignVertically = true;
  // this.game.scale.refresh();

    this.load.image('title', 'test_title.png');
    this.load.image('logo2', 'logo2.jpg');
  	this.load.image('bs', 'bs.png');
		this.load.image('bs2', 'bs2.png');
		this.load.image('bs3', 'bs3.png');
		this.load.image('bs4', 'bs4.png');
		this.load.image('bs10', 'bs10.png');
    this.load.image('bs12', 'bs12.png');
    this.load.image('bs14', 'bs14.png');
    this.load.image('bs16', 'bs16.png');
    this.load.image('bs17', 'bs17.png');
    this.load.image('bs18', 'bs18.png');
    this.load.image('bs19', 'bs19.png');
    this.load.image('bs20', 'bs20.png');
    this.load.image('bs21', 'bs21.png');
    this.load.image('bs22', 'bs22.png');
    this.load.image('bs23', 'bs23.png');
    this.load.image('bs24', 'bs24.png');
		this.load.image('censored', 'censored.png');
    this.load.image('math1', 'math1.png');
    this.load.image('math2', 'math2.png');
    this.load.image('math3', 'math3.png');
    this.load.image('math4', 'math4.png');
    this.load.image('math5', 'math5.png');
    this.load.image('math6', 'math6.png');
    this.load.image('math7', 'math7.png');
    this.load.image('math8', 'math8.png');
    this.load.image('math9', 'math9.png');
    this.load.image('math10', 'math10.png');
    this.load.image('math11', 'math11.png');
    this.load.image('math12', 'math12.png');
    this.load.image('math13', 'math13.png');
    this.load.image('math14', 'math14.png');
    this.load.image('math15', 'math15.png');
    this.load.image('math16', 'math16.png');
    this.load.image('math17', 'math17.png');
    this.load.image('math18', 'math18.png');
    this.load.image('math19', 'math19.png');
    this.load.image('math20', 'math20.png');
    this.load.image('math21', 'math21.png');
		this.load.image('fact', 'fact2.png');
    this.load.image('phone', 'phone.png');
    this.load.image('phone2', 'phone2.png');
    this.load.image('parking_lot', 'parking_lot.png');
    this.load.image('fence', 'fence.png');
    this.load.image('starburst', 'starburst.png');
    this.load.image('thought', 'thought.png');
    this.load.image('red_car', 'red_car.png');
    this.load.image('wind', 'wind.png');
    this.load.image('buildings', 'cutscene_buildings.png');
    this.load.image('c1i3_bg', 'level3_bg_cutscene.png');
    this.load.image('limo', 'limo.png');
    this.load.image('welcome_to_chicago', 'welcome_to_chicago.png');
    this.load.image('road', 'road.png');
    this.load.image('c5i1_bg', 'c5i1_bg.png');
    this.load.image('c5i2_bg', 'c5i2_bg.png');
    this.load.image('c5i2_bg2', 'c5i2_bg2.png');
    this.load.image('c5i3_bg', 'c5i3_bg.png');
    this.load.image('hill', 'hill.png');
    this.load.image('c6_bg', 'c6_bg.png');

    this.load.image('c1i0', 'c1i0.png');
    this.load.image('c1i1', 'c1i1.png');
    this.load.image('c1i2', 'c1i2.png');
    this.load.image('c1i3', 'c1i3.png');

    this.load.image('c2i1', 'c2i1.png');
    this.load.image('c2i2', 'c2i2.png');
    this.load.image('c2i3', 'c2i3.png');
    this.load.image('c2i4', 'c2i4.png');

    this.load.image('c3i1', 'c3i1.png');
    this.load.image('c3i2', 'c3i2.png');
    this.load.image('c3i3', 'c3i3.png');
    this.load.image('c3i4', 'c3i4.png');

    this.load.image('c4i1', 'c4i1.png');
    this.load.image('c4i2', 'c4i2.png');
    this.load.image('c4i3', 'c4i3.png');
    this.load.image('c4i4', 'c4i4.png');

    this.load.image('c5i1', 'c5i1.png');
    this.load.image('c5i2', 'c5i2.png');
    this.load.image('c5i3', 'c5i3.png');
    this.load.image('c5i4', 'c5i4.png');

    this.load.image('c6i1', 'c6i1.png');

    this.load.spritesheet('jeremy_straight_face', 'jeremy_straight_face.png', { frameWidth: 66, frameHeight: 96 });
    this.load.spritesheet('title_screen_background', 'blast.png', { frameWidth: 1400, frameHeight: 1400 });
    this.load.spritesheet('blast_cover', 'blast_cover.png', { frameWidth: 1400, frameHeight: 1400 });
    this.load.spritesheet('play_game', 'play_game.png', { frameWidth: 547, frameHeight: 135 });
    this.load.spritesheet('options', 'options.png', { frameWidth: 450, frameHeight: 141 });
    this.load.spritesheet('main_menu', 'main_menu.png', { frameWidth: 540, frameHeight: 130 });
    this.load.spritesheet('credits', 'credits.png', { frameWidth: 450, frameHeight: 150 });
    this.load.spritesheet('mute', 'mute.png', { frameWidth: 480, frameHeight: 130 });
    this.load.spritesheet('van', 'van.png', { frameWidth: 130, frameHeight: 50 });

    this.load.spritesheet('jeremy', 'jeremy.png', { frameWidth: 66, frameHeight: 96 });
		this.load.spritesheet('npc', 'npc2.png', { frameWidth: 64, frameHeight: 96 });
		this.load.spritesheet('bird', 'bird.png', { frameWidth: 100, frameHeight: 72 });
		this.load.spritesheet('tomato', 'tomato.png', { frameWidth: 180, frameHeight: 130 });
    this.load.spritesheet('bot', 'bot.png', { frameWidth: 280, frameHeight: 200 });
    this.load.spritesheet('buzz_saw', 'buzz_saw.png', { frameWidth: 63, frameHeight: 63 });
    this.load.spritesheet('wheel', 'wheel.png', { frameWidth: 33, frameHeight: 33 });
    this.load.spritesheet('small_bot', 'small_bot.png', { frameWidth: 128, frameHeight: 112 });
    this.load.spritesheet('demonetization', 'demonetization.png', { frameWidth: 128, frameHeight: 128 })
    this.load.spritesheet('torch', 'torch.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('boss_npc', 'npc_boss.png', { frameWidth: 256, frameHeight: 182 });
    this.load.spritesheet('milkshake', 'milkshake.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('boss_tomato', 'boss_tomato.png', { frameWidth: 180, frameHeight: 130 });
    this.load.spritesheet('boss_bot', 'boss_bot.png', { frameWidth: 280, frameHeight: 220 });
    this.load.spritesheet('explosion', 'explosion.png', { frameWidth: 280, frameHeight: 220 });
    this.load.spritesheet('trash_can', 'trash_can.png', { frameWidth: 34, frameHeight: 33 });
    this.load.spritesheet('regular_tomato', 'regular_tomato.png', { frameWidth: 180, frameHeight: 130 });
    this.load.spritesheet('rock', 'rock.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('dark_rock', 'dark_rock.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('egg', 'egg.png', { frameWidth: 180, frameHeight: 130 });
    this.load.spritesheet('spikey_ball', 'spikey_ball.png', { frameWidth: 180, frameHeight: 130 });
    this.load.spritesheet('snow', 'snow.png', { frameWidth: 4, frameHeight: 4 });
    this.load.spritesheet('confetti', 'confetti.png', { frameWidth: 4, frameHeight: 4 });
    this.load.spritesheet('snowflake', 'snowflake.png', { frameWidth: 35, frameHeight: 37 });
    this.load.spritesheet('boss_bird', 'boss_bird.png', { frameWidth: 192, frameHeight: 192 });
    this.load.spritesheet('machine', 'machine.png', { frameWidth: 69, frameHeight: 130 });
    this.load.spritesheet('gear', 'gear.png', { frameWidth: 34, frameHeight: 34 });
    this.load.spritesheet('platform', 'platform.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('boss_npc_md', 'npc_md1.png', { frameWidth: 160, frameHeight: 160 });
    this.load.spritesheet('eye', 'eye.png', { frameWidth: 34, frameHeight: 34 });
    this.load.spritesheet('drill', 'drill.png', { frameWidth: 38, frameHeight: 32 });
    this.load.spritesheet('canceled', 'canceled.png', { frameWidth: 759, frameHeight: 243 });
    this.load.spritesheet('check_flag', 'check_flag.png', { frameWidth: 34, frameHeight: 64 });
    this.load.spritesheet('keyboard', 'keyboard.png', { frameWidth: 64, frameHeight: 64 });
    this.load.spritesheet('controller', 'controller.png', { frameWidth: 200, frameHeight: 200 });
    this.load.spritesheet('enter_key', 'enter_key.png', { frameWidth: 128, frameHeight: 64 });
    this.load.spritesheet('blank_screen', 'blank_screen.png', { frameWidth: 1200, frameHeight: 600 });
    this.load.image('black', 'black.png');

    this.load.spritesheet('life_bar', 'life_bar.png', { frameWidth: 280, frameHeight: 50 });
    this.load.spritesheet('boss_life_bar', 'boss_life_bar.png', { frameWidth: 280, frameHeight: 50 });
    this.load.spritesheet('juice', 'apple_juice.png', { frameWidth: 26, frameHeight: 32 });
    this.load.spritesheet('balloon_blue', 'balloon_blue.png', { frameWidth: 32, frameHeight: 66 });
    this.load.spritesheet('balloon_red', 'balloon_red.png', { frameWidth: 32, frameHeight: 66 });
    this.load.spritesheet('balloon_green', 'balloon_green.png', { frameWidth: 32, frameHeight: 66 });
    this.load.spritesheet('balloon_yellow', 'balloon_yellow.png', { frameWidth: 32, frameHeight: 66 });
    this.load.spritesheet('balloon_purple', 'balloon_purple.png', { frameWidth: 32, frameHeight: 66 });
    this.load.spritesheet('balloon_pink', 'balloon_pink.png', { frameWidth: 32, frameHeight: 66 });
    this.load.spritesheet('flag', 'flag.png', { frameWidth: 34, frameHeight: 34 });
    this.load.spritesheet('car_wheel', 'car_wheel.png', { frameWidth: 33, frameHeight: 33 });
    this.load.spritesheet('liquid_green', 'liquid_green.png', { frameWidth: 34, frameHeight: 66 });
    this.load.spritesheet('liquid_blue', 'liquid_blue.png', { frameWidth: 34, frameHeight: 66 });
    this.load.spritesheet('liquid_purple', 'liquid_purple.png', { frameWidth: 34, frameHeight: 66 });
    this.load.spritesheet('water', 'water.png', { frameWidth: 66, frameHeight: 66 });
    this.load.spritesheet('lava', 'lava.png', { frameWidth: 34, frameHeight: 66 });

    this.load.tilemapTiledJSON("level1_map", "level1.json");
    this.load.image("level1_tileset", "level1_tileset.png");
    this.load.image("level1_bg1", "level1_bg1.png");
    this.load.image("level1_bg2", "level1_bg2.png");
    this.load.image("level1_sky", "level1_bg_sky.png");

    this.load.tilemapTiledJSON("level2_map", "level2.json");
    this.load.image("level2_tileset", "level2_tileset.png");
    this.load.image("level2_bg2", "level2_bg2.png");
    this.load.image("level2_bg1", "level2_bg1.png");
    this.load.image("forest_c", "bg_forest_c.png");
    this.load.image("forest_b", "bg_forest_b.png");
    this.load.image("level2_sky", "awesome_sunset_sky.png");

    this.load.tilemapTiledJSON("level3_map", "level3.json");
    this.load.image("level3_sky", "level3_bg_sky3.png"); //3
    this.load.image("night_sky", "level3_bg_sky6.png"); //3
    this.load.image("level3_tileset", "level3_tileset.png");
    this.load.image("level3_bg", "level3_bg4.png");
    this.load.image("level3_bg2", "level3_bg2.png");

    this.load.tilemapTiledJSON("level4_map", "level4.json");
    this.load.image("level4_sky", "level3_bg_sky2.png"); //4
    this.load.image("level4_tileset", "level4_tileset.png");
    this.load.image("level4_bg1", "level4_bg1.png");
    this.load.image("level4_bg2", "level4_bg2.png");
    this.load.image("level4_bg3", "level4_bg3.png");
    this.load.image("level4_bg4", "level4_bg4.png");

    this.load.tilemapTiledJSON("level5_map", "level5.json");
    this.load.image("level5_tileset", "level5_tileset.png");
    this.load.image("level5_bg", "level5_bg.png");

    this.load.tilemapTiledJSON("level6_map", "level6.json");
    this.load.image("level6_tileset", "level6_tileset.png");
    this.load.image("level6_bg1", "level6_bg1.png");
    this.load.image("level6_bg2", "level6_bg2.png");
    this.load.image("level6_bg3", "level6_bg3.png");

    this.load.image("options_bg", "options_bg.png");

    this.load.audio('drum', "one_kick_sound.mp3")
    this.load.audio('whoosh1', "whoosh1.mp3")
    this.load.audio('whoosh2', "whoosh2.mp3")
    this.load.audio('whoosh3', "whoosh3.mp3")
    this.load.audio('whack1', "whack1.mp3")
    this.load.audio('whack2', "whack2.mp3")
    this.load.audio('whack3', "whack3.mp3")
    this.load.audio('whack4', "whack4.mp3")
    this.load.audio('splat', "splat.mp3")
    this.load.audio('clang', "clang.mp3")
    this.load.audio('clang2', "clang2.mp3")
    this.load.audio('soft_hit', "soft_hit.mp3")
    this.load.audio('pop', "pop.mp3")
    this.load.audio('juice_sound', "juice.mp3")
    this.load.audio('player_hit', "player_hit.mp3")
    this.load.audio('crash', "crash.mp3")
    this.load.audio('bird_sound', "bird.mp3")
    this.load.audio('bird_sound2', "bird2.mp3")
    this.load.audio('bird_swoop', "bird_swoop.mp3")
    this.load.audio('bird_egg', "bird_egg.mp3")
    this.load.audio('bird_defeated', "bird_defeated.mp3")
    this.load.audio('jump_sound', "jump.mp3")
    this.load.audio('bonk', "bonk.mp3")
    this.load.audio('sizzle', "sizzle.mp3")
    this.load.audio('explosion', "explosion.mp3")
    this.load.audio('rock_sound', "rock_sound.mp3")
    this.load.audio('nooo_sound', "no.mp3")
    this.load.audio('boss_tomato_throw_sound', "boss_tomato_throw_sound.mp3")
    this.load.audio('boss_bot_throw_sound', "boss_bot_throw_sound.mp3")

    this.load.audio('vl1', ["vl1.m4a", "vl1.mp3"])
    this.load.audio('vl2', "vl2.mp3")
    this.load.audio('vl11', "vl11.mp3")
    this.load.audio('vl14', "vl14.mp3")
    this.load.audio('vl15', "vl15.mp3")
    this.load.audio('vl18', "vl18.mp3")
    this.load.audio('vl20', "vl20.mp3")
    this.load.audio('vl21', "vl21.m4a")
    this.load.audio('vl22', "vl22.mp3")
    this.load.audio('vl23', "vl23.mp3")
    this.load.audio('vl24', "vl24.mp3")
    this.load.audio('vl28', "vl28.mp3")
    this.load.audio('vl29', "vl29.mp3")
    this.load.audio('vl30', "vl30.mp3")
    this.load.audio('vl31', "vl31.mp3")
    this.load.audio('vl37', "vl37.mp3")
    this.load.audio('vl42', "vl42.mp3")
    this.load.audio('vl46', "vl46.mp3")
    this.load.audio('vl48', "vl48.mp3")
    this.load.audio('vl49', "vl49.mp3")
    this.load.audio('vl50', "vl50.mp3")
    this.load.audio('vl51', "vl51.mp3")
    this.load.audio('vl52', "vl52.mp3")
    this.load.audio('vl53', "vl53.mp3")
    this.load.audio('vl54', "vl54.mp3")
    this.load.audio('vl57', "vl57.mp3")
    this.load.audio('vl58', "vl58.mp3")
    this.load.audio('vl59', "vl59.mp3")
    this.load.audio('vl60', "vl60.mp3")
    this.load.audio('vl61', "vl61.mp3")
    this.load.audio('vl62', "vl62.mp3")
    this.load.audio('vl63', "vl63.mp3")
    this.load.audio('vl64', "vl64.mp3")
    this.load.audio('vl65', "vl65.mp3")
    this.load.audio('vl66', "vl66.mp3")
    this.load.audio('vl67', "vl67.mp3")
    this.load.audio('vl68', "vl68.mp3")
    this.load.audio('vl69', "vl69.mp3")
    this.load.audio('vl70', "vl70.mp3")
    this.load.audio('vl71', "vl71.mp3")

    this.load.audio('title_screen_soundtrack', "title_screen.mp3")
    this.load.audio('cutscene_soundtrack', "cutscene.mp3")
    this.load.audio('cutscene5_soundtrack', "cutscene5.mp3")
    this.load.audio('options_soundtrack', "options.mp3")
    this.load.audio('level_1_soundtrack', "level1.mp3")
    this.load.audio('level_2_soundtrack', "level2.mp3")
    this.load.audio('level_3_soundtrack', "level3.mp3")
    this.load.audio('level_4_soundtrack', "level4.mp3")
    this.load.audio('level_5_soundtrack', "level5.mp3")
    this.load.audio('level_6_soundtrack', "level6.mp3")
    this.load.audio('end_credits_soundtrack', "end_credits.mp3")

}

create (){
    // PLAYER ANIMATIONS
    this.anims.create({
        key: 'run_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 10, frames: [10,11,12,13] }),
        frameRate: 10,
        repeat: -1 // tells the animation to loop
        });
    this.anims.create({
        key: 'jump_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 17, end: 17 }),
        });
    this.anims.create({
        key: 'stand_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 3, frames: [3,30,31,30,3,3] }),
        frameRate: 4,
        repeat: -1
        });
    this.anims.create({
        key: 'stand_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 0, frames: [0,28,29,28,0,0] }),
        frameRate: 4,
        repeat: -1
        });
    this.anims.create({
        key: 'run_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 6, frames: [6,7,8,9] }),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'jump_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 14, end: 14 }),
        });
    this.anims.create({
        key: 'jump_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 15, frames: [15,16,16,16,14] }),
        frameRate: 10,
        });
    this.anims.create({
        key: 'jump_left_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 18, frames: [18,19,19,19,17] }),
        frameRate: 10,
        });
    this.anims.create({
        key: 'stand_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 1, frames: [1,2,2,2,0] }),
        frameRate: 10,
        repeat: 0,
        });
    this.anims.create({
        key: 'stand_left_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 4, frames: [4,5,5,5,3] }),
        frameRate: 10,
        repeat: 0
        });
    this.anims.create({
        key: 'hit_to_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 20, end: 20 }),
        });
    this.anims.create({
        key: 'hit_to_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 21, end: 21 }),
        });
    this.anims.create({
        key: 'duck_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 22, end: 22 }),
        });
    this.anims.create({
        key: 'duck_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 25, end: 25 }),
        });
    this.anims.create({
        key: 'duck_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 23, frames: [23,24,24,24,22] }),
        frameRate: 10,
        repeat: 0,
        });
    this.anims.create({
        key: 'duck_left_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 26, frames: [26,27,27,27,25] }),
        frameRate: 10,
        repeat: 0
        });

    // NPC ANIMATIONS
    this.anims.create({
        key: 'npc_walk_left',
        frames: this.anims.generateFrameNumbers('npc', { start: 4, frames: [3,4,5,4] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'npc_walk_right',
        frames: this.anims.generateFrameNumbers('npc', { start: 1, frames: [0,1,2,1] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'npc_stand_right',
        frames: this.anims.generateFrameNumbers('npc', { start: 1, end: 1 }),
        });
    this.anims.create({
        key: 'npc_stand_left',
        frames: this.anims.generateFrameNumbers('npc', { start: 4, end: 4 }),
        });
    this.anims.create({
    		key: 'npc_angry_walk_left',
    		frames: this.anims.generateFrameNumbers('npc', { start: 4, frames: [13,14,15,14] }),
    		frameRate: 6,
    		repeat: -1
    		});
    this.anims.create({
    		key: 'npc_angry_walk_right',
    		frames: this.anims.generateFrameNumbers('npc', { start: 1, frames: [10,11,12,11] }),
    		frameRate: 6,
    		repeat: -1
    		});
    this.anims.create({
    		key: 'npc_angry_stand_right',
    		frames: this.anims.generateFrameNumbers('npc', { start: 11, end: 11 }),
    		});
    this.anims.create({
    		key: 'npc_angry_stand_left',
    		frames: this.anims.generateFrameNumbers('npc', { start: 14, end: 14 }),
    		});
    this.anims.create({
        key: 'npc_hit_to_right',
        frames: this.anims.generateFrameNumbers('npc', { start: 6, end: 6 }),
        });
    this.anims.create({
        key: 'npc_hit_to_left',
        frames: this.anims.generateFrameNumbers('npc', { start: 7, end: 7 }),
        });

    // BIRD ANIMATIONS
    this.anims.create({
        key: 'bird_fly_left',
        frames: this.anims.generateFrameNumbers('bird', { start: 2, frames: [2,3] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'bird_fly_right',
        frames: this.anims.generateFrameNumbers('bird', { start: 1, frames: [0,1] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'bird_hit_to_right',
        frames: this.anims.generateFrameNumbers('bird', { start: 5, end: 5 }),
        });
    this.anims.create({
        key: 'bird_hit_to_left',
        frames: this.anims.generateFrameNumbers('bird', { start: 4, end: 4 }),
        });

    // TOMATO ANIMATIONS
    this.anims.create({
        key: 'red_tomato_roll_left',
        frames: this.anims.generateFrameNumbers('tomato', { start: 0, end: 0 }),
        });
    this.anims.create({
        key: 'red_tomato_jump_left',
        frames: this.anims.generateFrameNumbers('tomato', { start: 1, end: 1 }),
        });
    this.anims.create({
        key: 'red_tomato_hit_to_right',
        frames: this.anims.generateFrameNumbers('tomato', { start: 2, end: 2 }),
        });
    this.anims.create({
        key: 'red_tomato_roll_right',
        frames: this.anims.generateFrameNumbers('tomato', { start: 3, end: 3 }),
        });
    this.anims.create({
        key: 'red_tomato_jump_right',
        frames: this.anims.generateFrameNumbers('tomato', { start: 4, end: 4 }),
        });
    this.anims.create({
        key: 'red_tomato_hit_to_left',
        frames: this.anims.generateFrameNumbers('tomato', { start: 5, end: 5 }),
        });
    this.anims.create({
        key: 'green_tomato_roll_left',
        frames: this.anims.generateFrameNumbers('tomato', { start: 6, end: 6 }),
        });
    this.anims.create({
        key: 'green_tomato_jump_left',
        frames: this.anims.generateFrameNumbers('tomato', { start: 7, end: 7 }),
        });
    this.anims.create({
        key: 'green_tomato_hit_to_right',
        frames: this.anims.generateFrameNumbers('tomato', { start: 8, end: 8 }),
        });
    this.anims.create({
        key: 'green_tomato_roll_right',
        frames: this.anims.generateFrameNumbers('tomato', { start: 9, end: 9 }),
        });
    this.anims.create({
        key: 'green_tomato_jump_right',
        frames: this.anims.generateFrameNumbers('tomato', { start: 10, end: 10 }),
        });
    this.anims.create({
        key: 'green_tomato_hit_to_left',
        frames: this.anims.generateFrameNumbers('tomato', { start: 11, end: 11 }),
        });
    this.anims.create({
        key: 'tomato_splat',
        frames: this.anims.generateFrameNumbers('tomato', { start: 12, frames: [12,13,13,12] }),
        frameRate: 16,
        });

    // BOT ANIMATIONS
    this.anims.create({
        key: 'bot_roll_left',
        frames: this.anims.generateFrameNumbers('bot', { start: 0, frames: [0,1] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'bot_roll_right',
        frames: this.anims.generateFrameNumbers('bot', { start: 3, frames: [3,4] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'bot_stand_left',
        frames: this.anims.generateFrameNumbers('bot', { start: 0, end: 0 }),
        });
    this.anims.create({
        key: 'bot_stand_right',
        frames: this.anims.generateFrameNumbers('bot', { start: 3, end: 3 }),
        });
    this.anims.create({
        key: 'bot_throw',
        frames: this.anims.generateFrameNumbers('bot', { start: 2, frames: [2,2,0] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'bot_explode_left',
        frames: this.anims.generateFrameNumbers('bot', { start: 0, frames: [0,5,6,7,8,9,10,11,12,13,14,14,14,14,14] }),
        frameRate: 8,
        repeat: 1
        });
    this.anims.create({
        key: 'bot_explode_right',
        frames: this.anims.generateFrameNumbers('bot', { start: 3, frames: [3,15,16,17,18,19,20,21,22,23,24,24,24,24,24] }),
        frameRate: 8,
        repeat: 1
        });

    this.anims.create({
        key: 'balloon_blue',
        frames: this.anims.generateFrameNumbers('balloon_blue', { start: 0, frames: [0,1,2,3,4,5,4,3,2,1] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'balloon_purple',
        frames: this.anims.generateFrameNumbers('balloon_purple', { start: 0, frames: [0,1,2,3,4,5,4,3,2,1] }),
        frameRate: 7,
        repeat: -1
        });
    this.anims.create({
        key: 'balloon_red',
        frames: this.anims.generateFrameNumbers('balloon_red', { start: 0, frames: [0,1,2,3,4,5,4,3,2,1] }),
        frameRate: 8,
        repeat: -1
        });
    this.anims.create({
        key: 'balloon_yellow',
        frames: this.anims.generateFrameNumbers('balloon_yellow', { start: 0, frames: [0,1,2,3,4,5,4,3,2,1] }),
        frameRate: 9,
        repeat: -1
        });
    this.anims.create({
        key: 'balloon_green',
        frames: this.anims.generateFrameNumbers('balloon_green', { start: 0, frames: [0,1,2,3,4,5,4,3,2,1] }),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'balloon_pink',
        frames: this.anims.generateFrameNumbers('balloon_pink', { start: 0, frames: [0,1,2,3,4,5,4,3,2,1] }),
        frameRate: 11,
        repeat: -1
        });

    this.anims.create({
        key: 'flag_red',
        frames: this.anims.generateFrameNumbers('flag', { start: 0, frames: [0,1,2,3,4,5,6,7,8,9]}),
        frameRate: 10,
        repeat: -1
        });

    this.anims.create({
        key: 'flag_blue',
        frames: this.anims.generateFrameNumbers('flag', { start: 10, frames: [10,11,12,13,14,15,16,17,18,19]}),
        frameRate: 12,
        repeat: -1
        });

    this.anims.create({
        key: 'flag_yellow',
        frames: this.anims.generateFrameNumbers('flag', { start: 20, frames: [20,21,22,23,24,25,26,27,28,29]}),
        frameRate: 13,
        repeat: -1
        });

    this.anims.create({
        key: 'flag_green',
        frames: this.anims.generateFrameNumbers('flag', { start: 30, frames: [30,31,32,33,34,35,36,37,38,39]}),
        frameRate: 11,
        repeat: -1
        });

    this.anims.create({
        key: 'check_flag1',
        frames: this.anims.generateFrameNumbers('check_flag', { start: 0, frames: [0,1,2,3,4,5,6,7,8,9]}),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'check_flag2',
        frames: this.anims.generateFrameNumbers('check_flag', { start: 0, frames: [0,1,2,3,4,5,6,7,8,9]}),
        frameRate: 8,
        repeat: -1
        });

    // BOSS NPC ANIMATIONS
    this.anims.create({
        key: 'boss_npc_walk_left',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 0, frames: [0,1,2,1,0,3,4,3] }),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_npc_walk_right',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 10, frames: [10,11,12,11,10,13,14,13] }),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_npc_throw_left',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 0, frames: [0,5,6,7,7,7,7,7,7] }),
        frameRate: 10,
        repeat: 1
        });
    this.anims.create({
        key: 'boss_npc_throw_right',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 10, frames: [10,15,16,17,17,17,17,17,17] }),
        frameRate: 10,
        repeat: 1
        });
    this.anims.create({
        key: 'boss_npc_no_left',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 8, frames: [8] }),
        });
    this.anims.create({
        key: 'boss_npc_no_right',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 18, frames: [18] }),
        });
    this.anims.create({
        key: 'boss_npc_defeated_left',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 9, frames: [9] }),
        });
    this.anims.create({
        key: 'boss_npc_defeated_right',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 19, frames: [19] }),
        });
    this.anims.create({
        key: 'boss_npc_fall_left',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 0, frames: [0] }),
        });
    this.anims.create({
        key: 'boss_npc_fall_right',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 10, frames: [10] }),
        });

    this.anims.create({
        key: 'small_bot_throw_left',
        frames: this.anims.generateFrameNumbers('small_bot', { start: 0, frames: [0,1,2,0] }),
        frameRate: 6,
        repeat: 1
        });
    this.anims.create({
        key: 'small_bot_throw_right',
        frames: this.anims.generateFrameNumbers('small_bot', { start: 4, frames: [4,5,6,4] }),
        frameRate: 6,
        repeat: 1
        });
    this.anims.create({
        key: 'burn',
        frames: this.anims.generateFrameNumbers('torch', { start: 0, frames: [0,1] }),
        frameRate: 10,
        repeat: -1
        });

    this.anims.create({
        key: 'boss_tomato_walk_left',
        frames: this.anims.generateFrameNumbers('boss_tomato', { start: 0, frames: [0,1,0,2] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_tomato_throw_left',
        frames: this.anims.generateFrameNumbers('boss_tomato', { start: 0, frames: [0,3,4,0,0,0] }),
        frameRate: 6,
        repeat: 1
        });
    this.anims.create({
        key: 'boss_tomato_walk_right',
        frames: this.anims.generateFrameNumbers('boss_tomato', { start: 6, frames: [6,7,6,8] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_tomato_throw_right',
        frames: this.anims.generateFrameNumbers('boss_tomato', { start: 6, frames: [6,9,10,6,6,6] }),
        frameRate: 6,
        repeat: 1
        });

    //BOSS BIRD
    this.anims.create({
        key: 'boss_bird_right_closed',
        frames: this.anims.generateFrameNumbers('boss_bird', { start: 0, frames: [0,1,2,1] }),
        frameRate: 8,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bird_left_closed',
        frames: this.anims.generateFrameNumbers('boss_bird', { start: 9, frames: [9,10,11,10] }),
        frameRate: 8,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bird_right_open',
        frames: this.anims.generateFrameNumbers('boss_bird', { start: 3, frames: [3,4,5,4] }),
        frameRate: 8,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bird_left_open',
        frames: this.anims.generateFrameNumbers('boss_bird', { start: 12, frames: [12,13,14,13] }),
        frameRate: 8,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bird_right_hit',
        frames: this.anims.generateFrameNumbers('boss_bird', { start: 6, frames: [6,7,8,7] }),
        frameRate: 8,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bird_left_hit',
        frames: this.anims.generateFrameNumbers('boss_bird', { start: 15, frames: [15,16,17,16] }),
        frameRate: 8,
        repeat: -1
        });

    // BOSS BOT
    this.anims.create({
        key: 'boss_bot_roll_left',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 0, frames: [0,1] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bot_roll_right',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 3, frames: [3,4] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bot_stand_left',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 0, end: 0 }),
        });
    this.anims.create({
        key: 'boss_bot_stand_right',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 3, end: 3 }),
        });
    this.anims.create({
        key: 'boss_bot_throw_over',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 2, frames: [2,4,5,5,5,5,5] }),
        frameRate: 8,
        });
    this.anims.create({
        key: 'boss_bot_throw_under',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 2, frames: [5,4,2,3,3,2,2,2,2,2] }),
        frameRate: 8,
        });
    this.anims.create({
        key: 'boss_bot_explode',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 6, frames: [6,7,8,9,10,11,12,13,14,15,15,15,15,15,15,15,15] }),
        frameRate: 6,
        repeat: 1
        });
    this.anims.create({
        key: 'machine',
        frames: this.anims.generateFrameNumbers('machine', { start: 0, frames: [0,1,2,3] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'explosion_explode',
        frames: this.anims.generateFrameNumbers('explosion', { start: 0, frames: [0,1,2,3,4,5,6,7,8,0,1,2,3,4,5,6,7,8,9,10,11,12,13] }),
        frameRate: 6,
        });
    this.anims.create({
        key: 'liquid_green',
        frames: this.anims.generateFrameNumbers('liquid_green', { start: 0, frames: [0,1,2,3] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'liquid_blue',
        frames: this.anims.generateFrameNumbers('liquid_blue', { start: 0, frames: [0,1,2,3] }),
        frameRate: 5,
        repeat: -1
        });
    this.anims.create({
        key: 'liquid_purple',
        frames: this.anims.generateFrameNumbers('liquid_purple', { start: 0, frames: [0,1,2,3] }),
        frameRate: 7,
        repeat: -1
        });
    this.anims.create({
        key: 'lava_bubble',
        frames: this.anims.generateFrameNumbers('lava', { start: 0, frames: [0,1,2,3] }),
        frameRate: 4,
        repeat: -1
        });
    this.anims.create({
        key: 'drill_right',
        frames: this.anims.generateFrameNumbers('drill', { start: 0, frames: [0,1,2] }),
        frameRate: 13,
        repeat: -1
        });
    this.anims.create({
        key: 'drill_left',
        frames: this.anims.generateFrameNumbers('drill', { start: 3, frames: [3,4,5] }),
        frameRate: 13,
        repeat: -1
        });
    this.anims.create({
        key: 'water',
        frames: this.anims.generateFrameNumbers('water', { start: 0, frames: [0,1,2,3,4,5,6] }),
        frameRate: 4,
        repeat: -1
        });
    this.anims.create({
        key: 'jeremy_dance',
        frames: this.anims.generateFrameNumbers('jeremy_straight_face', { start: 2, frames: [2,3,4,3] }),
        frameRate: 8,
        repeat: -1
        });

    this.scene.launch('opening');
}

}
