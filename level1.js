import Preload from "./preload.js";
import Player from "./player.js";
import Fact from  "./fact.js";
import Npc from  "./npc.js";
import Bs from  "./bs.js";
import Bird from  "./bird.js";
import Censor from "./censor.js"
import Tomato from  "./tomato.js";
import Juice from "./juice.js"
import Balloon from "./balloon.js"
import Flag from "./flag.js"
import Bot from "./bot.js"
import BossNpc from "./boss_npc.js"
import Water from "./water.js"
import Level2 from "./level2.js";
import Barrier from "./barrier.js"


export default class Level1 extends Phaser.Scene {

constructor() {
  	super({key: "level1"});
}

create (){
    this.scene_name = 'level1'

    this.sky = this.add.tileSprite(0, -100, 1200, 800, "level1_sky"); // x, y, width, height
    this.sky.setOrigin(0, 0);
    this.sky.setScrollFactor(0); // repeats background
    this.sky.setTileScale(0.5,0.5);
    this.bg_1 = this.add.tileSprite(0, -100, 1200, 800, "level1_bg1"); // x, y, width, height
    this.bg_1.setOrigin(0, 0);
    this.bg_1.setScrollFactor(0); // repeats background
    this.bg_1.setTileScale(3,3);
    this.bg_2 = this.add.tileSprite(0, 0, 1200, 800, "level1_bg2");
    this.bg_2.setOrigin(0, 0);
    this.bg_2.setScrollFactor(0);
    this.bg_2.setTileScale(2.5,2.5);
    this.myCam = this.cameras.main;

    this.map = this.add.tilemap("level1_map");
    var tileset = this.map.addTilesetImage("tileset", "level1_tileset", 32, 32, 1, 2);  //margin=1, spacing=2, this is needed because of using the frame extruder program
    this.layers = []
    this.layers.layer = this.map.createStaticLayer(0, tileset, 0, 0); // layer index, tileset, x, y
    this.layers.push(this.layers.layer)
    this.layers.layer.setCollisionByProperty({ collides: true }); // tiles w/ collides set to true (in Tiled) will collide
    this.layers.layer.setScale(2)
    this.layers.layer.setTileLocationCallback(43, 5, 4, 1, this.topCollisionOnly, this);
    this.layers.layer.setTileLocationCallback(71, 5, 4, 1, this.topCollisionOnly, this);
    this.layers.layer.setTileLocationCallback(85, 4, 8, 1, this.topCollisionOnly, this);
    this.layers.layer.setTileLocationCallback(106, 3, 9, 1, this.topCollisionOnly, this);
    this.layers.layer.setTileLocationCallback(125, 5, 4, 1, this.topCollisionOnly, this);
    this.layers.layer.setTileLocationCallback(144, 5, 4, 1, this.topCollisionOnly, this);
    this.layers.layer.setTileLocationCallback(150, 3, 9, 1, this.topCollisionOnly, this);
    this.layers.layer.setTileLocationCallback(234, 5, 12, 1, this.topCollisionOnly, this);
    this.layers.layer.setTileLocationCallback(248, 3, 9, 1, this.topCollisionOnly, this);
    this.layers.layer.setTileLocationCallback(259, 2, 13, 1, this.topCollisionOnly, this);

    this.balloon_sprites = new Balloon(this)
    this.balloon_sprites.addBalloon(this, 1155, 255, 'blue')
    this.balloon_sprites.addBalloon(this, 1597, 255, 'blue') //tent
    this.balloon_sprites.addBalloon(this, 2785, 257, 'green')
    this.balloon_sprites.addBalloon(this, 2973, 257, 'green') //games
    this.balloon_sprites.addBalloon(this, 3335, 255, 'red')
    this.balloon_sprites.addBalloon(this, 3768, 255, 'red')
    this.balloon_sprites.addBalloon(this, 4175, 450, 'yellow') //bench
    this.balloon_sprites.addBalloon(this, 4700, 257, 'blue')
    this.balloon_sprites.addBalloon(this, 6625, 257, 'pink')
    this.balloon_sprites.addBalloon(this, 8040, 257, 'purple')
    this.balloon_sprites.addBalloon(this, 8900, 450, 'pink')
    this.balloon_sprites.addBalloon(this, 9400, 257, 'blue')
    this.balloon_sprites.addBalloon(this, 13660, 257, 'pink')
    this.balloon_sprites.addBalloon(this, 14270, 450, 'blue')
    this.balloon_sprites.addBalloon(this, 15025, 257, 'yellow')
    this.balloon_sprites.addBalloon(this, 15075, 257, 'purple')
    this.balloon_sprites.addBalloon(this, 15125, 257, 'green')
    this.balloon_sprites.addBalloon(this, 15175, 257, 'red')
    this.balloon_sprites.addBalloon(this, 15275, 257, 'blue')
    this.balloon_sprites.addBalloon(this, 15325, 257, 'pink')
    this.balloon_sprites.addBalloon(this, 15375, 257, 'yellow')
    this.balloon_sprites.addBalloon(this, 15425, 257, 'purple')
    this.balloon_sprites.addBalloon(this, 15525, 257, 'green')

    this.flag_sprites = new Flag(this)
    this.flag_sprites.addFlag(this, 1377, 165, 'blue')
    this.flag_sprites.addFlag(this, 3552, 165, 'red')
    this.flag_sprites.addFlag(this, 5475, 227, 'green')
    this.flag_sprites.addFlag(this, 5925, 227, 'green')
    this.flag_sprites.addFlag(this, 6820, 163, 'red')
    this.flag_sprites.addFlag(this, 6880, 163, 'yellow')
    this.flag_sprites.addFlag(this, 7270, 163, 'blue')
    this.flag_sprites.addFlag(this, 7330, 163, 'green')
    this.flag_sprites.addFlag(this, 7905, 291, 'blue')
    this.flag_sprites.addFlag(this, 9630, 165, 'blue')
    this.flag_sprites.addFlag(this, 10150, 165, 'blue')
    this.flag_sprites.addFlag(this, 10656, 291, 'green')
    this.flag_sprites.addFlag(this, 13347, 165, 'green')
    this.flag_sprites.addFlag(this, 14626, 165, 'yellow')
    this.flag_sprites.addFlag(this, 16610, 101, 'blue')
    this.flag_sprites.addFlag(this, 17380, 101, 'blue')

    this.player_x_spawn = 300 //300
    if (this.cp == 1){
        this.player_x_spawn = 8730}
    else if (this.cp == 2){
        this.player_x_spawn = 17150}

		this.player = new Player(this, this.player_x_spawn, -100);
    this.cameras.main.setBounds(0, 0, (this.map.widthInPixels*2), (this.map.heightInPixels*2));
    this.cameras.main.startFollow(this.player.player);

    this.bs_statements = this.physics.add.group(); // Must have this here, otherwise facts go through BS randomly
    this.censorships = this.physics.add.group();
    this.math_stuff = this.physics.add.group();
    this.milkshake = this.physics.add.group();

    this.juice_sprites = new Juice(this, this.player)
    this.juice_sprites.addJuice(this, 6200, 0, 'small')
    this.juice_sprites.addJuice(this, 10100, 0, 'small')
    if(!this.cp || this.cp < 2){
      this.juice_sprites.addJuice(this, 17125, 0, 'big')}

    this.npc_sprites = new Npc(this, this.player)
    this.bird_sprites = new Bird(this, this.player)
    this.tomato_sprites = new Tomato(this, this.player)
    this.bot_sprites = new Bot(this, this.player)
    this.boss_npc_sprites = new BossNpc(this, this.player)

    this.enemies = []
    for (var i=0;i<this.enemies.length;i++){
        this.enemies[i] = false
    }

    this.water_sprites = new Water(this, this.player)
    this.water_sprites.addWater(this, 12545, 640)
    this.water_sprites.addWater(this, 12480, 640)
    this.water_sprites.addWater(this, 12415, 640)
    this.water_sprites.addWater(this, 12350, 640)
    this.water_sprites.addWater(this, 12285, 640)
    this.water_sprites.addWater(this, 12220, 640)
    this.water_sprites.addWater(this, 12155, 640)
    this.water_sprites.addWater(this, 12090, 640)
    this.water_sprites.addWater(this, 12025, 640)
    this.water_sprites.addWater(this, 11960, 640)
    this.water_sprites.addWater(this, 11895, 640)
    this.water_sprites.addWater(this, 11830, 640)
    this.water_sprites.addWater(this, 11775, 640)

    this.barrier_sprites = new Barrier(this, this.player)
    this.barrier_sprites.addBarrier(this, 19250, 350, 1, 5, true) // x, y, x_scale, y_scale

      this.music = this.sound.add('level_1_soundtrack');
      var music_config = {
        mute: false,
        volume: 0.45,
        rate: 1,
        detune: 0,
        seek: 0,
        loop: true,
        delay: 0
      }
      this.music.play(music_config)

if (!this.vl1_played){
  this.vl1.play()
  this.vl1_played = true
}
// this.game.sound.mute = true

}

update(){

    this.player.update();
    this.npc_sprites.update(this.player.player);
    this.bird_sprites.update(this.player.player);
    this.tomato_sprites.update(this.player.player);
    this.bot_sprites.update(this.player.player);
    this.boss_npc_sprites.update(this.player.player);

    var px = this.player.player.x

    if (px > 5330 && !this.up_text_shown){
      this.up_text= this.add.text(5015, 100, 'Hold up (W) to aim upward', {fill: '#FFF'}).setFontSize(60).setStroke('#000', 4).setDepth(1000)
      this.up_text_shown = true}

    if (this.up_text && px > 6529){
      this.up_text.destroy()
    }

    if (px > 2670 && !this.vl21_played){
      this.vl21.play()
      this.vl21_played = true}

    if (px > 7530 && !this.vl29_played){
      this.vl29.play()
      this.vl29_played = true}

    if (px > 11100 && !this.vl57_played){
      this.vl57.play()
      this.vl57_played = true}

    if (px > 15320 && !this.vl22_played){
      this.vl22.play()
      this.vl22_played = true}

    if (px > 18425 && !this.vl2_played){
      this.time.delayedCall(2000, function() {
        this.vl2.play()
      }, [], this);
      this.vl2_played = true}

    if (px > 800 && (!this.enemies.enemy0) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 1600, 400, 'walk', 'none')
        this.enemies.enemy0 = true}

    if (px >  1400 && (!this.enemies.enemy1) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 2500, 200, 'stand', 'low')
        this.enemies.enemy1 = true}

	  if (px >  2200 && (!this.enemies.enemy2) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 3100, 200, 'walk', 'low')
        this.enemies.enemy2 = true}

    if (px >  2200 && (!this.enemies.enemy3) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 3300, 200, 'walk', 'none')
        this.enemies.enemy3 = true}

    if (px >  2400 && (!this.enemies.enemy4) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 2200, -100, 'run', 'none')
        this.enemies.enemy4 = true}

    if (px >  3200 && (!this.enemies.enemy5) && this.cp < 1 ) {
        this.bird_sprites.addBird(this, 4000, 150, 'drop')
        this.enemies.enemy5 = true}

    if (px >  3500 && (!this.enemies.enemy6) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 4300, 400, 'walk', 'low')
        this.enemies.enemy6 = true}

    if (px >  3500 && (!this.enemies.enemy7) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 3200, -100, 'run', 'none')
        this.enemies.enemy7 = true}

    if (px >  3700 && (!this.enemies.enemy8) && this.cp < 1 ) { // on red tent
        this.npc_sprites.addNPC(this, 4600, -100, 'stand', 'low')
        this.enemies.enemy8 = true}

    if (px >  4800 && (!this.enemies.enemy9) && this.cp < 1 ) { // on green building
        this.npc_sprites.addNPC(this, 5700, -100, 'stand', 'low')
        this.enemies.enemy9 = true}

    if (px >  6500 && (!this.enemies.enemy10) && this.cp < 1 ) { // ambush
        this.npc_sprites.addNPC(this, 7500, 400, 'walk', 'none')
        this.enemies.enemy10 = true}

    if (px >  6500 && (!this.enemies.enemy11) && this.cp < 1 ) { // ambush
        this.npc_sprites.addNPC(this, 7700, 400, 'walk', 'none')
        this.enemies.enemy11 = true}

    if (px >  6500 && (!this.enemies.enemy12) && this.cp < 1 ) { // ambush
        this.npc_sprites.addNPC(this, 7900, 400, 'walk', 'low')
        this.enemies.enemy12 = true}

    if (px >  6700 && (!this.enemies.enemy13) && this.cp < 1 ) { // ambush
        this.npc_sprites.addNPC(this, 6450, -100, 'run', 'none')
        this.enemies.enemy13 = true}

    if (px >  6750 && (!this.enemies.enemy14) && this.cp < 1 ) { // ambush
        this.npc_sprites.addNPC(this, 6350, -100, 'run', 'low')
        this.enemies.enemy14 = true}

    if (px >  7800 && (!this.enemies.enemy15) && this.cp < 1 ) {
        this.bird_sprites.addBird(this, 8600, 75, 'drop')
        this.enemies.enemy15 = true}

    if (px >  9200 && (!this.enemies.enemy16) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 10200, 400, 'walk', 'none')
        this.enemies.enemy16 = true}

    if (px >  8800 && (!this.enemies.enemy17) && this.cp < 2 ) { // on purple building
        this.npc_sprites.addNPC(this, 9950, 100, 'stand', 'low')
        this.enemies.enemy17 = true}

    if (px >  10500 && (!this.enemies.enemy18) && this.cp < 2 ) {
        this.tomato_sprites.addTomato(this, 11400, 400, 'red')
        this.enemies.enemy18 = true}

    if (px >  10500 && (!this.enemies.enemy19) && this.cp < 2 ) { // on island
        this.npc_sprites.addNPC(this, 12275, 200, 'stand', 'low')
        this.enemies.enemy19 = true}

    if (px >  12750 && (!this.enemies.enemy20) && this.cp < 2 ) {
        this.bird_sprites.addBird(this, 11850, 400, 'wave')
        this.enemies.enemy20 = true}

    if (px >  12750 && (!this.enemies.enemy21) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 13550, 200, 'walk', 'low')
        this.enemies.enemy21 = true}

    if (px >  12750 && (!this.enemies.enemy22) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 13750, 200, 'walk', 'none')
        this.enemies.enemy22 = true}

    if (px >  14300 && (!this.enemies.enemy23) && this.cp < 2 ) { // on red tent
        this.npc_sprites.addNPC(this, 15100, -100, 'stand', 'low')
        this.enemies.enemy23 = true}

    if (px >  14300 && (!this.enemies.enemy24) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 15100, 400, 'walk', 'none')
        this.enemies.enemy24 = true}

    if (px >  14300 && (!this.enemies.enemy25) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 15400, 400, 'run', 'none')
        this.enemies.enemy25 = true}

    if (px >  14300 && (!this.enemies.enemy26) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 15550, 400, 'run', 'none')
        this.enemies.enemy26 = true}

    if (px >  14300 && (!this.enemies.enemy27) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 15800, 400, 'run', 'low')
        this.enemies.enemy27 = true}

    if (px >  14300 && (!this.enemies.enemy28) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 13700, 400, 'run', 'low')
        this.enemies.enemy28 = true}

    if (px >  14300 && (!this.enemies.enemy29) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 14100, -100, 'run', 'none')
        this.enemies.enemy29 = true}

    if (px >  14300 && (!this.enemies.enemy30) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 13900, -100, 'run', 'none')
        this.enemies.enemy30 = true}

    if (px >  15750 && (!this.enemies.enemy31) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 16650, -100, 'stand', 'low')
        this.enemies.enemy31 = true}

    if (px >  18425 && (!this.enemies.enemy33)) {
        this.boss_npc_sprites.addBossNpc(this, 18900, -250, true) //18900
        this.enemies.enemy33 = true}

    // CHECKPOINTS
    if (!this.cp) {
        this.cp = 0}
    if (px > 8730 && this.cp == 0) {
        this.cp = 1}
    if (px > 17800 && this.cp == 1) {
        this.cp = 2}

    // END LEVEL
    if (this.data.values.boss_defeated == true){
      this.music.stop()
        this.time.delayedCall(2000, function() {
            this.start('cutscene2');
            }, [], this.scene);
        }

    // PARRALAX
    this.bg_1.tilePositionX += .1;
    this.bg_2.tilePositionX = this.myCam.scrollX * .2;

    //BOSS FIGHT
    if (px > 18420){
        this.myCam.stopFollow()
      }
}

topCollisionOnly(sprite, tile){
    tile.setCollision(false, false, true, false)} //left, right, top, bottom

} // class
