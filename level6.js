import Preload from "./preload.js";
import Player from "./player.js";
import Fact from  "./fact.js";
import BossNpcMd from "./boss_npc_md.js"
import DarkRock from "./dark_rock.js"
import Explosion from "./explosion.js"

export default class level6 extends Phaser.Scene {

constructor() {
  	super({key: "level6"});
}

create (){
    this.scene_name = 'level6'
    this.bg_3 = this.add.tileSprite(0, 0, 1200, 800, "level6_bg3");
    this.bg_3.setOrigin(0, 0);
    this.bg_3.setScrollFactor(0); // repeats background
    this.bg_3.setPosition(0, -40)
    this.bg_3.setTileScale(1.1,1.1);
    this.bg_2 = this.add.tileSprite(0, 1000, 1200, 800, "level6_bg2");
    this.bg_2.setOrigin(0, 0);
    this.bg_2.setScrollFactor(0); // repeats background
    this.bg_2.setPosition(0, -40)
    this.bg_2.setTileScale(1.1,1.1);
    this.bg_1 = this.add.tileSprite(0, 1000, 1200, 800, "level6_bg1");//            960x540
    this.bg_1.setOrigin(0, 0);
    this.bg_1.setScrollFactor(0); // repeats background
    this.bg_1.setPosition(0, 0)
    this.bg_1.setTileScale(1.11,1.11);
    this.myCam = this.cameras.main;

    this.map = this.add.tilemap("level6_map");
    var tileset = this.map.addTilesetImage("tileset", "level6_tileset", 32, 32, 1, 2);  //margin=1, spacing=2, this is needed because of using the frame extruder program
    this.layers = []
    this.layers.layer = this.map.createStaticLayer(0, tileset, 0, 0);
    this.layers.push(this.layers.layer)
    this.layers.layer.setCollisionByProperty({ collides: true });
    this.layers.layer.setScale(2)

    // this.layers.layer2 = this.map.createStaticLayer(1, tileset, 0, 0);
    // this.layers.push(this.layers.layer2)
    // this.layers.layer2.setCollisionByProperty({ collides: true });
    // this.layers.layer2.setScale(2)

    for (var i=0;i<this.layers.length;i++){
        // this.layers[i].setTileLocationCallback(20, 49, 9, 1, this.topCollisionOnly, this);
    }

    this.player_x_spawn = 385 // 385
    this.player_y_spawn = -50 // -50

    if (this.cp == 1){
        this.player_x_spawn = 1800
        this.player_y_spawn = 2800}

		this.player = new Player(this, this.player_x_spawn, this.player_y_spawn);
    this.cameras.main.setBounds(0, 0, (this.map.widthInPixels*2), (this.map.heightInPixels*2));
    this.player_y_at_jump = this.player.player.y
    this.time_at_landing = 0
    this.stop_follow = false
    this.boss_npc_md_sprites = new BossNpcMd(this, this.player)
    this.dark_rocks = this.physics.add.group();

    this.enemies = []
    for (var i=0;i<this.enemies.length;i++){
        this.enemies[i] = false}

        this.dark_rock_sprites = new DarkRock(this, this.player)
        this.dark_rock_sprites.addDarkRock(this, 2600, 2000)

  this.br_collisions = 0
  this.rock_x_spawn = 2600
  this.boss_transparent = false
  this.boss_transparent_time = null
  this.explosion_sprites = new Explosion(this)
  this.bombs = this.physics.add.group();

  this.music = this.sound.add('level_6_soundtrack');
  this.music_config = {
    mute: false,
    volume: 0.65,
    rate: 1,
    detune: 0,
    seek: 0,
    loop: true,
    delay: 0
  }
  // this.game.sound.mute = true;
  // this.fps_display = this.add.text(600, 5, "FPS: " + this.sys.game.loop.actualFps, {fill: '#fff'}).setFontSize(40).setStroke('#000', 4).setDepth(1000)

}

update(){
    this.player.update();
    this.boss_npc_md_sprites.update(this.player);
    this.dark_rock_sprites.update(this.player);

    // this.fps_display.setText("FPS: " + this.sys.game.loop.actualFps)
    // this.fps_display.setPosition(this.myCam.midPoint.x+380, this.myCam.midPoint.y-270)
    // console.log(this.game.loop.framesThisSecond)

    if (this.playerInBox(2706, 3060) && (!this.enemies.enemy1)) {
        this.boss_npc_md_sprites.addBossNpcMd(this, 2975, 2430, true)
        this.music.play(this.music_config)
        this.enemies.enemy1 = true}

    if (this.br_collisions == 0 && this.boss_npc_mds.children.entries.length > 0){
      this.physics.add.overlap(this.dark_rocks, this.boss_npc_mds.children.entries[0].glass,
        function(boss, rock){
          boss.body.immovable = true
          rock.body.immovable = false
          rock.setFrame(1)
          if (rock.data.values.collided_with_glass == false){
            this.scene.scene.rock_sound.play()
            if (boss.health > 1) {
              this.boss_transparent_time = this.manual_time
              this.boss_transparent = true}
            boss.health -= 1
            var boss_life_frame = Phaser.Math.FloorTo((boss.health/boss.max_health) * 18)
            boss.life_bar.setFrame(boss_life_frame)
          }
          rock.data.values.collided_with_glass = true
          boss.hit = true
          boss.time_at_hit = this.manual_time
          this.time.delayedCall(100, function() {
            rock.setPosition(this.rock_x_spawn, rock.data.values.y_origin-1000)
            rock.data.values.collided_with_glass = false
            rock.setVelocityX(0)
            rock.setFrame(0)
            }, [], this.scene);
        }, null, this);
      this.br_collisions += 1
    }
    if (this.br_collisions == 1 && this.boss_npc_mds.children.entries.length > 0){
      this.physics.add.collider(this.dark_rocks, this.boss_npc_mds,
        function(rock, boss){
          boss.body.immovable = true
          rock.body.immovable = false
          this.scene.scene.clang2.play()
          if (rock.x > boss.x){
            rock.setVelocityX(300)}
          else {
            rock.setVelocityX(-300)}
        }, null, this);
      this.br_collisions += 1
    }

    // CHECKPOINTS
    if (!this.cp) {
        this.cp = 0}
    if (this.playerInBox(2706, 3060) && this.cp <= 0) {
        this.cp = 1}

    if (!this.stop_follow){
    this.myCam.centerOnX(this.player.player.x)
    if (this.player.player.body.onFloor() || this.player.player.body.touching.down) {
            this.myCam.pan(this.player.player.x, this.player.player.y-90, 50, 'Linear', true);
        this.player_y_at_jump = this.player.player.y}
    else {
        this.y_offset = this.player.player.y - this.player_y_at_jump
        if (this.y_offset < 0){ // player staying above original jump point
                this.myCam.centerOnY(this.player_y_at_jump-90)}
        else {
            this.myCam.centerOn(this.player.player.x, this.player.player.y-90)}
        this.time_at_landing = this.manual_time}}

    // END LEVEL
    if (this.boss_defeated == true){
      this.music.stop()
      this.scene.start('cutscene7');}

    this.bg_3.tilePositionX = this.myCam.scrollX * .1;
    this.bg_2.tilePositionX = this.myCam.scrollX * .2;
    this.bg_1.tilePositionX = this.myCam.scrollX * .5;

    if (this.player.player.data.values.health > 0 ){
        if (this.player.player.x > 2605){
            this.stop_follow = true}}

    if (this.stop_follow && (this.player.player.x < 2010)){  // restart if player escapes boss arena
        this.music.stop()
        this.scene.start()
    }

} // update

playerInBox(x, y){
    var player = this.player.player
    if (player.x > x-100 && player.x < x+100 && player.y > y-100 && player.y < y+100){
        return true}
    else{
        return false}}

topCollisionOnly(sprite, tile){
    tile.setCollision(false, false, true, false)} //left, right, top, bottom

} // class
