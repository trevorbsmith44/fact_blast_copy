export default class MathStuff {

constructor(scene, bot, player){
  this.scene = scene
  this.player = player

  var altitudes = [-15, 65]
  var alt = altitudes[Math.floor(Math.random() * altitudes.length)];
  var math_array = ['math1','math2','math3','math4','math5','math6','math7','math8','math9','math10','math11','math12','math13','math14','math15','math16','math17','math18','math19','math20','math21'];
  var random_math = math_array[Math.floor(Math.random() * math_array.length)];

  var math_stuff = scene.math_stuff.create(bot.x-200, bot.y+alt, random_math);
  math_stuff.setScale(0.7)
  math_stuff.setDepth(910)
  math_stuff.setOffset(20,15)
  math_stuff.setSize(50,50, false)
  math_stuff.setAngle(45)
  math_stuff.setAngularVelocity(-50);
  math_stuff.body.setAllowGravity(false);
  math_stuff.setVelocityX(-250);
  this.scene.time.delayedCall(10000, function() {
      math_stuff.destroy()
  }, [], this);

  this.scene.player_math = this.scene.physics.add.collider(math_stuff, this.player.player, function(math, player){
      math.destroy()
      math.setVisible(false)
      this.player.player.data.values.time_at_collision = scene.manual_time
      scene.player_hit.play()
      this.player.player.data.values.hit_from = math.x
      this.player.damagePlayer(this.player, scene, 4)
    },
    function(math, player){
        return (this.player.player.data.values.post_hit_invincibility == false)
    }, this);

}

update(){
}


}
