export default class Options extends Phaser.Scene {

constructor() {
  	super({key: "options"});
}

create (){

  this.add.text(220, 5, "Keyboard", {fill: '#fff'}).setFontSize(50).setStroke('#000', 4).setDepth(1000)
  this.add.text(740, 5, "USB Controller", {fill: '#fff'}).setFontSize(50).setStroke('#000', 4).setDepth(1000)

  this.add.text(62, 405, "left", {fill: '#fff'}).setFontSize(30).setDepth(1000)
  this.add.text(165, 405, "duck", {fill: '#fff'}).setFontSize(30).setDepth(1000)
  this.add.text(260, 405, "right", {fill: '#fff'}).setFontSize(30).setDepth(1000)
  this.add.text(430, 405, "throw", {fill: '#fff'}).setFontSize(30).setDepth(1000)
  this.add.text(540, 405, "jump", {fill: '#fff'}).setFontSize(30).setDepth(1000)
  this.add.text(148, 137, "aim up", {fill: '#fff'}).setFontSize(30).setDepth(1000)
  this.add.text(148, 170, "(hold)", {fill: '#fff'}).setFontSize(30).setDepth(1000)

  this.add.text(530, 115, "pause", {fill: '#fff'}).setFontSize(30).setDepth(1000)

  this.add.text(750, 455, "left", {fill: '#fff'}).setFontSize(30).setDepth(1000)
  this.add.text(850, 455, "duck", {fill: '#fff'}).setFontSize(30).setDepth(1000)
  this.add.text(950, 455, "right", {fill: '#fff'}).setFontSize(30).setDepth(1000)
  this.add.text(1025, 75, "throw", {fill: '#fff'}).setFontSize(30).setDepth(1000)
  this.add.text(1060, 455, "jump", {fill: '#fff'}).setFontSize(30).setDepth(1000)
  this.add.text(840, 80, "aim up", {fill: '#fff'}).setFontSize(24).setDepth(1000)
  this.add.text(740, 110, "aim up", {fill: '#fff'}).setFontSize(24).setDepth(1000)
  this.add.text(1080, 110, "aim up", {fill: '#fff'}).setFontSize(24).setDepth(1000)

  this.add.image(200, 250, 'keyboard').setScale(1.5)
  this.add.image(200, 350, 'keyboard').setScale(1.5)
  this.add.image(100, 350, 'keyboard').setScale(1.5)
  this.add.image(300, 350, 'keyboard').setScale(1.5)
  this.add.image(475, 350, 'keyboard').setScale(1.5)
  this.add.image(575, 350, 'keyboard').setScale(1.5)
  this.add.image(575, 200, 'keyboard').setScale(1.5)
  this.add.image(950, 250, 'controller').setScale(2.3)

  this.add.text(182, 220, "W", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
  this.add.text(80, 320, "A", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
  this.add.text(182, 320, "S", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
  this.add.text(280, 320, "D", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
  this.add.text(460, 320, "K", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
  this.add.text(560, 320, "L", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
  this.add.text(560, 170, "P", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)

  this.add.text(1075, 225, "A", {fill: '#fff'}).setFontSize(25).setDepth(1000)
  this.add.text(1103, 202, "B", {fill: '#fff'}).setFontSize(25).setDepth(1000)
  this.add.text(1075, 180, "Y", {fill: '#fff'}).setFontSize(25).setDepth(1000)
  this.add.text(1047, 202, "X", {fill: '#fff'}).setFontSize(25).setDepth(1000)

  this.play_game = this.physics.add.sprite(160, 540, 'play_game')
  this.play_game.setScale(0.5)
  this.play_game.setInteractive().on('pointerup', () => this.startCutscene())
  this.play_game.setInteractive().on('pointerover', () => this.play_game.setFrame(1))
  this.play_game.setInteractive().on('pointerout', () => this.play_game.setFrame(0))
  this.play_game.body.setAllowGravity(false)

  this.main_menu = this.physics.add.sprite(500, 540, 'main_menu')
  this.main_menu.setScale(0.5)
  this.main_menu.setInteractive().on('pointerup', () => this.startTitleScreen())
  this.main_menu.setInteractive().on('pointerover', () => this.main_menu.setFrame(1))
  this.main_menu.setInteractive().on('pointerout', () => this.main_menu.setFrame(0))
  this.main_menu.body.setAllowGravity(false)

  this.credits = this.physics.add.sprite(800, 540, 'credits')
  this.credits.setScale(0.5)
  this.credits.setInteractive().on('pointerup', () => this.startCredits())
  this.credits.setInteractive().on('pointerover', () => this.credits.setFrame(1))
  this.credits.setInteractive().on('pointerout', () => this.credits.setFrame(0))
  this.credits.body.setAllowGravity(false)

  this.mute = this.physics.add.sprite(1050, 544, 'mute')
  this.mute.setScale(0.5)
  this.mute.setInteractive().on('pointerup', () => this.muteAll())
  if (this.game.sound.mute == false){
    this.mute.setFrame(1)}
  else{
    this.mute.setFrame(0)}
  this.mute.body.setAllowGravity(false)


  this.music = this.sound.add('options_soundtrack');
  var music_config = {
    mute: false,
    volume: 0.4,
    rate: 1,
    detune: 0,
    seek: 0,
    loop: true,
    delay: 0
  }
  this.music.play(music_config)
  this.key_i = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.I);
  this.key_n = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.N);
  this.key_l = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.L);
  this.key_v = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.V);
  this.key_g = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.G);
  this.key_s = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
  this.key_a = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
  this.key_b = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.B);
  this.key_f = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.F);

}

startCutscene(){
  if (this.key_i.isDown && this.key_n.isDown){
    this.scene.start('cutscene2')
  }
  else if (this.key_i.isDown && this.key_l.isDown){
    this.scene.start('cutscene3')
  }
  else if (this.key_n.isDown && this.key_v.isDown){
    this.scene.start('cutscene4')
  }
  else if (this.key_g.isDown && this.key_s.isDown){
    this.scene.start('cutscene5')
  }
  else if (this.key_f.isDown && this.key_b.isDown){
    this.scene.start('cutscene6p1')
  }
  else {
    this.scene.start('cutscene1_options')
  }
  this.music.stop()
}

startTitleScreen(){
  this.scene.start('title_screen')
  this.music.stop()
}

startCredits(){
  this.scene.start('credits')
  this.music.stop()
}

muteAll(){
  if (this.game.sound.mute == false){
    this.game.sound.mute = true
    this.mute.setFrame(0)}
  else{
    this.game.sound.mute = false
    this.mute.setFrame(1)}
}

} // class
