import Preload from "./preload.js";
import Player from "./player.js";
import Fact from  "./fact.js";
import Npc from  "./npc.js";
import Bs from  "./bs.js";
import Bird from  "./bird.js";
import Censor from "./censor.js"
import Tomato from  "./tomato.js";
import Juice from "./juice.js"
import Balloon from "./balloon.js"
import Flag from "./flag.js"
import Bot from "./bot.js"
import Snow from "./snow.js"
import Snowflake from "./snowflake.js"
import BossBird from "./boss_bird.js"
import Mocker from "./mocker.js"

import Level2 from "./level2.js";

export default class Level3 extends Phaser.Scene {

constructor() {
  	super({key: "level3"});
}

create (){
    this.scene_name = 'level3'
    this.sky = this.add.tileSprite(0, 0, 9000, 7000, "level3_sky");
    this.sky.setOrigin(0, 0.05);
    this.sky.setTileScale(2.5,2.5);

    this.bg_2 = this.add.tileSprite(0, -100, 1200, 800, "level3_bg2"); // clouds
    this.bg_2.setOrigin(0, 0);
    this.bg_2.setScrollFactor(0); // repeats background
    this.bg_2.setTileScale(3,3);

    this.bg_1 = this.add.tileSprite(0, -100, 1200, 800, "level3_bg"); // bg buildings
    this.bg_1.setOrigin(0, 0);
    this.bg_1.setScrollFactor(0); // repeats background
    this.bg_1.setPosition(0, 100)

    this.myCam = this.cameras.main;

    this.map = this.add.tilemap("level3_map");
    var tileset = this.map.addTilesetImage("tileset", "level3_tileset", 32, 32, 1, 2);  //margin=1, spacing=2, this is needed because of using the frame extruder program
    this.layers = []
    this.layers.layer = this.map.createStaticLayer(0, tileset, 0, 0);
    this.layers.push(this.layers.layer)
    this.layers.layer.setCollisionByProperty({ collides: true });
    this.layers.layer.setScale(2)

    this.layers.layer2 = this.map.createStaticLayer(1, tileset, 0, 0);
    this.layers.push(this.layers.layer2)
    this.layers.layer2.setCollisionByProperty({ collides: true });
    this.layers.layer2.setScale(2)

    this.layers.layer3 = this.map.createStaticLayer(2, tileset, 0, 0);
    this.layers.push(this.layers.layer3)
    this.layers.layer3.setCollisionByProperty({ collides: true });
    this.layers.layer3.setScale(2)

    this.layers.layer4 = this.map.createStaticLayer(3, tileset, 0, 0);
    this.layers.push(this.layers.layer4)
    this.layers.layer4.setCollisionByProperty({ collides: true });
    this.layers.layer4.setScale(2)

    this.layers.layer5 = this.map.createStaticLayer(4, tileset, 0, 0);
    this.layers.push(this.layers.layer5)
    this.layers.layer5.setCollisionByProperty({ collides: true });
    this.layers.layer5.setScale(2)

    for (var i=0;i<this.layers.length;i++){
        this.layers[i].setTileLocationCallback(20, 49, 9, 1, this.topCollisionOnly, this);
        this.layers[i].setTileLocationCallback(15, 50, 2, 1, this.topCollisionOnly, this);
        this.layers[i].setTileLocationCallback(8, 43, 12, 1, this.topCollisionOnly, this);
        this.layers[i].setTileLocationCallback(86, 38, 2, 1, this.topCollisionOnly, this);
        this.layers[i].setTileLocationCallback(68, 32, 9, 1, this.topCollisionOnly, this);
        this.layers[i].setTileLocationCallback(82, 46, 6, 1, this.topCollisionOnly, this);
        this.layers[i].setTileLocationCallback(90, 50, 5, 1, this.topCollisionOnly, this);
        this.layers[i].setTileLocationCallback(86, 42, 7, 1, this.topCollisionOnly, this);
        this.layers[i].setTileLocationCallback(63, 36, 8, 1, this.topCollisionOnly, this);
        this.layers[i].setTileLocationCallback(73, 38, 10, 1, this.topCollisionOnly, this);
    }

    for (var i=0;i<this.layers.length;i++){
        this.layers[i].setTileLocationCallback(113, 57, 21, 1, this.noCollision, this);}

    this.player_x_spawn = 142 // 142
    this.player_y_spawn = 3400 // 3400
    if (this.cp == 1){
        this.player_x_spawn = 3830 // 3830
        this.player_y_spawn = 2000} // 2000
    else if (this.cp == 2){
        this.player_x_spawn = 7870 // 7870
        this.player_y_spawn = 500} // 500

		this.player = new Player(this, this.player_x_spawn, this.player_y_spawn);
    this.cameras.main.setBounds(0, 0, (this.map.widthInPixels*2), (this.map.heightInPixels*2));
    this.player_y_at_jump = this.player.player.y
    this.time_at_landing = 0
    this.bs_statements = this.physics.add.group(); // Must have this here, otherwise facts go through BS randomly
    this.censorships = this.physics.add.group();
    this.math_stuff = this.physics.add.group();
    this.milkshake = this.physics.add.group();
    this.egg = this.physics.add.group();
    this.trash_can = this.physics.add.group();
    this.stop_follow = false

    this.juice_sprites = new Juice(this, this.player)
    this.juice_sprites.addJuice(this, 3420, 2400, 'small')
    this.juice_sprites.addJuice(this, 7649, 1200, 'big')

    this.enemies = []
    for (var i=0;i<this.enemies.length;i++){
        this.enemies[i] = false}

    this.npc_sprites = new Npc(this, this.player)

    this.mocker_sprites = new Mocker(this);
    this.mocker_sprites.addMocker(this, 7315, 3585, 'left')
    var c = 7415
    for (var i=0;i<12;i++){
      this.mocker_sprites.addMocker(this, c, 3585, 'right')
      c += 100}

    this.bird_sprites = new Bird(this, this.player)
    this.snow_sprites = new Snow(this)
    this.snowflake_sprites = new Snowflake(this, this.player)
    this.boss_bird_sprites = new BossBird(this, this.player)
    this.level3_started = false

    this.music = this.sound.add('level_3_soundtrack');
    var music_config = {
      mute: false,
      volume: 0.5,
      rate: 1,
      detune: 0,
      seek: 0,
      loop: true,
      delay: 0
    }
    this.music.play(music_config)
    // this.game.sound.mute = true
}

update(){
    this.player.update();
    this.npc_sprites.update(this.player.player);
    this.bird_sprites.update(this.player.player);
    this.boss_bird_sprites.update(this.player.player);
    this.snowflake_sprites.update(this.player.player);
    this.snow_sprites.update(this.player.player);

    if (!this.level3_started){
      this.level3_start_time = this.manual_time
      this.level3_started = true
    }

    var px = this.player.player.x
    var py = this.player.player.y

    if (px > 1330 && !this.vl54_played){
      this.vl54.play()
      this.vl54_played = true}

    if (px > 2000 && py < 2600 && !this.vl52_played){
      this.vl52.play()
      this.vl52_played = true}

    if (px > 5500 && px < 5640 && py < 2370 && !this.vl51_played){
      this.vl51.play()
      this.vl51_played = true}

    if (px > 6000 && py < 1600 && !this.vl48_played){
      this.vl48.play()
      this.vl48_played = true}

    if (px > 8370 && py < 1400 && !this.vl50_played) {
      this.time.delayedCall(5000, function() {
        this.vl50.play()
      }, [], this);
      this.vl50_played = true}

    // if (px > 12938 && !this.vl11_played){
    //   this.vl11.play()
    //   this.vl11_played = true}
    //
    // if (px > 15964 && !this.vl28_played){
    //   this.vl28.play()
    //   this.vl28_played = true}
    //
    // if (px > 18626 && !this.vl42_played){
    //   this.time.delayedCall(1500, function() {
    //     this.vl42.play()
    //   }, [], this);
    //   this.vl42_played = true}

    if ((this.manual_time < this.level3_start_time + 20000) && Math.trunc(this.manual_time)%2==0){
      this.snow_sprites.addSnow(this)
    }

    if (px > 300 && (!this.enemies.enemy0) && this.cp < 1 ) {
        this.bird_sprites.addBird(this, 1200, 3500, 'norm')
        this.enemies.enemy0 = true}

    if (px >  300 && (!this.enemies.enemy1) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 1250, 3300, 'stand', 'none')
        this.enemies.enemy1 = true}

	  if (px >  1420 && (!this.enemies.enemy2) && this.cp < 1 ) {
        this.snowflake_sprites.addSnowflake(this, 1300, 2900, 'down right')
        this.enemies.enemy2 = true}

    if (px < 1850 && py < 3100 && (!this.enemies.enemy4) && this.cp < 1 ) {
        this.bird_sprites.addBird(this, 2500, 3050, 'wave')
        this.enemies.enemy4 = true}

    if (px < 1200 && py < 3000 && (!this.enemies.enemy5) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 520, 2950, 'stand', 'low')
        this.enemies.enemy5 = true}

    if (px <  385 && py < 3000 && (!this.enemies.enemy6) && this.cp < 1 ) {
        this.snowflake_sprites.addSnowflake(this, 1200, 2400, 'down left')
        this.enemies.enemy6 = true}

    if (px >  650 && py < 2700 && (!this.enemies.enemy7) && this.cp < 1 ) {
        this.snowflake_sprites.addSnowflake(this, 250, 2280, 'down right')
        this.enemies.enemy7 = true}

    if (px >  650 && py < 2700 && (!this.enemies.enemy8) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 1390, 2550, 'stand', 'low')
        this.enemies.enemy8 = true}

    if (px >  526 && py < 2700 && (!this.enemies.enemy9) && this.cp < 1 ) {
        this.bird_sprites.addBird(this, 1500, 2400, 'drop')
        this.enemies.enemy9 = true}

    if (px > 1190 && py < 2600 && (!this.enemies.enemy10) && this.cp < 1 ) {
        this.bird_sprites.addBird(this, 884, 2200, 'dive')
        this.enemies.enemy10 = true}

    if (px >  1950 && py < 2600 && (!this.enemies.enemy11) && this.cp < 1 ) {
        this.snowflake_sprites.addSnowflake(this, 3300, 1900, 'down left')
        this.enemies.enemy11 = true}

    if (px >  2600 && py < 2500 && (!this.enemies.enemy12) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 3430, 2500, 'walk', 'none')
        this.enemies.enemy12 = true}

    if (px >  2600 && py < 2500 && (!this.enemies.enemy13) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 3530, 2500, 'stand', 'low')
        this.enemies.enemy13 = true}

    if (px >  2600 && py < 2500 && (!this.enemies.enemy14) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 3830, 2650, 'stand', 'none')
        this.enemies.enemy14 = true}

    if (px > 2960 && py < 2600 && (!this.enemies.enemy15) && this.cp < 1 ) {
        this.bird_sprites.addBird(this, 2240, 2550, 'wave')
        this.enemies.enemy15 = true}

    if (px > 3900 && py < 2700 && (!this.enemies.enemy16) && this.cp < 2 ) {
        this.bird_sprites.addBird(this, 3225, 2350, 'drop')
        this.enemies.enemy16 = true}

    // if (px > 3900 && py < 2700 && (!this.enemies.enemy17) && this.cp < 1 ) {
    //     this.bird_sprites.addBird(this, 4900, 2400, 'drop')
    //     this.enemies.enemy17 = true}

    if (px > 4550 && py < 2900 && (!this.enemies.enemy18) && this.cp < 2 ) {
        this.bird_sprites.addBird(this, 3900, 2700, 'wave')
        this.enemies.enemy18 = true}

    if (px > 3900 && py < 2900 && (!this.enemies.enemy21) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 4730, 2200, 'stand', 'low')
        this.enemies.enemy21 = true}

    if (px >  5000 && py < 3200 && (!this.enemies.enemy19) && this.cp < 2 ) {
        this.snowflake_sprites.addSnowflake(this, 4500, 2200, 'down right')
        this.enemies.enemy19 = true}

    if (px >  5000 && py < 3200 && (!this.enemies.enemy20) && this.cp < 2 ) {
        this.snowflake_sprites.addSnowflake(this, 4900, 2100, 'down right')
        this.enemies.enemy20 = true}

    if (px >  5000 && py < 3200 && (!this.enemies.enemy20_1) && this.cp < 2 ) {
        this.snowflake_sprites.addSnowflake(this, 4000, 2300, 'down right')
        this.enemies.enemy20_1 = true}

    if (px > 4500 && py < 2850 && (!this.enemies.enemy22) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 5320, 2600, 'stand', 'low')
        this.enemies.enemy22 = true}

    if (px > 5500 && py < 3700 && (!this.enemies.enemy23) && this.cp < 2 ) {
        this.bird_sprites.addBird(this, 4800, 3510, 'norm')
        this.enemies.enemy23 = true}

    if (px > 5500 && py < 3700 && (!this.enemies.enemy24) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 6270, 3200, 'stand', 'low')
        this.enemies.enemy24 = true}

    if (px > 6183 && py < 3300 && (!this.enemies.enemy25) && this.cp < 2 ) {
        this.bird_sprites.addBird(this, 6600, 2950, 'dive')
        this.enemies.enemy25 = true}

    if (px > 5800 && px < 6050 && py < 3150 && (!this.enemies.enemy26) && this.cp < 2 ) {
        this.bird_sprites.addBird(this, 5700, 2750, 'dive')
        this.enemies.enemy26 = true}

    if (px > 5740 && px < 5900 && py < 3010 && (!this.enemies.enemy27) && this.cp < 2 ) {
        this.bird_sprites.addBird(this, 6150, 2550, 'dive')
        this.enemies.enemy27 = true}

    if (px > 5400 && px < 5600 && py < 2880 && (!this.enemies.enemy28) && this.cp < 2 ) {
        this.bird_sprites.addBird(this, 5100, 2350, 'dive')
        this.enemies.enemy28 = true}

    if (px > 5500 && px < 5730 && py < 2620 && (!this.enemies.enemy29) && this.cp < 2 ) {
        this.bird_sprites.addBird(this, 6130, 2150, 'dive')
        this.enemies.enemy29 = true}

    if (px >  5750 && px < 5900 && py < 2495 && (!this.enemies.enemy30) && this.cp < 2 ) {
        this.snowflake_sprites.addSnowflake(this, 6130, 2000, 'down left')
        this.enemies.enemy30 = true}

    if (px >  5750 && px < 5900 && py < 2495 && (!this.enemies.enemy31) && this.cp < 2 ) {
        this.snowflake_sprites.addSnowflake(this, 4700, 1900, 'down right')
        this.enemies.enemy31 = true}

    if (px >  4250 && px < 4540 && py < 2235 && (!this.enemies.enemy32) && this.cp < 2 ) {
        this.snowflake_sprites.addSnowflake(this, 3400, 1700, 'down right')
        this.enemies.enemy32 = true}

    if (px >  4250 && px < 4540 && py < 2235 && (!this.enemies.enemy33) && this.cp < 2 ) {
        this.snowflake_sprites.addSnowflake(this, 5200, 1700, 'down left')
        this.enemies.enemy33 = true}

    if (px >  4250 && px < 4540 && py < 2235 && (!this.enemies.enemy34) && this.cp < 2 ) {
        this.snowflake_sprites.addSnowflake(this, 5100, 2500, 'up left')
        this.enemies.enemy34 = true}

    if (px > 4000 && px < 4240 && py < 2110 && (!this.enemies.enemy35) && this.cp < 2 ) {
        this.bird_sprites.addBird(this, 4000, 1300, 'drop')
        this.enemies.enemy35 = true}

    if (px > 5480 && px < 5650 && py < 2365 && (!this.enemies.enemy36) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 4420, 1900, 'stand', 'none')
        this.enemies.enemy36 = true}

    if (px > 5480 && px < 5650 && py < 2365 && (!this.enemies.enemy38) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 4660, 1900, 'stand', 'none')
        this.enemies.enemy38 = true}

    if (px > 5480 && px < 5650 && py < 2365 && (!this.enemies.enemy39) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 4860, 1900, 'stand', 'none')
        this.enemies.enemy39 = true}

    if (px > 5480 && px < 5650 && py < 2365 && (!this.enemies.enemy40) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 5120, 1800, 'stand', 'none')
        this.enemies.enemy40 = true}

    if (px > 4500 && py < 2000 && (!this.enemies.enemy41) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 5390, 1700, 'stand', 'none')
        this.enemies.enemy41 = true}

    if (px > 4500 && py < 2000 && (!this.enemies.enemy42) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 5560, 1700, 'stand', 'none')
        this.enemies.enemy42 = true}

    if (px > 4500 && py < 2000 && (!this.enemies.enemy43) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 5730, 1700, 'stand', 'none')
        this.enemies.enemy43 = true}
    //
    if (px > 6180 && py < 1600 && (!this.enemies.enemy44) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 7830, 1300, 'stand', 'low')
        this.enemies.enemy44 = true}

    if (px > 8370 && py < 1400 && (!this.enemies.enemy45)) {
        this.boss_bird_sprites.addBossBird(this, 8750, 700, true)
        this.enemies.enemy45 = true}

    // CHECKPOINTS
    if (!this.cp) {
        this.cp = 0}
    if (px > 3830 && py < 2700  && this.cp == 0) {
        this.cp = 1}
    if (px > 7870 && py < 1400 && this.cp == 1) {
        this.cp = 2}

        if (!this.stop_follow){
        this.myCam.centerOnX(px)
        if (this.player.player.body.onFloor()) {
                this.myCam.pan(px, py-90, 50, 'Linear', true);
            this.player_y_at_jump = py}
        else {
            this.y_offset = py - this.player_y_at_jump
            if (this.y_offset < 0){ // player staying above original jump point
                    this.myCam.centerOnY(this.player_y_at_jump-90)}
            else {
                this.myCam.centerOn(px, py-90)}
            this.time_at_landing = this.time.now}}

    //  PARRALAX
    this.bg_2.tilePositionX += .05;
    this.bg_1.tilePositionX = this.myCam.scrollX * .2;

    // BOSS FIGHT
    if (this.player.player.data.values.health > 0 ){
        if (px > 8363 && py < 1340){
            this.stop_follow = true}}
    else {
        this.stop_follow = false}

    if (this.stop_follow && (px < 7700 || py > 1600)){  // restart if player escapes boss arena
        this.music.stop()
        this.scene.start()
    }

    // END LEVEL
    if (this.data.values.boss_defeated == true){
      this.music.stop()
        this.time.delayedCall(2000, function() {
            this.start('cutscene4');
            }, [], this.scene);
        }

}

topCollisionOnly(sprite, tile){
    tile.setCollision(false, false, true, false)} //left, right, top, bottom

noCollision(sprite, tile){
    tile.setCollision(false, false, false, false)} //left, right, top, bottom

} // class
