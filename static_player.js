export default class StaticPlayer {

constructor(scene){
    scene.static_players = scene.physics.add.group();
}

addStaticPlayer(scene, x, y) {
		var static_player = scene.static_players.create(x, y, 'jeremy_straight_face');
		static_player.setActive(true).setVisible(true);
		static_player.setScale(3.5);
    // static_player.setDepth(2)

    static_player.body.setAllowGravity(false);
    static_player.body.allowGravity = false;
		scene.static_players.add(static_player);
    static_player.body.setAllowGravity(false);
    scene.anims.play('jeremy_dance', static_player);
}

}
