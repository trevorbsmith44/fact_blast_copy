export default class Snow {

constructor(scene, player){
    scene.snow_dots = scene.physics.add.group();
    this.player = player
    this.scene = scene
}

update(player){
  this.scene.snow_dots.children.each(function(snow) {
      if (snow.active){
          if (snow.y > (player.y+1000)) {
              snow.setPosition(Phaser.Math.Between(player.x-3000, player.x+3000), player.y-400)
            }

          if (snow.data){ // prevents TypeError
            snow.setFrame(0)
            snow.setAngle(325)
            snow.setVelocity(-Phaser.Math.Between(50, 100), snow.data.values.speed) // down left
            } // if snow.data exists

          }  // if snow active
    }, this); // looping through all snow_dots
}

addSnow(scene) {
    var x = Phaser.Math.Between(scene.player.player.x-3000, scene.player.player.x+3000)
    var y = scene.player.player.y-400
    var snow = this.scene.snow_dots.create(x, y, 'snow');
		snow.setActive(true).setVisible(true)
		snow.setData({'health': 1,
								 'speed': Phaser.Math.Between(150, 225),
                 'x_origin': x,
                 'y_origin': y,
                 'player_x_origin': this.scene.player.player.x,
                 'player_y_origin': this.scene.player.player.y
							 });
		snow.setSize(80, 50, false)
		snow.setOffset(15,5)
    snow.setScale(1.25)
    snow.setDepth(999)
    snow.name = 'snow'
		this.scene.snow_dots.add(snow)
		snow.body.setAllowGravity(false);
}

}
