export default class Juice {

constructor(scene, player){
    scene.juice = scene.physics.add.group();
    this.scene = scene
    this.player = player

    scene.physics.add.overlap(this.player.player, scene.juice, function(player, juice){
        scene.juice_sound.play()
        scene.time.delayedCall(100, function() {
            scene.vl31.play()
        }, [], this);
        if (juice.data.values.size == 'small'){
            this.player.healPlayer(this.player, juice, 6, scene)}
        else{
            this.player.healPlayer(this.player, juice, 18, scene)}
        }, null, this);
}

addJuice(scene, x, y, size) {
		var aj = scene.juice.create(x, y, 'juice');
		aj.setActive(true).setVisible(true)
		aj.setData({
                'layer_collisions': [],
                'size': size
              });

    for (var i=0;i<scene.layers.length;i++){
        aj.data.values.layer_collisions.push(scene.physics.add.collider(aj, scene.layers[i]));}
    if (size == 'small'){
        aj.setScale(2)}
    else{
        aj.setScale(3)}

		scene.juice.add(aj)
}

}
