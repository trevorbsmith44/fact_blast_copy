export default class CutScene6P1 extends Phaser.Scene {

constructor() {
  	super({key: "cutscene6p1"});
}

create(){
  this.add.image(600, 300, 'c6i1')
  this.may_skip = true
  this.input.gamepad.once('down', function (pad, button, index) {
			this.gamepad = pad;
    }, this);

  this.key_skip = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);

}

update(){
  if (Phaser.Input.Keyboard.JustDown(this.key_skip)) { //  || (this.gamepad.A)
      this.scene.start('cutscene6')
      }

  if (this.gamepad){
    if (this.gamepad.A && this.may_skip) {
        this.scene.start('cutscene6')}
    this.may_skip = ((this.gamepad.A) ? false : true);
  }
}

} // class
