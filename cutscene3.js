export default class CutScene3 extends Phaser.Scene {

constructor() {
  	super({key: "cutscene3"});
}

create(){
  this.current_image = 1
  this.c3i1 = this.add.image(600, 300, 'c3i1')
  this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
  this.may_skip = true
  this.input.gamepad.once('down', function (pad, button, index) {
			this.gamepad = pad;
    }, this);

  this.key_skip = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);

  this.music = this.sound.add('cutscene_soundtrack');
  var music_config = {
    mute: false,
    volume: 0.3,
    rate: 1,
    detune: 0,
    seek: 0,
    loop: true,
    delay: 0
  }
  this.music.play(music_config)
}

update(){
  if (Phaser.Input.Keyboard.JustDown(this.key_skip)) { //  || (this.gamepad.A)
    if (this.current_image == 1){
      this.add.image(600, 300, 'c3i2')
      this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 2}
    else if (this.current_image == 2){
      this.add.image(600, 300, 'c3i3')
      this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 3}
    else if (this.current_image == 3){
      this.add.image(600, 300, 'c3i4')
      this.current_image = 4}
    else if (this.current_image == 4){
      this.music.stop()
      this.scene.start('level3')}
      }

  if (this.gamepad){
    if (this.gamepad.A && this.may_skip) {
      if (this.current_image == 1){
        this.add.image(600, 300, 'c3i2')
        this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 2}
      else if (this.current_image == 2){
        this.add.image(600, 300, 'c3i3')
        this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 3}
      else if (this.current_image == 3){
        this.add.image(600, 300, 'c3i4')
        this.current_image = 4}
      else if (this.current_image == 4){
        this.music.stop()
        this.scene.start('level3')}}
    this.may_skip = ((this.gamepad.A) ? false : true);
  }
}

} // class


//c3i1
// this.sky = this.add.tileSprite(0, 0, 9000, 7000, "level3_sky");
// this.sky.setOrigin(0, 0.05);
// this.sky.setTileScale(2.5,2.5);
//
// this.bg_2 = this.add.tileSprite(0, -100, 1200, 800, "level3_bg2"); // clouds
// this.bg_2.setOrigin(0, 0);
// this.bg_2.setScrollFactor(0); // repeats background
// this.bg_2.setTileScale(3,3);
// this.bg_2.setPosition(50,-115)
//
// this.add.image(200, 220, 'c3i3_bg').setScale(1)
// this.add.image(-150, 375, 'limo').setScale(3)
// this.add.image(900, 300, 'welcome_to_chicago').setScale(0.5)
//
// this.wheel2 = this.physics.add.sprite(355, 460, 'car_wheel').setFrame(6).setScale(3).setAngle(20).body.setAllowGravity(false)
//
// this.player = this.physics.add.sprite(200, 190, 'jeremy').setScale(2.5);
// this.player.body.setAllowGravity(false)
// this.player.setFrame(22)
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
// this.text = this.add.text(210, 510, "The tomato monster has been slain, and", { fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(200, 545, 'Jeremy has reached Chicago by nightfall!', { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)

//c3i2

  // this.sky = this.add.tileSprite(0, 0, 9000, 7000, "level3_sky");
  // this.sky.setOrigin(0, 0.05);
  // this.sky.setTileScale(2.5,2.5);
  //
  // this.bg_2 = this.add.tileSprite(0, -100, 1200, 800, "level3_bg2"); // clouds
  // this.bg_2.setOrigin(0, 0);
  // this.bg_2.setScrollFactor(0); // repeats background
  // this.bg_2.setTileScale(3,3);
  // this.bg_2.setPosition(130,-150)
  //
  // this.bg_1 = this.add.tileSprite(0, -100, 1200, 800, "level3_bg"); // bg buildings
  // this.bg_1.setOrigin(0, 0);
  // this.bg_1.setScrollFactor(0); // repeats background
  // this.bg_1.setPosition(0, -80)
  //
  // this.buildings = this.add.image(680,-300, 'buildings')//.setScale(4.5)
  //
  // this.player = this.physics.add.sprite(200, 400, 'jeremy').setScale(4);
  // this.player.body.setAllowGravity(false)
  // this.player.setFrame(0)
  //
  // this.van = this.physics.add.sprite(1025, 410, 'van').setScale(1.5).body.setAllowGravity(false)
  // this.wheel1 = this.physics.add.sprite(970,457, 'car_wheel').setFrame(3).setScale(1.5).body.setAllowGravity(false)
  // this.wheel2 = this.physics.add.sprite(1075,457, 'car_wheel').setFrame(3).setScale(1.5).body.setAllowGravity(false)
  //
  // this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
  // this.text = this.add.text(210, 510, "There's the soccer-mom car he came for.", { fill: '#fff'})
  // this.text.setFontSize(34)
  // this.text.setStroke('#000', 4)
  // this.text2 = this.add.text(245, 545, "But something doesn't feel right...", { fill: '#fff'})
  // this.text2.setFontSize(34)
  // this.text2.setStroke('#000', 4)






//c3i3

// this.sky = this.add.tileSprite(0, 0, 9000, 7000, "level3_sky");
// this.sky.setOrigin(0, 0.05);
// this.sky.setTileScale(2.5,2.5);
//
// this.bg_2 = this.add.tileSprite(0, -100, 1200, 800, "level3_bg2"); // clouds
// this.bg_2.setOrigin(0, 0);
// this.bg_2.setScrollFactor(0); // repeats background
// this.bg_2.setTileScale(3,3);
// this.bg_2.setPosition(130,-150)
//
// this.bg_1 = this.add.tileSprite(0, -100, 1200, 800, "level3_bg"); // bg buildings
// this.bg_1.setOrigin(0, 0);
// this.bg_1.setScrollFactor(0); // repeats background
// this.bg_1.setPosition(0, -80)
//
// this.buildings = this.add.image(680,-300, 'buildings')//.setScale(4.5)
//
// var i
// for (i = 0; i<30; i+=1){
//   this.physics.add.sprite(Phaser.Math.Between(20, 1180), Phaser.Math.Between(50, 300), 'bird').setScale(0.75).setFrame(Phaser.Math.Between(0,1)).body.setAllowGravity(false)}
//
//   this.player = this.physics.add.sprite(850, 400, 'jeremy').setScale(4);
//   this.player.body.setAllowGravity(false)
//   this.player.setFrame(21)
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
// this.text = this.add.text(90, 510, "A swarm of censoring birds has invaded the city!", { fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(120, 545, "Jeremy must fend them off, don't be silenced!", { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)
