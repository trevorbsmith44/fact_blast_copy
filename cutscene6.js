export default class CutScene6 extends Phaser.Scene {

constructor() {
  	super({key: "cutscene6"});
}

create(){
  this.time.delayedCall(10000, function() {
    this.scene.start('level6')
      }, [], this);

  this.input.gamepad.once('down', function (pad, button, index) {
			this.gamepad = pad;
    }, this);

  this.key_skip = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);

  this.bg_3 = this.add.tileSprite(0, 0, 1200, 800, "c6_bg");
  this.bg_3.setOrigin(0, 0);
  this.bg_3.setScrollFactor(0); // repeats background
  this.bg_3.setPosition(0, -40)
  this.bg_3.setTileScale(2,2);

  this.physics.add.sprite(600, 0, 'jeremy').setScale(3).setFrame(14).setVelocityY(75).body.setAllowGravity(false)

  this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
  this.text = this.add.text(70, 515, "As Jeremy descends into the darkness, he knows there", {fill: '#fff'})
  this.text.setFontSize(34)
  this.text.setStroke('#000', 4)
  this.text2 = this.add.text(80 , 545, "is no turning back now. This is the lair of NPC MD.", { fill: '#fff'})
  this.text2.setFontSize(34)
  this.text2.setStroke('#000', 4)

}

update(){
  if (Phaser.Input.Keyboard.JustDown(this.key_skip)) { //  || (this.gamepad.A)
      this.scene.start('level6')}

  if (this.gamepad){
    if (this.gamepad.A && this.may_skip) {
      this.scene.start('level6')}
    this.may_skip = ((this.gamepad.A) ? false : true);
  }

  this.bg_3.tilePositionY += 10;

}

} // class










// i1
// this.add.image(600, 100, 'level2_sky')
// this.add.image(600, 250, 'i1_bg')
// this.add.image(1000, 480, 'hill')
//
//
// this.physics.add.sprite(1000, 330, 'jeremy_straight_face').setScale(3).setFrame(1).setFlip(true,false).body.setAllowGravity(false)
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
// this.text = this.add.text(115, 510, "The desert has been purged of those evil bots,", { fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(100 , 545, "and Jeremy has finally reached the Golden State.", { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)















// i3
// this.add.image(600, 100, 'level2_sky')
// this.add.image(550, 125, 'i3_bg').setScale(1.2)
//
// this.physics.add.sprite(200, 305, 'jeremy').setScale(4).setFrame(8).body.setAllowGravity(false)
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
// this.text = this.add.text(90, 515, "The front door is open as if they dare Jeremy to", {fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(83 , 545, "enter. Let's put an end to this hostile takeover!", { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)
