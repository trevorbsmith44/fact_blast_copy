export default class Credits extends Phaser.Scene {

constructor() {
  	super({key: "credits"});
}

create(){
  this.input.gamepad.once('down', function (pad, button, index) {
			this.gamepad = pad;
    }, this);
  this.key_skip = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);

// =============================================================================

  this.title_text = this.add.image(600, 700, 'title').setScale(0.75)

  this.text = this.add.text(320, 780, "based on Geeks + Gamers", {fill: '#12e73f'}).setFontSize(40).setStroke('#000', 4).setDepth(1000)

  this.text2 = this.add.text(50, 1000, "concept, coding, animations,", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)
  this.text3 = this.add.text(50, 1030, "artwork, level design & story ................. t-rev", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)

  this.text4 = this.add.text(50, 1200, "Geeks + Gamers owner", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)
  this.text5 = this.add.text(50, 1230, "& main character ....................... Jeremy Prime", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)

  this.text15 = this.add.text(50, 1400, "Original Music .............................. 12Fives", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)

  this.text6 = this.add.text(50, 1500, "testing & feedback .................... stupidstrudel", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)
this.text6_1 = this.add.text(50, 1550, "                                            ChorkyPig", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)
this.text6_2 = this.add.text(50, 1600, "                                             Bokthand", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)

  this.text8 = this.add.text(50, 1700,  "forest background ....... Tio Aimar @ opengameart.org", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)

  this.text10 = this.add.text(50, 1800, "city background ........................ craftpix.net", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)

  this.text12 = this.add.text(50, 1900, "skies .............................. webgradients.com", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)

  this.text19 = this.add.text(50, 2000, "Sound effects .......... freesound.org & zapsplat.com", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)

  this.text20 = this.add.text(50, 2100, "Special thanks ................. Phaser Discord Group", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)


  this.text14 = this.add.text(80, 2300, "made with JavaScript, Phaser, Piskel, Tiled & LMMS", {fill: '#12e73f'}).setFontSize(34).setStroke('#000', 4).setDepth(1000)

  this.music = this.sound.add('level_3_soundtrack');
  var music_config = {
    mute: false,
    volume: 0.5,
    rate: 1,
    detune: 0,
    seek: 0,
    loop: true,
    delay: 0
  }
  this.music.play(music_config)
}

update(){
  if (Phaser.Input.Keyboard.JustDown(this.key_skip)) { //  || (this.gamepad.A)
      this.music.stop()
      location.reload()}

  if (this.gamepad){
    if (this.gamepad.A) {
        this.music.stop()
        location.reload()}
  }

  this.title_text.y -= 1
  this.text.y -= 1
  this.text2.y -= 1
  this.text3.y -= 1
  this.text4.y -= 1
  this.text5.y -= 1
  this.text6.y -= 1
  this.text6_1.y -= 1
  this.text6_2.y -= 1
  this.text8.y -= 1
  this.text10.y -= 1
  this.text12.y -= 1
  this.text14.y -= 1
  this.text15.y -= 1
  this.text19.y -= 1
  this.text20.y -= 1

  if (this.text14.y <= -100){
    this.music.stop()
    if (!this.reloaded){
      location.reload()
      this.reloaded =  true}
  }

}

} // class
