import Fact from  "./fact.js";

export default class Player {

constructor(scene, x, y){
    this.scene = scene

    this.player = scene.physics.add.sprite(x, y, 'jeremy').setScale(1.5);
    this.player.setData({'health': 18, //18
                        'post_hit_invincibility': false,
                        'knock_back_time': 200,
                        'time_at_collision':null,
                        'time_at_damage':null,
                        'may_jump': true,
                        'may_throw': true,
                        'i':0,
                        'hit_from': null})
    this.player.state = 'jump_right'

    this.player.setDepth(900);
    this.player.setGravityY(1000)
    this.player.setMaxVelocity(300,900)
    this.player.name = 'player'

    // scene.cameras.main.startFollow(this.player);

    this.life_bar = scene.physics.add.sprite(140, 30, 'life_bar').setDepth(1000);
		this.life_bar.body.setAllowGravity(false);
		this.life_bar.setScrollFactor(0,0)
		this.life_bar.setFrame(18)

    //GAMEPAD
		scene.input.gamepad.once('down', function (pad, button, index) {
				this.gamepad = pad;
      }, this);

    this.key_left = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    this.key_right = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
    this.key_up = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
    this.key_down = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
    this.key_jump = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.L);
    this.key_throw = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.K);
    this.key_pause = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.P);

    this.player_layer = []
    for (var i=0;i<scene.layers.length;i++){
        this.player_layer[i] = scene.physics.add.collider(this.player, scene.layers[i]);}
    scene.sound.sounds = []
    scene.facts = scene.physics.add.group();
    scene.whoosh1 = scene.sound.add('whoosh1')
    scene.whoosh2 = scene.sound.add('whoosh2')
    scene.whoosh3 = scene.sound.add('whoosh3')
    scene.whooshes = [scene.whoosh1, scene.whoosh2, scene.whoosh3]
    scene.whack1 = scene.sound.add('whack1')
    scene.whack2 = scene.sound.add('whack2')
    scene.whack3 = scene.sound.add('whack3')
    scene.whack4 = scene.sound.add('whack4')
    scene.whacks = [scene.whack1, scene.whack2, scene.whack3, scene.whack4]
    scene.clang2 = scene.sound.add('clang2')
    scene.soft_hit = scene.sound.add('soft_hit')
    scene.pop = scene.sound.add('pop')
    scene.bird_sound = scene.sound.add('bird_sound')
    scene.bird_sound2 = scene.sound.add('bird_sound2')
    scene.bird_sounds = [scene.bird_sound, scene.bird_sound2]
    scene.crash = scene.sound.add('crash')
    scene.jump_sound = scene.sound.add('jump_sound')
    scene.player_hit = scene.sound.add('player_hit')
    scene.splat = scene.sound.add('splat')
    scene.juice_sound = scene.sound.add('juice_sound')
    scene.clang = scene.sound.add('clang')
    scene.bird_swoop = scene.sound.add('bird_swoop')
    scene.bird_egg = scene.sound.add('bird_egg')
    scene.bird_defeated = scene.sound.add('bird_defeated')
    scene.bonk = scene.sound.add('bonk')
    scene.sizzle = scene.sound.add('sizzle')
    scene.explosion = scene.sound.add('explosion')
    scene.rock_sound = scene.sound.add('rock_sound')
    scene.nooo_sound = scene.sound.add('nooo_sound')
    scene.boss_tomato_throw_sound = scene.sound.add('boss_tomato_throw_sound')
    scene.boss_bot_throw_sound = scene.sound.add('boss_bot_throw_sound')

    scene.vl1 = scene.sound.add('vl1')
    scene.vl2 = scene.sound.add('vl2')
    scene.vl11 = scene.sound.add('vl11')
    scene.vl18 = scene.sound.add('vl18')
    scene.vl14 = scene.sound.add('vl14')
    scene.vl15 = scene.sound.add('vl15')
    scene.vl20 = scene.sound.add('vl20')
    scene.vl21 = scene.sound.add('vl21')
    scene.vl22 = scene.sound.add('vl22')
    scene.vl23 = scene.sound.add('vl23')
    scene.vl24 = scene.sound.add('vl24')
    scene.vl28 = scene.sound.add('vl28')
    scene.vl29 = scene.sound.add('vl29')
    scene.vl30 = scene.sound.add('vl30')
    scene.vl31 = scene.sound.add('vl31')
    scene.vl37 = scene.sound.add('vl37')
    scene.vl42 = scene.sound.add('vl42')
    scene.vl46 = scene.sound.add('vl46')
    scene.vl48 = scene.sound.add('vl48')
    scene.vl49 = scene.sound.add('vl49')
    scene.vl50 = scene.sound.add('vl50')
    scene.vl51 = scene.sound.add('vl51')
    scene.vl52 = scene.sound.add('vl52')
    scene.vl53 = scene.sound.add('vl53')
    scene.vl54 = scene.sound.add('vl54')
    scene.vl57 = scene.sound.add('vl57')
    scene.vl58 = scene.sound.add('vl58')
    scene.vl59 = scene.sound.add('vl59')
    scene.vl60 = scene.sound.add('vl60')
    scene.vl61 = scene.sound.add('vl61')
    scene.vl62 = scene.sound.add('vl62')
    scene.vl63 = scene.sound.add('vl63')
    scene.vl64 = scene.sound.add('vl64')
    scene.vl65 = scene.sound.add('vl65')
    scene.vl66 = scene.sound.add('vl66')
    scene.vl67 = scene.sound.add('vl67')
    scene.vl68 = scene.sound.add('vl68')
    scene.vl69 = scene.sound.add('vl69')

    scene.manual_time = 0

    this.scene.physics.world.fixedStep = false;
}

init(data){
  this.data = data

  // this.scene.physics.world.setFPS(60)
}

create(){
}

update(){  // FINITE STATE MACHINE //

      this.scene.manual_time += (1000/this.scene.sys.game.loop.actualFps)
      // console.log(this.scene.sys.game.loop.actualFps)

      if (this.player.state.includes('duck')){
          this.player.setOffset(15, 45)
          this.player.setSize(37, 50, false)}
      else {
          this.player.setOffset(15, 5)
          this.player.setSize(37, 90, false)}

      if(this.gamepad){

        if (this.scene.game.use_game_properties){
          this.key_left.isDown = this.scene.game.left_key_pressed
          this.key_right.isDown = this.scene.game.right_key_pressed
          this.key_up.isDown = this.scene.game.up_key_pressed
          this.key_down.isDown = this.scene.game.down_key_pressed
          this.scene.game.use_game_properties = false
        }

      if (this.player.state == 'stand_right'){
          this.player.setVelocityX(0);
          this.player.anims.play('stand_right', true);
          if (!this.player.body.touching.down && !this.player.body.onFloor()){
              this.player.state = 'jump_right'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_jump) || (this.gamepad.A && this.player.data.values.may_jump)) {
              this.player.setVelocityY(-750);
              this.scene.jump_sound.play()
              this.player.state = 'jump_right';}
          else if (this.key_right.isDown || this.gamepad.right){
              this.player.state = 'run_right'}
          else if (this.key_left.isDown || this.gamepad.left){
              this.player.state = 'run_left'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_throw) || (this.gamepad.X && this.player.data.values.may_throw)) {
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)
              this.player.state = 'stand_right_and_throw'
              this.throwFact(35, 450, -200)}
          else if (this.key_down.isDown || this.gamepad.down){
              this.player.state = 'duck_right'}
          }

      if (this.player.state == 'stand_left'){
          this.player.setVelocityX(0);
          this.player.anims.play('stand_left', true);
          if (!this.player.body.touching.down && !this.player.body.onFloor()){
              this.player.state = 'jump_left'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_jump) || (this.gamepad.A && this.player.data.values.may_jump)){
              this.player.setVelocityY(-750);
              this.scene.jump_sound.play()
              this.player.state = 'jump_left';}
          else if (this.key_left.isDown || this.gamepad.left){
              this.player.state = 'run_left'}
          else if (this.key_right.isDown || this.gamepad.right){
              this.player.state = 'run_right'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_throw) || (this.gamepad.X && this.player.data.values.may_throw)) {
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)
              this.player.state = 'stand_left_and_throw'
              this.throwFact(-35, -450, -200)}
          else if (this.key_down.isDown || this.gamepad.down){
              this.player.state = 'duck_left'}
          }

      if (this.player.state == 'run_right'){
          this.player.setVelocityX(250);
          this.player.anims.play('run_right', true);
          if (!this.player.body.touching.down && !this.player.body.onFloor()){
              this.player.state = 'jump_right'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_jump) || (this.gamepad.A && this.player.data.values.may_jump)){
              this.player.setVelocityY(-750);
              this.scene.jump_sound.play()
              this.player.state = 'jump_right'}
          else if ((!this.key_right.isDown) && (!this.gamepad.right)){
              this.player.state = 'stand_right'}
          else if ((this.key_left.isDown && this.key_right.isUp) || this.gamepad.left){
              this.player.state = 'run_left'}
          else if (this.key_left.isDown && this.key_right.isDown){
              this.player.state = 'run_right'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_throw) || (this.gamepad.X && this.player.data.values.may_throw)){
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)
              this.player.state = 'stand_right_and_throw'
              this.throwFact(35, 450, -200)}
          else if (this.key_down.isDown || this.gamepad.down){
              this.player.state = 'duck_right'}
          }

      if (this.player.state == 'run_left'){
          this.player.setVelocityX(-250);
          this.player.anims.play('run_left', true);
          if (!this.player.body.touching.down && !this.player.body.onFloor()){
              this.player.state = 'jump_left'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_jump) || (this.gamepad.A && this.player.data.values.may_jump)){
              this.player.setVelocityY(-750);
              this.scene.jump_sound.play()
              this.player.state = 'jump_left'}
          else if (!this.key_left.isDown && (!this.gamepad.left)){
              this.player.state = 'stand_left'}
          else if ((this.key_right.isDown && this.key_left.isUp) || this.gamepad.right){
              this.player.state = 'run_right'}
          else if (this.key_left.isDown && this.key_right.isDown){
              this.player.state = 'run_left'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_throw) || (this.gamepad.X && this.player.data.values.may_throw)){
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)
              this.player.state = 'stand_left_and_throw'
              this.throwFact(-35, -450, -200)}
          else if (this.key_down.isDown || this.gamepad.down){
              this.player.state = 'duck_left'}
          }

      if (this.player.state == 'jump_right'){
          this.player.anims.play('jump_right', true);
          if (this.player.body.touching.down || this.player.body.onFloor()){
              this.player.state = 'stand_right'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){
              this.player.state = 'jump_right'}
          else if ((this.key_right.isDown && Phaser.Input.Keyboard.JustDown(this.key_throw)) || (this.gamepad.right && (this.gamepad.X && this.player.data.values.may_throw))) {
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)
              this.player.state = 'jump_right_and_throw'
              this.throwFact(35, 450, -200)
              this.player.setVelocityX(250);}
          else if (this.key_right.isDown || this.gamepad.right){
              this.player.setVelocityX(250);}
          else if (this.key_left.isDown || (this.gamepad.left)){
              this.player.state = 'jump_left'}
          else if ((Phaser.Input.Keyboard.JustDown(this.key_throw)) || ((!this.gamepad.right) && (this.gamepad.X && this.player.data.values.may_throw))) {
              this.player.setVelocityX(0);
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)
              this.player.state = 'jump_right_and_throw';
              this.throwFact(35, 450, -200)}
          else if (Phaser.Input.Keyboard.JustUp(this.key_right) || (!this.gamepad.right)){
              this.player.setVelocityX(0);}
            }

      if (this.player.state == 'jump_left'){
          this.player.anims.play('jump_left', true);
          if (this.player.body.touching.down || this.player.body.onFloor()){
              this.player.state = 'stand_left'}
          // else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){
          //     this.player.state = 'jump_left'}
          else if ((this.key_left.isDown && Phaser.Input.Keyboard.JustDown(this.key_throw)) || (this.gamepad.left && (this.gamepad.X && this.player.data.values.may_throw))) {
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)
              this.player.state = 'jump_left_and_throw'
              this.throwFact(-35, -450, -200)
              this.player.setVelocityX(-250);}
          else if (this.key_left.isDown || this.gamepad.left){
              this.player.setVelocityX(-250);}
          else if (this.key_right.isDown || (this.gamepad.right)){
              this.player.state = 'jump_right'}
          else if ((Phaser.Input.Keyboard.JustDown(this.key_throw)) || ((!this.gamepad.left) && (this.gamepad.X && this.player.data.values.may_throw))) {
              this.player.setVelocityX(0);
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)
              this.player.state = 'jump_left_and_throw';
              this.throwFact(-35, -450, -200)}
          else if (Phaser.Input.Keyboard.JustUp(this.key_left) || (!this.gamepad.left)){
              this.player.setVelocityX(0);}}

      if (this.player.state == 'stand_right_and_throw'){
          this.player.setVelocityX(0);
          this.player.anims.play('stand_right_and_throw', true);
          if (this.scene.time.now >= (this.scene.time_at_throw_start + 200)){
              this.player.state = 'stand_right'}}

      if (this.player.state == 'stand_left_and_throw'){
          this.player.setVelocityX(0);
          this.player.anims.play('stand_left_and_throw', true);
          if (this.scene.time.now >= (this.scene.time_at_throw_start + 200)){
              this.player.state = 'stand_left'}}

      if (this.player.state == 'jump_right_and_throw'){
          this.player.anims.play('jump_right_and_throw', true);
          if (this.scene.time.now >= (this.scene.time_at_throw_start + 200)){
              this.player.state = 'jump_right'}}

      if (this.player.state == 'jump_left_and_throw'){
          this.player.anims.play('jump_left_and_throw', true);
          if (this.scene.time.now >= (this.scene.time_at_throw_start + 200)){
              this.player.state = 'jump_left'}}

      if (this.player.state == 'hit_to_left'){
          this.player.anims.play('hit_to_left');
          if (this.key_left.isDown || this.gamepad.left){
              this.player.setVelocityX(-250);}
          else if (Phaser.Input.Keyboard.JustUp(this.key_left)){
          		this.player.setVelocityX(0);}
          else if ((this.key_right.isDown || this.gamepad.right) && (!this.player.body.touching.down && !this.player.body.onFloor())) {
              this.player.state = 'hit_to_right'}}

      if (this.player.state == 'hit_to_right'){
          this.player.anims.play('hit_to_right');
          if (this.key_right.isDown || this.gamepad.right){
              this.player.setVelocityX(250);}
          else if (Phaser.Input.Keyboard.JustUp(this.key_right)){
              this.player.setVelocityX(0);}
          else if ((this.key_left.isDown || this.gamepad.left) && (!this.player.body.touching.down && !this.player.body.onFloor())) {
              this.player.state = 'hit_to_left'}}

      if (this.player.state == 'duck_right'){
          this.player.setVelocityX(0);
          this.player.anims.play('duck_right', true);
          if (!this.gamepad.down && this.key_down.isUp){
              this.player.state = 'stand_right'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_throw) || (this.gamepad.X && this.player.data.values.may_throw)){
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)
              this.player.state = 'duck_right_and_throw'
              this.throwFact(35, 450, -200, this.scene.time.now)}
          else if (Phaser.Input.Keyboard.JustDown(this.key_left) || this.gamepad.left){
              this.player.state = 'duck_left'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_jump) || this.gamepad.A){
              this.player.setVelocityY(-750);
              this.scene.jump_sound.play()
              this.player.state = 'jump_right';}
          }

      if (this.player.state == 'duck_left'){
          this.player.setVelocityX(0);
          this.player.anims.play('duck_left', true);
          if (!this.gamepad.down && this.key_down.isUp){
              this.player.state = 'stand_left'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_throw) || (this.gamepad.X && this.player.data.values.may_throw)){
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)
              this.player.state = 'duck_left_and_throw'
              this.throwFact(-35, -450, -200, this.scene.time.now)}
          else if (Phaser.Input.Keyboard.JustDown(this.key_right) || this.gamepad.right){
              this.player.state = 'duck_right'}
          else if (Phaser.Input.Keyboard.JustDown(this.key_jump)  || this.gamepad.A) {
              this.player.setVelocityY(-750);
              this.scene.jump_sound.play()
              this.player.state = 'jump_left';}
          }

      if (this.player.state == 'duck_right_and_throw'){
          this.player.setVelocityX(0);
          this.player.anims.play('duck_right_and_throw', true);
          if (this.scene.time.now >= (this.scene.time_at_throw_start + 200)){
              this.player.state = 'duck_right'}}

      if (this.player.state == 'duck_left_and_throw'){
          this.player.setVelocityX(0);
          this.player.anims.play('duck_left_and_throw', true);
          if (this.scene.time.now >= (this.scene.time_at_throw_start + 200)){
              this.player.state = 'duck_left'}}

      this.player.data.values.may_jump = ((this.gamepad.A) ? false : true);
      this.player.data.values.may_throw = ((this.gamepad.X) ? false : true);

      if (Phaser.Input.Keyboard.JustDown(this.key_pause)){
        this.scene.scene.pause().launch('pause_scene', {prev: this.scene.scene_name,
                                                        left_key_pressed: this.key_left.isDown,
                                                        right_key_pressed: this.key_right.isDown,
                                                        up_key_pressed: this.key_up.isDown,
                                                        down_key_pressed: this.key_down.isDown,
                                                        left_button_pressed: this.key_left.isDown,
                                                        right_button_pressed: this.key_right.isDown,
                                                        up_button_pressed: this.key_up.isDown,
                                                        down_button_pressed: this.key_down.isDown})}
    }

    else { // no gamepad plugged in or no gamepad button key_pressed yet

      if (this.scene.game.use_game_properties){
        this.key_left.isDown = this.scene.game.left_key_pressed
        this.key_right.isDown = this.scene.game.right_key_pressed
        this.key_up.isDown = this.scene.game.up_key_pressed
        this.key_down.isDown = this.scene.game.down_key_pressed
        this.scene.game.use_game_properties = false
        this.scene.time.paused = false
      }

    if (this.player.state == 'stand_right'){                                                         ////
          this.player.setVelocityX(0);                                                         ////
          this.player.anims.play('stand_right', true);                                         ////
          if (!this.player.body.touching.down && !this.player.body.onFloor()){                    ////
              this.player.state = 'jump_right'}                                            ////
          else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
              this.player.setVelocityY(-750);
              this.scene.jump_sound.play()                                                             ////
              this.player.state = 'jump_right';}                                           ////
          else if (this.key_right.isDown){                                                     ////
              this.player.state = 'run_right'}                                             ////
          else if (this.key_left.isDown){                                                      ////
              this.player.state = 'run_left'}                                              ////
          else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)                         ////
              this.throwFact(35, 450, -200, this.scene.time.now)
              this.player.state = 'stand_right_and_throw'
              }
          else if (this.key_down.isDown){                                                      ////
              this.player.state = 'duck_right'}                                              ////
          }                                                                                   ////
                                                                                                ////
    if (this.player.state == 'stand_left'){                                                      ////
        this.player.setVelocityX(0);                                                         ////
        this.player.anims.play('stand_left', true);                                          ////
        if (!this.player.body.touching.down && !this.player.body.onFloor()){              ////
            this.player.state = 'jump_left'}                                             ////
        else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
            this.player.setVelocityY(-750);
            this.scene.jump_sound.play()                                                 ////
            this.player.state = 'jump_left';}                                            ////
        else if (this.key_left.isDown){                                                      ////
            this.player.state = 'run_left'}                                              ////
        else if (this.key_right.isDown){                                                     ////
            this.player.state = 'run_right'}                                             ////
        else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
            this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)                         ////
            this.player.state = 'stand_left_and_throw'
            this.throwFact(-35, -450, -200, this.scene.time.now)}
          else if (this.key_down.isDown){                                                      ////
              this.player.state = 'duck_left'}
        }                                 ////
                                                                                                ////
    if (this.player.state == 'run_right'){                                                       ////
        this.player.setVelocityX(250);                                                       ////
        this.player.anims.play('run_right', true);                                           ////
        if (!this.player.body.touching.down && !this.player.body.onFloor()){                                                    ////
            this.player.state = 'jump_right'}                                            ////
        else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
            this.player.setVelocityY(-750);
            this.scene.jump_sound.play()                                                        ////
            this.player.state = 'jump_right'}                                            ////
        else if (!this.key_right.isDown){                              ////
            this.player.state = 'stand_right'}                                           ////
        else if (this.key_left.isDown && this.key_right.isUp){                                    ////
            this.player.state = 'run_left'}                                              ////
        else if (this.key_left.isDown && this.key_right.isDown){                                  ////
            this.player.state = 'run_right'}                                             ////
        else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
            this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)                         ////
            this.player.state = 'stand_right_and_throw'
            this.throwFact(35, 450, -200, this.scene.time.now)}
        else if (this.key_down.isDown){                                                      ////
            this.player.state = 'duck_right'}
        }                                ////
                                                                                                ////
    if (this.player.state == 'run_left'){                                                        ////
        this.player.setVelocityX(-250);                                                      ////
        this.player.anims.play('run_left', true);                                            ////
        if (!this.player.body.touching.down && !this.player.body.onFloor()){                                                    ////
            this.player.state = 'jump_left'}                                             ////
        else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
            this.player.setVelocityY(-750);
            this.scene.jump_sound.play()                                                        ////
            this.player.state = 'jump_left'}                                             ////
        else if (!this.key_left.isDown) {                               ////
            this.player.state = 'stand_left'}                                            ////
        else if (this.key_right.isDown && this.key_left.isUp){                                    ////
            this.player.state = 'run_right'}                                             ////
        else if (this.key_left.isDown && this.key_right.isDown){                                  ////
            this.player.state = 'run_left'}                                              ////
        else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
            this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)                         ////
            this.player.state = 'stand_left_and_throw'
            this.throwFact(-35, -450, -200, this.scene.time.now)}
        else if (this.key_down.isDown){                                                      ////
            this.player.state = 'duck_left'}
        }                                 ////
                                                                                                ////
      if (this.player.state == 'jump_right'){
          this.player.anims.play('jump_right', true);                                          ////
          if (this.player.body.touching.down || this.player.body.onFloor()){                                                     ////
              this.player.state = 'stand_right'}                                           ////
          else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
              this.player.state = 'jump_right'}                                            ////
          else if (this.key_right.isDown && Phaser.Input.Keyboard.JustDown(this.key_throw)){        ////
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)                         ////
              this.player.state = 'jump_right_and_throw'
              this.throwFact(35, 450, -200, this.scene.time.now)                                 ////
              this.player.setVelocityX(250);}                                              ////
          else if (this.key_right.isDown){                                                     ////
              this.player.setVelocityX(250);}                                              ////
          else if (Phaser.Input.Keyboard.JustUp(this.key_right)){                              ////
              this.player.setVelocityX(0);}                                                ////
          // else if (Phaser.Input.Keyboard.JustDown(this.key_left)){                             ////
            else if (this.key_left.isDown){
              this.player.state = 'jump_left'}                                             ////
          else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)                         ////
              this.player.state = 'jump_right_and_throw'
              this.throwFact(35, 450, -200, this.scene.time.now)
            }}                                                                                  ////
                                                                                                  ////
      if (this.player.state == 'jump_left'){                                                       ////
          this.player.anims.play('jump_left', true);                                           ////
          if (this.player.body.touching.down || this.player.body.onFloor()){                     ////
              this.player.state = 'stand_left'}                                            ////
          else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
              this.player.state = 'jump_left'}                                             ////
          else if (this.key_left.isDown && Phaser.Input.Keyboard.JustDown(this.key_throw)){         ////
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)                         ////
              this.player.state = 'jump_left_and_throw'
              this.throwFact(-35, -450, -200, this.scene.time.now)                              ////
              this.player.setVelocityX(-250);}                                             ////
          else if (this.key_left.isDown){                                                      ////
              this.player.setVelocityX(-250);}                                             ////
          else if (Phaser.Input.Keyboard.JustUp(this.key_left)){                               ////
              this.player.setVelocityX(0);}                                                ////
          // else if (Phaser.Input.Keyboard.JustDown(this.key_right)){                            ////
          else if (this.key_right.isDown){
              this.player.state = 'jump_right'}                                            ////
          else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
              this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)                         ////
              this.player.state = 'jump_left_and_throw'
              this.throwFact(-35, -450, -200, this.scene.time.now)
            }}                                                                                ////
                                                                                                ////
    if (this.player.state == 'stand_right_and_throw'){                                           ////
        this.player.setVelocityX(0);                                                         ////
        this.player.anims.play('stand_right_and_throw', true);                               ////
        if (this.scene.time.now >= (this.scene.time_at_throw_start + 200)){                                        ////
            this.player.state = 'stand_right'}}                                          ////
                                                                                                ////
    if (this.player.state == 'stand_left_and_throw'){                                            ////
        this.player.setVelocityX(0);                                                         ////
        this.player.anims.play('stand_left_and_throw', true);                                ////
        if (this.scene.time.now >= (this.scene.time_at_throw_start + 200)){                                        ////
            this.player.state = 'stand_left'}}                                           ////
                                                                                                ////
    if (this.player.state == 'jump_right_and_throw'){                                            ////
        this.player.anims.play('jump_right_and_throw', true);                                ////
        if (this.scene.time.now >= (this.scene.time_at_throw_start + 200)){                                        ////
            this.player.state = 'jump_right'}}                                           ////
                                                                                                ////
    if (this.player.state == 'jump_left_and_throw'){                                             ////
        this.player.anims.play('jump_left_and_throw', true);                                 ////
        if (this.scene.time.now >= (this.scene.time_at_throw_start + 200)){                      ////
            this.player.state = 'jump_left'}}                                            ////
                                                                                                ////
    if (this.player.state == 'hit_to_left'){                                                     ////
        this.player.anims.play('hit_to_left');                                               ////
        if (this.key_left.isDown){                                                           ////
            this.player.setVelocityX(-250);}                                             ////
        else if (Phaser.Input.Keyboard.JustUp(this.key_left)){                            ////
        		this.player.setVelocityX(0);}                                                ////
        else if (this.key_right.isDown && (!this.player.body.touching.down && !this.player.body.onFloor())) {     ////
            this.player.state = 'hit_to_right'}}                                         ////
                                                                                                ////
    if (this.player.state == 'hit_to_right'){                                                    ////
        this.player.anims.play('hit_to_right');                                              ////
        if (this.key_right.isDown){                                                          ////
            this.player.setVelocityX(250);}                                              ////
        else if (Phaser.Input.Keyboard.JustUp(this.key_right)){                              ////
            this.player.setVelocityX(0);}                                                ////
        else if (this.key_left.isDown && (!this.player.body.touching.down && !this.player.body.onFloor())){     ////
            this.player.state = 'hit_to_left'}}                                          ////

    if (this.player.state == 'duck_right'){                                                         ////
        this.player.setVelocityX(0);                                                         ////
        this.player.anims.play('duck_right', true);
        if (!this.key_down.isDown){
            this.player.state = 'stand_right'}
        else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
            this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)                         ////
            this.player.state = 'duck_right_and_throw'
            this.throwFact(35, 450, -200, this.scene.time.now)}
        else if (Phaser.Input.Keyboard.JustDown(this.key_left)){                            ////
            this.player.state = 'duck_left'}
        else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
            this.player.setVelocityY(-750);
            this.scene.jump_sound.play()                                                        ////
            this.player.state = 'jump_right';}
        }

    if (this.player.state == 'duck_left'){                                                         ////
        this.player.setVelocityX(0);                                                         ////
        this.player.anims.play('duck_left', true);
        if (!this.key_down.isDown){
            this.player.state = 'stand_left'}
        else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
            this.scene.time_at_throw_start = Math.trunc(this.scene.time.now)                         ////
            this.player.state = 'duck_left_and_throw'
            this.throwFact(-35, -450, -200, this.scene.time.now)}
        else if (Phaser.Input.Keyboard.JustDown(this.key_right)){                            ////
            this.player.state = 'duck_right'}
        else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
            this.player.setVelocityY(-750);
            this.scene.jump_sound.play()                                                            ////
            this.player.state = 'jump_left';}
        }

      if (this.player.state == 'duck_right_and_throw'){                                           ////
          this.player.setVelocityX(0);                                                         ////
          this.player.anims.play('duck_right_and_throw', true);                               ////
          if (this.scene.time.now >= (this.scene.time_at_throw_start + 200)){                  ////
              this.player.state = 'duck_right'}}

      if (this.player.state == 'duck_left_and_throw'){                                           ////
          this.player.setVelocityX(0);                                                         ////
          this.player.anims.play('duck_left_and_throw', true);                               ////
          if (this.scene.time.now >= (this.scene.time_at_throw_start + 200)){                ////
              this.player.state = 'duck_left'}}


      if (Phaser.Input.Keyboard.JustDown(this.key_pause)){
        this.scene.scene.pause().launch('pause_scene', {prev: this.scene.scene_name,                          ////
                                                        left_key_pressed: this.key_left.isDown,               ////
                                                        right_key_pressed: this.key_right.isDown,             ////
                                                        up_key_pressed: this.key_up.isDown,               ////
                                                        down_key_pressed: this.key_down.isDown,           ////
                                                        cur_scene: this.scene})                  ////
      }

    } // no gamepad                                                                          ////

    if (this.player.data.values.health <= 0){
        this.player.data.values.knock_back_time = 10000}

    this.knockPlayerBack(this.scene.manual_time)

    if (this.player.data.values.post_hit_invincibility == true) {
    		this.scene.physics.world.removeCollider(this.scene.player_bs);
	      this.scene.physics.world.removeCollider(this.scene.player_censor);
        }

    //turn off post hit-invincibility
    if (Math.trunc(this.scene.manual_time) > (Math.trunc(this.player.data.values.time_at_damage) + 2000)) {
        this.player.data.values.post_hit_invincibility = false
        this.player.setAlpha(1)}

    //knock player off screen
    if (this.player.data.values.health <= 0){
        if (this.scene.manual_time < (this.player.data.values.time_at_collision + 10000)) {
            this.player.setPosition(this.player.x, this.player.y-5, this.player.z, this.player.w)
            if (this.player.x <= this.player.data.values.hit_from){
              this.player.setAngularVelocity(-400)}
            else{
              this.player.setAngularVelocity(400)}
          }
        for (var i=0;i<this.scene.layers.length;i++){
            this.scene.physics.world.removeCollider(this.player_layer[i]);}
        }

      if (this.player.y > ((this.scene.layers.layer.height*2)+100)){
        if (this.scene.scene.key == 'level5' && this.player.x > 24000 && this.player.data.values.health > 0) {
          this.scene.time.delayedCall(400, function() { // wait 400 ms, then fade
              this.scene.cameras.main.fade(1050);
          }, [], this);
          this.scene.time.delayedCall(1400, function() {
              this.scene.music.stop()
              this.scene.scene.start('cutscene6p1');
          }, [], this);}
        else{
        if (!this.crash_played){
          this.scene.crash.play()
          if (this.scene.music){
            this.scene.music.stop()}
          this.crash_played = true}
        this.scene.cameras.main.shake(500);
        this.scene.time.delayedCall(400, function() { // wait 400 ms, then fade
            this.scene.cameras.main.fade(300); // fade effect lasts 300 ms
        }, [], this);
        this.scene.time.delayedCall(800, function() {
            this.scene.scene.start(); // restart game
        }, [], this);}}

    if (this.scene.manual_time > this.player.data.values.splat_time+5000){
        this.player.setTint(0xffffff);}

} //update

throwFact(x_spawn, x_velocity, y_velocity){
    var y_spawn
    if (this.player.state.includes('duck')){
        y_spawn = this.player.y+20}
    else {
        y_spawn = this.player.y-10}
		this.fact = new Fact(this.scene, this, x_spawn, y_spawn, x_velocity, y_velocity)
    var whoosh_num = Math.floor(Math.random() * this.scene.whooshes.length)
    this.scene.whooshes[whoosh_num].play();
    // console.log(this.player.x)
    // console.log(this.player.y)
    // console.log(this.scene.cp)
    // console.log('')
	  }

damagePlayer(player, scene, damage){
		if (player.player.data.values.post_hit_invincibility){
				return}
		player.player.data.values.health -= damage;
		if (player.player.data.values.health > 0){
				player.player.setAlpha(0.5)}  // Opacity
    if (player.player.data.values.health >= 18){
        player.life_bar.setFrame(18)}
    else if (player.player.data.values.health <= 0){
        player.life_bar.setFrame(0)}
    else{
		    player.life_bar.setFrame(player.player.data.values.health)}
    if (player.player.data.values.health > 0){
		    player.player.data.values.post_hit_invincibility = true;}
		player.player.data.values.time_at_damage = scene.manual_time
		}

healPlayer(player, juice, amount, self){
		juice.destroy()
		player.player.data.values.health += amount;
		if(player.player.data.values.health > 18){
				player.player.data.values.health = 18}
		player.life_bar.setFrame(player.player.data.values.health)
		}

knockPlayerBack(now){

		if (this.player.data.values.time_at_collision && (now < (this.player.data.values.time_at_collision + this.player.data.values.knock_back_time))) {
				if (this.player.x <= this.player.data.values.hit_from){
						this.player.setVelocityX(-150)
						this.player.state = 'hit_to_left'}
				else{
						this.player.setVelocityX(150)
						this.player.state = 'hit_to_right'}}
    else if (now >= (this.player.data.values.time_at_collision + 200)  // 200 miliseconds after hit
    					&& this.player.state.includes('hit')
    					&& (this.player.body.onFloor() || this.player.body.touching.down)) {
    		this.resetPlayerState()}
    	}

resetPlayerState(){
		if (this.player.body.onFloor()){
			if (this.player.data.values.hit_from < this.player.x){
					this.player.state = 'stand_left'}
			else {
					this.player.state = 'stand_right'}}
		else{
			if (this.player.data.values.hit_from < this.player.x){
					this.player.state = 'jump_left'}
			else {
					this.player.state = 'jump_right'}}
		}

}
