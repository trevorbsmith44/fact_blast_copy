export default class Barrier {

constructor(scene, player){
    scene.barriers = scene.physics.add.group();
    this.scene = scene
    this.player = player
    this.scene.barrier_array = []
    this.scene.barrier_count = 0
}

addBarrier(scene, x, y, x_scale, y_scale, transparent) {
    var barrier = scene.add.tileSprite(x, y, 32 * x_scale, 32 * y_scale, 'platform');
    scene.barrier_array.push(barrier)
    this.scene.physics.add.existing(barrier, false);
    scene.physics.add.collider(barrier, this.player.player, function(barrier, player){
        barrier.body.immovable = true
        player.body.immovable = false
    });

		barrier.setActive(true).setVisible(true);
		barrier.setScale(2);
    barrier.setDepth(10);
    barrier.setFrame(4)
    barrier.state = 'forward'
    barrier.body.setAllowGravity(false)
    barrier.body.immovable = true

    barrier.setData({'id': this.scene.barrier_count,
                      'x_spawn': x,
                      'y_spawn': y,
                    });

    barrier.body.setVelocityX(0);
    barrier.body.setVelocityY(0);
    this.scene.barrier_count += 1

    if (transparent){
      barrier.setVisible(false)
    }
}

}
