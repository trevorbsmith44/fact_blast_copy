import Demonetization from  "./demonetization.js";

export default class SmallBot {

constructor(scene, player){
    scene.small_bots = scene.physics.add.group();
    this.scene = scene
    this.player = player

    this.player_small_bot = scene.physics.add.overlap(this.player.player, scene.small_bots, function(player, small_bot){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = small_bot.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 4)
        small_bot.body.immovable = true;
        }, function(player, small_bot){
            return (small_bot.data.values.health > 0
              && this.player.player.data.values.post_hit_invincibility == false)
        }, this);

}

update(player){
  this.scene.small_bots.children.each(function(small_bot) {
      if (small_bot.active){
          if (small_bot.state.includes('left')){
              small_bot.torch.setPosition(small_bot.x+3, small_bot.y+75)}
          else {
              small_bot.torch.setPosition(small_bot.x-3, small_bot.y+75)}
          small_bot.torch.anims.play('burn', true)
        if (small_bot.state == 'hover_left'){
                small_bot.setFrame(0);
                if (small_bot.data.values.moves){
                    small_bot.setVelocityX(-50)}
                else{
                    small_bot.setVelocityX(0)}
            if (small_bot.x < player.x+10 && small_bot.x>player.x-10){
                small_bot.setVelocityX(0)}
            if (small_bot.x < player.x-10){
                small_bot.state = 'hover_right'}
            if (this.scene.manual_time > small_bot.data.values.last_throw + small_bot.data.values.throw_wait){
                small_bot.time_at_throw_start = Math.trunc(this.scene.manual_time)
                small_bot.previous_state = small_bot.state
                small_bot.setData('throw_wait', Phaser.Math.Between(2000, 2000))
                this.scene.time.delayedCall(300, function() {
                  if (small_bot.data.values.health <= 0){
                    return}
                    var d = new Demonetization(this.scene.scene, small_bot, this.player, -80, -300, -50)
                    d.update()
                    }, [], this.scene);
                small_bot.state = 'throw_left'}
            }

      else  if (small_bot.state == 'hover_right'){
                small_bot.setFrame(4);
                if (small_bot.data.values.moves){
                    small_bot.setVelocityX(50)}
                else{
                    small_bot.setVelocityX(0)}
            if (small_bot.x < player.x+10 && small_bot.x>player.x-10){
                small_bot.setVelocityX(0)}
            if (small_bot.x > player.x+10){
                small_bot.state = 'hover_left'}
            if (this.scene.manual_time > small_bot.data.values.last_throw + small_bot.data.values.throw_wait){
                small_bot.time_at_throw_start = Math.trunc(this.scene.manual_time)
                small_bot.previous_state = small_bot.state
                small_bot.setData('throw_wait', Phaser.Math.Between(2000, 2000))
                this.scene.time.delayedCall(300, function() {
                    if (small_bot.data.values.health <= 0){
                      return}
                    var d = new Demonetization(this.scene.scene, small_bot, this.player, 80, 300, 50)
                    d.update()
                    }, [], this.scene);
                small_bot.state = 'throw_right'}
              }

        else if (small_bot.state == 'throw_left'){
            small_bot.data.values.last_throw = this.scene.manual_time;
            // small_bot.setVelocityX(0);
            small_bot.anims.play('small_bot_throw_left', true)
            if (this.scene.manual_time >= (small_bot.time_at_throw_start + 575)){
                small_bot.state = small_bot.previous_state}
            }

        else if (small_bot.state == 'throw_right'){
            small_bot.data.values.last_throw = this.scene.manual_time;
            // small_bot.setVelocityX(0);
            small_bot.anims.play('small_bot_throw_right', true)
            if (this.scene.manual_time >= (small_bot.time_at_throw_start + 575)){
                small_bot.state = small_bot.previous_state}
            }

        else if (small_bot.state == 'defeated_right'){
            small_bot.setFrame(7)
            small_bot.setAngularVelocity(-100)
            small_bot.setVelocityX(-300)
            small_bot.body.setAllowGravity(true)
            this.scene.time.delayedCall(500, function() {
              try {
                  this.physics.world.removeCollider(small_bot.data.values.layer_collision)}
              catch (TypeError){
                  console.log('Caught Small Bot TypeError')}
                }, [], this.scene);
            }

        else if (small_bot.state == 'defeated_left'){
            small_bot.setFrame(3)
            small_bot.setAngularVelocity(100)
            small_bot.setVelocityX(300)
            small_bot.body.setAllowGravity(true)
            this.scene.time.delayedCall(500, function() {
              try {
                  this.physics.world.removeCollider(small_bot.data.values.layer_collision)}
              catch (TypeError){
                  console.log('Caught Small Bot TypeError')}
                }, [], this.scene);
            }

        if (small_bot.data.values.health <= 0){
            small_bot.torch.setVisible(false);
            if (small_bot.state.includes('right')){
                small_bot.state = 'defeated_right'}
            else if (small_bot.state.includes('left')){
                small_bot.state = 'defeated_left'}
            }

        if (small_bot.y > ((this.scene.layers.layer.height*2)+1000)){
            small_bot.destroy()
          }

      } // if small_bot active
    }, this); // looping through all small_bots
} // update

addSmallBot(scene, x, y, moves) {
		var small_bot = scene.small_bots.create(x, y, 'small_bot');
		small_bot.setActive(true).setVisible(true)
		small_bot.setData({'health': 1,
								 'time_at_collision':null,
								 'knock_back_time': 150,
								 'bs_thrown': 0,
								 // 'layer_collision': scene.physics.add.collider(small_bot, scene.layers.layer),
						 		 'last_throw': 0,
                 'throw_wait': Phaser.Math.Between(2000, 2000),
                 'moves': moves
							 });

    small_bot.body.immovable = true;
    small_bot.setScale(1.4)
    small_bot.setOffset(41,18) //y=25
		small_bot.setSize(46, 80, false) //h=156
    small_bot.setDepth(500);

    small_bot.state = 'hover_left'

		scene.small_bots.add(small_bot)
    small_bot.body.setAllowGravity(false);
    scene.enemies_added += 1;

    small_bot.torch = scene.physics.add.sprite(x, y, 'torch');
    small_bot.torch.setScale(2.25);
    small_bot.torch.setSize(42,50)
    small_bot.torch.body.setAllowGravity(false);
    small_bot.torch.setDepth(400);
}

}
