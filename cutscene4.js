export default class CutScene4 extends Phaser.Scene {

constructor() {
  	super({key: "cutscene4"});
}

create(){
  this.current_image = 1
  this.c4i1 = this.add.image(600, 300, 'c4i1')
  this.add.text(980, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
  this.may_skip = true
  this.input.gamepad.once('down', function (pad, button, index) {
			this.gamepad = pad;
    }, this);

  this.key_skip = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
  this.explosion_sound = this.sound.add('explosion')

  this.music = this.sound.add('cutscene_soundtrack');
  var music_config = {
    mute: false,
    volume: 0.3,
    rate: 1,
    detune: 0,
    seek: 0,
    loop: true,
    delay: 0
  }
  this.music.play(music_config)
}

update(){
  if (Phaser.Input.Keyboard.JustDown(this.key_skip)) { //  || (this.gamepad.A)
    if (this.current_image == 1){
      if (!this.explosion_played){
        this.explosion_sound.play()
        this.explosion_played = true}
      this.add.image(600, 300, 'c4i2')
      this.add.text(980, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 2}
    else if (this.current_image == 2){
      this.add.image(600, 300, 'c4i3')
      this.add.text(980, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 3}
    else if (this.current_image == 3){
      this.add.image(600, 300, 'c4i4')
      this.current_image = 4}
    else if (this.current_image == 4){
      this.music.stop()
      this.scene.start('level4')}
      }

  if (this.gamepad){
    if (this.gamepad.A && this.may_skip) {
      if (this.current_image == 1){
        if (!this.explosion_played){
          this.explosion_sound.play()
          this.explosion_played = true}
        this.add.image(600, 300, 'c4i2')
        this.add.text(980, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 2}
      else if (this.current_image == 2){
        this.add.image(600, 300, 'c4i3')
        this.add.text(980, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 3}
      else if (this.current_image == 3){
        this.add.image(600, 300, 'c4i4')
        this.current_image = 4}
      else if (this.current_image == 4){
        this.music.stop()
        this.scene.start('level4')}}
    this.may_skip = ((this.gamepad.A) ? false : true);
  }
}

} // class

//c3i1








// c4i1
// this.bg_sky = this.add.tileSprite(0, 0, 9000, 7000, "level4_sky");
// this.bg_sky.setOrigin(0, 0.05);
// this.bg_sky.setTileScale(2.5,2.5);
// this.bg_3 = this.add.tileSprite(0, 0, 1200, 800, "level4_bg3");
// this.bg_3.setOrigin(0, 0);
// this.bg_3.setScrollFactor(0); // repeats background
// this.bg_3.setPosition(0, -200)
// this.bg_3.setTileScale(1.5,1.5);
// this.bg_2 = this.add.tileSprite(0, 1000, 1200, 800, "level4_bg2");
// this.bg_2.setOrigin(0, 0);
// this.bg_2.setScrollFactor(0); // repeats background
// this.bg_2.setPosition(0, -200)
// this.bg_2.setTileScale(1.5,1.5);
// this.bg_1 = this.add.tileSprite(0, 1000, 1200, 800, "level4_bg1");
// this.bg_1.setOrigin(0, 0);
// this.bg_1.setScrollFactor(0); // repeats background
// this.bg_1.setPosition(0, -125)
// this.bg_1.setTileScale(1.1,1.1);
//
// this.bottom_bar = this.add.image(600, 210, 'road').setScale(2.5)
// this.bottom_bar = this.add.image(900, 360, 'wind').setScale(0.5).setFlip(true, false)
// this.bottom_bar = this.add.image(1040, 350, 'wind').setScale(0.3).setFlip(true, true).setAngle(5)
//
// this.player = this.physics.add.sprite(570, 340, 'jeremy').setScale(1.5);
// this.player.body.setAllowGravity(false)
// this.player.setFrame(25)
//
// this.van = this.physics.add.sprite(600, 360, 'van').setScale(3).setFrame(1).body.setAllowGravity(false)
// this.wheel1 = this.physics.add.sprite(500, 440, 'car_wheel').setFrame(6).setScale(2.5).body.setAllowGravity(false)
// this.wheel2 = this.physics.add.sprite(700, 440, 'car_wheel').setFrame(6).setScale(2.5).body.setAllowGravity(false)
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
// this.text = this.add.text(210, 510, "With the birds cleared out, Jeremy is", { fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(200, 545, "California bound. All seems safe until...", { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)









//c4i2
// this.bg_sky = this.add.tileSprite(0, 0, 9000, 7000, "level4_sky");
// this.bg_sky.setOrigin(0, 0.05);
// this.bg_sky.setTileScale(2.5,2.5);
// this.bg_3 = this.add.tileSprite(0, 0, 1500, 800, "level4_bg3");
// this.bg_3.setOrigin(0, 0);
// this.bg_3.setScrollFactor(0); // repeats background
// this.bg_3.setPosition(-100, -200)
// this.bg_3.setTileScale(1.5,1.5);
// this.bg_2 = this.add.tileSprite(0, 1000, 1200, 800, "level4_bg2");
// this.bg_2.setOrigin(0, 0);
// this.bg_2.setScrollFactor(0); // repeats background
// this.bg_2.setPosition(0, -200)
// this.bg_2.setTileScale(1.5,1.5);
// this.bg_1 = this.add.tileSprite(0, 1000, 1200, 800, "level4_bg1");
// this.bg_1.setOrigin(0, 0);
// this.bg_1.setScrollFactor(0); // repeats background
// this.bg_1.setPosition(0, -75)
// this.bg_1.setTileScale(1.1,1.1);
//
// this.physics.add.sprite(700, 240, 'jeremy').setScale(0.5).setFrame(21).setAngle(-40).setAlpha(0.2).body.setAllowGravity(false)
// this.physics.add.sprite(740, 210, 'jeremy').setScale(0.7).setFrame(21).setAngle(0).setAlpha(0.3).body.setAllowGravity(false)
// this.physics.add.sprite(800, 180, 'jeremy').setScale(0.9).setFrame(21).setAngle(40).setAlpha(0.4).body.setAllowGravity(false)
// this.physics.add.sprite(900, 150, 'jeremy').setScale(1.1).setFrame(21).setAngle(80).setAlpha(0.5).body.setAllowGravity(false)
// this.physics.add.sprite(1000, 180, 'jeremy').setScale(1.3).setFrame(21).setAngle(120).setAlpha(0.6).body.setAllowGravity(false)
// this.physics.add.sprite(1100, 210, 'jeremy').setScale(1.5).setFrame(21).setAngle(140).setAlpha(1).body.setAllowGravity(false)
//
// this.physics.add.sprite(350, 300, 'explosion').setScale(3.25).setFrame(10).body.setAllowGravity(false)
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
// this.text = this.add.text(180, 510, "BOOM! A mysterious explosion sets his car", { fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(210 , 545, "ablaze as he passes through the desert!", { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)

















//c4i3
// this.bg_sky = this.add.tileSprite(0, 0, 9000, 7000, "level4_sky");
// this.bg_sky.setOrigin(0, 0.05);
// this.bg_sky.setTileScale(2.5,2.5);
// this.bg_4 = this.add.tileSprite(0, 0, 1500, 800, "level4_bg3");
// this.bg_4.setOrigin(0, 0);
// this.bg_4.setScrollFactor(0); // repeats background
// this.bg_4.setPosition(-250, -100)
// this.bg_4.setTileScale(1.5,1.5);
// this.bg_3 = this.add.tileSprite(0, 0, 1500, 800, "level4_bg3");
// this.bg_3.setOrigin(0, 0);
// this.bg_3.setScrollFactor(0); // repeats background
// this.bg_3.setPosition(200, -150)
// this.bg_3.setTileScale(1.5,1.5);
// this.bg_2 = this.add.tileSprite(0, 1000, 1200, 800, "level4_bg2");
// this.bg_2.setOrigin(0, 0);
// this.bg_2.setScrollFactor(0); // repeats background
// this.bg_2.setPosition(0, -225)
// this.bg_2.setTileScale(1.6,1.6);
// this.bg_1 = this.add.tileSprite(0, 1000, 1200, 800, "level4_bg1");
// this.bg_1.setOrigin(0, 0);
// this.bg_1.setScrollFactor(0); // repeats background
// this.bg_1.setPosition(0, -110)
// this.bg_1.setTileScale(1.2,1.2);
//
// this.add.image(320, 350, 'jeremy_straight_face').setScale(5)
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
// this.text = this.add.text(50, 510, "Stranded, Jeremy can hear the sound of vile machinery", { fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(75 , 545, "nearby, this land is plagued by an evil algorithm.", { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)
