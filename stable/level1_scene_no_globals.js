export default class Level1 extends Phaser.Scene {

	constructor() {
	  super("level1");
	}

preload (){
  	this.load.image('bs', 'assets/bs/bs.png');
		this.load.image('bs2', 'assets/bs/bs2.png');
		this.load.image('bs3', 'assets/bs/bs3.png');
		this.load.image('bs4', 'assets/bs/bs4.png');
		this.load.image('bs5', 'assets/bs/bs5.png');
		this.load.image('bs6', 'assets/bs/bs6.png');
		this.load.image('bs7', 'assets/bs/bs7.png');
		this.load.image('bs8', 'assets/bs/bs8.png');
		this.load.image('bs9', 'assets/bs/bs9.png');
		this.load.image('bs10', 'assets/bs/bs10.png');
		this.load.image('bs11', 'assets/bs/bs11.png');
		this.load.image('bs12', 'assets/bs/bs12.png');
		this.load.image('bs13', 'assets/bs/bs13.png');
		this.load.image('censored', 'assets/bs/censored.png');

		this.load.image('fact', 'assets/fact2.png');
		this.load.image('star', 'assets/star.png');
    this.load.spritesheet('jeremy', 'assets/jeremy.png', { frameWidth: 66, frameHeight: 96 });
		this.load.spritesheet('npc', 'assets/enemies/npc.png', { frameWidth: 64, frameHeight: 96 });
		this.load.spritesheet('bird', 'assets/enemies/bird.png', { frameWidth: 100, frameHeight: 72 });
		this.load.spritesheet('tomato', 'assets/enemies/tomato.png', { frameWidth: 180, frameHeight: 150 });
		this.load.spritesheet('life_bar', 'assets/life_bar.png', { frameWidth: 280, frameHeight: 50 });
		this.load.spritesheet('juice', 'assets/apple_juice.png', { frameWidth: 26, frameHeight: 34 });
    this.load.tilemapTiledJSON("map", "assets/maps/level3.json")
    this.load.image("tiles", "assets/futureish.png");
    }

create (){
    this.map = this.add.tilemap("map");
    var tileset = this.map.addTilesetImage("tileset", "tiles");
    this.layer = this.map.createStaticLayer(0, tileset, 0, 0); // layer index, tileset, x, y
    this.layer.setCollisionByProperty({ collides: true }); // tiles w/ collides set to true (in Tiled) will collide
		this.player = this.physics.add.sprite(400, 20, 'jeremy').setScale(1.5);
		this.player.setData({'health': 18,
										'post_hit_invincibility': false,
										'knock_back_time': 200,
										'time_at_collision':null,
										'time_at_damage':null,
										'may_jump': true,
										'may_throw': true,
										'hit_from': null})
		this.player.state = 'jump_right'
		this.player.setSize(37, 95, false)
		// player.setTint(Phaser.Display.Color.RandomRGB().color)  // no idea why this doesn't work
		this.player.setDepth(900);
		this.player.setOffset(15,0)
		this.player.setGravityY(250)

		//LIFE BAR
		this.life_bar = this.physics.add.sprite(140, 30, 'life_bar').setDepth(1000);
		this.life_bar.body.setAllowGravity(false);
		this.life_bar.setScrollFactor(0,0)
		this.life_bar.setFrame(18)

		//GAMEPAD
		this.input.gamepad.once('down', function (pad, button, index) {
				this.gamepad = pad;
				}, this);

		//JEREMY ANIMATIONS
    this.anims.create({
        key: 'run_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 10, frames: [10,11,12,13] }),
        frameRate: 10,
        repeat: -1 // tells the animation to loop
        });
    this.anims.create({
        key: 'jump_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 17, end: 17 }),
        });
    this.anims.create({
        key: 'stand_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 3, end: 3 }),
        });
    this.anims.create({
        key: 'run_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 6, frames: [6,7,8,9] }),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'jump_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 14, end: 14 }),
        });
		this.anims.create({
        key: 'jump_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 15, frames: [15,16,16,16,14] }),
				frameRate: 10,
        });
		this.anims.create({
				key: 'jump_left_and_throw',
				frames: this.anims.generateFrameNumbers('jeremy', { start: 18, frames: [18,19,19,19,17] }),
				frameRate: 10,
				});
    this.anims.create({
        key: 'stand_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 0, end: 0 }),
        });
		this.anims.create({
        key: 'stand_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 1, frames: [1,2,2,2,0] }),
				frameRate: 10,
				repeat: 0,
        });
		this.anims.create({
				key: 'stand_left_and_throw',
				frames: this.anims.generateFrameNumbers('jeremy', { start: 4, frames: [4,5,5,5,3] }),
				frameRate: 10,
				repeat: 0
				});
		this.anims.create({
				key: 'hit_to_left',
				frames: this.anims.generateFrameNumbers('jeremy', { start: 20, end: 20 }),
				});
		this.anims.create({
				key: 'hit_to_right',
				frames: this.anims.generateFrameNumbers('jeremy', { start: 21, end: 21 }),
				});

		//////////////////////////////////////////
		// NPC ANIMATIONS
		this.anims.create({
        key: 'npc_walk_left',
        frames: this.anims.generateFrameNumbers('npc', { start: 4, frames: [3,4,5,4] }),
        frameRate: 6,
        repeat: -1
        });
		this.anims.create({
				key: 'npc_walk_right',
				frames: this.anims.generateFrameNumbers('npc', { start: 1, frames: [0,1,2,1] }),
				frameRate: 6,
				repeat: -1
				});
		this.anims.create({
				key: 'npc_stand_right',
				frames: this.anims.generateFrameNumbers('npc', { start: 1, end: 1 }),
				});
		this.anims.create({
				key: 'npc_stand_left',
				frames: this.anims.generateFrameNumbers('npc', { start: 4, end: 4 }),
				});
		this.anims.create({
				key: 'npc_hit_to_right',
				frames: this.anims.generateFrameNumbers('npc', { start: 6, end: 6 }),
				});
		this.anims.create({
				key: 'npc_hit_to_left',
				frames: this.anims.generateFrameNumbers('npc', { start: 7, end: 7 }),
				});

		// BIRD ANIMATIONS
		this.anims.create({
				key: 'bird_fly_left',
				frames: this.anims.generateFrameNumbers('bird', { start: 2, frames: [2,3] }),
				frameRate: 6,
				repeat: -1
				});
		this.anims.create({
				key: 'bird_fly_right',
				frames: this.anims.generateFrameNumbers('bird', { start: 1, frames: [0,1] }),
				frameRate: 6,
				repeat: -1
				});
		this.anims.create({
				key: 'bird_hit_to_right',
				frames: this.anims.generateFrameNumbers('bird', { start: 5, end: 5 }),
				});
		this.anims.create({
				key: 'bird_hit_to_left',
				frames: this.anims.generateFrameNumbers('bird', { start: 4, end: 4 }),
				});

		// TOMATO ANIMATIONS
		this.anims.create({
				key: 'tomato_run_right',
				frames: this.anims.generateFrameNumbers('tomato', { start: 5, frames: [5,6,7,8] }),
				frameRate: 10,
				repeat: -1
				});
		this.anims.create({
				key: 'tomato_run_left',
				frames: this.anims.generateFrameNumbers('tomato', { start: 0, frames: [0,1,2,3] }),
				frameRate: 10,
				repeat: -1
				});
		this.anims.create({
				key: 'tomato_jump_right',
				frames: this.anims.generateFrameNumbers('tomato', { start: 9, end: 9 }),
				});
		this.anims.create({
				key: 'tomato_jump_left',
				frames: this.anims.generateFrameNumbers('tomato', { start: 4, end: 4 }),
				});
		this.anims.create({
				key: 'tomato_roll_left',
				frames: this.anims.generateFrameNumbers('tomato', { start: 10, end: 10 }),
				});
		this.anims.create({
				key: 'tomato_roll_right',
				frames: this.anims.generateFrameNumbers('tomato', { start: 12, end: 12 }),
				});

		this.npc_enemies = this.physics.add.group();
		this.addNPC(this, 800, 0)
		// this.addNPC(this, 1500, 0)
		this.addNPC(this, 1800, 0)
		// this.addNPC(this, 2400, 0)
		this.addNPC(this, 3500, 0)
		// this.addNPC(this, 3800, 0)
		this.addNPC(this, 4200, 0)
		// this.addNPC(this, 4800, 0)
		this.addNPC(this, 5300, 0)

		this.birds = this.physics.add.group();
		this.addBird(this, -2000, 50)
		this.addBird(this, 800, 100)
		// this.addBird(this, -800, 150)
		// this.addBird(this, 1800, 75)

		this.juice = this.physics.add.group();
		this.addJuice(this, 800, 0)
		this.addJuice(this, 4000, 0)

		this.tomatoes = this.physics.add.group();
		this.addTomato(this, 1000, 200)
		this.addTomato(this, 3500, 200)

    this.key_left = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    this.key_right = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
		this.key_up = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
    this.key_jump = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.L);
		this.key_throw = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.K);

    this.player_layer = this.physics.add.collider(this.player, this.layer);
    this.cameras.main.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);
    this.cameras.main.startFollow(this.player);

		this.facts = this.physics.add.group();
		this.physics.add.collider(this.layer, this.facts, this.dissolveFact, null, this);

		this.bs_statements = this.physics.add.group()
		this.physics.add.collider(this.layer, this.bs_statements, this.dissolveBS, null, this);
		this.physics.add.overlap(this.facts, this.bs_statements, this.facts_destroy_bs, null, this);

		this.censorships = this.physics.add.group()
		this.physics.add.collider(this.layer, this.censorships,
				// function(layer, censor){censor.destroy()}
				this.dissolveCensorship
				, null, this);
		this.physics.add.overlap(this.facts, this.censorships, this.facts_destroy_bs, null, this);


		// DAMAGE PLAYER AND SET INVINCIBILITY
		this.player_npc = this.physics.add.collider(this.player, this.npc_enemies, function(player, npc){
				this.player.data.values.time_at_collision = this.time.now
				this.player.data.values.hit_from = npc.x
				this.damagePlayer(this.player, this, 4)
				}, function(player, npc){
						return (npc.data.values.health > 0
							&& this.player.data.values.post_hit_invincibility == false)
				}, this);

		this.player_bird = this.physics.add.collider(this.player, this.birds, function(player, bird){
				this.player.data.values.time_at_collision = this.time.now
				this.player.data.values.hit_from = bird.x
				bird.setVelocityY(0)
				this.damagePlayer(this.player, this, 3)
				}, function(player, bird){
						return (bird.data.values.health > 0
							&& this.player.data.values.post_hit_invincibility == false)
				}, this);

		this.player_tomato = this.physics.add.collider(this.player, this.tomatoes, function(player, tomato){
				this.player.data.values.time_at_collision = this.time.now
				this.player.data.values.hit_from = tomato.x
				this.damagePlayer(this.player, this, 4)
				tomato.destroy()
				}, function(player, tomato){
						return (tomato.data.values.health > 0
							&& this.player.data.values.post_hit_invincibility == false)
				}, this);

		this.player_juice = this.physics.add.overlap(this.player, this.juice, function(player, juice){
				this.healPlayer(player, juice, 4, this)
				}, null, this);

		this.physics.add.overlap(this.facts, this.npc_enemies, function(fact, npc){
				fact.destroy()
				npc.data.values.time_at_collision = this.time.now
				npc.data.values.health -= 1;
				if (npc.data.values.health <= 0){
						if (this.time.now < (npc.data.values.time_at_collision + 5000)) {
								npc.data.values.knock_back_time = 5000
								if (this.player.x <= npc.x){
										npc.setAngularVelocity(400)}
								else{
										npc.setAngularVelocity(-400)}}
						this.physics.world.removeCollider(npc.data.values.layer_collision);
						return}
		}, null, this);

		this.physics.add.overlap(this.facts, this.birds, function(fact, bird){
				fact.destroy()
				bird.data.values.health -= 1;
				if (bird.data.values.health <= 0){
						bird.body.setAllowGravity(true);
						if (this.player.x <= bird.x){
								bird.setAngularVelocity(400)}
						else{
								bird.setAngularVelocity(-400)}
						return}
		}, null, this);

}

addNPC(self, x, y) {
		// npc = this.npc_enemies.create(Phaser.Math.Between(0, 800), Phaser.Math.Between(0, 300));
		self.npc = self.npc_enemies.create(x,y, 'npc');
		var npc = self.npc
		npc.setActive(true).setVisible(true)
		npc.setData({'health': 4,
								 'time_at_collision':null,
								 'knock_back_time': 300,
								 'bs_thrown': 0,
								 'layer_collision': self.physics.add.collider(npc, self.layer),
								 'throw_wait': Phaser.Math.Between(3000, 6000),
						 		 'last_throw': self.time.now
							 });
		npc.setSize(40, 95, false)
		npc.setOffset(15,0)
		npc.setScale(1.4).setTint(Phaser.Display.Color.RandomRGB().color)
		self.npc_enemies.add(npc)
}

addBird(self, x, y) {
		self.bird = self.birds.create(x,y, 'bird');
		var bird = self.bird
		bird.setActive(true).setVisible(true)
		bird.setData({'health': 1,
								 'throw_wait': Phaser.Math.Between(3000, 6000),
						 		 'last_throw': self.time.now,
								 'speed': Phaser.Math.Between(275,325)
							 });
		bird.setSize(80, 50, false)
		bird.setOffset(15,5)
		bird.state = 'fly_right'
		self.birds.add(bird)
		bird.body.setAllowGravity(false);
}

addTomato(self, x, y) {
		self.tomato = self.tomatoes.create(x,y, 'tomato');
		var tomato = self.tomato
		tomato.setActive(true).setVisible(true)
		tomato.setData({'health': 4,
								 'time_at_collision':null,
								 'knock_back_time': 300,
								 'layer_collision': self.physics.add.collider(tomato, this.layer),
								 'jump_wait': Phaser.Math.Between(3000, 6000),
						 		 'last_jump': self.time.now,
								 'last_turned_left': 0,
								 'last_turned_right': 0,
							 });
		tomato.setScale(0.70)
		tomato.setSize(80, 80, false)
		tomato.setOffset(50,35)
		tomato.state = 'roll_left'
		self.tomatoes.add(tomato)
}

addJuice(self, x, y) {
		this.aj = this.juice.create(x,y, 'juice');
		this.aj.setActive(true).setVisible(true)
		this.aj.setData({'layer_collision': self.physics.add.collider(this.aj, this.layer)});
		this.aj.setScale(2)
		this.juice.add(this.aj)
}

dissolveFact (fact, target){
		try{
			if (this.time.now < (fact.data.values.time_thrown + 500)){
					fact.onCollide = false;
					return}}
		catch(TypeError){
				console.log('Caught TypeError for fact')}
		fact.destroy()
		fact.setVisible(false)}

dissolveBS(bs, target){
		try{
			if (this.time.now < (bs.data.values.time_thrown + 3000)){
					bs.onCollide = false;
					return}}
		catch(TypeError){
				console.log('Caught TypeError for BS')}
		bs.destroy()
		bs.setVisible(false)}

dissolveCensorship(censor, target){
		censor.destroy()
		censor.setVisible(false)}

facts_destroy_bs(fact, bs){
		this.time.delayedCall(100, function() {
				fact.destroy();}, [], this);
		bs.destroy();
	}

throwFactGamePad(x_spawn, x_velocity, y_velocity, now){
		var fact = this.facts.create((this.player.x + x_spawn), this.player.y-10, 'fact');
		fact.setData('time_thrown', now)
		fact.setDepth(800)
		fact.setSize(35,20)
		fact.setBounce(1);
		if (!this.gamepad.up && this.key_up.isUp){
				fact.setVelocity(x_velocity, y_velocity);}
		else{
				if (this.player.state.includes('right')){
						fact.setVelocity(x_velocity-250, y_velocity-400);}
				else if (this.player.state.includes('left')){
						fact.setVelocity(x_velocity+250, y_velocity-400);}}
	}

throwFactKeyboard(x_spawn, x_velocity, y_velocity, now){
	var fact = facts.create((this.player.x + x_spawn), this.player.y-10, 'fact');
	fact.setData('time_thrown', now)
	fact.setSize(45,20)
	fact.setBounce(1);
		if (this.key_up.isUp){
				fact.setVelocity(x_velocity, y_velocity);}
		else{
				if (this.player.state.includes('right')){
						fact.setVelocity(x_velocity-250, y_velocity-400);}
				else if (this.player.state.includes('left')){
						fact.setVelocity(x_velocity+250, y_velocity-400);}}
		fact.onCollide = true;
		}

damagePlayer(player, self, damage){
		if (player.data.values.post_hit_invincibility){
				return}
		player.data.values.health -= damage;
		if (player.data.values.health > 0){
				player.setAlpha(0.5)}  // Opacity
		self.life_bar.setFrame(this.player.data.values.health)
		player.data.values.post_hit_invincibility = true;
		player.data.values.time_at_damage = self.time.now
		}

healPlayer(player, juice, amount, self){
		juice.destroy()
		player.data.values.health += amount;
		if(player.data.values.health > 18){
				player.data.values.health = 18}
		self.life_bar.setFrame(player.data.values.health)
		}

knockPlayerBack(now){
		if (now < (this.player.data.values.time_at_collision + this.player.data.values.knock_back_time)) {
				if (this.player.x <= this.player.data.values.hit_from){
						this.player.setVelocityX(-150)
						this.player.state = 'hit_to_left'}
				else{
						this.player.setVelocityX(150)
						this.player.state = 'hit_to_right'}}

else if (now >= (this.player.data.values.time_at_collision + 200)  // 200 miliseconds after hit
					&& this.player.state.includes('hit')
					&& this.player.body.onFloor()) {
		this.resetPlayerState()}
	}

resetPlayerState(){
		if (this.player.body.onFloor()){
			if (this.player.data.values.hit_from < this.player.x){
					this.player.state = 'stand_left'}
			else {
					this.player.state = 'stand_right'}}
		else{
			if (this.player.data.values.hit_from < this.player.x){
					this.player.state = 'jump_left'}
			else {
					this.player.state = 'jump_right'}}
		}

throwBS(self, npc){
		var bs_array = ['bs','bs2','bs3','bs4','bs5','bs6','bs7','bs8','bs9','bs10','bs11','bs12','bs13'];

		var random_bs = bs_array[Math.floor(Math.random() * bs_array.length)];

		var bs = this.bs_statements.create(npc.x, npc.y-45, random_bs);
		bs.setData('time_thrown', self.time.now)
		bs.setSize(120,80)
		bs.setBounce(1);
		if (npc.state.includes('right')){
				bs.setVelocity(250, -200);}
		else if (npc.state.includes('left')){
				bs.setVelocity(-250, -200);}
		npc.setData({'throw_wait': Phaser.Math.Between(4000, 7000),
								 'last_throw': self.time.now,
								 'may_throw': false})
		npc.data.values.bs_thrown += 1

		this.player_bs = self.physics.add.collider(bs, this.player, function(bs, player){  // ONLY RUNS DURING COLLISION, DOES NOT LOOP
				bs.destroy()
				bs.setVisible(false)
				player.data.values.time_at_collision = self.time.now
				player.data.values.hit_from = bs.x
				this.damagePlayer(player, self, 4)
			}, this.checkPostHitInvincibility, self);
	}

dropCensorship(self, bird){
		self.censor = this.censorships.create(bird.x, bird.y, 'censored');
		var censor = self.censor
		bird.setData({'throw_wait': Phaser.Math.Between(4000, 7000),
								 'last_throw': self.time.now,
								 'may_throw': false})

		self.player_censor = self.physics.add.collider(self.censor, self.player, function(censor, player){  // ONLY RUNS DURING COLLISION, DOES NOT LOOP
				censor.destroy()
				censor.setVisible(false)
				player.data.values.time_at_collision = self.time.now
				player.data.values.hit_from = censor.x
				self.damagePlayer(player, self, 4)
			}, self.checkPostHitInvincibility, self);
	}

checkPostHitInvincibility(){
		return (this.player.data.values.post_hit_invincibility == false)}


update (){ // FINITE STATE MACHINE //

		if(this.gamepad){
		if (this.player.state == 'stand_right'){
				this.player.setVelocityX(0);
				this.player.anims.play('stand_right', true);
				if (!this.player.body.onFloor()){
						this.player.state = 'jump_right'}
				else if (Phaser.Input.Keyboard.JustDown(this.key_jump) || (this.gamepad.A && this.player.data.values.may_jump)) {
						this.player.setVelocityY(-650);
						this.player.state = 'jump_right';}
				else if (this.key_right.isDown || this.gamepad.right){
						this.player.state = 'run_right'}
				else if (this.key_left.isDown || this.gamepad.left){
						this.player.state = 'run_left'}
				else if (Phaser.Input.Keyboard.JustDown(this.key_throw) || (this.gamepad.X && this.player.data.values.may_throw)) {
						this.time_at_throw_start = Math.trunc(this.time.now)
						this.player.state = 'stand_right_and_throw'
						this.throwFactGamePad(35, 450, -200, this.time.now)
					}}

		if (this.player.state == 'stand_left'){
				this.player.setVelocityX(0);
				this.player.anims.play('stand_left', true);
				if (!this.player.body.onFloor()){
						this.player.state = 'jump_left'}
				else if (Phaser.Input.Keyboard.JustDown(this.key_jump) || (this.gamepad.A && this.player.data.values.may_jump)){
						this.player.setVelocityY(-650);
						this.player.state = 'jump_left';}
				else if (this.key_left.isDown || this.gamepad.left){
						this.player.state = 'run_left'}
				else if (this.key_right.isDown || this.gamepad.right){
						this.player.state = 'run_right'}
				else if (Phaser.Input.Keyboard.JustDown(this.key_throw) || (this.gamepad.X && this.player.data.values.may_throw)) {
						this.time_at_throw_start = Math.trunc(this.time.now)
						this.player.state = 'stand_left_and_throw'
						this.throwFactGamePad(-35, -450, -200, this.time.now)
					}}

		if (this.player.state == 'run_right'){
				this.player.setVelocityX(200);
				this.player.anims.play('run_right', true);
				if (!this.player.body.onFloor()){
						this.player.state = 'jump_right'}
				else if (Phaser.Input.Keyboard.JustDown(this.key_jump) || (this.gamepad.A && this.player.data.values.may_jump)){
						this.player.setVelocityY(-650);
						this.player.state = 'jump_right'}
				else if ((this.key_right.isUp) && (!this.gamepad.right)){
						this.player.state = 'stand_right'}
				else if ((this.key_left.isDown && this.key_right.isUp) || this.gamepad.left){
						this.player.state = 'run_left'}
				else if (this.key_left.isDown && this.key_right.isDown){
						this.player.state = 'run_right'}
				else if (Phaser.Input.Keyboard.JustDown(this.key_throw) || (this.gamepad.X && this.player.data.values.may_throw)){
						this.time_at_throw_start = Math.trunc(this.time.now)
						this.player.state = 'stand_right_and_throw'
						this.throwFactGamePad(35, 450, -200, this.time.now)
					}}

		if (this.player.state == 'run_left'){
				this.player.setVelocityX(-200);
				this.player.anims.play('run_left', true);
				if (!this.player.body.onFloor()){
						this.player.state = 'jump_left'}
				else if (Phaser.Input.Keyboard.JustDown(this.key_jump) || (this.gamepad.A && this.player.data.values.may_jump)){
						this.player.setVelocityY(-650);
						this.player.state = 'jump_left'}
				else if (this.key_left.isUp && (!this.gamepad.left)){
						this.player.state = 'stand_left'}
				else if ((this.key_right.isDown && this.key_left.isUp) || this.gamepad.right){
						this.player.state = 'run_right'}
				else if (this.key_left.isDown && this.key_right.isDown){
						this.player.state = 'run_left'}
				else if (Phaser.Input.Keyboard.JustDown(this.key_throw) || (this.gamepad.X && this.player.data.values.may_throw)){
						this.time_at_throw_start = Math.trunc(this.time.now)
						this.player.state = 'stand_left_and_throw'
						this.throwFactGamePad(-35, -450, -200, this.time.now)
					}}

		if (this.player.state == 'jump_right'){
				this.player.anims.play('jump_right', true);
				if (this.player.body.onFloor()){
						this.player.state = 'stand_right'}
				else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){
						this.player.state = 'jump_right'}
				else if ((this.key_right.isDown && Phaser.Input.Keyboard.JustDown(this.key_throw)) || (this.gamepad.right && (this.gamepad.X && this.player.data.values.may_throw))) {
						this.time_at_throw_start = Math.trunc(this.time.now)
						this.player.state = 'jump_right_and_throw'
						this.throwFactGamePad(35, 450, -200, this.time.now)
						this.player.setVelocityX(250);}
				else if (this.key_right.isDown || this.gamepad.right){
						this.player.setVelocityX(250);}
						else if (Phaser.Input.Keyboard.JustDown(this.key_left) || (this.gamepad.left)){
								this.player.state = 'jump_left'}
				else if ((this.key_right.isUp && Phaser.Input.Keyboard.JustDown(this.key_throw)) || ((!this.gamepad.right) && (this.gamepad.X && this.player.data.values.may_throw))) {
						this.player.setVelocityX(0);
						this.time_at_throw_start = Math.trunc(this.time.now)
						this.player.state = 'jump_right_and_throw';
						this.throwFactGamePad(35, 450, -200, this.time.now)
					}
				else if (Phaser.Input.Keyboard.JustUp(this.key_right) || (!this.gamepad.right)){
						this.player.setVelocityX(0);}}

		if (this.player.state == 'jump_left'){
				this.player.anims.play('jump_left', true);
				if (this.player.body.onFloor()){
						this.player.state = 'stand_left'}
				else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){
						this.player.state = 'jump_left'}
				else if ((this.key_left.isDown && Phaser.Input.Keyboard.JustDown(this.key_throw)) || (this.gamepad.left && (this.gamepad.X && this.player.data.values.may_throw))) {
						this.time_at_throw_start = Math.trunc(this.time.now)
						this.player.state = 'jump_left_and_throw'
						this.throwFactGamePad(-35, -450, -200, this.time.now)
						this.player.setVelocityX(-250);}
				else if (this.key_left.isDown || this.gamepad.left){
						this.player.setVelocityX(-250);}
						else if (Phaser.Input.Keyboard.JustDown(this.key_right) || (this.gamepad.right)){
								this.player.state = 'jump_right'}
				else if ((this.key_right.isUp && Phaser.Input.Keyboard.JustDown(this.key_throw)) || ((!this.gamepad.left) && (this.gamepad.X && this.player.data.values.may_throw))) {
						this.player.setVelocityX(0);
						this.time_at_throw_start = Math.trunc(this.time.now)
						this.player.state = 'jump_left_and_throw';
						this.throwFactGamePad(-35, -450, -200, this.time.now)
					}
				else if (Phaser.Input.Keyboard.JustUp(this.key_left) || (!this.gamepad.left)){
						this.player.setVelocityX(0);}}

		if (this.player.state == 'stand_right_and_throw'){
				this.player.setVelocityX(0);
				this.player.anims.play('stand_right_and_throw', true);
				if (this.time.now >= (this.time_at_throw_start + 250)){ // gives 250 miliseconds so it plays the animation once
						this.player.state = 'stand_right'}}

		if (this.player.state == 'stand_left_and_throw'){
				this.player.setVelocityX(0);
				this.player.anims.play('stand_left_and_throw', true);
				if (this.time.now >= (this.time_at_throw_start + 250)){
						this.player.state = 'stand_left'}}

		if (this.player.state == 'jump_right_and_throw'){
				this.player.anims.play('jump_right_and_throw', true);
				if (this.time.now >= (this.time_at_throw_start + 250)){
						this.player.state = 'jump_right'}}

		if (this.player.state == 'jump_left_and_throw'){
				this.player.anims.play('jump_left_and_throw', true);
				if (this.time.now >= (this.time_at_throw_start + 250)){
						this.player.state = 'jump_left'}}

		if (this.player.state == 'hit_to_left'){
				this.player.anims.play('hit_to_left');
				if (this.key_left.isDown || this.gamepad.left){
						this.player.setVelocityX(-250);}
				// else if (Phaser.Input.Keyboard.JustUp(this.key_left)){
				// 		this.player.setVelocityX(0);}
				else if ((this.key_right.isDown || this.gamepad.right) && (!this.player.body.onFloor())) {
						this.player.state = 'hit_to_right'}}

		if (this.player.state == 'hit_to_right'){
				this.player.anims.play('hit_to_right');
				if (this.key_right.isDown || this.gamepad.right){
						this.player.setVelocityX(250);}
				else if (Phaser.Input.Keyboard.JustUp(this.key_right)){
						this.player.setVelocityX(0);}
				else if (this.key_left.isDown || this.gamepad.left){
						this.player.state = 'hit_to_left'}}

		this.player.data.values.may_jump = ((this.gamepad.A) ? false : true);
		this.player.data.values.may_throw = ((this.gamepad.X) ? false : true);
}
else{ // KEYBOARD ONLY - NO GAMEPAD																////
	if (this.player.state == 'stand_right'){                                                         ////
				this.player.setVelocityX(0);                                                         ////
				this.player.anims.play('stand_right', true);                                         ////
				if (!this.player.body.onFloor()){                                                    ////
						this.player.state = 'jump_right'}                                            ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
						this.player.setVelocityY(-650);                                              ////
						this.player.state = 'jump_right';}                                           ////
				else if (this.key_right.isDown){                                                     ////
						this.player.state = 'run_right'}                                             ////
				else if (this.key_left.isDown){                                                      ////
						this.player.state = 'run_left'}                                              ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
						this.time_at_throw_start = Math.trunc(this.time.now)                         ////
						this.player.state = 'stand_right_and_throw'
						this.throwFactKeyboard(35, 450, -200, this.time.now)
					}}                                ////
                                                                                                ////
		if (this.player.state == 'stand_left'){                                                      ////
				this.player.setVelocityX(0);                                                         ////
				this.player.anims.play('stand_left', true);                                          ////
				if (!this.player.body.onFloor()){                                                    ////
						this.player.state = 'jump_left'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
						this.player.setVelocityY(-650);                                              ////
						this.player.state = 'jump_left';}                                            ////
				else if (this.key_left.isDown){                                                      ////
						this.player.state = 'run_left'}                                              ////
				else if (this.key_right.isDown){                                                     ////
						this.player.state = 'run_right'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
						this.time_at_throw_start = Math.trunc(this.time.now)                         ////
						this.player.state = 'stand_left_and_throw'
						this.throwFactKeyboard(-35, -450, -200, this.time.now)
					}}                                 ////
                                                                                                ////
		if (this.player.state == 'run_right'){                                                       ////
				this.player.setVelocityX(200);                                                       ////
				this.player.anims.play('run_right', true);                                           ////
				if (!this.player.body.onFloor()){                                                    ////
						this.player.state = 'jump_right'}                                            ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
						this.player.setVelocityY(-650);                                              ////
						this.player.state = 'jump_right'}                                            ////
				else if (Phaser.Input.Keyboard.JustUp(this.key_right)){                              ////
						this.player.state = 'stand_right'}                                           ////
				else if (this.key_left.isDown && this.key_right.isUp){                                    ////
						this.player.state = 'run_left'}                                              ////
				else if (this.key_left.isDown && this.key_right.isDown){                                  ////
						this.player.state = 'run_right'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
						this.time_at_throw_start = Math.trunc(this.time.now)                         ////
						this.player.state = 'stand_right_and_throw'
						this.throwFactKeyboard(35, 450, -200, this.time.now)
					}}                                ////
                                                                                                ////
		if (this.player.state == 'run_left'){                                                        ////
				this.player.setVelocityX(-200);                                                      ////
				this.player.anims.play('run_left', true);                                            ////
				if (!this.player.body.onFloor()){                                                    ////
						this.player.state = 'jump_left'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
						this.player.setVelocityY(-650);                                              ////
						this.player.state = 'jump_left'}                                             ////
				else if (Phaser.Input.Keyboard.JustUp(this.key_left)){                               ////
						this.player.state = 'stand_left'}                                            ////
				else if (this.key_right.isDown && this.key_left.isUp){                                    ////
						this.player.state = 'run_right'}                                             ////
				else if (this.key_left.isDown && this.key_right.isDown){                                  ////
						this.player.state = 'run_left'}                                              ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
						this.time_at_throw_start = Math.trunc(this.time.now)                         ////
						this.player.state = 'stand_left_and_throw'
						this.throwFactKeyboard(-35, -450, -200, this.time.now)
					}}                                 ////
                                                                                                ////
		if (this.player.state == 'jump_right'){                                                      ////
				this.player.anims.play('jump_right', true);                                          ////
				if (this.player.body.onFloor()){                                                     ////
						this.player.state = 'stand_right'}                                           ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
						this.player.state = 'jump_right'}                                            ////
				else if (this.key_right.isDown && Phaser.Input.Keyboard.JustDown(this.key_throw)){        ////
						this.time_at_throw_start = Math.trunc(this.time.now)                         ////
						this.player.state = 'jump_right_and_throw'
						this.throwFactKeyboard(35, 450, -200, this.time.now)                                 ////
						this.player.setVelocityX(250);}                                              ////
				else if (this.key_right.isDown){                                                     ////
						this.player.setVelocityX(250);}                                              ////
				else if (Phaser.Input.Keyboard.JustUp(this.key_right)){                              ////
						this.player.setVelocityX(0);}                                                ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_left)){                             ////
						this.player.state = 'jump_left'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
						this.time_at_throw_start = Math.trunc(this.time.now)                         ////
						this.player.state = 'jump_right_and_throw'
						this.throwFactKeyboard(35, 450, -200, this.time.now)
					}}                                 ////
                                                                                                ////
		if (this.player.state == 'jump_left'){                                                       ////
				this.player.anims.play('jump_left', true);                                           ////
				if (this.player.body.onFloor()){                                                     ////
						this.player.state = 'stand_left'}                                            ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_jump)){                             ////
						this.player.state = 'jump_left'}                                             ////
				else if (this.key_left.isDown && Phaser.Input.Keyboard.JustDown(this.key_throw)){         ////
						this.time_at_throw_start = Math.trunc(this.time.now)                         ////
						this.player.state = 'jump_left_and_throw'
						this.throwFactKeyboard(-35, -450, -200, this.time.now)                              ////
						this.player.setVelocityX(-250);}                                             ////
				else if (this.key_left.isDown){                                                      ////
						this.player.setVelocityX(-250);}                                             ////
				else if (Phaser.Input.Keyboard.JustUp(this.key_left)){                               ////
						this.player.setVelocityX(0);}                                                ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_right)){                            ////
						this.player.state = 'jump_right'}                                            ////
				else if (Phaser.Input.Keyboard.JustDown(this.key_throw)){                            ////
						this.time_at_throw_start = Math.trunc(this.time.now)                         ////
						this.player.state = 'jump_left_and_throw'
						this.throwFactKeyboard(-35, -450, -200, this.time.now)
					}}                                  ////
                                                                                                ////
		if (this.player.state == 'stand_right_and_throw'){                                           ////
				this.player.setVelocityX(0);                                                         ////
				this.player.anims.play('stand_right_and_throw', true);                               ////
				if (this.time.now >= (this.time_at_throw_start + 250)){                                        ////
						this.player.state = 'stand_right'}}                                          ////
                                                                                                ////
		if (this.player.state == 'stand_left_and_throw'){                                            ////
				this.player.setVelocityX(0);                                                         ////
				this.player.anims.play('stand_left_and_throw', true);                                ////
				if (this.time.now >= (this.time_at_throw_start + 250)){                                        ////
						this.player.state = 'stand_left'}}                                           ////
                                                                                                ////
		if (this.player.state == 'jump_right_and_throw'){                                            ////
				this.player.anims.play('jump_right_and_throw', true);                                ////
				now = Math.trunc(this.time.now);                                                ////                                               ////
				if (now >= (this.time_at_throw_start + 250)){                                        ////
						this.player.state = 'jump_right'}}                                           ////
                                                                                                ////
		if (this.player.state == 'jump_left_and_throw'){                                             ////
				this.player.anims.play('jump_left_and_throw', true);                                 ////
				now = Math.trunc(this.time.now);                                                ////                                             ////
				if (now >= (this.time_at_throw_start + 250)){                                        ////
						this.player.state = 'jump_left'}}                                            ////
                                                                                                ////
		if (this.player.state == 'hit_to_left'){                                                     ////
				this.player.anims.play('hit_to_left');                                               ////
				if (this.key_left.isDown){                                                           ////
						this.player.setVelocityX(-250);}                                             ////
				// else if (Phaser.Input.Keyboard.JustUp(this.key_left)){                            ////
				// 		this.player.setVelocityX(0);}                                                ////
				else if (this.key_right.isDown && (!this.player.body.onFloor())) {                        ////
						this.player.state = 'hit_to_right'}}                                         ////
                                                                                                ////
		if (this.player.state == 'hit_to_right'){                                                    ////
				this.player.anims.play('hit_to_right');                                              ////
				if (this.key_right.isDown){                                                          ////
						this.player.setVelocityX(250);}                                              ////
				else if (Phaser.Input.Keyboard.JustUp(this.key_right)){                              ////
						this.player.setVelocityX(0);}                                                ////
				else if (this.key_left.isDown){                                                      ////
						this.player.state = 'hit_to_left'}}                                          ////
		}                                                                                               ////

		if (this.player.data.values.health <= 0){
				this.player.data.values.knock_back_time = 5000}

		this.npc_enemies.children.each(function(npc) {

				if (npc.active){
				// NPC ANIMATIONS
				var player_npc_x_diff = ((this.player.x >= npc.x) ? (this.player.x - npc.x) : (npc.x - this.player.x));
				var player_npc_y_diff = ((this.player.y >= npc.y) ? (this.player.y - npc.y) : (npc.y - this.player.y));
				if ((player_npc_x_diff < 800) && (player_npc_y_diff < 400)){  // Player in the NPC 'chase box'
						if (npc.x > this.player.x+10){
								npc.setVelocityX(-50);
								npc.state = 'walk_left'
								npc.anims.play('npc_walk_left', true);}
						else if (npc.x < this.player.x-10){
								npc.setVelocityX(50);
								npc.state = 'walk_right'
								npc.anims.play('npc_walk_right', true);}
						else if ((this.player.x+10) > npc.x && npc.x > (this.player.x-10)){
								npc.setVelocityX(0);
								npc.state = 'stand_right'
								npc.anims.play('npc_stand_right', true)}
						//THROW BS
								if (this.time.now > (npc.data.values.last_throw + npc.data.values.throw_wait)) {
										this.throwBS(this, npc)}
							} // player in chase box
				else {
						if (npc.x < this.player.x){
								npc.state = 'stand_right'
								npc.setVelocityX(0);
								npc.anims.play('npc_stand_right', true)}
						else{
								npc.state = 'stand_left'
								npc.setVelocityX(0);
								npc.anims.play('npc_stand_left', true)}}

				if (this.time.now < (npc.data.values.time_at_collision + npc.data.values.knock_back_time)) {
						if (npc.data.values.health <= 0){
								npc.setPosition(npc.x, npc.y-6, npc.z, npc.w)}
						if (npc.x <= this.player.x){
								npc.setVelocityX(-300)
								npc.anims.play('npc_hit_to_left', true);}
						else{
								npc.setVelocityX(300)
								npc.anims.play('npc_hit_to_right', true);}
							}

				if (npc.y > (this.layer.height+100)){  //shake camera if NPC falls off screen and has no health
						if (npc.data.values.health <= 0){
								this.cameras.main.shake(100, 0.005);}
						this.time.delayedCall(400, function() {
						npc.destroy()
						npc.setVisible(false)
						}, [], this);}

				} // if NPC active
			}, this); // looping through all NPCs


			this.birds.children.each(function(bird) {
					if (bird.active){
					if (bird.x > (this.player.x + 800)){
							bird.state = 'fly_left'}
					else if (bird.x < (this.player.x-800)){
							bird.state = 'fly_right'}

					if (bird.state == 'fly_left'){
							bird.setVelocityX(-(bird.data.values.speed))
							bird.anims.play('bird_fly_left', true);}
					else if (bird.state == 'fly_right'){
							bird.setVelocityX(bird.data.values.speed)
							bird.anims.play('bird_fly_right', true);}

					if (bird.data.values.health <= 0){
							if (bird.x <= this.player.x){
									bird.setVelocityX(-300)
									bird.anims.play('bird_hit_to_left', true);}
							else{
									bird.setVelocityX(300)
									bird.anims.play('bird_hit_to_right', true);}}
					if (bird.y > (this.layer.height+100)){
							bird.destroy()
							bird.setVisible(false)}

					try{
							if (this.time.now > (bird.data.values.last_throw + bird.data.values.throw_wait)) {
									this.dropCensorship(this, bird)}
								}
					catch(TypeError){
							console.log('Caught bird TypeError')} // This is usually OK
					}  // if bird active
				}, this); // looping through all birds

			this.tomatoes.children.each(function(tomato) {
					// console.log('now', this.time.now)
					// console.log('last right', tomato.data.values.last_turned_right)
					// console.log('last left', tomato.data.values.last_turned_left)

					if (tomato.active){
							if(tomato.state == 'roll_left'){
									tomato.setVelocityX(-300)
									tomato.anims.play('tomato_roll_left', true);
									tomato.setAngularVelocity(-200)
							}
							else if(tomato.state == 'roll_right'){
									tomato.setVelocityX(300)
									tomato.anims.play('tomato_roll_right', true);
									tomato.setAngularVelocity(200)
							}
					if ((tomato.x > (this.player.x + 1000)) ||
					((tomato.state == 'roll_right') && this.time.now > (tomato.data.values.last_turned_right + 5000))){
							tomato.data.values.last_turned_left = this.time.now
							tomato.state = 'roll_left'}
					else if ((tomato.x < (this.player.x-1000)) ||
					((tomato.state == 'roll_left') && this.time.now > (tomato.data.values.last_turned_left + 5000))){
							tomato.data.values.last_turned_right = this.time.now
							tomato.state = 'roll_right'}

					if (tomato.data.values.health <= 0){
							if (tomato.x <= this.player.x){
									tomato.setVelocityX(-300)
									tomato.setAngularVelocity(400)
								}
							else{
									tomato.setAngularVelocity(-400)
								}
							if (tomato.y > (layer.height+100)){
									tomato.destroy()
									tomato.setVisible(false)}
						}
					}  // if tomato active
				}, this); // looping through all tomatoes





				this.knockPlayerBack(this.time.now)

				// if (player.data.values.post_hit_invincibility == true) {
				// 		this.physics.world.removeCollider(player_bs);}

				//turn off post hit-invincibility
				if (Math.trunc(this.time.now) > (Math.trunc(this.player.data.values.time_at_damage) + 2000)) {
						this.player.data.values.post_hit_invincibility = false
						this.player.setAlpha(1)}

				//knock player off screen
				if (this.player.data.values.health <= 0){
						if (this.time.now < (this.player.data.values.time_at_collision + 5000)) {
								this.player.setPosition(this.player.x, this.player.y-5, this.player.z, this.player.w)
								this.player.setAngularVelocity(-400)}
						this.physics.world.removeCollider(this.player_layer);
						}

				if (this.player.y > (this.layer.height+100)){
						this.cameras.main.shake(500);
						this.time.delayedCall(400, function() { // wait 400 ms, then fade
							this.cameras.main.fade(300); // fade effect lasts 400 ms
						}, [], this);
						this.time.delayedCall(800, function() {
							this.scene.restart(); // restart game
						}, [], this);
						}


}

}

class Level2 extends Phaser.Scene {
	constructor() {
	  super("level2");
	}

		preload(){

		}

		create(){
				// SceneA.addNPC(SceneA, 800, 0)
				// this.addNPC(this, 800, 0)
		}

		update(){

		}
}
