class Level1 extends Phaser.Scene {

	constructor() {
	  super("level1");
	}

preload (){
  	this.load.image('bs', 'assets/bs/bs.png');
		this.load.image('bs2', 'assets/bs/bs2.png');
		this.load.image('bs3', 'assets/bs/bs3.png');
		this.load.image('bs4', 'assets/bs/bs4.png');
		this.load.image('bs5', 'assets/bs/bs5.png');
		this.load.image('bs6', 'assets/bs/bs6.png');
		this.load.image('bs7', 'assets/bs/bs7.png');
		this.load.image('bs8', 'assets/bs/bs8.png');
		this.load.image('bs9', 'assets/bs/bs9.png');
		this.load.image('bs10', 'assets/bs/bs10.png');
		this.load.image('bs11', 'assets/bs/bs11.png');
		this.load.image('bs12', 'assets/bs/bs12.png');
		this.load.image('bs13', 'assets/bs/bs13.png');
		this.load.image('censored', 'assets/bs/censored.png');

		this.load.image('fact', 'assets/fact2.png');
		this.load.image('star', 'assets/star.png');
    this.load.spritesheet('jeremy', 'assets/jeremy.png', { frameWidth: 66, frameHeight: 96 });
		this.load.spritesheet('npc', 'assets/enemies/npc.png', { frameWidth: 64, frameHeight: 96 });
		this.load.spritesheet('bird', 'assets/enemies/bird.png', { frameWidth: 100, frameHeight: 72 });
		this.load.spritesheet('tomato', 'assets/enemies/tomato.png', { frameWidth: 180, frameHeight: 150 });
		this.load.spritesheet('life_bar', 'assets/life_bar.png', { frameWidth: 280, frameHeight: 50 });
		this.load.spritesheet('juice', 'assets/apple_juice.png', { frameWidth: 26, frameHeight: 34 });
    this.load.tilemapTiledJSON("map", "assets/maps/level3.json")
    this.load.image("tiles", "assets/futureish.png");
    }

create (){
    map = this.add.tilemap("map");
    tileset = map.addTilesetImage("tileset", "tiles");
    layer = map.createStaticLayer(0, tileset, 0, 0); // layer index, tileset, x, y
    layer.setCollisionByProperty({ collides: true }); // tiles w/ collides set to true (in Tiled) will collide
		player = this.physics.add.sprite(400, 20, 'jeremy').setScale(1.5);
		player.setData({'health': 18,
										'post_hit_invincibility': false,
										'knock_back_time': 200,
										'time_at_collision':null,
										'may_jump': true,
										'may_throw': true,
										'hit_from': null})
		player.state = 'jump_right'
		player.setSize(37, 95, false)
		// player.setTint(Phaser.Display.Color.RandomRGB().color)  // no idea why this doesn't work
		player.setDepth(900);
		player.setOffset(15,0)
		player.setGravityY(250)

		//LIFE BAR
		life_bar = this.physics.add.sprite(140, 30, 'life_bar').setDepth(1000);
		life_bar.body.setAllowGravity(false);
		life_bar.setScrollFactor(0,0)
		life_bar.setFrame(18)

		//GAMEPAD
		this.input.gamepad.once('down', function (pad, button, index) {
				gamepad = pad;
				}, this);

		//JEREMY ANIMATIONS
    this.anims.create({
        key: 'run_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 10, frames: [10,11,12,13] }),
        frameRate: 10,
        repeat: -1 // tells the animation to loop
        });
    this.anims.create({
        key: 'jump_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 17, end: 17 }),
        });
    this.anims.create({
        key: 'stand_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 3, end: 3 }),
        });
    this.anims.create({
        key: 'run_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 6, frames: [6,7,8,9] }),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'jump_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 14, end: 14 }),
        });
		this.anims.create({
        key: 'jump_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 15, frames: [15,16,16,16,14] }),
				frameRate: 10,
        });
		this.anims.create({
				key: 'jump_left_and_throw',
				frames: this.anims.generateFrameNumbers('jeremy', { start: 18, frames: [18,19,19,19,17] }),
				frameRate: 10,
				});
    this.anims.create({
        key: 'stand_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 0, end: 0 }),
        });
		this.anims.create({
        key: 'stand_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 1, frames: [1,2,2,2,0] }),
				frameRate: 10,
				repeat: 0,
        });
		this.anims.create({
				key: 'stand_left_and_throw',
				frames: this.anims.generateFrameNumbers('jeremy', { start: 4, frames: [4,5,5,5,3] }),
				frameRate: 10,
				repeat: 0
				});
		this.anims.create({
				key: 'hit_to_left',
				frames: this.anims.generateFrameNumbers('jeremy', { start: 20, end: 20 }),
				});
		this.anims.create({
				key: 'hit_to_right',
				frames: this.anims.generateFrameNumbers('jeremy', { start: 21, end: 21 }),
				});

		//////////////////////////////////////////
		// NPC ANIMATIONS
		this.anims.create({
        key: 'npc_walk_left',
        frames: this.anims.generateFrameNumbers('npc', { start: 4, frames: [3,4,5,4] }),
        frameRate: 6,
        repeat: -1
        });
		this.anims.create({
				key: 'npc_walk_right',
				frames: this.anims.generateFrameNumbers('npc', { start: 1, frames: [0,1,2,1] }),
				frameRate: 6,
				repeat: -1
				});
		this.anims.create({
				key: 'npc_stand_right',
				frames: this.anims.generateFrameNumbers('npc', { start: 1, end: 1 }),
				});
		this.anims.create({
				key: 'npc_stand_left',
				frames: this.anims.generateFrameNumbers('npc', { start: 4, end: 4 }),
				});
		this.anims.create({
				key: 'npc_hit_to_right',
				frames: this.anims.generateFrameNumbers('npc', { start: 6, end: 6 }),
				});
		this.anims.create({
				key: 'npc_hit_to_left',
				frames: this.anims.generateFrameNumbers('npc', { start: 7, end: 7 }),
				});

		// BIRD ANIMATIONS
		this.anims.create({
				key: 'bird_fly_left',
				frames: this.anims.generateFrameNumbers('bird', { start: 2, frames: [2,3] }),
				frameRate: 6,
				repeat: -1
				});
		this.anims.create({
				key: 'bird_fly_right',
				frames: this.anims.generateFrameNumbers('bird', { start: 1, frames: [0,1] }),
				frameRate: 6,
				repeat: -1
				});
		this.anims.create({
				key: 'bird_hit_to_right',
				frames: this.anims.generateFrameNumbers('bird', { start: 5, end: 5 }),
				});
		this.anims.create({
				key: 'bird_hit_to_left',
				frames: this.anims.generateFrameNumbers('bird', { start: 4, end: 4 }),
				});

		// TOMATO ANIMATIONS
		this.anims.create({
				key: 'tomato_run_right',
				frames: this.anims.generateFrameNumbers('tomato', { start: 5, frames: [5,6,7,8] }),
				frameRate: 10,
				repeat: -1
				});
		this.anims.create({
				key: 'tomato_run_left',
				frames: this.anims.generateFrameNumbers('tomato', { start: 0, frames: [0,1,2,3] }),
				frameRate: 10,
				repeat: -1
				});
		this.anims.create({
				key: 'tomato_jump_right',
				frames: this.anims.generateFrameNumbers('tomato', { start: 9, end: 9 }),
				});
		this.anims.create({
				key: 'tomato_jump_left',
				frames: this.anims.generateFrameNumbers('tomato', { start: 4, end: 4 }),
				});
		this.anims.create({
				key: 'tomato_roll_left',
				frames: this.anims.generateFrameNumbers('tomato', { start: 10, end: 10 }),
				});
		this.anims.create({
				key: 'tomato_roll_right',
				frames: this.anims.generateFrameNumbers('tomato', { start: 12, end: 12 }),
				});

		npc_enemies = this.physics.add.group();
		// this.addNPC(this, 1300, 0)
		// this.addNPC(this, 1500, 0)
		// this.addNPC(this, 1800, 0)
		// this.addNPC(this, 2400, 0)
		// this.addNPC(this, 3500, 0)
		// this.addNPC(this, 3800, 0)
		// this.addNPC(this, 4200, 0)
		// this.addNPC(this, 4800, 0)
		// this.addNPC(this, 5300, 0)

		birds = this.physics.add.group();
		// this.addBird(this, -2000, 50)
		// this.addBird(this, 800, 100)
		// this.addBird(this, -800, 150)
		// this.addBird(this, 1800, 75)

		juice = this.physics.add.group();
		this.addJuice(this, 2000, 0)
		// this.addJuice(this, 4000, 0)

		tomatoes = this.physics.add.group();
		// this.addTomato(this, 1000, 200)
		// this.addTomato(this, 3500, 200)

    key_left = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    key_right = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
		key_up = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
    key_jump = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.L);
		key_throw = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.K);

    player_layer = this.physics.add.collider(player, layer);
    this.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
    this.cameras.main.startFollow(player);

		facts = this.physics.add.group();
		this.physics.add.collider(layer, facts, this.dissolveFact, null, this);

		bs_statements = this.physics.add.group()
		this.physics.add.collider(layer, bs_statements, this.dissolveBS, null, this);
		this.physics.add.overlap(facts, bs_statements, this.facts_destroy_bs, null, this);

		censorships = this.physics.add.group()
		this.physics.add.collider(layer, censorships,
				// function(layer, censor){censor.destroy()}
				this.dissolveCensorship
				, null, this);
		this.physics.add.overlap(facts, censorships, this.facts_destroy_bs, null, this);


		// DAMAGE PLAYER AND SET INVINCIBILITY
		player_npc = this.physics.add.collider(player, npc_enemies, function(player, npc){
				player.data.values.time_at_collision = this.time.now
				player.data.values.hit_from = npc.x
				this.damagePlayer(player, this.time.now, 4)
				}, function(player, npc){
						return (npc.data.values.health > 0
							&& player.data.values.post_hit_invincibility == false)
				}, this);

		player_bird = this.physics.add.collider(player, birds, function(player, bird){
				player.data.values.time_at_collision = this.time.now
				player.data.values.hit_from = bird.x
				bird.setVelocityY(0)
				this.damagePlayer(player, this.time.now, 3)
				}, function(player, bird){
						return (bird.data.values.health > 0
							&& player.data.values.post_hit_invincibility == false)
				}, this);

		player_tomato = this.physics.add.collider(player, tomatoes, function(player, tomato){
				player.data.values.time_at_collision = this.time.now
				player.data.values.hit_from = tomato.x
				this.damagePlayer(player, this.time.now, 4)
				tomato.destroy()
				}, function(player, tomato){
						return (tomato.data.values.health > 0
							&& player.data.values.post_hit_invincibility == false)
				}, this);

		player_juice = this.physics.add.overlap(player, juice, function(player, juice){
				this.healPlayer(player, juice, 4)
				}, null, this);

		this.physics.add.overlap(facts, npc_enemies, function(fact, npc){
				fact.destroy()
				npc.data.values.time_at_collision = this.time.now
				npc.data.values.health -= 1;
				if (npc.data.values.health <= 0){
						if (this.time.now < (npc.data.values.time_at_collision + 5000)) {
								npc.data.values.knock_back_time = 5000
								if (player.x <= npc.x){
										npc.setAngularVelocity(400)}
								else{
										npc.setAngularVelocity(-400)}}
						this.physics.world.removeCollider(npc.data.values.layer_collision);
						return}
		}, null, this);

		this.physics.add.overlap(facts, birds, function(fact, bird){
				fact.destroy()
				bird.data.values.health -= 1;
				if (bird.data.values.health <= 0){
						bird.body.setAllowGravity(true);
						if (player.x <= bird.x){
								bird.setAngularVelocity(400)}
						else{
								bird.setAngularVelocity(-400)}
						return}
		}, null, this);

}

addNPC(self, x, y) {
		// npc = npc_enemies.create(Phaser.Math.Between(0, 800), Phaser.Math.Between(0, 300));
		npc = npc_enemies.create(x,y, 'npc');
		npc.setActive(true).setVisible(true)
		npc.setData({'health': 4,
								 'time_at_collision':null,
								 'knock_back_time': 300,
								 'bs_thrown': 0,
								 'layer_collision': self.physics.add.collider(npc, layer),
								 'throw_wait': Phaser.Math.Between(3000, 6000),
						 		 'last_throw': self.time.now
							 });
		npc.setSize(40, 95, false)
		npc.setOffset(15,0)
		npc.setScale(1.4).setTint(Phaser.Display.Color.RandomRGB().color)
		npc_enemies.add(npc)
}

addBird(self, x, y) {
		bird = birds.create(x,y, 'bird');
		bird.setActive(true).setVisible(true)
		bird.setData({'health': 1,
								 'throw_wait': Phaser.Math.Between(3000, 6000),
						 		 'last_throw': self.time.now,
								 'speed': Phaser.Math.Between(275,325)
							 });
		bird.setSize(80, 50, false)
		bird.setOffset(15,5)
		bird.state = 'fly_right'
		birds.add(bird)
		bird.body.setAllowGravity(false);
}

addTomato(self, x, y) {
		tomato = tomatoes.create(x,y, 'tomato');
		tomato.setActive(true).setVisible(true)
		tomato.setData({'health': 4,
								 'time_at_collision':null,
								 'knock_back_time': 300,
								 'layer_collision': self.physics.add.collider(tomato, layer),
								 'jump_wait': Phaser.Math.Between(3000, 6000),
						 		 'last_jump': self.time.now,
								 'last_turned_left': 0,
								 'last_turned_right': 0,
							 });
		tomato.setScale(0.70)
		tomato.setSize(80, 80, false)
		tomato.setOffset(50,35)
		tomato.state = 'roll_left'
		tomatoes.add(tomato)
}

addJuice(self, x, y) {
		aj = juice.create(x,y, 'juice');
		aj.setActive(true).setVisible(true)
		aj.setData({'layer_collision': self.physics.add.collider(aj, layer)});
		aj.setScale(2)
		juice.add(aj)
}

dissolveFact (fact, target){
		try{
			if (this.time.now < (fact.data.values.time_thrown + 500)){
					fact.onCollide = false;
					return}}
		catch(TypeError){
				console.log('Caught TypeError for fact')}
		fact.destroy()
		fact.setVisible(false)}

dissolveBS(bs, target){
		try{
			if (this.time.now < (bs.data.values.time_thrown + 3000)){
					bs.onCollide = false;
					return}}
		catch(TypeError){
				console.log('Caught TypeError for BS')}
		bs.destroy()
		bs.setVisible(false)}

dissolveCensorship(censor, target){
		censor.destroy()
		censor.setVisible(false)}

facts_destroy_bs(fact, bs){
		this.time.delayedCall(100, function() {
				fact.destroy();}, [], this);
		bs.destroy();
	}

throwFactGamePad(x_spawn, x_velocity, y_velocity, now){
		var fact = facts.create((player.x + x_spawn), player.y-10, 'fact');
		fact.setData('time_thrown', now)
		fact.setDepth(800)
		fact.setSize(35,20)
		fact.setBounce(1);
		if (!gamepad.up && key_up.isUp){
				fact.setVelocity(x_velocity, y_velocity);}
		else{
				if (player.state.includes('right')){
						fact.setVelocity(x_velocity-250, y_velocity-400);}
				else if (player.state.includes('left')){
						fact.setVelocity(x_velocity+250, y_velocity-400);}}
	}

throwFactKeyboard(x_spawn, x_velocity, y_velocity, now){
	var fact = facts.create((player.x + x_spawn), player.y-10, 'fact');
	fact.setData('time_thrown', now)
	fact.setSize(45,20)
	fact.setBounce(1);
		if (key_up.isUp){
				fact.setVelocity(x_velocity, y_velocity);}
		else{
				if (player.state.includes('right')){
						fact.setVelocity(x_velocity-250, y_velocity-400);}
				else if (player.state.includes('left')){
						fact.setVelocity(x_velocity+250, y_velocity-400);}}
		fact.onCollide = true;
		}

damagePlayer(player, time, damage){
		if (player.data.values.post_hit_invincibility){
				return}
		player.data.values.health -= damage;
		if (player.data.values.health > 0){
				player.setAlpha(0.5)}  // Opacity
		life_bar.setFrame(player.data.values.health)
		player.data.values.post_hit_invincibility = true;
		time_at_damage = time
		}

healPlayer(player, juice, amount){
		juice.destroy()
		player.data.values.health += amount;
		if(player.data.values.health > 18){
				player.data.values.health = 18}
		life_bar.setFrame(player.data.values.health)
		}

knockPlayerBack(now){
		if (now < (player.data.values.time_at_collision + player.data.values.knock_back_time)) {
				if (player.x <= player.data.values.hit_from){
						player.setVelocityX(-150)
						player.state = 'hit_to_left'}
				else{
						player.setVelocityX(150)
						player.state = 'hit_to_right'}}

else if (now >= (player.data.values.time_at_collision + 200)  // 200 miliseconds after hit
					&& player.state.includes('hit')
					&& player.body.onFloor()) {
		this.resetPlayerState()}
	}

resetPlayerState(){
		if (player.body.onFloor()){
			if (player.data.values.hit_from < player.x){
					player.state = 'stand_left'}
			else {
					player.state = 'stand_right'}}
		else{
			if (player.data.values.hit_from < player.x){
					player.state = 'jump_left'}
			else {
					player.state = 'jump_right'}}
		}

throwBS(self, npc){
		var bs_array = ['bs','bs2','bs3','bs4','bs5','bs6','bs7','bs8','bs9','bs10','bs11','bs12','bs13'];

		var random_bs = bs_array[Math.floor(Math.random() * bs_array.length)];

		var bs = bs_statements.create(npc.x, npc.y-45, random_bs);
		bs.setData('time_thrown', self.time.now)
		bs.setSize(120,80)
		bs.setBounce(1);
		if (npc.state.includes('right')){
				bs.setVelocity(250, -200);}
		else if (npc.state.includes('left')){
				bs.setVelocity(-250, -200);}
		npc.setData({'throw_wait': Phaser.Math.Between(4000, 7000),
								 'last_throw': self.time.now,
								 'may_throw': false})
		npc.data.values.bs_thrown += 1

		player_bs = self.physics.add.collider(bs, player, function(bs, player){  // ONLY RUNS DURING COLLISION, DOES NOT LOOP
				bs.destroy()
				bs.setVisible(false)
				player.data.values.time_at_collision = self.time.now
				player.data.values.hit_from = bs.x
				this.damagePlayer(player, self.time.now, 4)
			}, this.checkPostHitInvincibility, self);
	}

dropCensorship(self, bird){
		var censor = censorships.create(bird.x, bird.y, 'censored');
		bird.setData({'throw_wait': Phaser.Math.Between(4000, 7000),
								 'last_throw': self.time.now,
								 'may_throw': false})

		player_censor = self.physics.add.collider(censor, player, function(censor, player){  // ONLY RUNS DURING COLLISION, DOES NOT LOOP
				censor.destroy()
				censor.setVisible(false)
				player.data.values.time_at_collision = self.time.now
				player.data.values.hit_from = censor.x
				this.damagePlayer(player, self.time.now, 4)
			}, this.checkPostHitInvincibility, self);
	}

checkPostHitInvincibility(){
		return (player.data.values.post_hit_invincibility == false)}


update (){ // FINITE STATE MACHINE //

		if(gamepad){
		if (player.state == 'stand_right'){
				player.setVelocityX(0);
				player.anims.play('stand_right', true);
				if (!player.body.onFloor()){
						player.state = 'jump_right'}
				else if (Phaser.Input.Keyboard.JustDown(key_jump) || (gamepad.A && player.data.values.may_jump)) {
						player.setVelocityY(-650);
						player.state = 'jump_right';}
				else if (key_right.isDown || gamepad.right){
						player.state = 'run_right'}
				else if (key_left.isDown || gamepad.left){
						player.state = 'run_left'}
				else if (Phaser.Input.Keyboard.JustDown(key_throw) || (gamepad.X && player.data.values.may_throw)) {
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'stand_right_and_throw'
						this.throwFactGamePad(35, 450, -200, this.time.now)
					}}

		if (player.state == 'stand_left'){
				player.setVelocityX(0);
				player.anims.play('stand_left', true);
				if (!player.body.onFloor()){
						player.state = 'jump_left'}
				else if (Phaser.Input.Keyboard.JustDown(key_jump) || (gamepad.A && player.data.values.may_jump)){
						player.setVelocityY(-650);
						player.state = 'jump_left';}
				else if (key_left.isDown || gamepad.left){
						player.state = 'run_left'}
				else if (key_right.isDown || gamepad.right){
						player.state = 'run_right'}
				else if (Phaser.Input.Keyboard.JustDown(key_throw) || (gamepad.X && player.data.values.may_throw)) {
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'stand_left_and_throw'
						this.throwFactGamePad(-35, -450, -200, this.time.now)
					}}

		if (player.state == 'run_right'){
				player.setVelocityX(200);
				player.anims.play('run_right', true);
				if (!player.body.onFloor()){
						player.state = 'jump_right'}
				else if (Phaser.Input.Keyboard.JustDown(key_jump) || (gamepad.A && player.data.values.may_jump)){
						player.setVelocityY(-650);
						player.state = 'jump_right'}
				else if ((key_right.isUp) && (!gamepad.right)){
						player.state = 'stand_right'}
				else if ((key_left.isDown && key_right.isUp) || gamepad.left){
						player.state = 'run_left'}
				else if (key_left.isDown && key_right.isDown){
						player.state = 'run_right'}
				else if (Phaser.Input.Keyboard.JustDown(key_throw) || (gamepad.X && player.data.values.may_throw)){
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'stand_right_and_throw'
						this.throwFactGamePad(35, 450, -200, this.time.now)
					}}

		if (player.state == 'run_left'){
				player.setVelocityX(-200);
				player.anims.play('run_left', true);
				if (!player.body.onFloor()){
						player.state = 'jump_left'}
				else if (Phaser.Input.Keyboard.JustDown(key_jump) || (gamepad.A && player.data.values.may_jump)){
						player.setVelocityY(-650);
						player.state = 'jump_left'}
				else if (key_left.isUp && (!gamepad.left)){
						player.state = 'stand_left'}
				else if ((key_right.isDown && key_left.isUp) || gamepad.right){
						player.state = 'run_right'}
				else if (key_left.isDown && key_right.isDown){
						player.state = 'run_left'}
				else if (Phaser.Input.Keyboard.JustDown(key_throw) || (gamepad.X && player.data.values.may_throw)){
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'stand_left_and_throw'
						this.throwFactGamePad(-35, -450, -200, this.time.now)
					}}

		if (player.state == 'jump_right'){
				player.anims.play('jump_right', true);
				if (player.body.onFloor()){
						player.state = 'stand_right'}
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){
						player.state = 'jump_right'}
				else if ((key_right.isDown && Phaser.Input.Keyboard.JustDown(key_throw)) || (gamepad.right && (gamepad.X && player.data.values.may_throw))) {
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'jump_right_and_throw'
						this.throwFactGamePad(35, 450, -200, this.time.now)
						player.setVelocityX(250);}
				else if (key_right.isDown || gamepad.right){
						player.setVelocityX(250);}
						else if (Phaser.Input.Keyboard.JustDown(key_left) || (gamepad.left)){
								player.state = 'jump_left'}
				else if ((key_right.isUp && Phaser.Input.Keyboard.JustDown(key_throw)) || ((!gamepad.right) && (gamepad.X && player.data.values.may_throw))) {
						player.setVelocityX(0);
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'jump_right_and_throw';
						this.throwFactGamePad(35, 450, -200, this.time.now)
					}
				else if (Phaser.Input.Keyboard.JustUp(key_right) || (!gamepad.right)){
						player.setVelocityX(0);}}

		if (player.state == 'jump_left'){
				player.anims.play('jump_left', true);
				if (player.body.onFloor()){
						player.state = 'stand_left'}
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){
						player.state = 'jump_left'}
				else if ((key_left.isDown && Phaser.Input.Keyboard.JustDown(key_throw)) || (gamepad.left && (gamepad.X && player.data.values.may_throw))) {
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'jump_left_and_throw'
						this.throwFactGamePad(-35, -450, -200, this.time.now)
						player.setVelocityX(-250);}
				else if (key_left.isDown || gamepad.left){
						player.setVelocityX(-250);}
						else if (Phaser.Input.Keyboard.JustDown(key_right) || (gamepad.right)){
								player.state = 'jump_right'}
				else if ((key_right.isUp && Phaser.Input.Keyboard.JustDown(key_throw)) || ((!gamepad.left) && (gamepad.X && player.data.values.may_throw))) {
						player.setVelocityX(0);
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'jump_left_and_throw';
						this.throwFactGamePad(-35, -450, -200, this.time.now)
					}
				else if (Phaser.Input.Keyboard.JustUp(key_left) || (!gamepad.left)){
						player.setVelocityX(0);}}

		if (player.state == 'stand_right_and_throw'){
				player.setVelocityX(0);
				player.anims.play('stand_right_and_throw', true);
				now = Math.trunc(this.time.now);
				if (now >= (time_at_throw_start + 250)){ // gives 250 miliseconds so it plays the animation once
						player.state = 'stand_right'}}

		if (player.state == 'stand_left_and_throw'){
				player.setVelocityX(0);
				player.anims.play('stand_left_and_throw', true);
				now = Math.trunc(this.time.now);
				if (now >= (time_at_throw_start + 250)){
						player.state = 'stand_left'}}

		if (player.state == 'jump_right_and_throw'){
				player.anims.play('jump_right_and_throw', true);
				now = Math.trunc(this.time.now);
				if (now >= (time_at_throw_start + 250)){
						player.state = 'jump_right'}}

		if (player.state == 'jump_left_and_throw'){
				player.anims.play('jump_left_and_throw', true);
				now = Math.trunc(this.time.now);
				if (now >= (time_at_throw_start + 250)){
						player.state = 'jump_left'}}

		if (player.state == 'hit_to_left'){
				player.anims.play('hit_to_left');
				if (key_left.isDown || gamepad.left){
						player.setVelocityX(-250);}
				// else if (Phaser.Input.Keyboard.JustUp(key_left)){
				// 		player.setVelocityX(0);}
				else if ((key_right.isDown || gamepad.right) && (!player.body.onFloor())) {
						player.state = 'hit_to_right'}}

		if (player.state == 'hit_to_right'){
				player.anims.play('hit_to_right');
				if (key_right.isDown || gamepad.right){
						player.setVelocityX(250);}
				else if (Phaser.Input.Keyboard.JustUp(key_right)){
						player.setVelocityX(0);}
				else if (key_left.isDown || gamepad.left){
						player.state = 'hit_to_left'}}

		player.data.values.may_jump = ((gamepad.A) ? false : true);
		player.data.values.may_throw = ((gamepad.X) ? false : true);
}
else{ // KEYBOARD ONLY - NO GAMEPAD																////
	if (player.state == 'stand_right'){                                                         ////
				player.setVelocityX(0);                                                         ////
				player.anims.play('stand_right', true);                                         ////
				if (!player.body.onFloor()){                                                    ////
						player.state = 'jump_right'}                                            ////
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){                             ////
						player.setVelocityY(-650);                                              ////
						player.state = 'jump_right';}                                           ////
				else if (key_right.isDown){                                                     ////
						player.state = 'run_right'}                                             ////
				else if (key_left.isDown){                                                      ////
						player.state = 'run_left'}                                              ////
				else if (Phaser.Input.Keyboard.JustDown(key_throw)){                            ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'stand_right_and_throw'
						this.throwFactKeyboard(35, 450, -200, this.time.now)
					}}                                ////
                                                                                                ////
		if (player.state == 'stand_left'){                                                      ////
				player.setVelocityX(0);                                                         ////
				player.anims.play('stand_left', true);                                          ////
				if (!player.body.onFloor()){                                                    ////
						player.state = 'jump_left'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){                             ////
						player.setVelocityY(-650);                                              ////
						player.state = 'jump_left';}                                            ////
				else if (key_left.isDown){                                                      ////
						player.state = 'run_left'}                                              ////
				else if (key_right.isDown){                                                     ////
						player.state = 'run_right'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(key_throw)){                            ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'stand_left_and_throw'
						this.throwFactKeyboard(-35, -450, -200, this.time.now)
					}}                                 ////
                                                                                                ////
		if (player.state == 'run_right'){                                                       ////
				player.setVelocityX(200);                                                       ////
				player.anims.play('run_right', true);                                           ////
				if (!player.body.onFloor()){                                                    ////
						player.state = 'jump_right'}                                            ////
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){                             ////
						player.setVelocityY(-650);                                              ////
						player.state = 'jump_right'}                                            ////
				else if (Phaser.Input.Keyboard.JustUp(key_right)){                              ////
						player.state = 'stand_right'}                                           ////
				else if (key_left.isDown && key_right.isUp){                                    ////
						player.state = 'run_left'}                                              ////
				else if (key_left.isDown && key_right.isDown){                                  ////
						player.state = 'run_right'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(key_throw)){                            ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'stand_right_and_throw'
						this.throwFactKeyboard(35, 450, -200, this.time.now)
					}}                                ////
                                                                                                ////
		if (player.state == 'run_left'){                                                        ////
				player.setVelocityX(-200);                                                      ////
				player.anims.play('run_left', true);                                            ////
				if (!player.body.onFloor()){                                                    ////
						player.state = 'jump_left'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){                             ////
						player.setVelocityY(-650);                                              ////
						player.state = 'jump_left'}                                             ////
				else if (Phaser.Input.Keyboard.JustUp(key_left)){                               ////
						player.state = 'stand_left'}                                            ////
				else if (key_right.isDown && key_left.isUp){                                    ////
						player.state = 'run_right'}                                             ////
				else if (key_left.isDown && key_right.isDown){                                  ////
						player.state = 'run_left'}                                              ////
				else if (Phaser.Input.Keyboard.JustDown(key_throw)){                            ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'stand_left_and_throw'
						this.throwFactKeyboard(-35, -450, -200, this.time.now)
					}}                                 ////
                                                                                                ////
		if (player.state == 'jump_right'){                                                      ////
				player.anims.play('jump_right', true);                                          ////
				if (player.body.onFloor()){                                                     ////
						player.state = 'stand_right'}                                           ////
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){                             ////
						player.state = 'jump_right'}                                            ////
				else if (key_right.isDown && Phaser.Input.Keyboard.JustDown(key_throw)){        ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'jump_right_and_throw'
						this.throwFactKeyboard(35, 450, -200, this.time.now)                                 ////
						player.setVelocityX(250);}                                              ////
				else if (key_right.isDown){                                                     ////
						player.setVelocityX(250);}                                              ////
				else if (Phaser.Input.Keyboard.JustUp(key_right)){                              ////
						player.setVelocityX(0);}                                                ////
				else if (Phaser.Input.Keyboard.JustDown(key_left)){                             ////
						player.state = 'jump_left'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(key_throw)){                            ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'jump_right_and_throw'
						this.throwFactKeyboard(35, 450, -200, this.time.now)
					}}                                 ////
                                                                                                ////
		if (player.state == 'jump_left'){                                                       ////
				player.anims.play('jump_left', true);                                           ////
				if (player.body.onFloor()){                                                     ////
						player.state = 'stand_left'}                                            ////
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){                             ////
						player.state = 'jump_left'}                                             ////
				else if (key_left.isDown && Phaser.Input.Keyboard.JustDown(key_throw)){         ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'jump_left_and_throw'
						this.throwFactKeyboard(-35, -450, -200, this.time.now)                              ////
						player.setVelocityX(-250);}                                             ////
				else if (key_left.isDown){                                                      ////
						player.setVelocityX(-250);}                                             ////
				else if (Phaser.Input.Keyboard.JustUp(key_left)){                               ////
						player.setVelocityX(0);}                                                ////
				else if (Phaser.Input.Keyboard.JustDown(key_right)){                            ////
						player.state = 'jump_right'}                                            ////
				else if (Phaser.Input.Keyboard.JustDown(key_throw)){                            ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'jump_left_and_throw'
						this.throwFactKeyboard(-35, -450, -200, this.time.now)
					}}                                  ////
                                                                                                ////
		if (player.state == 'stand_right_and_throw'){                                           ////
				player.setVelocityX(0);                                                         ////
				player.anims.play('stand_right_and_throw', true);                               ////
				now = Math.trunc(this.time.now);                                                ////                                               ////
				if (now >= (time_at_throw_start + 250)){                                        ////
						player.state = 'stand_right'}}                                          ////
                                                                                                ////
		if (player.state == 'stand_left_and_throw'){                                            ////
				player.setVelocityX(0);                                                         ////
				player.anims.play('stand_left_and_throw', true);                                ////
				now = Math.trunc(this.time.now);                                                ////                                             ////
				if (now >= (time_at_throw_start + 250)){                                        ////
						player.state = 'stand_left'}}                                           ////
                                                                                                ////
		if (player.state == 'jump_right_and_throw'){                                            ////
				player.anims.play('jump_right_and_throw', true);                                ////
				now = Math.trunc(this.time.now);                                                ////                                               ////
				if (now >= (time_at_throw_start + 250)){                                        ////
						player.state = 'jump_right'}}                                           ////
                                                                                                ////
		if (player.state == 'jump_left_and_throw'){                                             ////
				player.anims.play('jump_left_and_throw', true);                                 ////
				now = Math.trunc(this.time.now);                                                ////                                             ////
				if (now >= (time_at_throw_start + 250)){                                        ////
						player.state = 'jump_left'}}                                            ////
                                                                                                ////
		if (player.state == 'hit_to_left'){                                                     ////
				player.anims.play('hit_to_left');                                               ////
				if (key_left.isDown){                                                           ////
						player.setVelocityX(-250);}                                             ////
				// else if (Phaser.Input.Keyboard.JustUp(key_left)){                            ////
				// 		player.setVelocityX(0);}                                                ////
				else if (key_right.isDown && (!player.body.onFloor())) {                        ////
						player.state = 'hit_to_right'}}                                         ////
                                                                                                ////
		if (player.state == 'hit_to_right'){                                                    ////
				player.anims.play('hit_to_right');                                              ////
				if (key_right.isDown){                                                          ////
						player.setVelocityX(250);}                                              ////
				else if (Phaser.Input.Keyboard.JustUp(key_right)){                              ////
						player.setVelocityX(0);}                                                ////
				else if (key_left.isDown){                                                      ////
						player.state = 'hit_to_left'}}                                          ////
		}                                                                                               ////

		if (player.data.values.health <= 0){
				player.data.values.knock_back_time = 5000}

		npc_enemies.children.each(function(npc) {

				if (npc.active){
				// NPC ANIMATIONS
				var player_npc_x_diff = ((player.x >= npc.x) ? (player.x - npc.x) : (npc.x - player.x));
				var player_npc_y_diff = ((player.y >= npc.y) ? (player.y - npc.y) : (npc.y - player.y));
				if ((player_npc_x_diff < 800) && (player_npc_y_diff < 400)){  // Player in the NPC 'chase box'
						if (npc.x > player.x+10){
								npc.setVelocityX(-50);
								npc.state = 'walk_left'
								npc.anims.play('npc_walk_left', true);}
						else if (npc.x < player.x-10){
								npc.setVelocityX(50);
								npc.state = 'walk_right'
								npc.anims.play('npc_walk_right', true);}
						else if ((player.x+10) > npc.x && npc.x > (player.x-10)){
								npc.setVelocityX(0);
								npc.state = 'stand_right'
								npc.anims.play('npc_stand_right', true)}
						//THROW BS
								if (this.time.now > (npc.data.values.last_throw + npc.data.values.throw_wait)) {
										this.throwBS(this, npc)}
							} // player in chase box
				else {
						if (npc.x < player.x){
								npc.state = 'stand_right'
								npc.setVelocityX(0);
								npc.anims.play('npc_stand_right', true)}
						else{
								npc.state = 'stand_left'
								npc.setVelocityX(0);
								npc.anims.play('npc_stand_left', true)}}

				if (this.time.now < (npc.data.values.time_at_collision + npc.data.values.knock_back_time)) {
						if (npc.data.values.health <= 0){
								npc.setPosition(npc.x, npc.y-6, npc.z, npc.w)}
						if (npc.x <= player.x){
								npc.setVelocityX(-300)
								npc.anims.play('npc_hit_to_left', true);}
						else{
								npc.setVelocityX(300)
								npc.anims.play('npc_hit_to_right', true);}
							}

				if (npc.y > (layer.height+100)){  //shake camera if NPC falls off screen and has no health
						if (npc.data.values.health <= 0){
								this.cameras.main.shake(100, 0.005);}
						this.time.delayedCall(400, function() {
						npc.destroy()
						npc.setVisible(false)
						}, [], this);}

				} // if NPC active
			}, this); // looping through all NPCs


			birds.children.each(function(bird) {
					if (bird.active){
					if (bird.x > (player.x + 800)){
							bird.state = 'fly_left'}
					else if (bird.x < (player.x-800)){
							bird.state = 'fly_right'}

					if (bird.state == 'fly_left'){
							bird.setVelocityX(-(bird.data.values.speed))
							bird.anims.play('bird_fly_left', true);}
					else if (bird.state == 'fly_right'){
							bird.setVelocityX(bird.data.values.speed)
							bird.anims.play('bird_fly_right', true);}

					if (bird.data.values.health <= 0){
							if (bird.x <= player.x){
									bird.setVelocityX(-300)
									bird.anims.play('bird_hit_to_left', true);}
							else{
									bird.setVelocityX(300)
									bird.anims.play('bird_hit_to_right', true);}}
					if (bird.y > (layer.height+100)){
							bird.destroy()
							bird.setVisible(false)}

					try{
							if (this.time.now > (bird.data.values.last_throw + bird.data.values.throw_wait)) {
									this.dropCensorship(this, bird)}}
					catch(TypeError){
							console.log('Caught bird TypeError')} // This is OK
					}  // if bird active
				}, this); // looping through all birds

			tomatoes.children.each(function(tomato) {
					// console.log('now', this.time.now)
					// console.log('last right', tomato.data.values.last_turned_right)
					// console.log('last left', tomato.data.values.last_turned_left)

					if (tomato.active){
							if(tomato.state == 'roll_left'){
									tomato.setVelocityX(-300)
									tomato.anims.play('tomato_roll_left', true);
									tomato.setAngularVelocity(-200)
							}
							else if(tomato.state == 'roll_right'){
									tomato.setVelocityX(300)
									tomato.anims.play('tomato_roll_right', true);
									tomato.setAngularVelocity(200)
							}
					if ((tomato.x > (player.x + 1000)) ||
					((tomato.state == 'roll_right') && this.time.now > (tomato.data.values.last_turned_right + 5000))){
							tomato.data.values.last_turned_left = this.time.now
							tomato.state = 'roll_left'}
					else if ((tomato.x < (player.x-1000)) ||
					((tomato.state == 'roll_left') && this.time.now > (tomato.data.values.last_turned_left + 5000))){
							tomato.data.values.last_turned_right = this.time.now
							tomato.state = 'roll_right'}

					if (tomato.data.values.health <= 0){
							if (tomato.x <= player.x){
									tomato.setVelocityX(-300)
									tomato.setAngularVelocity(400)
								}
							else{
									tomato.setAngularVelocity(-400)
								}
							if (tomato.y > (layer.height+100)){
									tomato.destroy()
									tomato.setVisible(false)}
						}
					}  // if tomato active
				}, this); // looping through all tomatoes





				this.knockPlayerBack(this.time.now)

				// if (player.data.values.post_hit_invincibility == true) {
				// 		this.physics.world.removeCollider(player_bs);}

				//turn off post hit-invincibility
				if (Math.trunc(this.time.now) > (Math.trunc(time_at_damage) + 2000)) {
						player.data.values.post_hit_invincibility = false
						player.setAlpha(1)}

				//knock player off screen
				if (player.data.values.health <= 0){
						if (this.time.now < (player.data.values.time_at_collision + 5000)) {
								player.setPosition(player.x, player.y-5, player.z, player.w)
								player.setAngularVelocity(-400)}
						this.physics.world.removeCollider(player_layer);
						}

				if (player.y > (layer.height+100)){
						this.cameras.main.shake(500);
						this.time.delayedCall(400, function() { // wait 400 ms, then fade
							this.cameras.main.fade(300); // fade effect lasts 400 ms
						}, [], this);
						this.time.delayedCall(800, function() {
							this.scene.restart(); // restart game
						}, [], this);
						}


}

}

class Level2 extends Phaser.Scene {
	constructor() {
	  super("level2");
	}

		preload(){

		}

		create(){
				// SceneA.addNPC(SceneA, 800, 0)
				// this.addNPC(this, 800, 0)
		}

		update(){

		}
}


var config = {
		type: Phaser.AUTO,
		width: 1200,
		height: 650,
    backgroundColor: '#272765',
		input: {
				gamepad: true
		},
		physics: {
				default: 'arcade',
				arcade: {
						gravity: { y: 800 },
						debug: false
				}
		},
		// scene: {
		// 		preload: preload,
		// 		create: create,
		// 		update: update
		// }
		scene: [Level1]
};

var player;
var npc_enemies;
var birds;
var censorships;
var juice;
var aj;
var tomatoes;
var npc;
var bird;
var tomato;
var platforms;
var facts;
var bs_statements;
var game = new Phaser.Game(config);
var score;
var scoreText;
var tileset;
var map;
var layer;
var currently_throwing;
var time_at_throw_start
var time_at_damage
var player_layer
var player_npc;
var player_bird;
var player_tomato;
var player_juice;
var player_bs;
var player_censor;
var npc_layer
var gamepad
var gamepad_A_last_pressed = 0
var gamepad_X_last_pressed = 0
var gamepad_left_last_pressed
var gamepad_right_last_pressed
var key_left;
var key_right;
var key_up;
var key_jump;
var key_throw;
var now;
var life_bar;
