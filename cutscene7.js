import Confetti from "./confetti.js"
import StaticPlayer from "./static_player.js"

export default class CutScene7 extends Phaser.Scene {

constructor() {
  	super({key: "cutscene7"});
}

create(){
  // this.time.delayedCall(10000, function() {
  //   this.scene.start('level6')
  //     }, [], this);

  this.input.gamepad.once('down', function (pad, button, index) {
			this.gamepad = pad;
    }, this);
  this.key_skip = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);



  this.add.image(600, 300, 'level2_sky')
  this.add.image(550, 495, 'hill').setScale(2)
  // this.player = this.physics.add.sprite(600, 300, 'jeremy_straight_face').setScale(3.5).setFrame(2).body.setAllowGravity(false)
  this.static_player_sprites = new StaticPlayer(this)
  this.player = this.static_player_sprites.addStaticPlayer(this, 600, 300)

  this.confetti_sprites = new Confetti(this)

  this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5).setDepth(1000)
  this.text = this.add.text(20, 515, "NPC culture has been defeated, now our voices will never", {fill: '#fff'})
  this.text.setFontSize(34).setStroke('#000', 4).setDepth(1000)
  this.text2 = this.add.text(80 , 545, "be silenced, and it's all thanks to Geeks + Gamers!", { fill: '#fff'})
  this.text2.setFontSize(34).setStroke('#000', 4).setDepth(1000)
  this.c7_started = false

  this.music = this.sound.add('end_credits_soundtrack');
  var music_config = {
    mute: false,
    volume: 0.3,
    rate: 1,
    detune: 0,
    seek: 0,
    loop: true,
    delay: 0
  }
  this.music.play(music_config)

  this.vl71 = this.sound.add('vl71')
  if (!this.vl71_played){
    this.time.delayedCall(1000, function() {
      this.vl71.play()
      }, [], this)
    this.vl71_played = true}

    this.time.delayedCall(9000, function() {
      this.add.text(980, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
    }, [], this)
}

update(){
  if (!this.c7_started){
    this.c7_start_time = this.time.now
    this.c7_started = true
  }

  if (Phaser.Input.Keyboard.JustDown(this.key_skip)) { //  || (this.gamepad.A)

      this.end_level()}

  if (this.gamepad){
    if (this.gamepad.A) {
        this.end_level()}}

  this.confetti_sprites.update();

  if (this.time.now < this.c7_start_time + 10000){
    this.confetti_sprites.addConfetti(this)
    // this.confetti_sprites.addConfetti(this)
  }
}

end_level(){
  this.cameras.main.fade(2100, 0, 0, 0);
  this.time.delayedCall(2000, function() {
    this.music.stop()
    this.scene.start('credits')
  }, [], this)
}

} // class
