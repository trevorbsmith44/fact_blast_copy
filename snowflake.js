export default class Snowflake {

constructor(scene, player){
    scene.snowflakes = scene.physics.add.group();
    this.player = player
    this.scene = scene

    this.player_snowflake = scene.physics.add.collider(this.player.player, scene.snowflakes, function(player, snowflake){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = snowflake.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 3)
        }, function(player, snowflake){
            return (snowflake.data.values.health > 0
              && this.player.player.data.values.post_hit_invincibility == false)
        }, this);

}

update(player){
  this.scene.snowflakes.children.each(function(snowflake) {
      if (snowflake.active){
      if (snowflake.y > ((this.scene.layers.layer.height*2)+100)){
          snowflake.destroy()
          snowflake.setVisible(false)}

      if (snowflake.data){ // prevents TypeError
          // DIAGONAL
              if (snowflake.data.values.direction == 'up right') {
                  snowflake.setAngularVelocity(50)
                  snowflake.setVelocity(200, -snowflake.data.values.speed)} // up right
              else if (snowflake.data.values.direction == 'up left') {
                  snowflake.setAngularVelocity(-50)
                  snowflake.setVelocity(-200, -snowflake.data.values.speed)} // up left
              else if (snowflake.data.values.direction == 'down right') {
                  snowflake.setAngularVelocity(50)
                  snowflake.setVelocity(200, snowflake.data.values.speed)} // down right
              else if (snowflake.data.values.direction == 'down left') {
                  snowflake.setAngularVelocity(-50)
                  snowflake.setVelocity(-200, snowflake.data.values.speed)} // down left

          } // if snowflake.data exists

      }  // if snowflake active
    }, this); // looping through all snowflakes
}

addSnowflake(scene, x, y, direction) {
		var snowflake = this.scene.snowflakes.create(x,y, 'snowflake');
		snowflake.setActive(true).setVisible(true)
		snowflake.setData({'health': 1,
								 'throw_wait': Phaser.Math.Between(2000, 4000),
						 		 'last_throw': 0,
								 'speed': Phaser.Math.Between(100, 200),
                 'x_origin': x,
                 'y_origin': y,
                 'player_x_origin': this.scene.player.player.x,
                 'player_y_origin': this.scene.player.player.y,
                 'direction': direction
							 });
		snowflake.setSize(80, 50, false)
		snowflake.setOffset(15,5)
    snowflake.setScale(4)
    snowflake.setDepth(500)
    snowflake.setCircle(17,1,2)
		this.scene.snowflakes.add(snowflake)
		snowflake.body.setAllowGravity(false);

    this.scene.enemies_added += 1;
}

}
