import Preload from "./preload.js";
import Player from "./player.js";
import Fact from  "./fact.js";
import Npc from  "./npc.js";
import Bs from  "./bs.js";
import Bird from  "./bird.js";
import Censor from "./censor.js"
import Tomato from  "./tomato.js";
import Juice from "./juice.js"
import Balloon from "./balloon.js"
import CarWheel from "./car_wheel.js"
import Flag from "./flag.js"
import Bot from "./bot.js"
import SmallBot from "./small_bot.js"
import BossTomato from "./boss_tomato.js"

export default class Level2 extends Phaser.Scene {

constructor() {
  	super({key: "level2"});
}

create (){
  this.scene_name = 'level2'
    this.sky = this.add.tileSprite(0, -100, 1200, 800, "level2_sky"); // x, y, width, height
    this.sky.setOrigin(0, 0);
    this.sky.setScrollFactor(0); // repeats background
    this.sky.setTileScale(0.5,0.5);
    this.bg_1 = this.add.tileSprite(0, -100, 1200, 800, "forest_b"); // clouds
    this.bg_1.setOrigin(0, 0);
    this.bg_1.setScrollFactor(0); // repeats background
    this.bg_1.setPosition(0, 150)
    // this.bg_1.setTileScale(3,3);
    this.bg_3 = this.add.tileSprite(0, 0, 1200, 800, "forest_c"); // dark green hills
    this.bg_3.setOrigin(0, 0);
    this.bg_3.setScrollFactor(0);
    this.bg_3.setPosition(0, 200)
    // this.bg_3.setTileScale(2.5,2.5);
    this.bg_2 = this.add.tileSprite(0, 0, 1200, 800, "level2_bg2"); // road and light green grass
    this.bg_2.setOrigin(0, -.001);
    this.bg_2.setScrollFactor(0);
    this.bg_2.setTileScale(2.5,2.5);

    this.map = this.add.tilemap("level2_map");
    var tileset = this.map.addTilesetImage("tileset", "level2_tileset", 32, 32, 1, 2);  //margin=1, spacing=2, this is needed because of using the frame extruder program
    this.layers = []
    this.layers.layer = this.map.createStaticLayer(0, tileset, 0, 0); // layer index, tileset, x, y
    this.layers.push(this.layers.layer)
    this.layers.layer.setCollisionByProperty({ collides: true }); // tiles w/ collides set to true (in Tiled) will collide
    this.layers.layer.setScale(2)

    this.car_wheel_sprites = new CarWheel(this)
    this.car_wheel_sprites.addCarWheel(this, 70, 545, 2.25, 0) //bus
    this.car_wheel_sprites.addCarWheel(this, 510, 545, 2.25, 0) //bus
    this.car_wheel_sprites.addCarWheel(this, 620, 545, 2, 1) //car
    this.car_wheel_sprites.addCarWheel(this, 774, 545, 2, 1) //car
    this.car_wheel_sprites.addCarWheel(this, 888, 545, 2, 2)
    this.car_wheel_sprites.addCarWheel(this, 1025, 545, 2, 2)
    this.car_wheel_sprites.addCarWheel(this, 1150, 545, 2, 3)
    this.car_wheel_sprites.addCarWheel(this, 1280, 545, 2, 3)
    this.car_wheel_sprites.addCarWheel(this, 1410, 545, 2, 3)
    this.car_wheel_sprites.addCarWheel(this, 1530, 545, 2, 3)
    this.car_wheel_sprites.addCarWheel(this, 1730, 545, 2.25, 0)
    this.car_wheel_sprites.addCarWheel(this, 2090, 545, 2.25, 0)
    this.car_wheel_sprites.addCarWheel(this, 2355, 535, 3, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 2450, 535, 3, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 2920, 545, 2.5, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 3000, 545, 2.5, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 3325, 545, 2.5, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 3460, 545, 2.25, 5) //jeep
    this.car_wheel_sprites.addCarWheel(this, 3580, 545, 2.25, 5) //jeep
    this.car_wheel_sprites.addCarWheel(this, 3710, 545, 2, 1)
    this.car_wheel_sprites.addCarWheel(this, 3840, 545, 2, 1)
    this.car_wheel_sprites.addCarWheel(this, 3950, 545, 2, 3)
    this.car_wheel_sprites.addCarWheel(this, 4100, 545, 2, 3)
    this.car_wheel_sprites.addCarWheel(this, 4355, 545, 2.25, 5) //truck
    this.car_wheel_sprites.addCarWheel(this, 4540, 545, 2.25, 5) //truck
    this.car_wheel_sprites.addCarWheel(this, 4670, 545, 2, 3)
    this.car_wheel_sprites.addCarWheel(this, 4800, 545, 2, 3)
    this.car_wheel_sprites.addCarWheel(this, 4920, 545, 2, 4)
    this.car_wheel_sprites.addCarWheel(this, 5070, 545, 2, 4)
    this.car_wheel_sprites.addCarWheel(this, 5250, 545, 2.25, 5)
    this.car_wheel_sprites.addCarWheel(this, 5435, 545, 2.25, 5)
    this.car_wheel_sprites.addCarWheel(this, 5630, 545, 2, 3)
    this.car_wheel_sprites.addCarWheel(this, 5770, 545, 2, 3)
    this.car_wheel_sprites.addCarWheel(this, 5880, 545, 2, 2)
    this.car_wheel_sprites.addCarWheel(this, 6020, 545, 2, 2)
    this.car_wheel_sprites.addCarWheel(this, 6210, 545, 2, 2)
    this.car_wheel_sprites.addCarWheel(this, 6330, 545, 2, 2)
    this.car_wheel_sprites.addCarWheel(this, 6530, 545, 2.25, 0)
    this.car_wheel_sprites.addCarWheel(this, 6892, 545, 2.25, 0)
    this.car_wheel_sprites.addCarWheel(this, 7155, 535, 3, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 7250, 535, 3, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 7830, 545, 2.5, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 7900, 545, 2.5, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 8255, 545, 2.5, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 8370, 535, 3, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 8465, 535, 3, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 9130, 535, 3, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 9230, 535, 3, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 9330, 535, 3, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 9430, 535, 3, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 10200, 545, 2.5, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 10280, 545, 2.5, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 10620, 545, 2.5, 0) //semi
    this.car_wheel_sprites.addCarWheel(this, 10810, 545, 2, 1) //car
    this.car_wheel_sprites.addCarWheel(this, 10945, 545, 2, 1) //car
    this.car_wheel_sprites.addCarWheel(this, 11140, 545, 2.25, 0) //bus
    this.car_wheel_sprites.addCarWheel(this, 11500, 545, 2.25, 0) //bus
    this.car_wheel_sprites.addCarWheel(this, 11715, 545, 2.25, 0) //bus
    this.car_wheel_sprites.addCarWheel(this, 12160, 545, 2.25, 0) //bus
    this.car_wheel_sprites.addCarWheel(this, 12350, 545, 2.25, 5) //jeep
    this.car_wheel_sprites.addCarWheel(this, 12480, 545, 2.25, 5) //jeep
    this.car_wheel_sprites.addCarWheel(this, 12670, 545, 2, 2) //car
    this.car_wheel_sprites.addCarWheel(this, 12800, 545, 2, 2) //car
    this.car_wheel_sprites.addCarWheel(this, 13055, 545, 2.25, 5) //truck
    this.car_wheel_sprites.addCarWheel(this, 13245, 545, 2.25, 5) //truck
    this.car_wheel_sprites.addCarWheel(this, 13440, 545, 2, 4) //car
    this.car_wheel_sprites.addCarWheel(this, 13570, 545, 2, 4) //car
    this.car_wheel_sprites.addCarWheel(this, 13690, 545, 2, 1) //car
    this.car_wheel_sprites.addCarWheel(this, 13820, 545, 2, 1) //car
    this.car_wheel_sprites.addCarWheel(this, 13950, 545, 2, 2) //car
    this.car_wheel_sprites.addCarWheel(this, 14070, 545, 2, 2) //car
    this.car_wheel_sprites.addCarWheel(this, 14275, 545, 2.25, 0) //bus
    this.car_wheel_sprites.addCarWheel(this, 14460, 545, 2.25, 0) //bus
    this.car_wheel_sprites.addCarWheel(this, 14780, 545, 2, 2) //car
    this.car_wheel_sprites.addCarWheel(this, 14910, 545, 2, 2) //car
    this.car_wheel_sprites.addCarWheel(this, 15170, 545, 2, 3) //car
    this.car_wheel_sprites.addCarWheel(this, 15290, 545, 2, 3) //car
    this.car_wheel_sprites.addCarWheel(this, 15490, 545, 2.25, 0) //bus
    this.car_wheel_sprites.addCarWheel(this, 15850, 545, 2.25, 0) //bus
    this.car_wheel_sprites.addCarWheel(this, 16070, 545, 2, 3) //car
    this.car_wheel_sprites.addCarWheel(this, 16250, 545, 2, 3) //car
    this.car_wheel_sprites.addCarWheel(this, 16520, 545, 2, 3) //car
    this.car_wheel_sprites.addCarWheel(this, 16640, 545, 2, 3) //car
    this.car_wheel_sprites.addCarWheel(this, 16960, 545, 2.25, 5) //truck
    this.car_wheel_sprites.addCarWheel(this, 17150, 545, 2.25, 5) //truck
    this.car_wheel_sprites.addCarWheel(this, 17285, 545, 2, 2) //car
    this.car_wheel_sprites.addCarWheel(this, 17400, 545, 2, 2) //car
    this.car_wheel_sprites.addCarWheel(this, 17665, 545, 2.25, 5) //truck
    this.car_wheel_sprites.addCarWheel(this, 17790, 545, 2.25, 5) //truck
    this.car_wheel_sprites.addCarWheel(this, 18100, 545, 2, 3) //car
    this.car_wheel_sprites.addCarWheel(this, 19050, 545, 2, 3) //car

    this.player_x_spawn = 100 //100
    if (this.cp == 1){
        this.player_x_spawn = 10592}
    else if (this.cp == 2){
        this.player_x_spawn = 17696} //17696

		this.player = new Player(this, this.player_x_spawn, -100);
    this.cameras.main.setBounds(0, 0, (this.map.widthInPixels*2), (this.map.heightInPixels*2));
    this.cameras.main.startFollow(this.player.player);

    this.bs_statements = this.physics.add.group(); // Must have this here, otherwise facts go through BS randomly
    this.censorships = this.physics.add.group();
    this.math_stuff = this.physics.add.group();
    this.demonetization = this.physics.add.group();
    this.milkshake = this.physics.add.group();
    this.regular_tomato = this.physics.add.group();
    this.trash_can = this.physics.add.group();

    this.juice_sprites = new Juice(this, this.player)
    this.juice_sprites.addJuice(this, 6262, 0, 'small')
    this.juice_sprites.addJuice(this, 10150, 0, 'small')
    this.juice_sprites.addJuice(this, 17100, 0, 'big')

    this.npc_sprites = new Npc(this, this.player)
    this.bird_sprites = new Bird(this, this.player)
    this.tomato_sprites = new Tomato(this, this.player)
    this.bot_sprites = new Bot(this, this.player)
    this.small_bot_sprites = new SmallBot(this, this.player)
    this.boss_tomato_sprites = new BossTomato(this, this.player)

    this.enemies = []
    for (var i=0;i<this.enemies.length;i++){
        this.enemies[i] = false}

    this.music = this.sound.add('level_2_soundtrack');
    var music_config = {
      mute: false,
      volume: 0.5,
      rate: 1,
      detune: 0,
      seek: 0,
      loop: true,
      delay: 0
    }
    this.music.play(music_config)
    // this.game.sound.mute = true

}

update(){
    this.player.update();
    this.npc_sprites.update(this.player.player);
    this.bird_sprites.update(this.player.player);
    this.tomato_sprites.update(this.player.player);
    this.bot_sprites.update(this.player.player);
    this.boss_tomato_sprites.update(this.player.player)
    this.small_bot_sprites.update(this.player.player);

    var px = this.player.player.x

    if (px > 500 && !this.vl18_played){
      this.vl18.play()
      this.vl18_played = true}

    if (px > 3450 && !this.vl20_played){
      this.vl20.play()
      this.vl20_played = true}

    if (px > 9200 && !this.vl37_played){
      this.vl37.play()
      this.vl37_played = true}

    if (px > 12938 && !this.vl11_played){
      this.vl11.play()
      this.vl11_played = true}

    if (px > 15964 && !this.vl28_played){
      this.vl28.play()
      this.vl28_played = true}

    if (px > 18626 && !this.vl42_played){
      this.time.delayedCall(2050, function() {
        this.vl42.play()
      }, [], this);
      this.vl42_played = true}

    if (px > 150 && (!this.enemies.enemy0) && this.cp < 1 ) {
        this.tomato_sprites.addTomato(this, 1300, 300, 'red')
        this.enemies.enemy0 = true}

    if (px > 1568 && (!this.enemies.enemy1) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 2400, 200, 'stand', 'low')
        this.enemies.enemy1 = true}

    if (px > 3104 && (!this.enemies.enemy2) && this.cp < 1 ) {
        this.tomato_sprites.addTomato(this, 2336, 200, 'red')
        this.enemies.enemy2 = true}

    if (px > 3900 && (!this.enemies.enemy3) && this.cp < 1 ) {
        this.tomato_sprites.addTomato(this, 5024, 300, 'green')
        this.enemies.enemy3 = true}

    if (px > 3424 && (!this.enemies.enemy4) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 4340, 200, 'stand', 'low')
        this.enemies.enemy4 = true}

    if (px > 992 && (!this.enemies.enemy5) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 1824, 200, 'walk', 'none')
        this.enemies.enemy5 = true}

    if (px >  4576 && (!this.enemies.enemy6) && this.cp < 1 ) {
        this.bird_sprites.addBird(this, 5344, 50, 'drop')
        this.enemies.enemy6 = true}

    if (px > 5850 && (!this.enemies.enemy7) && this.cp < 1 ) {
        this.tomato_sprites.addTomato(this, 4300, 300, 'green')
        this.enemies.enemy7 = true}

    if (px > 6176 && (!this.enemies.enemy8) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 7008, 200, 'walk', 'none')
        this.enemies.enemy8 = true}

    if (px > 7200 && (!this.enemies.enemy9) && this.cp < 1 ) {
        this.tomato_sprites.addTomato(this, 8096, 200, 'red')
        this.enemies.enemy9 = true}

    if (px > 7200 && (!this.enemies.enemy10) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 8250, 200, 'stand', 'none')
        this.enemies.enemy10 = true}

    if (px > 7700 && (!this.enemies.enemy11) && this.cp < 1 ) {
        this.small_bot_sprites.addSmallBot(this, 10592, 250, false)
        this.enemies.enemy11 = true}

    if (px > 10610 && (!this.enemies.enemy12) && this.cp < 2 ) {
        this.small_bot_sprites.addSmallBot(this, 12192, 250, false)
        this.enemies.enemy12 = true}

    if (px > 12750 && (!this.enemies.enemy12_1) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 13750, 200, 'stand', 'none')
        this.enemies.enemy12_1 = true}

    if (px > 13755 && (!this.enemies.enemy12_2) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 14755, 200, 'stand', 'none')
        this.enemies.enemy12_2 = true}

    if (px > 12225 && (!this.enemies.enemy13) && this.cp < 2 ) {
        this.tomato_sprites.addTomato(this, 13472, 300, 'green')
        this.enemies.enemy13 = true}

    if (px > 13216 && (!this.enemies.enemy14) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 15870, 200, 'stand', 'none')
        this.enemies.enemy14 = true}

    if (px > 15000 && (!this.enemies.enemy15) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 15940, 200, 'stand', 'low')
        this.enemies.enemy15 = true}

    if (px > 15264 && (!this.enemies.enemy16) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 16544, 200, 'stand', 'none')
        this.enemies.enemy16 = true}

    if (px > 15550 && (!this.enemies.enemy17) && this.cp < 2 ) {
        this.tomato_sprites.addTomato(this, 14700, 300, 'green')
        this.enemies.enemy17 = true}

    if (px > 15675 && (!this.enemies.enemy17_1) && this.cp < 2 ) {
        this.tomato_sprites.addTomato(this, 16900, 300, 'green')
        this.enemies.enemy17_1 = true}

    if (px > 16160 && (!this.enemies.enemy18) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 17120, 200, 'stand', 'low')
        this.enemies.enemy18 = true}

    if (px > 18000 && (!this.enemies.enemy18_1)) {
        this.npc_sprites.addNPC(this, 19025, 200, 'stand', 'none')
        this.enemies.enemy18_1 = true}

    if (px > 18626 && (!this.enemies.enemy19)) {
        this.boss_tomato_sprites.addBossTomato(this, 18975, -200, true)
        this.enemies.enemy19 = true}

//18626

    // CHECKPOINTS
    if (!this.cp) {
        this.cp = 0}
    if (px > 10592 && this.cp == 0) {
        this.cp = 1}
    if (px > 17696 && this.cp == 1) {
        this.cp = 2}

    // END LEVEL
    if (this.data.values.boss_defeated == true){
        this.music.stop()
        this.time.delayedCall(2000, function() {
            this.start('cutscene3');
            }, [], this.scene);
        }

    // PARALLAX
    this.bg_2.tilePositionX += 10;
    this.bg_3.tilePositionX += 2;
    this.bg_1.tilePositionX += 1;

    // BOSS FIGHT
    if (px > 18625){
        this.cameras.main.stopFollow()
        this.stop_follow = true
      }

    if (this.stop_follow && (px < 17960)) {  // restart if player escapes boss arena
        this.stop_follow = false
        this.music.stop()
        this.scene.start();
    }
}

topCollisionOnly(sprite, tile){
    tile.setCollision(false, false, true, false)} //left, right, top, bottom

} // class
