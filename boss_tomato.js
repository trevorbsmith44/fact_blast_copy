import RegularTomato from  "./regular_tomato.js";
import TrashCan from  "./trash_can.js";

export default class BossTomato {

constructor(scene, player){
    scene.boss_tomatoes = scene.physics.add.group();
    this.scene = scene
    this.player = player
    scene.whack1 = scene.sound.add('whack1')
    scene.splat = scene.sound.add('splat')
    scene.clang2 = scene.sound.add('clang2')

    this.player_boss_tomato = scene.physics.add.overlap(this.player.player, scene.boss_tomatoes,
      function(player, boss_tomato){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = boss_tomato.x
        scene.whack1.play()
        this.player.damagePlayer(this.player, scene, 6)
        boss_tomato.body.immovable = true;
        }, function(player, boss_tomato){
            return (boss_tomato.data.values.health > 0
              && this.player.player.data.values.post_hit_invincibility == false)
        }, this);

    this.npc_boss_tomato = scene.physics.add.collider(scene.npc_enemies, scene.boss_tomatoes,
      function(npc, boss_tomato){
        npc.setFrame(7)
        scene.whack1.play()
        npc.data.values.health = 0
        npc.data.values.time_at_collision = this.scene.manual_time
        npc.data.values.knock_back_time = 10000
        npc.setAngularVelocity(300)
        this.scene.physics.world.removeCollider(npc.data.values.layer_collision);
        this.already_collided_with_npc = true
      }, function(npc, boss_tomato){
          return (!this.already_collided_with_npc)
      }, this);

    this.regular_tomato_boss_tomato = scene.physics.add.overlap(scene.regular_tomato, scene.boss_tomatoes,
      function(projectile, boss_tomato){
        projectile.setFrame(1)
        projectile.can_hurt_boss = false
        this.scene.time.delayedCall(100, function() {
          projectile.destroy()
        }, [], this.scene);
        scene.splat.play()
        boss_tomato.data.values.health -= 1;
        var boss_life_frame = Phaser.Math.CeilTo((boss_tomato.data.values.health/boss_tomato.data.values.max_health) * 18)
        boss_tomato.life_bar.setFrame(boss_life_frame)
        boss_tomato.time_at_hit = scene.manual_time
        if (!boss_tomato.state.includes('hit')){
            boss_tomato.state_before_hit = boss_tomato.state}
        if (boss_tomato.state.includes('throw_left') || boss_tomato.state == 'hit_left' || boss_tomato.state == 'defeated_left'){
            boss_tomato.state = 'hit_left'}
        else {
            boss_tomato.state = 'hit_right'}
        }, function(projectile, boss_tomato){
            return (projectile.can_hurt_boss == true)
        }, this);

}

update(player){
  this.scene.boss_tomatoes.children.each(function(boss_tomato) {
      if (boss_tomato.active){
        // console.log(boss_tomato.state)

        for (var i=0;i<boss_tomato.projectiles.length;i++){
            boss_tomato.projectiles[i].update()
        }

        if (boss_tomato.state == 'walk_left_throw_left'){
                boss_tomato.anims.play('boss_tomato_walk_left', true);
                boss_tomato.setVelocityX(-50);
            if (boss_tomato.x < player.x){
                boss_tomato.state = 'walk_right_throw_right'}
            else if (boss_tomato.x < boss_tomato.data.values.origin_x-100 && boss_tomato.x > player.x){
                boss_tomato.state = 'walk_right_throw_left'}
            if (this.scene.manual_time > boss_tomato.data.values.last_throw + boss_tomato.data.values.throw_wait){
                boss_tomato.time_at_throw_start = Math.trunc(this.scene.manual_time)
                boss_tomato.previous_state = boss_tomato.state
                boss_tomato.setData('throw_wait', Phaser.Math.Between(1000, 1500))
                this.scene.time.delayedCall(350, function() {
                    if (boss_tomato.state.includes('hit') || boss_tomato.state.includes('defeated')){
                        return}
                    var diff = boss_tomato.x - player.x
                    var rand_array = [0,1,2];
                    if (boss_tomato.data.values.health < 6){
                      var random_num = rand_array[Math.floor(Math.random() * rand_array.length)];}
                    else {
                      random_num = 0}
                    if (random_num == 0 || boss_tomato.data.values.consecutive_cans == 4){
                        boss_tomato.projectiles.push(new RegularTomato(this, boss_tomato, this.player, 'left'))
                        boss_tomato.data.values.consecutive_cans = 0}
                    else{
                        boss_tomato.projectiles.push(new TrashCan(this, boss_tomato, this.player, 'left'))
                        boss_tomato.data.values.consecutive_cans += 1}
                    }, [], this.scene);
                boss_tomato.state = 'throw_left'}
            }

        else if (boss_tomato.state == 'walk_right_throw_left'){
                boss_tomato.anims.play('boss_tomato_walk_left', true);
                boss_tomato.setVelocityX(50);
              if (boss_tomato.x < player.x){
                  boss_tomato.state = 'walk_right_throw_right'}
              if (boss_tomato.x > boss_tomato.data.values.origin_x && boss_tomato.x>player.x){
                  boss_tomato.state = 'walk_left_throw_left'}
            if (this.scene.manual_time > boss_tomato.data.values.last_throw + boss_tomato.data.values.throw_wait){
                boss_tomato.time_at_throw_start = Math.trunc(this.scene.manual_time)
                boss_tomato.previous_state = boss_tomato.state
                boss_tomato.setData('throw_wait', Phaser.Math.Between(1000, 1500))
                this.scene.time.delayedCall(350, function() {
                  if (boss_tomato.state.includes('hit') || boss_tomato.state.includes('defeated')){
                      return}
                    var diff = boss_tomato.x - player.x
                    var rand_array = [0,1,2];
                    // var rand_array = [0];
                    if (boss_tomato.data.values.health < 6){
                      var random_num = rand_array[Math.floor(Math.random() * rand_array.length)];}
                    else {
                      random_num = 0}
                    if (random_num == 0 || boss_tomato.data.values.consecutive_cans == 4){
                        boss_tomato.projectiles.push(new RegularTomato(this, boss_tomato, this.player, 'left'))
                        boss_tomato.data.values.consecutive_cans = 0}
                    else{
                        boss_tomato.projectiles.push(new TrashCan(this, boss_tomato, this.player, 'left', diff))
                        boss_tomato.data.values.consecutive_cans += 1}
                    }, [], this.scene);
                boss_tomato.state = 'throw_left'}
              }
        else if (boss_tomato.state == 'walk_right_throw_right'){
                boss_tomato.anims.play('boss_tomato_walk_right', true);
                boss_tomato.setVelocityX(50);
            if (boss_tomato.x > player.x){
                boss_tomato.state = 'walk_left_throw_left'}
            else if (boss_tomato.x > boss_tomato.data.values.origin_x+100 && boss_tomato.x < player.x){
                boss_tomato.state = 'walk_left_throw_right'}
            if (this.scene.manual_time > boss_tomato.data.values.last_throw + boss_tomato.data.values.throw_wait){
                boss_tomato.time_at_throw_start = Math.trunc(this.scene.manual_time)
                boss_tomato.previous_state = boss_tomato.state
                boss_tomato.setData('throw_wait', Phaser.Math.Between(1000, 1500))
                this.scene.time.delayedCall(350, function() {
                  if (boss_tomato.state.includes('hit') || boss_tomato.state.includes('defeated')){
                      return}
                    var diff = player.x - boss_tomato.x
                    var rand_array = [0,1,2];
                    // var rand_array = [0];
                    if (boss_tomato.data.values.health < 6){
                      var random_num = rand_array[Math.floor(Math.random() * rand_array.length)];}
                    else {
                      random_num = 0}
                    if (random_num == 0 || boss_tomato.data.values.consecutive_cans == 4){
                        boss_tomato.projectiles.push(new RegularTomato(this, boss_tomato, this.player, 'right'))
                        boss_tomato.data.values.consecutive_cans = 0}
                    else{
                        boss_tomato.projectiles.push(new TrashCan(this, boss_tomato, this.player, 'right', diff))
                        boss_tomato.data.values.consecutive_cans += 1}
                    }, [], this.scene);
                boss_tomato.state = 'throw_right'}
            }

        else if (boss_tomato.state == 'walk_left_throw_right'){
                boss_tomato.anims.play('boss_tomato_walk_right', true);
                boss_tomato.setVelocityX(-50);
              if (boss_tomato.x > player.x){
                  boss_tomato.state = 'walk_left_throw_left'}
              if (boss_tomato.x < boss_tomato.data.values.origin_x && boss_tomato.x<player.x){
                  boss_tomato.state = 'walk_right_throw_right'}
            if (this.scene.manual_time > boss_tomato.data.values.last_throw + boss_tomato.data.values.throw_wait){
                boss_tomato.time_at_throw_start = Math.trunc(this.scene.manual_time)
                boss_tomato.previous_state = boss_tomato.state
                boss_tomato.setData('throw_wait', Phaser.Math.Between(1000, 1500))
                this.scene.time.delayedCall(350, function() {
                  if (boss_tomato.state.includes('hit') || boss_tomato.state.includes('defeated')){
                      return}
                    var diff = player.x - boss_tomato.x
                    var rand_array = [0,1,2];
                    // var rand_array = [0];
                    if (boss_tomato.data.values.health < 6){
                      var random_num = rand_array[Math.floor(Math.random() * rand_array.length)];}
                    else {
                      random_num = 0}
                    if (random_num == 0 || boss_tomato.data.values.consecutive_cans == 4){
                        boss_tomato.projectiles.push(new RegularTomato(this, boss_tomato, this.player, 'right'))
                        boss_tomato.data.values.consecutive_cans = 0}
                    else{
                        boss_tomato.projectiles.push(new TrashCan(this, boss_tomato, this.player, 'right', diff))
                        boss_tomato.data.values.consecutive_cans += 1}
                    }, [], this.scene);
                boss_tomato.state = 'throw_right'}
              }

        else if (boss_tomato.state == 'throw_left'){
            boss_tomato.data.values.last_throw = this.scene.manual_time;
            boss_tomato.setVelocityX(0);
            if (!boss_tomato.data.values.throw_sound_playing){
              this.scene.boss_tomato_throw_sound.play()
              boss_tomato.data.values.throw_sound_playing = true}
            boss_tomato.anims.play('boss_tomato_throw_left', true)
            if (this.scene.manual_time >= (boss_tomato.time_at_throw_start + 750)){
              boss_tomato.data.values.throw_sound_playing = false
              boss_tomato.state = boss_tomato.previous_state}
            }

        else if (boss_tomato.state == 'throw_right'){
            boss_tomato.data.values.last_throw = this.scene.manual_time;
            boss_tomato.setVelocityX(0);
            if (!boss_tomato.data.values.throw_sound_playing){
              this.scene.boss_tomato_throw_sound.play()
              boss_tomato.data.values.throw_sound_playing = true}
            boss_tomato.anims.play('boss_tomato_throw_right', true)
            if (this.scene.manual_time >= (boss_tomato.time_at_throw_start + 750)){
              boss_tomato.data.values.throw_sound_playing = false
              boss_tomato.state = boss_tomato.previous_state}
            }

        else if (boss_tomato.state == 'defeated_right'){
            boss_tomato.setVelocityX(0);
            boss_tomato.setFrame(11);
            this.scene.time.delayedCall(2000, function() {
              try {
                  // this.physics.world.removeCollider(boss_tomato.data.values.layer_collision)
                  for (var i=0;i<this.scene.scene.layers.length;i++){
                      this.physics.world.removeCollider(boss_tomato.data.values.layer_collisions[i]);}
                    }
              catch (TypeError){
                  console.log('Caught Boss Tomato TypeError')}
                }, [], this.scene);
            }

        else if (boss_tomato.state == 'defeated_left'){
          boss_tomato.setVelocityX(0);
          boss_tomato.setFrame(5);
            this.scene.time.delayedCall(2000, function() {
              try {
                  // this.physics.world.removeCollider(boss_tomato.data.values.layer_collision)
                  for (var i=0;i<this.scene.scene.layers.length;i++){
                      this.physics.world.removeCollider(boss_tomato.data.values.layer_collisions[i]);}
                }
              catch (TypeError){
                  console.log('Caught Boss Tomato TypeError')}
                }, [], this.scene);
            }

        else if (boss_tomato.state == 'hit_right'){
            boss_tomato.setVelocityX(0);
            boss_tomato.setFrame(11)
            if (this.scene.manual_time > boss_tomato.time_at_hit + 1000){
                boss_tomato.state = boss_tomato.state_before_hit}}

        else if (boss_tomato.state == 'hit_left'){
            boss_tomato.setVelocityX(0);
            boss_tomato.setFrame(5)
            if (this.scene.manual_time > boss_tomato.time_at_hit + 1000){
                boss_tomato.state = boss_tomato.state_before_hit}}

        else if (boss_tomato.state == 'fall_left'){
            boss_tomato.setVelocityX(0);
            boss_tomato.setFrame(0)
            if (boss_tomato.body.onFloor()){
                boss_tomato.state = 'walk_left_throw_left'}}

        else if (boss_tomato.state == 'fall_right'){
            boss_tomato.setVelocityX(0);
            boss_tomato.setFrame(0)
            if (boss_tomato.body.onFloor()){
                boss_tomato.state = 'walk_right_throw_right'}}

        if (boss_tomato.data.values.health <= 0){
            if (boss_tomato.state.includes('throw_left') || boss_tomato.state.includes('hit_left')){
                boss_tomato.state = 'defeated_left'}
            else if (boss_tomato.state.includes('throw_right') || boss_tomato.state.includes('hit_right')){
                boss_tomato.state = 'defeated_right'}}

        if (boss_tomato.y > ((this.scene.layers.layer.height*2)+1000)){
            boss_tomato.life_bar.setVisible(false)
            if (!this.crash_tomato_played){
              this.scene.crash.play()
              this.crash_tomato_played = true}
            this.scene.cameras.main.shake(100, 0.01);
            this.scene.time.delayedCall(350, function() {
            try {
            if (boss_tomato.data.values.boss == true){
                this.scene.data.values.boss_defeated = true
                }}
            catch (TypeError){
                console.log('Caught Boss Tomato TypeError')}
            boss_tomato.destroy()
            boss_tomato.setVisible(false)
            }, [], this);
          }

        if(boss_tomato.state.includes('defeated') && boss_tomato.data.values.boss == false){
          for (var i=0;i<this.scene.barrier_array.length;i++){
            if (this.scene.barrier_array[i].x < 10000){
              this.scene.barrier_array[i].body.setAllowGravity(true)}}}

      } // if boss_tomato active
    }, this); // looping through all boss_tomatoes
} // update

addBossTomato(scene, x, y, level_boss) {
    var boss_health = 6
    if (!level_boss){
      boss_health = 4
    }
		var boss_tomato = scene.boss_tomatoes.create(x, y, 'boss_tomato');
		boss_tomato.setActive(true).setVisible(true)
		boss_tomato.setData({'health': boss_health, // 6
								 'time_at_collision':null,
								 'knock_back_time': 150,
								 'bs_thrown': 0,
								 'layer_collisions': [],
						 		 'last_throw': 0,
                 'throw_wait': Phaser.Math.Between(1500, 3500),
                 'boss': level_boss,
                 'origin_x': x,
                 'throw_sound_playing': false,
                 'consecutive_cans': 0,
                 'max_health': boss_health
							 });

     for (var i=0;i<scene.layers.length;i++){
         boss_tomato.data.values.layer_collisions.push(scene.physics.add.collider(boss_tomato, scene.layers[i]));}

    boss_tomato.body.immovable = true;
    boss_tomato.setScale(2.7)
    // boss_tomato.setOffset(92,67) //y=25
		// boss_tomato.setSize(72, 114, false) //h=156
    boss_tomato.setCircle(42,48,30)
    boss_tomato.setDepth(740);
    boss_tomato.setMaxVelocity(300, 800)
    boss_tomato.projectiles = []
    if (boss_tomato.x > this.player.player.x){
        boss_tomato.state = 'fall_left'}
    else if (boss_tomato.x <= this.player.player.x){
        boss_tomato.state = 'fall_right'}
		scene.boss_tomatoes.add(boss_tomato)
    scene.enemies_added += 1;

    boss_tomato.life_bar = scene.physics.add.sprite(1060, 30, 'boss_life_bar').setDepth(1000);
    boss_tomato.life_bar.body.setAllowGravity(false);
    boss_tomato.life_bar.setScrollFactor(0,0)
    boss_tomato.life_bar.setFrame(18)

}

}
