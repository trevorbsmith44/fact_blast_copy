export default class CarWheel {

constructor(scene){
    scene.car_wheels = scene.physics.add.group();
}

addCarWheel(scene, x, y, scale, frame) {
		var car_wheel = scene.car_wheels.create(x, y, 'car_wheel');
		car_wheel.setActive(true).setVisible(true);
		car_wheel.setScale(scale);
    car_wheel.setDepth(2);
    car_wheel.setFrame(frame)

		scene.car_wheels.add(car_wheel);
    car_wheel.body.setAllowGravity(false);
    car_wheel.setAngularVelocity(500);
}

}
