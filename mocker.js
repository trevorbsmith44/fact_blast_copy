export default class Mocker {

constructor(scene){
    scene.mockers = scene.physics.add.group();
}

addMocker(scene, x, y, facing) {
		var mocker = scene.mockers.create(x, y, 'npc');
		mocker.setActive(true).setVisible(true);
		mocker.setScale(1.4);
    if (facing == 'right'){
        mocker.setFrame(8)}
    else{
      mocker.setFrame(9)}
    mocker.setDepth(1)


		scene.mockers.add(mocker);
    mocker.body.setAllowGravity(false);
    mocker.body.allowGravity = false;
}

}
