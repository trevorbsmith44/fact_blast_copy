export default class CutScene1 extends Phaser.Scene {

constructor() {
  	super({key: "cutscene1"});
}

create(){
  this.current_image = 1
  this.c1i1 = this.add.image(600, 300, 'c1i0')
  this.add.text(400, 490, "Press A", {fill: '#fff'}).setFontSize(100).setStroke('#000', 4)
  this.may_skip = true
  this.input.gamepad.once('down', function (pad, button, index) {
			this.gamepad = pad;
    }, this);

  this.key_skip = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);

  this.music = this.sound.add('cutscene_soundtrack');
  var music_config = {
    mute: false,
    volume: 0.3,
    rate: 1,
    detune: 0,
    seek: 0,
    loop: true,
    delay: 0
  }
  this.music.play(music_config)
}

update(){
  if (Phaser.Input.Keyboard.JustDown(this.key_skip)) { //  || (this.gamepad.A)
    if (this.current_image == 1){
      this.add.image(600, 300, 'c1i1')
      this.add.text(980, 450, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 2}
    else if (this.current_image == 2){
      this.add.image(600, 300, 'c1i2')
      this.add.text(980, 450, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 3}
    else if (this.current_image == 3){
      this.add.image(600, 300, 'c1i3')
      this.add.text(980, 450, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 4}
    else if (this.current_image == 4){
      this.music.stop()
      this.scene.start('level1')}
      }

  if (this.gamepad){
    if (this.gamepad.A && this.may_skip) {
      if (this.current_image == 1){
        this.add.image(600, 300, 'c1i1')
        this.add.text(980, 450, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 2}
      if (this.current_image == 2){
        this.add.image(600, 300, 'c1i2')
        this.add.text(980, 450, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 3}
      else if (this.current_image == 3){
        this.add.image(600, 300, 'c1i3')
        this.add.text(980, 450, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 4}
      else if (this.current_image == 4){
        this.music.stop()
        this.scene.start('level1')}}
    this.may_skip = ((this.gamepad.A) ? false : true);
  }
}

} // class

//image 2
// this.sky = this.add.tileSprite(0, -100, 1200, 800, "level1_sky"); // x, y, width, height
// this.sky.setOrigin(0, 0);
// this.sky.setScrollFactor(0); // repeats background
// this.sky.setTileScale(0.5,0.5);
// this.bg_1 = this.add.tileSprite(0, -100, 1200, 800, "level1_bg1"); // x, y, width, height
// this.bg_1.setOrigin(0, 0);
// this.bg_1.setScrollFactor(0); // repeats background
// this.bg_1.setTileScale(3,3);
// this.bg_1.tilePositionX = 100
// this.bg_2 = this.add.tileSprite(0, -25, 1200, 800, "level1_bg2");
// this.bg_2.setOrigin(0, 0);
// // this.bg_2.setScrollFactor(100);
// this.bg_2.setTileScale(2.5,2.65);
// this.bg_2.tilePositionX = 1000
//
// this.phone = this.add.image(600, 275, 'phone')
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
//
// this.text = this.add.text(150, 510, 'A post online reveals that NPC culture has ', { fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(150, 545, 'set out to take over the entire internet!', { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)
// this.text3 = this.add.text(500, 424, 'At last, the conquest', { fill: '#000'})
// this.text3.setFontSize(17)
// this.text4 = this.add.text(500, 444, 'has begun. #CancelG+G', { fill: '#000'})
// this.text4.setFontSize(17)
// this.text5 = this.add.text(440, 480, '38k', { fill: '#fff'})
// this.text5.setFontSize(19)
//
// this.eye1 = this.physics.add.sprite(550,185, 'eye').setScale(1.5);
// this.eye1.body.setAllowGravity(false)
// this.eye1.setFrame(0)
//
// this.eye2 = this.physics.add.sprite(600,185, 'eye').setScale(1.5);
// this.eye2.body.setAllowGravity(false)
// this.eye2.setFrame(0).setAngle(90)





// // // image3
// this.sky = this.add.tileSprite(0, -100, 1200, 800, "level1_sky"); // x, y, width, height
// this.sky.setOrigin(0, 0);
// this.sky.setScrollFactor(0); // repeats background
// this.sky.setTileScale(0.5,0.5);
// this.bg_1 = this.add.tileSprite(0, -100, 1200, 800, "level1_bg1"); // x, y, width, height
// this.bg_1.setOrigin(0, 0);
// this.bg_1.setScrollFactor(0); // repeats background
// this.bg_1.setTileScale(3,3);
// this.bg_1.tilePositionX = 50
// this.bg_2 = this.add.tileSprite(0, -25, 1200, 800, "level1_bg2");
// this.bg_2.setOrigin(0, 0);
// // this.bg_2.setScrollFactor(100);
// this.bg_2.setTileScale(2.5,2.65);
// this.bg_2.tilePositionX = 500
//
// this.player = this.physics.add.sprite(100,350, 'jeremy').setScale(4);
// this.player.body.setAllowGravity(false)
// this.player.setFrame(20)
//
// this.physics.add.sprite(700,350, 'npc').setScale(4).setFrame(5).body.setAllowGravity(false)
//
// this.physics.add.sprite(900,350, 'npc').setScale(4).setFrame(5).body.setAllowGravity(false)
//
// this.physics.add.sprite(1100,350, 'npc').setScale(4).setFrame(5).body.setAllowGravity(false)
//
// this.phone = this.add.image(230, 360, 'phone2').setScale(0.1).setAngle(30)
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
//
// this.text = this.add.text(50, 510, "They're already trying to shut down Geeks and Gamers!", { fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(100, 545, 'Jeremy must use their greatest weakness: FACTS!', { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)
