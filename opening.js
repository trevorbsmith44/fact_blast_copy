export default class Opening extends Phaser.Scene {

constructor() {
  	super({key: "opening"});
}

create (){
  this.play_game = this.physics.add.sprite(600, 300, 'blank_screen')
  this.play_game.setInteractive().on('pointerup', () => this.startTitleScreen())
  this.play_game.body.setAllowGravity(false)
  this.add.text(460, 300, "click anywhere", {fill: '#fff'}).setFontSize(30)
}

startTitleScreen(){
  this.scene.start('title_screen')
}

} // class
