import Bs from  "./bs.js";

export default class Npc {

constructor(scene, player){
    scene.npc_enemies = scene.physics.add.group();
    this.scene = scene
    this.player = player

    this.player_npc = scene.physics.add.collider(this.player.player, scene.npc_enemies, function(player, npc){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = npc.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 3)
        }, function(player, npc){
            return (npc.data.values.health > 0
                    && this.player.player.data.values.post_hit_invincibility == false
                    && this.player.player.data.values.health > 0)
        }, this);

    if (scene.platforms) {
      this.scene.platform_array.forEach(function(platform) {
        scene.physics.add.collider(platform, scene.npc_enemies, null, null, scene);}) }
}

update(player){
  this.scene.npc_enemies.children.each(function(npc) {
      if (npc.active){
      // var player_npc_x_diff = ((player.x >= npc.x) ? (player.x - npc.x) : (npc.x - player.x));
      // var player_npc_y_diff = ((player.y >= npc.y) ? (player.y - npc.y) : (npc.y - player.y));
      // if ((player_npc_x_diff < 800) && (player_npc_y_diff < 400)){  // Player in the NPC 'chase box'

          if (player.data.values.health <= 0 && npc.data.values.health >= 0){
              npc.state = 'celebrating'
              npc.setVelocityX(0)
              if (npc.x >= player.x){
                  npc.setFrame(9)}
              else {
                  npc.setFrame(8)}
          }

          if (npc.state != 'celebrating' || npc.data.values.health <= 0) {
            if (npc.x > player.x+10){
              npc.setVelocityX(-(npc.data.values.speed));
              npc.state = 'walk_left'
              if (npc.data.values.speed != 0) {
                if (npc.data.values.health == 2){
                  npc.anims.play('npc_walk_left', true)}
                else{
                  npc.anims.play('npc_angry_walk_left', true)}
                }
              else {
                if (npc.data.values.health == 2){
                  npc.anims.play('npc_stand_left', true);}
                else{
                  npc.anims.play('npc_angry_stand_left', true)}}
            }
          else if (npc.x < player.x-10){
              npc.setVelocityX(npc.data.values.speed);
              npc.state = 'walk_right'
              if (npc.data.values.speed != 0) {
                if (npc.data.values.health == 2){
                  npc.anims.play('npc_walk_right', true);}
                else{
                  npc.anims.play('npc_angry_walk_right', true)}
              }
              else {
                if (npc.data.values.health == 2){
                  npc.anims.play('npc_stand_right', true);}
                else{
                  npc.anims.play('npc_angry_stand_right', true)}}
            }
          else if ((player.x+10) > npc.x && npc.x > (player.x-10)){
              npc.setVelocityX(0);
              npc.state = 'stand_right'
              if (npc.data.values.health == 2){
                npc.anims.play('npc_stand_right', true)}
              else{
                npc.anims.play('npc_angry_stand_right', true)}
            }
          //THROW BS
              if ((this.scene.manual_time > (npc.data.values.last_throw + npc.data.values.throw_wait)) && npc.body.onFloor()) {
                  this.bs = new Bs(this.scene, npc, this.player)
                }

      if (npc.data.values.time_at_collision && (this.scene.manual_time < (npc.data.values.time_at_collision + npc.data.values.knock_back_time))) {
          if (npc.data.values.health <= 0){
              npc.state = 'defeated'
              npc.setPosition(npc.x, npc.y-6, npc.z, npc.w)}
          if (npc.x <= npc.data.values.hit_from){
              npc.setVelocityX(-300)
              npc.anims.play('npc_hit_to_left', true);}
          else{
              npc.setVelocityX(300)
              npc.anims.play('npc_hit_to_right', true);}
            }

      if (npc.y > ((this.scene.layers.layer.height*2)+100)){
          this.scene.time.delayedCall(400, function() {
          npc.destroy()
          npc.setVisible(false)
          }, [], this);}}

      } // if NPC active
    }, this); // looping through all NPCs
}

addNPC(scene, x, y, speed, freq) {
		var npc = scene.npc_enemies.create(x, y, 'npc');
		npc.setActive(true).setVisible(true)
		npc.setData({'health': 2,
								 'time_at_collision':null,
								 'knock_back_time': 150,
								 'bs_thrown': 0,
						 		 'last_throw': 0,
                 'layer_collisions': []
							 });

    for (var i=0;i<scene.layers.length;i++){
        npc.data.values.layer_collisions.push(scene.physics.add.collider(npc, scene.layers[i]));}

    if (speed == 'stand'){
        npc.setData('speed', 0)}
    else if (speed == 'walk'){
        npc.setData('speed', 50)}
    else if (speed == 'run'){
        npc.setData('speed', 150)}

    if (freq == 'none'){
        npc.setData('min_wait', 1000000000000)
        npc.setData('max_wait', 1000000000000)}
    if (freq == 'low'){
        npc.setData('min_wait', 3000)
        npc.setData('max_wait', 6000)}
    if (freq == 'high'){
        npc.setData('min_wait', 1000)
        npc.setData('max_wait', 3000)}

    npc.setData('throw_wait', Phaser.Math.Between(npc.data.values.min_wait, npc.data.values.max_wait))

		npc.setSize(35, 95, false)
		npc.setOffset(15,0)
		npc.setScale(1.4)
    npc.setDepth(500)
    npc.setMaxVelocity(300, 800)
    // npc.setTint(Phaser.Display.Color.RandomRGB().color)
		scene.npc_enemies.add(npc)
    scene.enemies_added += 1;
}

facts_destroy_bs(fact, bs){
		this.time.delayedCall(100, function() {
				fact.destroy();}, [], this.scene);
		bs.destroy();
	}


}
