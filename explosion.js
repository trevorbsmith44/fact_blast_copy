export default class Explosion {

constructor(scene){
    scene.explosions = scene.physics.add.group();
}

addExplosion(scene, x, y) {
		var explosion = scene.explosions.create(x, y, 'explosion');
		explosion.setActive(true).setVisible(true);
		explosion.setScale(2.5);
    explosion.setDepth(960)

    explosion.body.setAllowGravity(false);
    explosion.body.allowGravity = false;
		scene.explosions.add(explosion);
    explosion.body.setAllowGravity(false);
    scene.anims.play('explosion_explode', explosion);
}

}
