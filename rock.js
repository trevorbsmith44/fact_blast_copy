export default class Rock {

constructor(scene, player){
    scene.rocks = scene.physics.add.group();
    this.player = player
    this.scene = scene

    this.player_rock = scene.physics.add.collider(this.player.player, scene.rocks, function(player, rock){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = rock.x
        scene.bonk.play()
        this.player.damagePlayer(this.player, scene, 6)
        rock.setVelocityX(0)
        }, function(player, rock){
            return (rock.data.values.health > 0
              && this.player.player.data.values.post_hit_invincibility == false)
        }, this);

}

update(player){
  this.scene.rocks.children.each(function(rock) {
      if (rock.active){
          if (rock.y > ((this.scene.layers.layer.height*2)+rock.data.values.respawn)){
              rock.setPosition(rock.data.values.x_origin, rock.data.values.y_origin)
            }

          if (rock.data){ // prevents TypeError
              rock.setAngularVelocity(50)

              } // if rock.data exists

      }  // if rock active
    }, this); // looping through all rocks
}

addRock(scene, x, y) {
		var rock = this.scene.rocks.create(x,y, 'rock');
		rock.setActive(true).setVisible(true)
		rock.setData({'health': 1,
								 'throw_wait': Phaser.Math.Between(2000, 4000),
						 		 'last_throw': 0,
								 'speed': Phaser.Math.Between(100, 200),
                 'x_origin': x,
                 'y_origin': y,
                 'player_x_origin': this.scene.player.player.x,
                 'player_y_origin': this.scene.player.player.y,
                 'respawn': Phaser.Math.Between(150,350),
							 });
		rock.setSize(80, 50, false)
		rock.setOffset(15,5)
    rock.setScale(Phaser.Math.Between(3.3,3.5))
    rock.setDepth(500)
    rock.setCircle(15,1,2)
    rock.setMaxVelocity(300,600)
    rock.setAngle(Phaser.Math.Between(0,359))

		this.scene.rocks.add(rock)

    this.scene.enemies_added += 1;
}

}
