export default class Bs {

constructor(scene, npc, player){
  this.scene = scene
  this.player = player

  for (var i=0;i<scene.layers.length;i++){
      scene.physics.add.collider(scene.layers[i], scene.bs_statements, this.dissolveBS, null, this);
      }

  var bs_array = ['bs','bs2','bs4','bs10','bs10','bs10','bs10','bs12','bs14','bs16','bs17','bs18','bs19','bs20','bs21','bs22','bs23','bs24'];
  var random_bs = bs_array[Math.floor(Math.random() * bs_array.length)];

  var bs = scene.bs_statements.create(npc.x, npc.y-45, random_bs);
  bs.setData({'time_thrown': scene.manual_time,
              'bs_facts': scene.physics.add.overlap(scene.facts, bs, this.facts_destroy_bs, null, scene)
            });
  bs.setSize(120,80);
  bs.setBounce(1);
  bs.setMaxVelocity(900,850);
  bs.setData();
  bs.setDepth(500)
  try{
  if (npc.state.includes('right')){
      bs.setVelocity(250, -200);}
  else if (npc.state.includes('left')){
      bs.setVelocity(-250, -200);}}
  catch(TypeError){
    console.log('caught TypeError')
  }
  npc.setData({'throw_wait': Phaser.Math.Between(npc.data.values.min_wait, npc.data.values.max_wait),
               'last_throw': scene.manual_time,
               'may_throw': false})
  npc.data.values.bs_thrown += 1

  // scene.physics.add.overlap(scene.facts, scene.bs_statements, this.facts_destroy_bs, null, scene);

  scene.physics.add.overlap(bs, this.player.player, function(bs, player){
      bs.destroy()
      bs.setVisible(false)
      scene.player_hit.play()
      this.player.player.data.values.time_at_collision = scene.manual_time
      this.player.player.data.values.hit_from = bs.x
      this.player.damagePlayer(this.player, scene, 4)
    }, function(bs, player){
      // console.log(this.player.player.data.values.post_hit_invincibility == false)
      return (this.player.player.data.values.post_hit_invincibility == false)
    }, this);

}

update(){
}

dissolveBS(bs, target){
		try{
			if (this.scene.manual_time < (bs.data.values.time_thrown + 3000)){
					bs.onCollide = false;
					return}
        }
		catch(TypeError){
				console.log('Caught TypeError for BS')}
		bs.destroy()
		bs.setVisible(false)}

facts_destroy_bs(fact, bs){
		this.time.delayedCall(100, function() {
				fact.destroy();}, [], this.scene);
		bs.destroy();
	}

}
