export default class CutScene2 extends Phaser.Scene {

constructor() {
  	super({key: "cutscene2"});
}

create(){
  this.current_image = 1
  this.c2i1 = this.add.image(600, 300, 'c2i1')
  this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
  this.may_skip = true
  this.input.gamepad.once('down', function (pad, button, index) {
			this.gamepad = pad;
    }, this);

  this.key_skip = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);

  this.music = this.sound.add('cutscene_soundtrack');
  var music_config = {
    mute: false,
    volume: 0.3,
    rate: 1,
    detune: 0,
    seek: 0,
    loop: true,
    delay: 0
  }
  this.music.play(music_config)


}

update(){
  if (Phaser.Input.Keyboard.JustDown(this.key_skip)) { //  || (this.gamepad.A)
    if (this.current_image == 1){
      this.add.image(600, 300, 'c2i2')
      this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 2}
    else if (this.current_image == 2){
      this.add.image(600, 300, 'c2i3')
      this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
      this.current_image = 3}
    else if (this.current_image == 3){
      this.add.image(600, 300, 'c2i4')
      this.current_image = 4}
    else if (this.current_image == 4){
      this.music.stop()
      this.scene.start('level2')}
      }

  if (this.gamepad){
    if (this.gamepad.A && this.may_skip) {
      if (this.current_image == 1){
        this.add.image(600, 300, 'c2i2')
        this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 2}
      else if (this.current_image == 2){
        this.add.image(600, 300, 'c2i3')
        this.add.text(5, 5, "Press A", {fill: '#fff'}).setFontSize(40).setStroke('#000', 4)
        this.current_image = 3}
      else if (this.current_image == 3){
        this.add.image(600, 300, 'c2i4')
        this.current_image = 4}
      else if (this.current_image == 4){
        this.music.stop()
        this.scene.start('level2')}}
    this.may_skip = ((this.gamepad.A) ? false : true);
  }
}
} // class

// c2i1 ==========================================================
// this.sky = this.add.tileSprite(0, -100, 1200, 800, "level2_sky"); // x, y, width, height
// this.sky.setOrigin(0, 0);
// this.sky.setScrollFactor(0); // repeats background
// this.sky.setTileScale(0.5,0.5);
//
// this.fence = this.add.image(600, 400, 'fence')
// this.fence.setScale(3.7)
//
// this.parking_lot = this.add.image(600, 370, 'parking_lot')
// this.parking_lot.setScale(1.35)
// this.wheel1 = this.physics.add.sprite(50,457, 'car_wheel').setFrame(3).setScale(1.5).body.setAllowGravity(false)
// this.wheel2 = this.physics.add.sprite(150,457, 'car_wheel').setFrame(3).setScale(1.5).body.setAllowGravity(false)
//
// this.wheel3 = this.physics.add.sprite(220,457, 'car_wheel').setFrame(2).setScale(1.5).body.setAllowGravity(false)
// this.wheel4 = this.physics.add.sprite(320,457, 'car_wheel').setFrame(2).setScale(1.5).body.setAllowGravity(false)
//
// this.wheel5 = this.physics.add.sprite(450,457, 'car_wheel').setFrame(0).setScale(1.7).body.setAllowGravity(false)
// this.wheel6 = this.physics.add.sprite(535,457, 'car_wheel').setFrame(0).setScale(1.7).body.setAllowGravity(false)
//
// this.wheel7 = this.physics.add.sprite(620,457, 'car_wheel').setFrame(2).setScale(1.5).body.setAllowGravity(false)
// this.wheel8 = this.physics.add.sprite(710,457, 'car_wheel').setFrame(2).setScale(1.5).body.setAllowGravity(false)
//
// this.wheel9 = this.physics.add.sprite(785,457, 'car_wheel').setFrame(3).setScale(1.5).body.setAllowGravity(false)
// this.wheel10 = this.physics.add.sprite(885,457, 'car_wheel').setFrame(3).setScale(1.5).body.setAllowGravity(false)
//
// this.wheel11 = this.physics.add.sprite(1000,457, 'car_wheel').setFrame(0).setScale(1.7).body.setAllowGravity(false)
// this.wheel12 = this.physics.add.sprite(1125,457, 'car_wheel').setFrame(0).setScale(1.7).body.setAllowGravity(false)
//
// this.player = this.physics.add.sprite(350,300, 'jeremy').setScale(5);
// this.player.body.setAllowGravity(false)
// this.player.setFrame(8)
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
// this.text = this.add.text(60, 510, "As the sun sets, Jeremy has reached the parking lot. ", { fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(85, 545, 'Now he can set out west to stop the NPC takeover.', { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)
// ====================================================================



// c2i3 ==========================================================
// this.sky = this.add.tileSprite(0, -100, 1200, 800, "level2_sky"); // x, y, width, height
// this.sky.setOrigin(0, 0);
// this.sky.setScrollFactor(0); // repeats background
// this.sky.setTileScale(0.5,0.5);
//
// this.bg_1 = this.add.tileSprite(0, -100, 1200, 800, "forest_b"); // clouds
// this.bg_1.setOrigin(0, 0);
// this.bg_1.setScrollFactor(0); // repeats background
// this.bg_1.setPosition(0, 150)
//
// this.bg_3 = this.add.tileSprite(0, 0, 1200, 800, "forest_c"); // dark green hills
// this.bg_3.setOrigin(0, 0);
// this.bg_3.setScrollFactor(0);
// this.bg_3.setPosition(0, 200)
//
// this.bg_2 = this.add.tileSprite(0, 0, 1200, 800, "level2_bg2"); // road and light green grass
// this.bg_2.setOrigin(0, -.001);
// this.bg_2.setScrollFactor(0);
// this.bg_2.setTileScale(2.5,2.5);
//
// this.red_car = this.add.image(800, 400, 'red_car').setScale(4.5)
//
// this.wheel1 = this.physics.add.sprite(620, 500, 'car_wheel').setFrame(6).setScale(4).body.setAllowGravity(false)
// this.wheel2 = this.physics.add.sprite(930, 500, 'car_wheel').setFrame(6).setScale(4).setAngle(20).body.setAllowGravity(false)
//
// this.player = this.physics.add.sprite(800, 140, 'jeremy').setScale(3);
// this.player.body.setAllowGravity(false)
// this.player.setFrame(22)
//
// this.wind = this.add.image(350, 340, 'wind').setScale(1)
//
// this.bottom_bar = this.add.image(600, 900, 'black').setScale(4.5)
// this.text = this.add.text(30, 510, "Now he must ride on cars on the interstate to get there.", { fill: '#fff'})
// this.text.setFontSize(34)
// this.text.setStroke('#000', 4)
// this.text2 = this.add.text(110, 545, 'But a threatening group of tomatoes await him...', { fill: '#fff'})
// this.text2.setFontSize(34)
// this.text2.setStroke('#000', 4)


// this.add.text(140, 50, 'Level 6: The Final Battle', {fill: '#FFF'}).setFontSize(60).setStroke('#000', 4)
// this.add.text(150, 180, "To return to this level, hold   ", {fill: '#FFF'}).setFontSize(50).setStroke('#000', 6)
// this.add.text(550, 280, "and", {fill: '#FFF'}).setFontSize(50).setStroke('#000', 6)
// this.add.text(200, 380, "while clicking 'play game'", {fill: '#FFF'}).setFontSize(50).setStroke('#000', 6)
// this.add.image(470, 300, 'keyboard').setScale(1.5)
// this.add.image(720, 300, 'keyboard').setScale(1.5)
// this.add.text(450, 270, "F", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
// this.add.text(700, 270, "B", {fill: '#000'}).setFontSize(50).setStroke('#000', 2).setDepth(1000)
// this.add.text(355, 480, "Press A", {fill: '#fff'}).setFontSize(110).setStroke('#000', 4)
