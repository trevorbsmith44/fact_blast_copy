export default class Machine {

constructor(scene, player){
    scene.machines = scene.physics.add.group();
    this.scene = scene
    this.player = player

    this.scene.player_machine = this.scene.physics.add.collider(scene.machines, this.player.player, function(player, machine){
      player.body.immovable = false;
      machine.body.immovable = true;
      },
      null, this);
}

update(player){
  this.scene.machines.children.each(function(machine) {
      if (machine.active){
        machine.gear1.setPosition(machine.x-30, machine.y-28);
        machine.gear2.setPosition(machine.x+32, machine.y+2);
        machine.gear3.setPosition(machine.x+66, machine.y-23);

        machine.setTint('0xff'+machine.colors[machine.data.values.health]);
        machine.gear1.setTint('0xff'+machine.colors[machine.data.values.health]);
        machine.gear2.setTint('0xff'+machine.colors[machine.data.values.health]);
        machine.gear3.setTint('0xff'+machine.colors[machine.data.values.health]);

        if (machine.data.values.health > 0){
          if (this.scene.time.now > machine.data.values.last_spawned + 2000){
              machine.data.values.can_spawn = true}
          if (machine.data.values.can_spawn == true){
              this.scene.npc_sprites.addNPC(this.scene, machine.x, machine.y, 'walk', 'none')
              machine.data.values.can_spawn = false
              machine.data.values.last_spawned = this.scene.time.now
            }}

        if (machine.data.values.health <= 0 && machine.data.values.destroyed == false){
            if (machine.data.values.explosion_added == false){
              this.scene.explosion_sprites.addExplosion(this.scene, machine.x, machine.y)
              machine.data.values.explosion_added = true}
            if (this.scene.explosions.children['entries'][0].frame.name == 10){
              if (!machine.data.values.explosion_played){
                this.scene.explosion.play()
                machine.data.values.explosion_played = true}
              machine.data.values.destroyed = true
              this.destroyMachine(machine)
              }}
      }
    }, this);
}

addMachine(scene, x, y) {
		var machine = scene.machines.create(x, y, 'machine');
		machine.setActive(true).setVisible(true);
		machine.setScale(3);
    machine.setDepth(950)
    machine.setSize(62, 129, false)
    machine.setOffset(5,0)

    machine.setData( {'health': 42, // 42
                      'layer_collisions': [],
                      'can_spawn': true,
                      'last_spawned': scene.time.now,
                      'destroyed': false,
                      'explosion_added': false,
                      'explosion_played': false
							       });


    machine.body.setAllowGravity(true);
    machine.body.allowGravity = false;

		scene.machines.add(machine);
    scene.anims.play('machine', machine);
    machine.body.immovable = true;

    machine.gear1 = scene.physics.add.sprite(x, y, 'gear');
    machine.gear1.body.setAllowGravity(false);
    machine.gear1.setDepth(951);
    machine.gear1.setFrame(0);
    machine.gear1.setScale(3)
    machine.gear1.setAngularVelocity(200);

    machine.gear2 = scene.physics.add.sprite(x, y, 'gear');
    machine.gear2.body.setAllowGravity(false);
    machine.gear2.setDepth(951);
    machine.gear2.setFrame(1);
    machine.gear2.setScale(3)
    machine.gear2.setAngularVelocity(-200);

    machine.gear3 = scene.physics.add.sprite(x, y, 'gear');
    machine.gear3.body.setAllowGravity(false);
    machine.gear3.setDepth(951);
    machine.gear3.setFrame(1);
    machine.gear3.setScale(3)
    machine.gear3.setAngularVelocity(200);

    machine.colors = ['0000','0404','0a0a','1010','1616','1c1c','2222','2828','2e2e','3434','3a3a','4040','4646','4c4c','5252','5858','5e5e','6464','6a6a','7070','7676','7c7c','8282','8888','8e8e','9494','9a9a','a0a0','a6a6','acac','b2b2','b8b8','bebe','c4c4','caca','d0d0','d6d6','dcdc','e2e2','e8e8','eeee','f4f4','fafa']

    for (var i=0;i<scene.layers.length;i++){
        machine.data.values.layer_collisions.push(scene.physics.add.collider(machine, scene.layers[i]));}
}

destroyMachine(machine){
    machine.destroy()
    machine.setVisible(false)
    machine.gear1.destroy()
    machine.gear2.destroy()
    machine.gear3.destroy()
}

}
