export default class Egg {

constructor(scene, boss, player, direction){
  this.scene = scene
  this.player = player

var x_spawn
var x_velocity
// var y_velocity = Phaser.Math.Between(-500,300)
// var y_velocity = -300
var angular
var angle

var y_velocity = 0

  if (direction == 'left'){
      x_spawn = -30
      x_velocity = -350
      angle = 30
      angular = -350}
  else if (direction == 'right'){
      x_spawn = 30
      x_velocity = 350
      angle = -30
      angular = 350}

  this.egg = scene.egg.create(boss.x+x_spawn, boss.y+150, 'egg');
  // egg.setScale(2.2)
  this.egg.setDepth(800)
  this.egg.setFrame(2)
  this.egg.setCircle(33, 57, 35)
  this.egg.setAngle(angle)
  this.egg.setAngularVelocity(angular);
  // egg.body.setAllowGravity(false);
  this.egg.setVelocity(x_velocity, y_velocity);
  this.egg.layer_collisions = []
  this.egg.collided_with_fact = false
  for (var i=0;i<scene.layers.length;i++){
      this.egg.layer_collisions.push(scene.physics.add.collider(this.egg, scene.layers[i]));}

  this.scene.player_egg = this.scene.physics.add.overlap(this.egg, this.player.player, function(egg, player){
      scene.player_hit.play()
      this.player.player.data.values.time_at_collision = scene.manual_time
      this.player.player.data.values.hit_from = egg.x
      this.player.damagePlayer(this.player, scene, 5)
    },
    function(egg, player){
        return (this.player.player.data.values.post_hit_invincibility == false && this.player.player.data.values.health > 0)
    }, this);

}

update(){
    if (this.egg.y > (((this.scene.layers.layer.height)*2)+100)){
        this.egg.destroy()
    }

}

}
