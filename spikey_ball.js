export default class SpikeyBall {

constructor(scene, player){
  scene.spikey_balls = scene.physics.add.group();
  this.scene = scene
  this.player = player

  this.scene.player_spikey_ball = this.scene.physics.add.overlap(this.scene.spikey_balls, this.player.player, function(player, spikey_ball){
      this.player.player.data.values.time_at_collision = scene.manual_time
      this.player.player.data.values.hit_from = spikey_ball.x
      scene.player_hit.play()
      this.player.damagePlayer(this.player, scene, spikey_ball.data.values.damage)
      },
      function(spikey_ball, player){
          return (this.player.player.data.values.post_hit_invincibility == false && this.player.player.data.values.health > 0)
      }, this);

  this.npc_spikey_ball = scene.physics.add.collider(scene.npc_enemies, scene.spikey_balls,
    function(npc, ball){
      npc.setFrame(7)
      var whack_num = Math.floor(Math.random() * scene.whacks.length)
      // console.log(scene.scene.key)
      if (scene.scene.key == 'level5'){
        scene.whacks[whack_num].play();}
      npc.data.values.health = 0
      npc.data.values.hit_from = ball.x
      npc.data.values.time_at_collision = this.scene.manual_time
      npc.data.values.knock_back_time = 10000
      npc.setAngularVelocity(300)
      for (var i=0;i<scene.layers.length;i++){
          scene.physics.world.removeCollider(npc.data.values.layer_collisions[i]);}
      this.already_collided_with_npc = true
    }, function(npc, ball){
    }, this);

}

update(player){
      this.scene.spikey_balls.children.each(function(spikey_ball) {
          if (spikey_ball.active){
              if (spikey_ball.body.onFloor()){
                  spikey_ball.setVelocityX(this.spikey_ball.data.values.x_velocity);}
              if (spikey_ball.y > (((this.scene.layers.layer.height)*2)+100)){
                  if (spikey_ball.data.values.respawn == true) {
                      spikey_ball.setPosition(spikey_ball.data.values.x_spawn, spikey_ball.data.values.y_spawn)}
                  else {
                      spikey_ball.destroy}
              }
        }  // if spikey ball active
      }, this); // looping through all spikey balls
}

addSpikeyBall(scene, x, y, direction, respawn, damage){
    var x_velocity
    var angular
    var angle
    var y_velocity = 0

    if (direction == 'left'){
        x_velocity = -350
        angle = 30
        angular = -350}
    else if (direction == 'right'){
        x_velocity = 350
        angle = -30
        angular = 350}

    this.spikey_ball = scene.spikey_balls.create(x, y, 'spikey_ball');
    this.spikey_ball.setScale(1.4)
    this.spikey_ball.setDepth(800)
    this.spikey_ball.setCircle(40, 50, 30)
    this.spikey_ball.setAngle(angle)
    this.spikey_ball.setAngularVelocity(angular);
    this.spikey_ball.layer_collisions = []
    this.spikey_ball.setMaxVelocity(300,600)
    this.spikey_ball.setData({'x_spawn': 600,
                              'y_spawn': 960,
                              'x_velocity': x_velocity,
                              'respawn': respawn,
                              'damage': damage
                            });
    for (var i=0;i<scene.layers.length;i++){
        this.spikey_ball.layer_collisions.push(scene.physics.add.collider(this.spikey_ball, scene.layers[i]));}
    this.spikey_ball.collided_with_fact = false
}

}
