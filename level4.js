import Preload from "./preload.js";
import Player from "./player.js";
import Fact from  "./fact.js";
import Npc from  "./npc.js";
import Bs from  "./bs.js";
import Bird from  "./bird.js";
import Censor from "./censor.js"
import Tomato from  "./tomato.js";
import Juice from "./juice.js"
import Balloon from "./balloon.js"
import Flag from "./flag.js"
import Bot from "./bot.js"
import BossBot from "./boss_bot.js"
import Rock from "./rock.js"
import SmallBot from "./small_bot.js"
import SpikeyBall from  "./spikey_ball.js";
import Lava from "./lava.js"
import Liquid from "./liquid.js"
import Demonetization from "./demonetization.js"

import Level2 from "./level2.js";

export default class level4 extends Phaser.Scene {

constructor() {
  	super({key: "level4"});
}

create (){
    this.scene_name = 'level4'
    this.bg_sky = this.add.tileSprite(0, 0, 9000, 7000, "level4_sky");
    this.bg_sky.setOrigin(0, 0.05);
    this.bg_sky.setTileScale(2.5,2.5);
    this.bg_4 = this.add.tileSprite(0, 0, 1200, 800, "level4_bg4");
    this.bg_4.setOrigin(0, 0);
    this.bg_4.setScrollFactor(0); // repeats background
    this.bg_4.setPosition(0, -200)
    this.bg_4.setTileScale(1.5,1.5);
    this.bg_3 = this.add.tileSprite(0, 0, 1200, 800, "level4_bg3");
    this.bg_3.setOrigin(0, 0);
    this.bg_3.setScrollFactor(0); // repeats background
    this.bg_3.setPosition(0, -200)
    this.bg_3.setTileScale(1.5,1.5);
    this.bg_2 = this.add.tileSprite(0, 1000, 1200, 800, "level4_bg2");
    this.bg_2.setOrigin(0, 0);
    this.bg_2.setScrollFactor(0); // repeats background
    this.bg_2.setPosition(0, -200)
    this.bg_2.setTileScale(1.5,1.5);
    this.bg_1 = this.add.tileSprite(0, 1000, 1200, 800, "level4_bg1");
    this.bg_1.setOrigin(0, 0);
    this.bg_1.setScrollFactor(0); // repeats background
    this.bg_1.setPosition(0, -125)
    this.bg_1.setTileScale(1.1,1.1);
    this.myCam = this.cameras.main;

    this.map = this.add.tilemap("level4_map");
    var tileset = this.map.addTilesetImage("tileset", "level4_tileset", 32, 32, 1, 2);  //margin=1, spacing=2, this is needed because of using the frame extruder program
    this.layers = []
    this.layers.layer = this.map.createStaticLayer(0, tileset, 0, 0);
    this.layers.push(this.layers.layer)
    this.layers.layer.setCollisionByProperty({ collides: true });
    this.layers.layer.setScale(2)

    this.layers.layer2 = this.map.createStaticLayer(1, tileset, 0, 0);
    this.layers.push(this.layers.layer2)
    this.layers.layer2.setCollisionByProperty({ collides: true });
    this.layers.layer2.setScale(2)

    for (var i=0;i<this.layers.length;i++){
        // this.layers[i].setTileLocationCallback(20, 49, 9, 1, this.topCollisionOnly, this);
    }

    this.player_x_spawn = 4200 // 4200
    this.player_y_spawn = 1550 // 1550
    if (this.cp == 1){
        this.player_x_spawn = 8300 //
        this.player_y_spawn = 2650} //
    else if (this.cp == 2){
        this.player_x_spawn = 3500 //
        this.player_y_spawn = 2630} //
    else if (this.cp == 3){
        this.player_x_spawn = 7530 // 7530
        this.player_y_spawn = 420} // 420

		this.player = new Player(this, this.player_x_spawn, this.player_y_spawn);
    this.cameras.main.setBounds(0, 0, (this.map.widthInPixels*2), (this.map.heightInPixels*2));
    this.player_y_at_jump = this.player.player.y
    this.time_at_landing = 0
    this.bs_statements = this.physics.add.group(); // Must have this here, otherwise facts go through BS randomly
    this.censorships = this.physics.add.group();
    this.math_stuff = this.physics.add.group();
    this.milkshake = this.physics.add.group();
    this.egg = this.physics.add.group();
    this.trash_can = this.physics.add.group();
    this.demonetization = this.physics.add.group();
    this.rock = this.physics.add.group();
    this.spikey_ball = this.physics.add.group();
    this.stop_follow = false

    this.juice_sprites = new Juice(this, this.player)
    if (!this.cp || this.cp < 1){
      this.juice_sprites.addJuice(this, 7600, 3500, 'small')}
    if (!this.cp || this.cp < 2){
      this.juice_sprites.addJuice(this, 3488, 2670, 'small')}
    if (!this.cp || this.cp < 3){
      this.juice_sprites.addJuice(this, 7560, 500, 'big')}

    this.enemies = []
    for (var i=0;i<this.enemies.length;i++){
        this.enemies[i] = false}

    this.npc_sprites = new Npc(this, this.player)
    this.bird_sprites = new Bird(this, this.player)
    this.bot_sprites = new Bot(this, this.player)
    this.boss_bot_sprites = new BossBot(this, this.player)
    this.small_bot_sprites = new SmallBot(this, this.player)
    this.rock_sprites = new Rock(this, this.player)
    this.spikey_ball_sprites = new SpikeyBall(this, this.player)

    this.rock_sprites.addRock(this, 7240, 3070)
    this.rock_sprites.addRock(this, 6320, 3170)
    this.rock_sprites.addRock(this, 5460, 3070)
    this.rock_sprites.addRock(this, 5050, 3070)
    this.rock_sprites.addRock(this, 4490, 3070)
    this.rock_sprites.addRock(this, 4250, 3070)

    this.spikey_ball_sprites.addSpikeyBall(this, 3000, 2000, 'right', true, 9)
    this.spikey_ball_sprites.addSpikeyBall(this, 2000, 1900, 'right', true, 9)
    this.spikey_ball_sprites.addSpikeyBall(this, 1000, 1700, 'right', true, 9)
    this.spikey_ball_sprites.addSpikeyBall(this, 3900, 2700, 'right', true, 9)

    this.lava_sprites = new Lava(this, this.player)
    // this.lava_sprites.addLava(this, 7200, 3600, 1, 1)

    this.liquid_sprites = new Liquid(this)
    this.lava_sprites.addLava(this, 7200, 3780)
    this.lava_sprites.addLava(this, 7263, 3780)
    this.lava_sprites.addLava(this, 7200, 3730)
    this.lava_sprites.addLava(this, 7263, 3730)

    this.lava_sprites.addLava(this, 8733, 3780)
    this.lava_sprites.addLava(this, 8796, 3780)
    this.lava_sprites.addLava(this, 8859, 3780)
    this.lava_sprites.addLava(this, 8922, 3780)
    this.lava_sprites.addLava(this, 8985, 3780)
    this.lava_sprites.addLava(this, 8733, 3730)
    this.lava_sprites.addLava(this, 8796, 3730)
    this.lava_sprites.addLava(this, 8859, 3730)
    this.lava_sprites.addLava(this, 8922, 3730)
    this.lava_sprites.addLava(this, 8985, 3730)

    this.lava_sprites.addLava(this, 6241, 3780)
    this.lava_sprites.addLava(this, 6304, 3780)
    this.lava_sprites.addLava(this, 6367, 3780)
    this.lava_sprites.addLava(this, 6241, 3730)
    this.lava_sprites.addLava(this, 6304, 3730)
    this.lava_sprites.addLava(this, 6367, 3730)

    this.lava_sprites.addLava(this, 5409, 3780)
    this.lava_sprites.addLava(this, 5472, 3780)
    this.lava_sprites.addLava(this, 5535, 3780)
    this.lava_sprites.addLava(this, 5409, 3730)
    this.lava_sprites.addLava(this, 5472, 3730)
    this.lava_sprites.addLava(this, 5535, 3730)

    this.lava_sprites.addLava(this, 5025, 3780)
    this.lava_sprites.addLava(this, 5088, 3780)
    this.lava_sprites.addLava(this, 5025, 3730)
    this.lava_sprites.addLava(this, 5088, 3730)

    this.lava_sprites.addLava(this, 4705, 3780)
    this.lava_sprites.addLava(this, 4768, 3780)
    this.lava_sprites.addLava(this, 4831, 3780)
    this.lava_sprites.addLava(this, 4705, 3730)
    this.lava_sprites.addLava(this, 4768, 3730)
    this.lava_sprites.addLava(this, 4831, 3730)

    this.lava_sprites.addLava(this, 4450, 3780)
    this.lava_sprites.addLava(this, 4513, 3780)
    this.lava_sprites.addLava(this, 4450, 3730)
    this.lava_sprites.addLava(this, 4513, 3730)

    this.lava_sprites.addLava(this, 4193, 3780)
    this.lava_sprites.addLava(this, 4256, 3780)
    this.lava_sprites.addLava(this, 4319, 3780)
    this.lava_sprites.addLava(this, 4193, 3730)
    this.lava_sprites.addLava(this, 4256, 3730)
    this.lava_sprites.addLava(this, 4319, 3730)

    this.lava_sprites.addLava(this, 3873, 3780)
    this.lava_sprites.addLava(this, 3936, 3780)
    this.lava_sprites.addLava(this, 3999, 3780)
    this.lava_sprites.addLava(this, 3873, 3730)
    this.lava_sprites.addLava(this, 3936, 3730)
    this.lava_sprites.addLava(this, 3999, 3730)

    this.lava_sprites.addLava(this, 4705, 1280)
    this.lava_sprites.addLava(this, 4768, 1280)

    this.lava_sprites.addLava(this, 5472, 1280)
    this.lava_sprites.addLava(this, 5535, 1280)
    this.lava_sprites.addLava(this, 5598, 1280)

    this.lava_sprites.addLava(this, 6049, 1280)
    this.lava_sprites.addLava(this, 6112, 1280)
    this.lava_sprites.addLava(this, 6174, 1280)

    this.lava_sprites.addLava(this, 7008, 960)
    this.lava_sprites.addLava(this, 7071, 960)

    this.lava_sprites.addLava(this, 7328, 832)
    this.lava_sprites.addLava(this, 7391, 832)

    this.lava_sprites.addLava(this, 7650, 705)
    this.lava_sprites.addLava(this, 7713, 705)
    this.lava_sprites.addLava(this, 7776, 705)

    this.lava_sprites.addLava(this, 8928, 705)

    this.music = this.sound.add('level_4_soundtrack');
    var music_config = {
      mute: false,
      volume: 0.5,
      rate: 1,
      detune: 0,
      seek: 0,
      loop: true,
      delay: 0
    }
    this.music.play(music_config)
    // this.game.sound.mute = true;
    // this.fps_display = this.add.text(600, 5, "FPS: " + this.sys.game.loop.actualFps, {fill: '#fff'}).setFontSize(40).setStroke('#000', 4).setDepth(1000)

}

update(){
    this.player.update();
    this.npc_sprites.update(this.player.player);
    this.bird_sprites.update(this.player.player);
    this.small_bot_sprites.update(this.player.player);
    this.bot_sprites.update(this.player.player);
    this.boss_bot_sprites.update(this.player.player);
    this.rock_sprites.update(this.player.player);
    this.spikey_ball_sprites.update(this.player.player);

    // this.fps_display.setText("FPS: " + this.sys.game.loop.actualFps)
    // this.fps_display.setPosition(this.myCam.midPoint.x+380, this.myCam.midPoint.y-270)

    var px = this.player.player.x
    var py = this.player.player.y

    if (this.playerInBox(px, py, 4450, 1720) && !this.vl58_played){
      this.vl58.play()
      this.vl58_played = true}

    if (this.playerInBox(px, py, 6600, 2800) && !this.vl14_played){
      this.vl14.play()
      this.vl14_played = true}

    if (this.playerInBox(px, py, 5315, 3515) && !this.vl60_played){
      this.vl60.play()
      this.vl60_played = true}

    if (this.playerInBox(px, py, 2390, 3570) && !this.vl46_played){
      this.time.delayedCall(1000, function() {
        this.vl46.play()
      }, [], this);
      this.vl46_played = true}

    if (this.playerInBox(px, py, 2950, 2950) && !this.vl15_played){
      this.vl15.play()
      this.vl15_played = true}

    if (this.playerInBox(px, py, 3270, 1145) && !this.vl49_played){
      this.vl49.play()
      this.vl49_played = true}

    if (this.playerInBox(px, py, 8463, 500) && !this.vl59_played) {
      this.time.delayedCall(2000, function() {
        this.vl59.play()
      }, [], this);
      this.vl59_played = true}

    if (this.playerInBox(px, py, 4450, 1720) && (!this.enemies.enemy0) && this.cp < 1 ) {
        this.small_bot_sprites.addSmallBot(this, 5240, 1720, false)
        this.enemies.enemy0 = true}

    if (this.playerInBox(px, py, 4900, 1720) && (!this.enemies.enemy1) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 5900, 1720, 'walk', 'low')
        this.enemies.enemy1 = true}

    if (this.playerInBox(px, py, 6020, 1720) && (!this.enemies.enemy2) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 5550, 2160, 'run', 'none')
        this.enemies.enemy2 = true}

    if (this.playerInBox(px, py, 5100, 1720) && (!this.enemies.enemy2_1) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 4600, 1100, 'run', 'none')
        this.enemies.enemy2_1 = true}

    if (this.playerInBox(px, py, 4800, 1720) && (!this.enemies.enemy2_2) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 5800, 1100, 'run', 'none')
        this.enemies.enemy2_2 = true}

    if (this.playerInBox(px, py, 6020, 1720) && (!this.enemies.enemy3) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 5390, 2160, 'run', 'none')
        this.enemies.enemy3 = true}

    if (this.playerInBox(px, py, 6020, 1720) && (!this.enemies.enemy4) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 5290, 2160, 'run', 'none')
        this.enemies.enemy4 = true}

    if (this.playerInBox(px, py, 6020, 1720) && (!this.enemies.enemy5) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 5090, 2160, 'stand', 'low')
        this.enemies.enemy5 = true}

    if (this.playerInBox(px, py, 5800, 2170) && (!this.enemies.enemy6) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 6170, 1700, 'run', 'none')
        this.enemies.enemy6 = true}

    if (this.playerInBox(px, py, 4450, 2200) && (!this.enemies.enemy7) && this.cp < 1 ) {
        this.small_bot_sprites.addSmallBot(this, 6250, 2750, false)
        this.enemies.enemy7 = true}

    if (this.playerInBox(px, py, 4450, 2200) && (!this.enemies.enemy7_1) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 5300, 2750, 'walk', 'none')
        this.enemies.enemy7_1 = true}

    if (this.playerInBox(px, py, 4450, 2200) && (!this.enemies.enemy7_2) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 5800, 2750, 'walk', 'none')
        this.enemies.enemy7_2 = true}

    if (this.playerInBox(px, py, 4450, 2200) && (!this.enemies.enemy7_3) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 6300, 2750, 'walk', 'none')
        this.enemies.enemy7_3 = true}

    if (this.playerInBox(px, py, 6300, 2800) && (!this.enemies.enemy8) && this.cp < 1 ) {
        this.bot_sprites.addBot(this, 7100, 2750, 'stand')
        this.enemies.enemy8 = true}

    if (this.playerInBox(px, py, 7120, 2800) && (!this.enemies.enemy9) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 8000, 2750, 'run', 'none')
        this.enemies.enemy9 = true}

    if (this.playerInBox(px, py, 7760, 2800) && (!this.enemies.enemy10) && this.cp < 1 ) {
        this.npc_sprites.addNPC(this, 7100, 2300, 'run', 'none')
        this.enemies.enemy10 = true}

    if (this.playerInBox(px, py, 7760, 2850) && (!this.enemies.enemy11) && this.cp < 1 ) {
        this.small_bot_sprites.addSmallBot(this, 8500, 2740, true)
        this.enemies.enemy11 = true}

    if (this.playerInBox(px, py, 8500, 3000) && (!this.enemies.enemy12) && this.cp < 2 ) {
        this.bot_sprites.addBot(this, 7750, 3450, 'roll')
        this.enemies.enemy12 = true}

    if (this.playerInBox(px, py, 5740, 3570) && (!this.enemies.enemy13) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 4930, 3450, 'stand', 'none')
        this.enemies.enemy13 = true}

    if (this.playerInBox(px, py, 6620, 3570) && (!this.enemies.enemy14) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 5750, 3450, 'stand', 'low')
        this.enemies.enemy14 = true}

    if (this.playerInBox(px, py, 6620, 3570) && (!this.enemies.enemy15) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 4610, 3450, 'stand', 'none')
        this.enemies.enemy15 = true}

    if (this.playerInBox(px, py, 3900, 3570) && (!this.enemies.enemy16) && this.cp < 2 ) {
        this.small_bot_sprites.addSmallBot(this, 2900, 3600, false)
        this.enemies.enemy16 = true}

    if (this.playerInBox(px, py, 2390, 3570) && (!this.enemies.enemy17) && this.cp < 2 ) {
        this.bot_sprites.addBot(this, 1580, 3450, 'roll')
        this.enemies.enemy17 = true}

    if (this.playerInBox(px, py, 2390, 3570) && (!this.enemies.enemy18) && this.cp < 2 ) {
        this.bot_sprites.addBot(this, 1180, 3450, 'roll')
        this.enemies.enemy18 = true}

    if (this.playerInBox(px, py, 1700, 3570) && (!this.enemies.enemy19) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 740, 3400, 'stand', 'low')
        this.enemies.enemy19 = true}

    if (this.playerInBox(px, py, 600, 3320) && (!this.enemies.enemy20) && this.cp < 2 ) {
        this.bot_sprites.addBot(this, 2140, 3100, 'stand')
        this.enemies.enemy20 = true}

    if (this.playerInBox(px, py, 1600, 3200) && (!this.enemies.enemy21) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 2530, 2900, 'stand', 'none')
        this.enemies.enemy21 = true}

    if (this.playerInBox(px, py, 2000, 3200) && (!this.enemies.enemy22) && this.cp < 2 ) {
        this.npc_sprites.addNPC(this, 2840, 2900, 'stand', 'low')
        this.enemies.enemy22 = true}

    if (this.playerInBox(px, py, 2320, 3000) && (!this.enemies.enemy23) && this.cp < 2 ) {
        this.bird_sprites.addBird(this, 1600, 3000, 'wave')
        this.enemies.enemy23 = true}

    if (this.playerInBox(px, py, 2660, 2900) && (!this.enemies.enemy24) && this.cp < 2 ) {
        this.small_bot_sprites.addSmallBot(this, 3450, 2680, false)
        this.enemies.enemy24 = true}

    if (this.playerInBox(px, py, 1030, 1400) && (!this.enemies.enemy25) && this.cp < 3 ) {
        this.npc_sprites.addNPC(this, 1850, 1250, 'stand', 'low')
        this.enemies.enemy25 = true}

    if (this.playerInBox(px, py, 1800, 1270) && (!this.enemies.enemy26) && this.cp < 3 ) {
        this.npc_sprites.addNPC(this, 2600, 1250, 'walk', 'none')
        this.enemies.enemy26 = true}

    if (this.playerInBox(px, py, 1800, 1270) && (!this.enemies.enemy26_2) && this.cp < 3 ) {
        this.npc_sprites.addNPC(this, 2700, 1250, 'walk', 'none')
        this.enemies.enemy26_2 = true}

    if (this.playerInBox(px, py, 1800, 1270) && (!this.enemies.enemy26_3) && this.cp < 3 ) {
        this.npc_sprites.addNPC(this, 2800, 1130, 'walk', 'none')
        this.enemies.enemy26_3 = true}

    if (this.playerInBox(px, py, 1800, 1270) && (!this.enemies.enemy26_4) && this.cp < 3 ) {
        this.npc_sprites.addNPC(this, 2900, 1130, 'walk', 'none')
        this.enemies.enemy26_4 = true}

    if (this.playerInBox(px, py, 2750, 1150) && (!this.enemies.enemy27) && this.cp < 3 ) {
        this.bot_sprites.addBot(this, 3700, 1100, 'stand')
        this.enemies.enemy27 = true}

    if (this.playerInBox(px, py, 3700, 1150) && (!this.enemies.enemy27_1) && this.cp < 3 ) {
        this.bird_sprites.addBird(this, 2200, 1075, 'wave')
        this.enemies.enemy27_1 = true}

    if (this.playerInBox(px, py, 3700, 1150) && (!this.enemies.enemy28) && this.cp < 3 ) {
        this.bot_sprites.addBot(this, 4400, 1100, 'stand')
        this.enemies.enemy28 = true}

    if (this.playerInBox(px, py, 4500, 1150) && (!this.enemies.enemy29) && this.cp < 3 ) {
        this.small_bot_sprites.addSmallBot(this, 5680, 1150, false)
        this.enemies.enemy29 = true}

    if (this.playerInBox(px, py, 4500, 1150) && (!this.enemies.enemy30) && this.cp < 3 ) {
        this.small_bot_sprites.addSmallBot(this, 6000, 1080, false)
        this.enemies.enemy30 = true}

    if (this.playerInBox(px, py, 6600, 950) && (!this.enemies.enemy31) && this.cp < 3 ) {
        this.npc_sprites.addNPC(this, 7480, 560, 'stand', 'none')
        this.enemies.enemy31 = true}

    if (this.playerInBox(px, py, 8463, 500) && (!this.enemies.enemy32)) {
        this.boss_bot_sprites.addBossBot(this, 8820, -200, true)
        this.enemies.enemy32 = true}

    // CHECKPOINTS
    if (!this.cp) {
        this.cp = 0}
    if (this.playerInBox(px, py, 8415, 2800) && this.cp <= 0) {
        this.cp = 1}
    if (this.playerInBox(px, py, 3600, 2680) && this.cp <= 1) {
        this.cp = 2}
    if (this.playerInBox(px, py, 8100, 505) && this.cp <= 2) {
        this.cp = 3}

    if (!this.stop_follow){
    this.myCam.centerOnX(this.player.player.x)
    if (this.player.player.body.onFloor()) {
            this.myCam.pan(this.player.player.x, this.player.player.y-90, 50, 'Linear', true);
        this.player_y_at_jump = this.player.player.y}
    else {
        this.y_offset = this.player.player.y - this.player_y_at_jump
        if (this.y_offset < 0){ // player staying above original jump point
                this.myCam.centerOnY(this.player_y_at_jump-90)}
        else {
            this.myCam.centerOn(this.player.player.x, this.player.player.y-90)}
        this.time_at_landing = this.time.now}}

    this.bg_4.tilePositionX = this.myCam.scrollX * .06;
    this.bg_3.tilePositionX = this.myCam.scrollX * .1;
    this.bg_2.tilePositionX = this.myCam.scrollX * .2;
    this.bg_1.tilePositionX = this.myCam.scrollX * .5;

    // BOSS FIGHT
    if (this.player.player.data.values.health > 0 ){
        if (this.player.player.x > 8363 && this.player.player.y < 1340){
            this.stop_follow = true}}
    else {
        this.stop_follow = false}

    // END LEVEL
    if (this.data.values.boss_defeated == true){
        this.music.stop()
        this.time.delayedCall(2000, function() {
            this.start('cutscene5');
            }, [], this.scene);
        }

    if (this.stop_follow && (this.player.player.x < 7700 || this.player.player.y > 750)){  // restart if player escapes boss arena
        this.music.stop()
        this.scene.start()
    }

}

playerInBox(px, py, x, y){
    if (px > x-100 && px < x+100 && py > y-100 && py < y+100){
        return true}
    else{
        return false}}

topCollisionOnly(sprite, tile){
    tile.setCollision(false, false, true, false)} //left, right, top, bottom

} // class
