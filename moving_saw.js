export default class MovingSaw {

constructor(scene, player){
  scene.moving_saws = scene.physics.add.group();
  this.scene = scene
  this.player = player

  this.scene.player_moving_saw = this.scene.physics.add.overlap(this.scene.moving_saws, this.player.player, function(player, moving_saw){
      this.player.player.data.values.time_at_collision = scene.manual_time
      this.player.player.data.values.hit_from = moving_saw.x
      scene.player_hit.play()
      this.player.damagePlayer(this.player, scene, 6)
      },
      function(moving_saw, player){
          return (this.player.player.data.values.post_hit_invincibility == false && this.player.player.data.values.health > 0)
      }, this);
}

update(player){
      this.scene.moving_saws.children.each(function(moving_saw) {
        if (moving_saw.active){
          if (moving_saw.state == 'forward') {
              if (moving_saw.data.values.direction == 'horizontal'){
                  moving_saw.body.setVelocityX(moving_saw.data.values.x_velocity)
                  if ((moving_saw.data.values.speed > 0 && moving_saw.x > (moving_saw.data.values.x_spawn + moving_saw.data.values.distance)) ||
                      (moving_saw.data.values.speed < 0 && moving_saw.x < (moving_saw.data.values.x_spawn - moving_saw.data.values.distance))){
                      moving_saw.state = 'backward'}}
              else {
                  moving_saw.body.setVelocityY(moving_saw.data.values.y_velocity)
                  if ((moving_saw.data.values.speed > 0 && moving_saw.y >= (moving_saw.data.values.y_spawn + moving_saw.data.values.distance)) ||
                      (moving_saw.data.values.speed < 0 && moving_saw.y < (moving_saw.data.values.y_spawn - moving_saw.data.values.distance))){
                      moving_saw.state = 'backward'}}}
          else if (moving_saw.state == 'backward') {
              if (moving_saw.data.values.direction == 'horizontal'){
                  moving_saw.body.setVelocityX(-(moving_saw.data.values.x_velocity))
                  if ((moving_saw.data.values.speed < 0 && moving_saw.x > (moving_saw.data.values.x_spawn)) ||
                      (moving_saw.data.values.speed > 0 && moving_saw.x < (moving_saw.data.values.x_spawn))){
                      moving_saw.state = 'forward'}}
              else {
                  moving_saw.body.setVelocityY(-(moving_saw.data.values.y_velocity))
                  if ((moving_saw.data.values.speed < 0 && moving_saw.y > (moving_saw.data.values.y_spawn)) ||
                      (moving_saw.data.values.speed > 0 && moving_saw.y <= (moving_saw.data.values.y_spawn))){
                      moving_saw.state = 'forward'}}}
      }  // if saw active
      }, this); // looping through all saws
} // update

addMovingSaw(scene, x, y, direction, speed, distance, size){
    var x_velocity
    var y_velocity
    if (direction == 'horizontal'){
        x_velocity = speed
        y_velocity = 0}
    else {
        x_velocity = 0
        y_velocity = speed}

    this.moving_saw = scene.moving_saws.create(x, y, 'buzz_saw');
    this.moving_saw.setScale(size)
    this.moving_saw.setDepth(800)
    this.moving_saw.setCircle(30, 2, 2)
    this.moving_saw.setAngularVelocity(225)
    // this.moving_saw.setFrame(3)
    this.moving_saw.state = 'forward'
    this.moving_saw.body.setAllowGravity(false);
    this.moving_saw.setData({'x_spawn': x,
                              'y_spawn': y,
                              'direction': direction,
                              'speed': speed,
                              'distance': distance,
                              'x_velocity': x_velocity,
                              'y_velocity': y_velocity
                            });

    this.moving_saw.setVelocityX(this.moving_saw.data.values.x_velocity);
    this.moving_saw.setVelocityY(this.moving_saw.data.values.y_velocity);

    return this.moving_saw
}

}
