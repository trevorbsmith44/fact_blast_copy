export default class DarkRock {

constructor(scene, player){
    // scene.dark_rocks = scene.physics.add.group();
    this.player = player
    this.scene = scene

    this.player_dark_rock = scene.physics.add.collider(this.player.player, scene.dark_rocks, function(player, dark_rock){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = dark_rock.x
        scene.bonk.play()
        this.player.damagePlayer(this.player, scene, 6)
        dark_rock.setVelocityX(0)
        }, function(player, dark_rock){
            return (dark_rock.data.values.collided_with_fact == false
              && this.player.player.data.values.post_hit_invincibility == false)
        }, this);

        scene.physics.add.overlap(scene.dark_rocks, scene.boss_npc_mds,
          null, null, this);
}

update(player){
  this.scene.dark_rocks.children.each(function(dark_rock) {
      if (dark_rock.active){
          if (dark_rock.y > ((this.scene.layers.layer.height*2)+dark_rock.data.values.respawn)){
              dark_rock.setPosition(this.scene.rock_x_spawn, dark_rock.data.values.y_origin)
              dark_rock.setVelocityX(0)
              dark_rock.data.values.collided_with_fact = false;
            }

          if (dark_rock.data){ // prevents TypeError
              dark_rock.setAngularVelocity(50)

              } // if dark_rock.data exists

      }  // if dark_rock active
    }, this); // looping through all dark_rocks
}

addDarkRock(scene, x, y) {
		var dark_rock = this.scene.dark_rocks.create(x,y, 'dark_rock');
		dark_rock.setActive(true).setVisible(true)
		dark_rock.setData({'health': 1,
								 'throw_wait': Phaser.Math.Between(2000, 4000),
						 		 'last_throw': 0,
								 'speed': Phaser.Math.Between(100, 200),
                 'x_origin': x,
                 'y_origin': y,
                 'current_x_spawn': x,
                 'player_x_origin': this.scene.player.player.x,
                 'player_y_origin': this.scene.player.player.y,
                 'respawn': Phaser.Math.Between(150,350),
                 'collided_with_fact': false,
                 'collided_with_glass': false
							 });
		dark_rock.setSize(80, 50, false)
		dark_rock.setOffset(15,5)
    dark_rock.setScale(3.3)
    dark_rock.setDepth(500)
    dark_rock.setCircle(12,3,2)
    dark_rock.setMaxVelocity(300,600)
    dark_rock.setAngle(Phaser.Math.Between(0,359))

		this.scene.dark_rocks.add(dark_rock)

    this.scene.enemies_added += 1;
}

}
