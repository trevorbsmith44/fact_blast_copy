import Bs from  "./bs.js";

export default class BossBot {

constructor(scene, player){
    scene.boss_bots = scene.physics.add.group();
    this.scene = scene
    this.player = player

    this.player_boss_bot = scene.physics.add.overlap(this.player.player, scene.boss_bots, function(player, boss_bot){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = boss_bot.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 6)
        boss_bot.body.immovable = true;
        }, function(player, boss_bot){
            return (boss_bot.head.health > 0
              && this.player.player.data.values.post_hit_invincibility == false)
        }, this);

}

update(player){
  this.scene.boss_bots.children.each(function(boss_bot) {
      if (boss_bot.active){
        // console.log(boss_bot.state)

        boss_bot.wheel1.setPosition(boss_bot.x-55, boss_bot.y+135)
        boss_bot.wheel2.setPosition(boss_bot.x-19, boss_bot.y+135)
        boss_bot.wheel3.setPosition(boss_bot.x+19, boss_bot.y+135)
        boss_bot.wheel4.setPosition(boss_bot.x+55, boss_bot.y+135)
        boss_bot.head.setPosition(boss_bot.x-30, boss_bot.y-110)
        boss_bot.chest.setPosition(boss_bot.x-34, boss_bot.y-35)
        boss_bot.pipe.setPosition(boss_bot.x-32, boss_bot.y+36)

        if (boss_bot.buzz_saw.direction == 'left'){
            if (boss_bot.data.values.over_under == 0){
                boss_bot.buzz_saw.setVelocityX(-500)}
            else{
                boss_bot.buzz_saw.setVelocityX(0)
                this.scene.time.delayedCall(150, function() {
                    boss_bot.buzz_saw.setVelocityX(-500)
                    }, [], this.scene);}}
        else if (boss_bot.buzz_saw.direction == 'right'){
            boss_bot.buzz_saw.setVelocityX(500)}
        else{
          // this.scene.time.delayedCall(50, function() {
              boss_bot.buzz_saw.setPosition(boss_bot.x-205, boss_bot.y-40)
              // }, [], this.scene);
            }

        if (boss_bot.buzz_saw.x < boss_bot.data.values.x_origin-910){
            boss_bot.buzz_saw.direction = 'right'}
        if (boss_bot.buzz_saw.x > boss_bot.x-185 && boss_bot.buzz_saw.direction == 'right'){
            boss_bot.buzz_saw.direction = 'still'}
        if (boss_bot.buzz_saw.x > boss_bot.x-225 && boss_bot.buzz_saw.direction == 'right'){
            if (boss_bot.data.values.over_under == 1 && boss_bot.state != 'explode'){
                boss_bot.setFrame(5)}}

        // console.log(boss_bot.buzz_saw.direction)
        if (boss_bot.state == 'roll_left_throw_left'){
            boss_bot.anims.play('boss_bot_roll_left', true);
            boss_bot.setVelocityX(-boss_bot.data.values.speed);
            boss_bot.wheel1.setAngularVelocity(-100);
            boss_bot.wheel2.setAngularVelocity(-100);
            boss_bot.wheel3.setAngularVelocity(-100);
            boss_bot.wheel4.setAngularVelocity(-100);
            if (boss_bot.x < boss_bot.data.values.x_origin-150){
                boss_bot.last_right_turn = this.scene.manual_time
                boss_bot.state = 'roll_right_throw_left'}
            if (this.scene.manual_time > boss_bot.data.values.last_throw + boss_bot.data.values.throw_wait){
                boss_bot.time_at_throw_start = Math.trunc(this.scene.manual_time)
                boss_bot.previous_state = boss_bot.state
                boss_bot.setData('throw_wait', Phaser.Math.Between(6000, 8000))
                boss_bot.data.values.over_under = Math.floor(Math.random() * 2)
                boss_bot.buzz_saw.direction = 'left'
                boss_bot.state = 'throw_left'}}

        else if (boss_bot.state == 'roll_right_throw_left'){
            boss_bot.anims.play('boss_bot_roll_left', true);
            boss_bot.setVelocityX(boss_bot.data.values.speed);
            boss_bot.wheel1.setAngularVelocity(100);
            boss_bot.wheel2.setAngularVelocity(100);
            boss_bot.wheel3.setAngularVelocity(100);
            boss_bot.wheel4.setAngularVelocity(100);
            if (boss_bot.x > boss_bot.data.values.x_origin) {
                boss_bot.state = 'roll_left_throw_left'}
            if (this.scene.manual_time > boss_bot.data.values.last_throw + boss_bot.data.values.throw_wait){
                boss_bot.time_at_throw_start = Math.trunc(this.scene.manual_time)
                boss_bot.previous_state = boss_bot.state
                boss_bot.setData('throw_wait', Phaser.Math.Between(6000, 8000))
                boss_bot.data.values.over_under = Math.floor(Math.random() * 2)
                boss_bot.buzz_saw.direction = 'left'
                boss_bot.state = 'throw_left'}}

        else if (boss_bot.state == 'throw_left'){
            var anim_len
            if (boss_bot.data.values.over_under == 0){
                boss_bot.buzz_saw.y = boss_bot.y-30
                anim_len = 700}
            else {
                boss_bot.buzz_saw.y = boss_bot.y+90
                anim_len = 650}
            boss_bot.data.values.last_throw = this.scene.manual_time;
            boss_bot.setVelocityX(0);
            if (!boss_bot.data.values.throw_sound_playing){
              this.scene.boss_bot_throw_sound.play()
              boss_bot.data.values.throw_sound_playing = true}
            boss_bot.wheel1.setAngularVelocity(0);
            boss_bot.wheel2.setAngularVelocity(0);
            boss_bot.wheel3.setAngularVelocity(0);
            boss_bot.wheel4.setAngularVelocity(0);
            if (boss_bot.data.values.over_under == 0){
                boss_bot.anims.play('boss_bot_throw_over', true)}
            else{
                boss_bot.anims.play('boss_bot_throw_under', true)}
            if (this.scene.manual_time >= (boss_bot.time_at_throw_start + anim_len)){
                boss_bot.data.values.throw_sound_playing = false
                boss_bot.state = boss_bot.previous_state}}

        else if (boss_bot.state == 'hit'){
            boss_bot.setVelocityX(0);
            boss_bot.setFrame(6)
            if (this.scene.manual_time > boss_bot.time_at_hit+50){
                boss_bot.state = boss_bot.previous_state}}

        else if (boss_bot.state == 'explode'){
            boss_bot.setVelocityX(0);
            boss_bot.buzz_saw.setVisible(false);
            boss_bot.wheel1.setAngularVelocity(0);
            boss_bot.wheel2.setAngularVelocity(0);
            boss_bot.wheel3.setAngularVelocity(0);
            boss_bot.wheel4.setAngularVelocity(0);
            boss_bot.anims.play('boss_bot_explode', true)
            this.scene.time.delayedCall(1000, function() {
                boss_bot.wheel1.setVisible(false);
                boss_bot.wheel2.setVisible(false);
                boss_bot.wheel3.setVisible(false);
                boss_bot.wheel4.setVisible(false);
                // console.log(this.scene)
                }, [], this.scene);
            this.scene.time.delayedCall(1550, function() {
              boss_bot.destroy()
              }, [], this.scene);

            this.scene.time.delayedCall(1000, function() {
              boss_bot.life_bar.setVisible(false)
              try {
              if (!boss_bot.data.values.explosion_played){
                this.scene.scene.explosion.play()
                boss_bot.data.values.explosion_played = true}}
              catch (TypeError){
                  console.log('Caught Boss Bot TypeError on sound')}
              }, [], this.scene);

            try {
                if (boss_bot.data.values.boss == true){
                  this.scene.time.delayedCall(2500, function() {
                      this.scene.scene.data.values.boss_defeated = true
                    }, [], this.scene);}}
            catch (TypeError){
                console.log('Caught Boss Bot TypeError')}
              }

        if (boss_bot.head.health <= 0){
            boss_bot.life_bar.setFrame(0)
            boss_bot.head.destroy()
            boss_bot.chest.destroy()
            boss_bot.pipe.destroy()
            boss_bot.state = 'explode'}

        boss_bot.buzz_saw.health = boss_bot.head.health;

        if(boss_bot.state.includes('explode') && boss_bot.data.values.boss == false){
          for (var i=0;i<this.scene.barrier_array.length;i++){
              if (this.scene.barrier_array[i].x < 25000){
                this.scene.barrier_array[i].body.setAllowGravity(true)}}}

        if (boss_bot.head.just_damaged && boss_bot.head.health > 0){
          var boss_life_frame = Phaser.Math.CeilTo((boss_bot.head.health/boss_bot.data.values.max_health) * 18)
          boss_bot.life_bar.setFrame(boss_life_frame)
          boss_bot.setTint(0xff0000)
          boss_bot.data.values.time_at_hit = this.scene.manual_time
          boss_bot.head.just_damaged = false
        }

        if (this.scene.manual_time > boss_bot.data.values.time_at_hit + 15){
          boss_bot.setTint(0xffffff)
        }

      } // if boss_bot active
    }, this); // looping through all boss_bots
} // update

addBossBot(scene, x, y, level_boss) {
    var boss_health = 12
    if (!level_boss){
      boss_health = 8
    }
		var boss_bot = scene.boss_bots.create(x, y, 'boss_bot');
		boss_bot.setActive(true).setVisible(true)
		boss_bot.setData({
								 'time_at_collision':null,
								 'knock_back_time': 150,
								 'bs_thrown': 0,
								 'layer_collisions': [],
                 'boss': level_boss,
						 		 'last_throw': scene.manual_time,
                 'throw_wait': Phaser.Math.Between(4000, 6000),
                 'x_origin': x,
                 'explosion_played': false,
                 'throw_sound_playing': false,
                 'max_health': boss_health
							 });

   for (var i=0;i<scene.layers.length;i++){
       boss_bot.data.values.layer_collisions.push(scene.physics.add.collider(boss_bot, scene.layers[i]));}

    boss_bot.state = 'roll_right_throw_left'
    boss_bot.previous_state = 'roll_right_throw_left'
    boss_bot.setData('speed', 50)
    boss_bot.setMaxVelocity(300, 800)

    boss_bot.buzz_saw = scene.physics.add.sprite(x, y, 'buzz_saw');
    boss_bot.buzz_saw.setScale(2);
    boss_bot.buzz_saw.setCircle(28, 3, 5)
    boss_bot.buzz_saw.setFrame(0);
    boss_bot.buzz_saw.body.setAllowGravity(false);
    boss_bot.buzz_saw.setAngularVelocity(200)
    boss_bot.buzz_saw.setDepth(750);
    boss_bot.buzz_saw.direction = 'still'

    scene.physics.add.overlap(this.player.player, boss_bot.buzz_saw, function(player, saw){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = saw.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 8)
        saw.body.immovable = true;
        }, function(player, saw){
            return (saw.health > 0
            && this.player.player.data.values.post_hit_invincibility == false && this.player.player.data.values.health > 0)
        }, this);

    boss_bot.wheel1 = scene.physics.add.sprite(x, y, 'wheel');
    boss_bot.wheel1.setScale(1.4)
    boss_bot.wheel1.body.setAllowGravity(false);
    boss_bot.wheel1.setDepth(750);

    boss_bot.wheel2 = scene.physics.add.sprite(x, y, 'wheel');
    boss_bot.wheel2.setScale(1.4)
    boss_bot.wheel2.body.setAllowGravity(false);
    boss_bot.wheel2.setDepth(750);

    boss_bot.wheel3 = scene.physics.add.sprite(x, y, 'wheel');
    boss_bot.wheel3.setScale(1.4)
    boss_bot.wheel3.body.setAllowGravity(false);
    boss_bot.wheel3.setDepth(750);

    boss_bot.wheel4 = scene.physics.add.sprite(x, y, 'wheel');
    boss_bot.wheel4.setScale(1.4)
    boss_bot.wheel4.body.setAllowGravity(false);
    boss_bot.wheel4.setDepth(750);

    boss_bot.body.immovable = true;
    boss_bot.setScale(2)
    boss_bot.setOffset(95,164)
		boss_bot.setSize(90, 26, false)
    boss_bot.setDepth(740);
		scene.boss_bots.add(boss_bot)
    scene.enemies_added += 1;

    boss_bot.head = scene.physics.add.sprite(x, y, 'car_wheel');
    boss_bot.head.setScale(2);
    boss_bot.head.body.setAllowGravity(false);
    boss_bot.head.setSize(30,45)
    boss_bot.head.setDepth(750);
    boss_bot.head.health = boss_health; // 12
    boss_bot.head.setVisible(false)
    boss_bot.head.just_damaged = false

    boss_bot.chest = scene.physics.add.sprite(x, y, 'car_wheel');
    boss_bot.chest.setScale(2);
    boss_bot.chest.body.setAllowGravity(false);
    boss_bot.chest.setSize(96,40)
    boss_bot.chest.setDepth(750);
    boss_bot.chest.setVisible(false)

    boss_bot.pipe = scene.physics.add.sprite(x, y, 'car_wheel');
    boss_bot.pipe.setScale(2);
    boss_bot.pipe.body.setAllowGravity(false);
    boss_bot.pipe.setSize(18,50)
    boss_bot.pipe.setDepth(750);
    boss_bot.pipe.setVisible(false)

    scene.physics.add.overlap(this.player.player, boss_bot.chest, function(player, chest){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = chest.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 3)
        chest.body.immovable = true;
        }, function(player, chest){
            return (this.player.player.data.values.post_hit_invincibility == false)
        }, this);

    scene.physics.add.overlap(this.player.player, boss_bot.head, function(player, head){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = head.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 3)
        head.body.immovable = true;
        }, function(player, head){
            return (this.player.player.data.values.post_hit_invincibility == false)
        }, this);

    scene.physics.add.overlap(this.player.player, boss_bot.pipe, function(player, pipe){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = pipe.x
        scene.player_hit.play()
        this.player.damagePlayer(this.player, scene, 3)
        pipe.body.immovable = true;
        }, function(player, pipe){
            return (this.player.player.data.values.post_hit_invincibility == false)
        }, this);

      boss_bot.life_bar = scene.physics.add.sprite(1060, 30, 'boss_life_bar').setDepth(1000);
      boss_bot.life_bar.body.setAllowGravity(false);
      boss_bot.life_bar.setScrollFactor(0,0)
      boss_bot.life_bar.setFrame(18)

}

}
