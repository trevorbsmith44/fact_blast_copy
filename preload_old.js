import Player from "./player.js";
import Fact from  "./fact.js";
import Npc from  "./npc.js";
import Bs from  "./bs.js";
import Bird from  "./bird.js";
import Censor from "./censor.js"
import Tomato from  "./tomato.js";

export default class Preload extends Phaser.Scene {
constructor() {
  	super("preload");
}

preload (){

    this.load.image('title', 'assets/test_title.png');
    this.load.image('logo2', 'assets/logo2.jpg');
  	this.load.image('bs', 'assets/bs/bs.png');
		this.load.image('bs2', 'assets/bs/bs2.png');
		this.load.image('bs3', 'assets/bs/bs3.png');
		this.load.image('bs4', 'assets/bs/bs4.png');
		this.load.image('bs10', 'assets/bs/bs10.png');
    this.load.image('bs12', 'assets/bs/bs12.png');
    this.load.image('bs14', 'assets/bs/bs14.png');
    this.load.image('bs16', 'assets/bs/bs16.png');
    this.load.image('bs17', 'assets/bs/bs17.png');
    this.load.image('bs18', 'assets/bs/bs18.png');
    this.load.image('bs19', 'assets/bs/bs19.png');
    this.load.image('bs20', 'assets/bs/bs20.png');
    this.load.image('bs21', 'assets/bs/bs21.png');
    this.load.image('bs22', 'assets/bs/bs22.png');
    this.load.image('bs23', 'assets/bs/bs23.png');
    this.load.image('bs24', 'assets/bs/bs24.png');
		this.load.image('censored', 'assets/bs/censored.png');
    this.load.image('math1', 'assets/math/math1.png');
    this.load.image('math2', 'assets/math/math2.png');
    this.load.image('math3', 'assets/math/math3.png');
    this.load.image('math4', 'assets/math/math4.png');
    this.load.image('math5', 'assets/math/math5.png');
    this.load.image('math6', 'assets/math/math6.png');
    this.load.image('math7', 'assets/math/math7.png');
    this.load.image('math8', 'assets/math/math8.png');
    this.load.image('math9', 'assets/math/math9.png');
    this.load.image('math10', 'assets/math/math10.png');
    this.load.image('math11', 'assets/math/math11.png');
    this.load.image('math12', 'assets/math/math12.png');
    this.load.image('math13', 'assets/math/math13.png');
    this.load.image('math14', 'assets/math/math14.png');
    this.load.image('math15', 'assets/math/math15.png');
    this.load.image('math16', 'assets/math/math16.png');
    this.load.image('math17', 'assets/math/math17.png');
    this.load.image('math18', 'assets/math/math18.png');
    this.load.image('math19', 'assets/math/math19.png');
    this.load.image('math20', 'assets/math/math20.png');
    this.load.image('math21', 'assets/math/math21.png');
		this.load.image('fact', 'assets/fact2.png');

    this.load.image('black', 'assets/extra/black.png');
    this.load.image('phone', 'assets/phone.png');
    this.load.image('phone2', 'assets/phone2.png');
    this.load.image('parking_lot', 'assets/parking_lot.png');
    this.load.image('fence', 'assets/fence.png');
    this.load.image('starburst', 'assets/starburst.png');
    this.load.image('thought', 'assets/thought.png');
    this.load.image('red_car', 'assets/red_car.png');
    this.load.image('wind', 'assets/wind.png');
    this.load.image('buildings', 'assets/maps/cutscene_buildings.png');
    this.load.image('c1i3_bg', 'assets/level3_bg_cutscene.png');
    this.load.image('limo', 'assets/limo.png');
    this.load.image('welcome_to_chicago', 'assets/welcome_to_chicago.png');
    this.load.image('road', 'assets/road.png');
    this.load.image('gate', 'assets/extra/gate.jpg');
    this.load.image('c5i1_bg', 'assets/c5i1_bg.png');
    this.load.image('c5i2_bg', 'assets/c5i2_bg.png');
    this.load.image('c5i2_bg2', 'assets/c5i2_bg2.png');
    this.load.image('c5i3_bg', 'assets/c5i3_bg.png');
    this.load.image('hill', 'assets/hill.png');
    this.load.image('c6_bg', 'assets/c6_bg.png');

    this.load.image('c1i0', 'assets/cutscenes/c1i0.png');
    this.load.image('c1i1', 'assets/cutscenes/c1i1.png');
    this.load.image('c1i2', 'assets/cutscenes/c1i2.png');
    this.load.image('c1i3', 'assets/cutscenes/c1i3.png');

    this.load.image('c2i1', 'assets/cutscenes/c2i1.png');
    this.load.image('c2i2', 'assets/cutscenes/c2i2.png');
    this.load.image('c2i3', 'assets/cutscenes/c2i3.png');
    this.load.image('c2i4', 'assets/cutscenes/c2i4.png');

    this.load.image('c3i1', 'assets/cutscenes/c3i1.png');
    this.load.image('c3i2', 'assets/cutscenes/c3i2.png');
    this.load.image('c3i3', 'assets/cutscenes/c3i3.png');
    this.load.image('c3i4', 'assets/cutscenes/c3i4.png');

    this.load.image('c4i1', 'assets/cutscenes/c4i1.png');
    this.load.image('c4i2', 'assets/cutscenes/c4i2.png');
    this.load.image('c4i3', 'assets/cutscenes/c4i3.png');
    this.load.image('c4i4', 'assets/cutscenes/c4i4.png');

    this.load.image('c5i1', 'assets/cutscenes/c5i1.png');
    this.load.image('c5i2', 'assets/cutscenes/c5i2.png');
    this.load.image('c5i3', 'assets/cutscenes/c5i3.png');
    this.load.image('c5i4', 'assets/cutscenes/c5i4.png');

    this.load.spritesheet('jeremy_straight_face', 'assets/jeremy_straight_face.png', { frameWidth: 66, frameHeight: 96 });
    this.load.spritesheet('title_screen_background', 'assets/blast.png', { frameWidth: 1400, frameHeight: 1400 });
    this.load.spritesheet('play_game', 'assets/play_game.png', { frameWidth: 547, frameHeight: 135 });
    this.load.spritesheet('options', 'assets/options.png', { frameWidth: 450, frameHeight: 141 });
    this.load.spritesheet('main_menu', 'assets/main_menu.png', { frameWidth: 540, frameHeight: 130 });
    this.load.spritesheet('credits', 'assets/credits.png', { frameWidth: 450, frameHeight: 150 });
    this.load.spritesheet('mute', 'assets/mute.png', { frameWidth: 480, frameHeight: 130 });
    this.load.spritesheet('van', 'assets/van.png', { frameWidth: 130, frameHeight: 50 });

    this.load.spritesheet('jeremy', 'assets/jeremy.png', { frameWidth: 66, frameHeight: 96 });
		this.load.spritesheet('npc', 'assets/enemies/npc2.png', { frameWidth: 64, frameHeight: 96 });
		this.load.spritesheet('bird', 'assets/enemies/bird.png', { frameWidth: 100, frameHeight: 72 });
		this.load.spritesheet('tomato', 'assets/enemies/tomato.png', { frameWidth: 180, frameHeight: 130 });
    this.load.spritesheet('bot', 'assets/enemies/bot.png', { frameWidth: 280, frameHeight: 200 });
    this.load.spritesheet('buzz_saw', 'assets/enemies/buzz_saw.png', { frameWidth: 63, frameHeight: 63 });
    this.load.spritesheet('wheel', 'assets/enemies/wheel.png', { frameWidth: 33, frameHeight: 33 });
    this.load.spritesheet('small_bot', 'assets/enemies/small_bot.png', { frameWidth: 128, frameHeight: 112 });
    this.load.spritesheet('demonetization', 'assets/enemies/demonetization.png', { frameWidth: 128, frameHeight: 128 })
    this.load.spritesheet('torch', 'assets/enemies/torch.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('boss_npc', 'assets/enemies/npc_boss.png', { frameWidth: 256, frameHeight: 182 });
    this.load.spritesheet('milkshake', 'assets/enemies/milkshake.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('boss_tomato', 'assets/enemies/boss_tomato.png', { frameWidth: 180, frameHeight: 130 });
    this.load.spritesheet('boss_bot', 'assets/enemies/boss_bot.png', { frameWidth: 280, frameHeight: 220 });
    this.load.spritesheet('explosion', 'assets/enemies/explosion.png', { frameWidth: 280, frameHeight: 220 });
    this.load.spritesheet('trash_can', 'assets/enemies/trash_can.png', { frameWidth: 34, frameHeight: 33 });
    this.load.spritesheet('regular_tomato', 'assets/enemies/regular_tomato.png', { frameWidth: 180, frameHeight: 130 });
    this.load.spritesheet('rock', 'assets/rock.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('dark_rock', 'assets/dark_rock.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('egg', 'assets/enemies/egg.png', { frameWidth: 180, frameHeight: 130 });
    this.load.spritesheet('spikey_ball', 'assets/enemies/spikey_ball.png', { frameWidth: 180, frameHeight: 130 });
    this.load.spritesheet('snow', 'assets/snow.png', { frameWidth: 4, frameHeight: 4 });
    this.load.spritesheet('confetti', 'assets/confetti.png', { frameWidth: 4, frameHeight: 4 });
    this.load.spritesheet('snowflake', 'assets/enemies/snowflake.png', { frameWidth: 35, frameHeight: 37 });
    this.load.spritesheet('boss_bird', 'assets/enemies/boss_bird.png', { frameWidth: 192, frameHeight: 192 });
    this.load.spritesheet('machine', 'assets/enemies/machine.png', { frameWidth: 69, frameHeight: 130 });
    this.load.spritesheet('gear', 'assets/enemies/gear.png', { frameWidth: 34, frameHeight: 34 });
    this.load.spritesheet('platform', 'assets/platform.png', { frameWidth: 32, frameHeight: 32 });
    this.load.spritesheet('boss_npc_md', 'assets/enemies/npc_md1.png', { frameWidth: 160, frameHeight: 160 });
    this.load.spritesheet('eye', 'assets/enemies/eye.png', { frameWidth: 34, frameHeight: 34 });
    this.load.spritesheet('drill', 'assets/enemies/drill.png', { frameWidth: 38, frameHeight: 32 });
    this.load.spritesheet('canceled', 'assets/enemies/canceled.png', { frameWidth: 759, frameHeight: 243 });
    this.load.spritesheet('check_flag', 'assets/check_flag.png', { frameWidth: 34, frameHeight: 64 });
    this.load.spritesheet('keyboard', 'assets/keyboard.png', { frameWidth: 64, frameHeight: 64 });
    this.load.spritesheet('controller', 'assets/controller.png', { frameWidth: 200, frameHeight: 200 });
    this.load.spritesheet('enter_key', 'assets/enter_key.png', { frameWidth: 128, frameHeight: 64 });
    this.load.spritesheet('blank_screen', 'assets/blank_screen.png', { frameWidth: 1200, frameHeight: 600 });

    this.load.spritesheet('life_bar', 'assets/life_bar.png', { frameWidth: 280, frameHeight: 50 });
    this.load.spritesheet('juice', 'assets/apple_juice.png', { frameWidth: 26, frameHeight: 32 });
    this.load.spritesheet('balloon_blue', 'assets/balloon_blue.png', { frameWidth: 32, frameHeight: 66 });
    this.load.spritesheet('balloon_red', 'assets/balloon_red.png', { frameWidth: 32, frameHeight: 66 });
    this.load.spritesheet('balloon_green', 'assets/balloon_green.png', { frameWidth: 32, frameHeight: 66 });
    this.load.spritesheet('balloon_yellow', 'assets/balloon_yellow.png', { frameWidth: 32, frameHeight: 66 });
    this.load.spritesheet('balloon_purple', 'assets/balloon_purple.png', { frameWidth: 32, frameHeight: 66 });
    this.load.spritesheet('balloon_pink', 'assets/balloon_pink.png', { frameWidth: 32, frameHeight: 66 });
    this.load.spritesheet('flag', 'assets/flag.png', { frameWidth: 34, frameHeight: 34 });
    this.load.spritesheet('car_wheel', 'assets/car_wheel.png', { frameWidth: 33, frameHeight: 33 });
    this.load.spritesheet('liquid_green', 'assets/liquid_green.png', { frameWidth: 34, frameHeight: 66 });
    this.load.spritesheet('liquid_blue', 'assets/liquid_blue.png', { frameWidth: 34, frameHeight: 66 });
    this.load.spritesheet('liquid_purple', 'assets/liquid_purple.png', { frameWidth: 34, frameHeight: 66 });
    this.load.spritesheet('water', 'assets/water.png', { frameWidth: 66, frameHeight: 66 });
    this.load.spritesheet('lava', 'assets/lava.png', { frameWidth: 34, frameHeight: 66 });

    this.load.tilemapTiledJSON("level1_map", "assets/maps/level1.json");
    this.load.image("level1_tileset", "assets/maps/level1_tileset.png");
    this.load.image("level1_bg1", "assets/maps/level1_bg1.png");
    this.load.image("level1_bg2", "assets/maps/level1_bg2.png");
    this.load.image("level1_sky", "assets/maps/level1_bg_sky.png");

    this.load.tilemapTiledJSON("level2_map", "assets/maps/level2.json");
    this.load.image("level2_tileset", "assets/maps/level2_tileset.png");
    this.load.image("level2_bg2", "assets/maps/level2_bg2.png");
    this.load.image("level2_bg1", "assets/maps/level2_bg1.png");
    this.load.image("forest_c", "assets/maps/bg_forest_c.png");
    this.load.image("forest_b", "assets/maps/bg_forest_b.png");
    this.load.image("level2_sky", "assets/maps/awesome_sunset_sky.png");

    this.load.tilemapTiledJSON("level3_map", "assets/maps/level3.json");
    this.load.image("level3_sky", "assets/maps/level3_bg_sky3.png"); //3
    this.load.image("night_sky", "assets/maps/level3_bg_sky6.png"); //3
    this.load.image("level3_tileset", "assets/maps/level3_tileset.png");
    this.load.image("level3_bg", "assets/maps/level3_bg4.png");
    this.load.image("level3_bg2", "assets/maps/level3_bg2.png");

    this.load.tilemapTiledJSON("level4_map", "assets/maps/level4.json");
    this.load.image("level4_sky", "assets/maps/level3_bg_sky2.png"); //4
    this.load.image("level4_tileset", "assets/maps/level4_tileset.png");
    this.load.image("level4_bg1", "assets/maps/level4_bg1.png");
    this.load.image("level4_bg2", "assets/maps/level4_bg2.png");
    this.load.image("level4_bg3", "assets/maps/level4_bg3.png");
    this.load.image("level4_bg4", "assets/maps/level4_bg4.png");

    this.load.tilemapTiledJSON("level5_map", "assets/maps/level5.json");
    this.load.image("level5_tileset", "assets/maps/level5_tileset.png");
    this.load.image("level5_bg", "assets/maps/level5_bg.png");

    this.load.tilemapTiledJSON("level6_map", "assets/maps/level6.json");
    this.load.image("level6_tileset", "assets/maps/level6_tileset.png");
    this.load.image("level6_bg1", "assets/maps/level6_bg1.png");
    this.load.image("level6_bg2", "assets/maps/level6_bg2.png");
    this.load.image("level6_bg3", "assets/maps/level6_bg3.png");

    this.load.image("options_bg", "assets/maps/options_bg.png");

    this.load.audio('drum', "assets/sound_effects/one_kick_sound.mp3")
    this.load.audio('whoosh1', "assets/sound_effects/whoosh1.mp3")
    this.load.audio('whoosh2', "assets/sound_effects/whoosh2.mp3")
    this.load.audio('whoosh3', "assets/sound_effects/whoosh3.mp3")
    this.load.audio('whack1', "assets/sound_effects/whack1.mp3")
    this.load.audio('whack2', "assets/sound_effects/whack2.mp3")
    this.load.audio('whack3', "assets/sound_effects/whack3.mp3")
    this.load.audio('whack4', "assets/sound_effects/whack4.mp3")
    this.load.audio('splat', "assets/sound_effects/splat.mp3")
    this.load.audio('clang', "assets/sound_effects/clang.mp3")
    this.load.audio('clang2', "assets/sound_effects/clang2.mp3")
    this.load.audio('soft_hit', "assets/sound_effects/soft_hit.mp3")
    this.load.audio('pop', "assets/sound_effects/pop.mp3")
    this.load.audio('juice_sound', "assets/sound_effects/juice.mp3")
    this.load.audio('player_hit', "assets/sound_effects/player_hit.mp3")
    this.load.audio('crash', "assets/sound_effects/crash.mp3")
    this.load.audio('bird_sound', "assets/sound_effects/bird.mp3")
    this.load.audio('bird_sound2', "assets/sound_effects/bird2.mp3")
    this.load.audio('bird_swoop', "assets/sound_effects/bird_swoop.mp3")
    this.load.audio('bird_egg', "assets/sound_effects/bird_egg.mp3")
    this.load.audio('bird_defeated', "assets/sound_effects/bird_defeated.mp3")
    this.load.audio('jump_sound', "assets/sound_effects/jump.mp3")
    this.load.audio('bonk', "assets/sound_effects/bonk.mp3")
    this.load.audio('sizzle', "assets/sound_effects/sizzle.mp3")
    this.load.audio('explosion', "assets/sound_effects/explosion.mp3")
    this.load.audio('rock_sound', "assets/sound_effects/rock_sound.mp3")
    this.load.audio('nooo_sound', "assets/sound_effects/no.mp3")
    this.load.audio('boss_tomato_throw_sound', "assets/sound_effects/boss_tomato_throw_sound.mp3")
    this.load.audio('boss_bot_throw_sound', "assets/sound_effects/boss_bot_throw_sound.mp3")

    this.load.audio('vl1', "assets/voice_lines/vl1.mp3")
    this.load.audio('vl2', "assets/voice_lines/vl2.mp3")
    this.load.audio('vl11', "assets/voice_lines/vl11.mp3")
    this.load.audio('vl14', "assets/voice_lines/vl14.mp3")
    this.load.audio('vl15', "assets/voice_lines/vl15.mp3")
    this.load.audio('vl18', "assets/voice_lines/vl18.mp3")
    this.load.audio('vl20', "assets/voice_lines/vl20.mp3")
    this.load.audio('vl21', "assets/voice_lines/vl21.mp3")
    this.load.audio('vl22', "assets/voice_lines/vl22.mp3")
    this.load.audio('vl23', "assets/voice_lines/vl23.mp3")
    this.load.audio('vl24', "assets/voice_lines/vl24.mp3")
    this.load.audio('vl28', "assets/voice_lines/vl28.mp3")
    this.load.audio('vl29', "assets/voice_lines/vl29.mp3")
    this.load.audio('vl30', "assets/voice_lines/vl30.mp3")
    this.load.audio('vl31', "assets/voice_lines/vl31.mp3")
    this.load.audio('vl37', "assets/voice_lines/vl37.mp3")
    this.load.audio('vl42', "assets/voice_lines/vl42.mp3")
    this.load.audio('vl46', "assets/voice_lines/vl46.mp3")
    this.load.audio('vl48', "assets/voice_lines/vl48.mp3")
    this.load.audio('vl49', "assets/voice_lines/vl49.mp3")
    this.load.audio('vl50', "assets/voice_lines/vl50.mp3")
    this.load.audio('vl51', "assets/voice_lines/vl51.mp3")
    this.load.audio('vl52', "assets/voice_lines/vl52.mp3")
    this.load.audio('vl53', "assets/voice_lines/vl53.mp3")
    this.load.audio('vl54', "assets/voice_lines/vl54.mp3")
    this.load.audio('vl57', "assets/voice_lines/vl57.mp3")
    this.load.audio('vl58', "assets/voice_lines/vl58.mp3")
    this.load.audio('vl59', "assets/voice_lines/vl59.mp3")
    this.load.audio('vl60', "assets/voice_lines/vl60.mp3")
    this.load.audio('vl61', "assets/voice_lines/vl61.mp3")
    this.load.audio('vl62', "assets/voice_lines/vl62.mp3")
    this.load.audio('vl63', "assets/voice_lines/vl63.mp3")
    this.load.audio('vl64', "assets/voice_lines/vl64.mp3")
    this.load.audio('vl65', "assets/voice_lines/vl65.mp3")
    this.load.audio('vl66', "assets/voice_lines/vl66.mp3")
    this.load.audio('vl67', "assets/voice_lines/vl67.mp3")
    this.load.audio('vl68', "assets/voice_lines/vl68.mp3")
    this.load.audio('vl69', "assets/voice_lines/vl69.mp3")
    this.load.audio('vl70', "assets/voice_lines/vl70.mp3")
    this.load.audio('vl71', "assets/voice_lines/vl71.mp3")

    this.load.audio('title_screen_soundtrack', "assets/music/title_screen.mp3")
    this.load.audio('cutscene_soundtrack', "assets/music/cutscene.mp3")
    this.load.audio('cutscene5_soundtrack', "assets/music/cutscene5.mp3")
    this.load.audio('options_soundtrack', "assets/music/options.mp3")
    this.load.audio('level_1_soundtrack', "assets/music/level1.mp3")
    this.load.audio('level_2_soundtrack', "assets/music/level2.mp3")
    this.load.audio('level_3_soundtrack', "assets/music/level3.mp3")
    this.load.audio('level_4_soundtrack', "assets/music/level4.mp3")
    this.load.audio('level_5_soundtrack', "assets/music/level5.mp3")
    this.load.audio('level_6_soundtrack', "assets/music/level6.mp3")
    this.load.audio('end_credits_soundtrack', "assets/music/end_credits.mp3")

//`
}

create (){
    // PLAYER ANIMATIONS
    this.anims.create({
        key: 'run_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 10, frames: [10,11,12,13] }),
        frameRate: 10,
        repeat: -1 // tells the animation to loop
        });
    this.anims.create({
        key: 'jump_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 17, end: 17 }),
        });
    this.anims.create({
        key: 'stand_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 3, frames: [3,30,31,30,3,3] }),
        frameRate: 4,
        repeat: -1
        });
    this.anims.create({
        key: 'stand_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 0, frames: [0,28,29,28,0,0] }),
        frameRate: 4,
        repeat: -1
        });
    this.anims.create({
        key: 'run_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 6, frames: [6,7,8,9] }),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'jump_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 14, end: 14 }),
        });
    this.anims.create({
        key: 'jump_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 15, frames: [15,16,16,16,14] }),
        frameRate: 10,
        });
    this.anims.create({
        key: 'jump_left_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 18, frames: [18,19,19,19,17] }),
        frameRate: 10,
        });
    this.anims.create({
        key: 'stand_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 1, frames: [1,2,2,2,0] }),
        frameRate: 10,
        repeat: 0,
        });
    this.anims.create({
        key: 'stand_left_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 4, frames: [4,5,5,5,3] }),
        frameRate: 10,
        repeat: 0
        });
    this.anims.create({
        key: 'hit_to_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 20, end: 20 }),
        });
    this.anims.create({
        key: 'hit_to_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 21, end: 21 }),
        });
    this.anims.create({
        key: 'duck_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 22, end: 22 }),
        });
    this.anims.create({
        key: 'duck_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 25, end: 25 }),
        });
    this.anims.create({
        key: 'duck_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 23, frames: [23,24,24,24,22] }),
        frameRate: 10,
        repeat: 0,
        });
    this.anims.create({
        key: 'duck_left_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 26, frames: [26,27,27,27,25] }),
        frameRate: 10,
        repeat: 0
        });

    // NPC ANIMATIONS
    this.anims.create({
        key: 'npc_walk_left',
        frames: this.anims.generateFrameNumbers('npc', { start: 4, frames: [3,4,5,4] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'npc_walk_right',
        frames: this.anims.generateFrameNumbers('npc', { start: 1, frames: [0,1,2,1] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'npc_stand_right',
        frames: this.anims.generateFrameNumbers('npc', { start: 1, end: 1 }),
        });
    this.anims.create({
        key: 'npc_stand_left',
        frames: this.anims.generateFrameNumbers('npc', { start: 4, end: 4 }),
        });
    this.anims.create({
    		key: 'npc_angry_walk_left',
    		frames: this.anims.generateFrameNumbers('npc', { start: 4, frames: [13,14,15,14] }),
    		frameRate: 6,
    		repeat: -1
    		});
    this.anims.create({
    		key: 'npc_angry_walk_right',
    		frames: this.anims.generateFrameNumbers('npc', { start: 1, frames: [10,11,12,11] }),
    		frameRate: 6,
    		repeat: -1
    		});
    this.anims.create({
    		key: 'npc_angry_stand_right',
    		frames: this.anims.generateFrameNumbers('npc', { start: 11, end: 11 }),
    		});
    this.anims.create({
    		key: 'npc_angry_stand_left',
    		frames: this.anims.generateFrameNumbers('npc', { start: 14, end: 14 }),
    		});
    this.anims.create({
        key: 'npc_hit_to_right',
        frames: this.anims.generateFrameNumbers('npc', { start: 6, end: 6 }),
        });
    this.anims.create({
        key: 'npc_hit_to_left',
        frames: this.anims.generateFrameNumbers('npc', { start: 7, end: 7 }),
        });

    // BIRD ANIMATIONS
    this.anims.create({
        key: 'bird_fly_left',
        frames: this.anims.generateFrameNumbers('bird', { start: 2, frames: [2,3] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'bird_fly_right',
        frames: this.anims.generateFrameNumbers('bird', { start: 1, frames: [0,1] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'bird_hit_to_right',
        frames: this.anims.generateFrameNumbers('bird', { start: 5, end: 5 }),
        });
    this.anims.create({
        key: 'bird_hit_to_left',
        frames: this.anims.generateFrameNumbers('bird', { start: 4, end: 4 }),
        });

    // TOMATO ANIMATIONS
    this.anims.create({
        key: 'red_tomato_roll_left',
        frames: this.anims.generateFrameNumbers('tomato', { start: 0, end: 0 }),
        });
    this.anims.create({
        key: 'red_tomato_jump_left',
        frames: this.anims.generateFrameNumbers('tomato', { start: 1, end: 1 }),
        });
    this.anims.create({
        key: 'red_tomato_hit_to_right',
        frames: this.anims.generateFrameNumbers('tomato', { start: 2, end: 2 }),
        });
    this.anims.create({
        key: 'red_tomato_roll_right',
        frames: this.anims.generateFrameNumbers('tomato', { start: 3, end: 3 }),
        });
    this.anims.create({
        key: 'red_tomato_jump_right',
        frames: this.anims.generateFrameNumbers('tomato', { start: 4, end: 4 }),
        });
    this.anims.create({
        key: 'red_tomato_hit_to_left',
        frames: this.anims.generateFrameNumbers('tomato', { start: 5, end: 5 }),
        });
    this.anims.create({
        key: 'green_tomato_roll_left',
        frames: this.anims.generateFrameNumbers('tomato', { start: 6, end: 6 }),
        });
    this.anims.create({
        key: 'green_tomato_jump_left',
        frames: this.anims.generateFrameNumbers('tomato', { start: 7, end: 7 }),
        });
    this.anims.create({
        key: 'green_tomato_hit_to_right',
        frames: this.anims.generateFrameNumbers('tomato', { start: 8, end: 8 }),
        });
    this.anims.create({
        key: 'green_tomato_roll_right',
        frames: this.anims.generateFrameNumbers('tomato', { start: 9, end: 9 }),
        });
    this.anims.create({
        key: 'green_tomato_jump_right',
        frames: this.anims.generateFrameNumbers('tomato', { start: 10, end: 10 }),
        });
    this.anims.create({
        key: 'green_tomato_hit_to_left',
        frames: this.anims.generateFrameNumbers('tomato', { start: 11, end: 11 }),
        });
    this.anims.create({
        key: 'tomato_splat',
        frames: this.anims.generateFrameNumbers('tomato', { start: 12, frames: [12,13,13,12] }),
        frameRate: 16,
        });

    // BOT ANIMATIONS
    this.anims.create({
        key: 'bot_roll_left',
        frames: this.anims.generateFrameNumbers('bot', { start: 0, frames: [0,1] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'bot_roll_right',
        frames: this.anims.generateFrameNumbers('bot', { start: 3, frames: [3,4] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'bot_stand_left',
        frames: this.anims.generateFrameNumbers('bot', { start: 0, end: 0 }),
        });
    this.anims.create({
        key: 'bot_stand_right',
        frames: this.anims.generateFrameNumbers('bot', { start: 3, end: 3 }),
        });
    this.anims.create({
        key: 'bot_throw',
        frames: this.anims.generateFrameNumbers('bot', { start: 2, frames: [2,2,0] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'bot_explode_left',
        frames: this.anims.generateFrameNumbers('bot', { start: 0, frames: [0,5,6,7,8,9,10,11,12,13,14,14,14,14,14] }),
        frameRate: 8,
        repeat: 1
        });
    this.anims.create({
        key: 'bot_explode_right',
        frames: this.anims.generateFrameNumbers('bot', { start: 3, frames: [3,15,16,17,18,19,20,21,22,23,24,24,24,24,24] }),
        frameRate: 8,
        repeat: 1
        });

    this.anims.create({
        key: 'balloon_blue',
        frames: this.anims.generateFrameNumbers('balloon_blue', { start: 0, frames: [0,1,2,3,4,5,4,3,2,1] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'balloon_purple',
        frames: this.anims.generateFrameNumbers('balloon_purple', { start: 0, frames: [0,1,2,3,4,5,4,3,2,1] }),
        frameRate: 7,
        repeat: -1
        });
    this.anims.create({
        key: 'balloon_red',
        frames: this.anims.generateFrameNumbers('balloon_red', { start: 0, frames: [0,1,2,3,4,5,4,3,2,1] }),
        frameRate: 8,
        repeat: -1
        });
    this.anims.create({
        key: 'balloon_yellow',
        frames: this.anims.generateFrameNumbers('balloon_yellow', { start: 0, frames: [0,1,2,3,4,5,4,3,2,1] }),
        frameRate: 9,
        repeat: -1
        });
    this.anims.create({
        key: 'balloon_green',
        frames: this.anims.generateFrameNumbers('balloon_green', { start: 0, frames: [0,1,2,3,4,5,4,3,2,1] }),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'balloon_pink',
        frames: this.anims.generateFrameNumbers('balloon_pink', { start: 0, frames: [0,1,2,3,4,5,4,3,2,1] }),
        frameRate: 11,
        repeat: -1
        });

    this.anims.create({
        key: 'flag_red',
        frames: this.anims.generateFrameNumbers('flag', { start: 0, frames: [0,1,2,3,4,5,6,7,8,9]}),
        frameRate: 10,
        repeat: -1
        });

    this.anims.create({
        key: 'flag_blue',
        frames: this.anims.generateFrameNumbers('flag', { start: 10, frames: [10,11,12,13,14,15,16,17,18,19]}),
        frameRate: 12,
        repeat: -1
        });

    this.anims.create({
        key: 'flag_yellow',
        frames: this.anims.generateFrameNumbers('flag', { start: 20, frames: [20,21,22,23,24,25,26,27,28,29]}),
        frameRate: 13,
        repeat: -1
        });

    this.anims.create({
        key: 'flag_green',
        frames: this.anims.generateFrameNumbers('flag', { start: 30, frames: [30,31,32,33,34,35,36,37,38,39]}),
        frameRate: 11,
        repeat: -1
        });

    this.anims.create({
        key: 'check_flag1',
        frames: this.anims.generateFrameNumbers('check_flag', { start: 0, frames: [0,1,2,3,4,5,6,7,8,9]}),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'check_flag2',
        frames: this.anims.generateFrameNumbers('check_flag', { start: 0, frames: [0,1,2,3,4,5,6,7,8,9]}),
        frameRate: 8,
        repeat: -1
        });

    // BOSS NPC ANIMATIONS
    this.anims.create({
        key: 'boss_npc_walk_left',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 0, frames: [0,1,2,1,0,3,4,3] }),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_npc_walk_right',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 10, frames: [10,11,12,11,10,13,14,13] }),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_npc_throw_left',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 0, frames: [0,5,6,7,7,7,7,7,7] }),
        frameRate: 10,
        repeat: 1
        });
    this.anims.create({
        key: 'boss_npc_throw_right',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 10, frames: [10,15,16,17,17,17,17,17,17] }),
        frameRate: 10,
        repeat: 1
        });
    this.anims.create({
        key: 'boss_npc_no_left',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 8, frames: [8] }),
        });
    this.anims.create({
        key: 'boss_npc_no_right',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 18, frames: [18] }),
        });
    this.anims.create({
        key: 'boss_npc_defeated_left',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 9, frames: [9] }),
        });
    this.anims.create({
        key: 'boss_npc_defeated_right',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 19, frames: [19] }),
        });
    this.anims.create({
        key: 'boss_npc_fall_left',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 0, frames: [0] }),
        });
    this.anims.create({
        key: 'boss_npc_fall_right',
        frames: this.anims.generateFrameNumbers('boss_npc', { start: 10, frames: [10] }),
        });

    this.anims.create({
        key: 'small_bot_throw_left',
        frames: this.anims.generateFrameNumbers('small_bot', { start: 0, frames: [0,1,2,0] }),
        frameRate: 6,
        repeat: 1
        });
    this.anims.create({
        key: 'small_bot_throw_right',
        frames: this.anims.generateFrameNumbers('small_bot', { start: 4, frames: [4,5,6,4] }),
        frameRate: 6,
        repeat: 1
        });
    this.anims.create({
        key: 'burn',
        frames: this.anims.generateFrameNumbers('torch', { start: 0, frames: [0,1] }),
        frameRate: 10,
        repeat: -1
        });

    this.anims.create({
        key: 'boss_tomato_walk_left',
        frames: this.anims.generateFrameNumbers('boss_tomato', { start: 0, frames: [0,1,0,2] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_tomato_throw_left',
        frames: this.anims.generateFrameNumbers('boss_tomato', { start: 0, frames: [0,3,4,0,0,0] }),
        frameRate: 6,
        repeat: 1
        });
    this.anims.create({
        key: 'boss_tomato_walk_right',
        frames: this.anims.generateFrameNumbers('boss_tomato', { start: 6, frames: [6,7,6,8] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_tomato_throw_right',
        frames: this.anims.generateFrameNumbers('boss_tomato', { start: 6, frames: [6,9,10,6,6,6] }),
        frameRate: 6,
        repeat: 1
        });

    //BOSS BIRD
    this.anims.create({
        key: 'boss_bird_right_closed',
        frames: this.anims.generateFrameNumbers('boss_bird', { start: 0, frames: [0,1,2,1] }),
        frameRate: 8,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bird_left_closed',
        frames: this.anims.generateFrameNumbers('boss_bird', { start: 9, frames: [9,10,11,10] }),
        frameRate: 8,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bird_right_open',
        frames: this.anims.generateFrameNumbers('boss_bird', { start: 3, frames: [3,4,5,4] }),
        frameRate: 8,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bird_left_open',
        frames: this.anims.generateFrameNumbers('boss_bird', { start: 12, frames: [12,13,14,13] }),
        frameRate: 8,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bird_right_hit',
        frames: this.anims.generateFrameNumbers('boss_bird', { start: 6, frames: [6,7,8,7] }),
        frameRate: 8,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bird_left_hit',
        frames: this.anims.generateFrameNumbers('boss_bird', { start: 15, frames: [15,16,17,16] }),
        frameRate: 8,
        repeat: -1
        });

    // BOSS BOT
    this.anims.create({
        key: 'boss_bot_roll_left',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 0, frames: [0,1] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bot_roll_right',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 3, frames: [3,4] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'boss_bot_stand_left',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 0, end: 0 }),
        });
    this.anims.create({
        key: 'boss_bot_stand_right',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 3, end: 3 }),
        });
    this.anims.create({
        key: 'boss_bot_throw_over',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 2, frames: [2,4,5,5,5,5,5] }),
        frameRate: 8,
        });
    this.anims.create({
        key: 'boss_bot_throw_under',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 2, frames: [5,4,2,3,3,2,2,2,2,2] }),
        frameRate: 8,
        });
    this.anims.create({
        key: 'boss_bot_explode',
        frames: this.anims.generateFrameNumbers('boss_bot', { start: 6, frames: [6,7,8,9,10,11,12,13,14,15,15,15,15,15,15,15,15] }),
        frameRate: 6,
        repeat: 1
        });
    this.anims.create({
        key: 'machine',
        frames: this.anims.generateFrameNumbers('machine', { start: 0, frames: [0,1,2,3] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'explosion_explode',
        frames: this.anims.generateFrameNumbers('explosion', { start: 0, frames: [0,1,2,3,4,5,6,7,8,0,1,2,3,4,5,6,7,8,9,10,11,12,13] }),
        frameRate: 6,
        });
    this.anims.create({
        key: 'liquid_green',
        frames: this.anims.generateFrameNumbers('liquid_green', { start: 0, frames: [0,1,2,3] }),
        frameRate: 6,
        repeat: -1
        });
    this.anims.create({
        key: 'liquid_blue',
        frames: this.anims.generateFrameNumbers('liquid_blue', { start: 0, frames: [0,1,2,3] }),
        frameRate: 5,
        repeat: -1
        });
    this.anims.create({
        key: 'liquid_purple',
        frames: this.anims.generateFrameNumbers('liquid_purple', { start: 0, frames: [0,1,2,3] }),
        frameRate: 7,
        repeat: -1
        });
    this.anims.create({
        key: 'lava_bubble',
        frames: this.anims.generateFrameNumbers('lava', { start: 0, frames: [0,1,2,3] }),
        frameRate: 4,
        repeat: -1
        });
    this.anims.create({
        key: 'drill_right',
        frames: this.anims.generateFrameNumbers('drill', { start: 0, frames: [0,1,2] }),
        frameRate: 13,
        repeat: -1
        });
    this.anims.create({
        key: 'drill_left',
        frames: this.anims.generateFrameNumbers('drill', { start: 3, frames: [3,4,5] }),
        frameRate: 13,
        repeat: -1
        });
    this.anims.create({
        key: 'water',
        frames: this.anims.generateFrameNumbers('water', { start: 0, frames: [0,1,2,3,4,5,6] }),
        frameRate: 4,
        repeat: -1
        });
    this.anims.create({
        key: 'jeremy_dance',
        frames: this.anims.generateFrameNumbers('jeremy_straight_face', { start: 2, frames: [2,3,4,3] }),
        frameRate: 8,
        repeat: -1
        });

    this.scene.launch('opening');
}

}
