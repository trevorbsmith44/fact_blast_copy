export default class Tomato {

constructor(scene, player){
    scene.tomatoes = scene.physics.add.group();
    this.scene = scene
    this.player = player

    this.player_tomato = scene.physics.add.overlap(this.player.player, scene.tomatoes, function(player, tomato){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = tomato.x
        scene.splat.play();
        this.player.damagePlayer(this.player, scene, 3)
        tomato.hit_player = true
        this.scene.time.delayedCall(150, function() {
            tomato.state = 'splat'
            tomato.setData('time_at_splat', this.scene.manual_time)
            this.player.player.setTint(0x00ff00);
            this.player.player.setData('splat_time', this.scene.manual_time)
            }, [], this);
        }, function(player, tomato){
            return (tomato.data.values.health > 0
              && this.player.player.data.values.post_hit_invincibility == false)
        }, this);

}

update(){
    this.scene.tomatoes.children.each(function(tomato) {

  if (tomato.active){
      if (tomato.state == 'splat'){
          tomato.setVelocity(0, 50)
          tomato.setAngularVelocity(0)
          if (this.scene.manual_time < tomato.data.values.time_at_splat+300){
              tomato.anims.play('tomato_splat', true);}
          else if (this.scene.manual_time >= tomato.data.values.time_at_splat+300){
              tomato.destroy()}
          this.scene.time.delayedCall(300, function() {
              tomato.destroy()
              }, [], this);
          }

      if(tomato.state == 'roll_left'){
          tomato.setVelocityX(-600)
          tomato.anims.play(tomato.data.values.color + 'tomato_roll_left', true);
          tomato.setAngularVelocity(-500)
      }
      else if(tomato.state == 'roll_right'){
          tomato.setVelocityX(600)
          tomato.anims.play(tomato.data.values.color + 'tomato_roll_right', true);
          tomato.setAngularVelocity(500)
      }
      // TURNING
      if (tomato.body && tomato.data){
          if (tomato.data.values.last_turned_left == 0){
              tomato.data.values.last_turned_left = this.scene.manual_time}
          if (tomato.data.values.last_turned_right == 0){
              tomato.data.values.last_turned_right = this.scene.manual_time}
      if (tomato.body.onFloor()){
          if ((tomato.x > (this.player.x + 1000)) ||
          ((tomato.state == 'roll_right') && this.scene.manual_time > (tomato.data.values.last_turned_right + tomato.data.values.turn_wait))){
              tomato.data.values.last_turned_left = this.scene.manual_time
              tomato.state = 'roll_left'}
          else if ((tomato.x < (this.player.x-1000)) ||
          ((tomato.state == 'roll_left') && this.scene.manual_time > (tomato.data.values.last_turned_left + tomato.data.values.turn_wait))){
              tomato.data.values.last_turned_right = this.scene.manual_time
              tomato.state = 'roll_right'}}}

      if (tomato.data){
          if (tomato.data.values.health <= 0 && tomato.state != 'splat'){
              if (tomato.x <= this.player.player.x){
                  tomato.anims.play(tomato.data.values.color + 'tomato_hit_to_left', true);
                  tomato.setVelocityX(-600)
                  tomato.setAngularVelocity(-50)
                }
              else{
                  tomato.anims.play(tomato.data.values.color + 'tomato_hit_to_right', true);
                  tomato.setVelocityX(600)
                  tomato.setAngularVelocity(50)
                }
              if (tomato.y > ((this.scene.layers.layer.height*2)+100)){
                  tomato.destroy()
                  tomato.setVisible(false)}}}

          if (tomato.data){
          if (tomato.data.values.color.includes('green') && tomato.state != 'splat'){
              if (this.scene.manual_time > tomato.data.values.last_jump + tomato.data.values.jump_wait){
                  if (tomato.x < this.player.player.x) {
                      if (tomato.state == 'roll_left'){
                          tomato.data.values.last_turned_right = this.scene.manual_time}
                      if (tomato.body.onFloor()){
                          tomato.state = 'jump_right'}}
                  else if (tomato.x >= this.player.player.x) {
                      if (tomato.state == 'roll_right'){
                          tomato.data.values.last_turned_left = this.scene.manual_time}
                      if (tomato.body.onFloor()){
                          tomato.state = 'jump_left'}}
                  tomato.setData({'last_jump': this.scene.manual_time,
                                  'jump_wait': Phaser.Math.Between(500, 1000)})}

              if (tomato.state == 'jump_right' && tomato.data.values.health > 0){
                  tomato.setAngularVelocity(50)
                  tomato.setVelocityX(600)
                  tomato.anims.play(tomato.data.values.color + 'tomato_jump_right', true);
                  if (this.scene.manual_time > tomato.data.values.last_jump+50) {
                      if (tomato.body.onFloor()){
                          tomato.state = 'roll_right'}}}

              else if (tomato.state == 'jump_left' && tomato.data.values.health > 0){
                  tomato.setAngularVelocity(-50)
                  tomato.setVelocityX(-600)
                  tomato.anims.play(tomato.data.values.color + 'tomato_jump_left', true);
                  if (this.scene.manual_time > tomato.data.values.last_jump+50) {
                      if (tomato.body.onFloor()){
                          tomato.state = 'roll_left'}}}

              if (tomato.state.includes('jump') && tomato.body.onFloor()){
                  tomato.setVelocityY(-700)}

            } // if color == green
          } // if tomato.data exists
      }  // if tomato active
    }, this); // looping through all tomatoes
} // update


addTomato(scene, x, y, color) {
		var tomato = scene.tomatoes.create(x,y, 'tomato');
		tomato.setActive(true).setVisible(true)
		tomato.setData({'health': 1,
								 'time_at_collision':null,
								 'knock_back_time': 300,
								 'layer_collisions': [],
								 'jump_wait': Phaser.Math.Between(1000, 2000),
						 		 'last_jump': 0,
								 'last_turned_left': 0,
								 'last_turned_right': 0,
                 'turn_wait': Phaser.Math.Between(4000, 6000),
                 'splat': false,
                 'color': color + '_'
							 });

   for (var i=0;i<scene.layers.length;i++){
       tomato.data.values.layer_collisions.push(scene.physics.add.collider(tomato, scene.layers[i]));}

    var size = Phaser.Math.FloatBetween(1.3, 1.7)
		tomato.setScale(size)
		tomato.setSize(80, 80, false)
		tomato.setOffset(50,25)
    tomato.setDepth(950)
    if (scene.player.player.x <= x){
		    tomato.state = 'roll_left'}
    else {
		    tomato.state = 'roll_right'}
		scene.tomatoes.add(tomato)
    scene.enemies_added += 1;
}

}
