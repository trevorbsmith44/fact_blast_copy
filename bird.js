import Censor from "./censor.js"

export default class Bird {

constructor(scene, player){
    scene.birds = scene.physics.add.group();
    this.player = player
    this.scene = scene

    this.player_bird = scene.physics.add.collider(this.player.player, scene.birds, function(player, bird){
        this.player.player.data.values.time_at_collision = this.scene.manual_time
        this.player.player.data.values.hit_from = bird.x
        scene.player_hit.play()
        if (bird.data.values.type != 'wave'){
            bird.setVelocityY(0)}
        this.player.damagePlayer(this.player, scene, 3)
        }, function(player, bird){
            return (bird.data.values.health > 0
              && this.player.player.data.values.post_hit_invincibility == false)
        }, this);

}

update(player){
  this.scene.birds.children.each(function(bird) {
      if (bird.active){
      if (bird.data.values.type == 'drop' || bird.data.values.type == 'wave' || bird.data.values.type == 'norm') {
          if (bird.x > (player.x + 800)){
              bird.state = 'fly_left'}
          else if (bird.x < (player.x-800)){
              bird.state = 'fly_right'}}
      if (bird.state == 'fly_left'){
          bird.setVelocityX(-(bird.data.values.speed))
          bird.anims.play('bird_fly_left', true);}
      else if (bird.state == 'fly_right'){
          bird.setVelocityX(bird.data.values.speed)
          bird.anims.play('bird_fly_right', true);}

      if (bird.data.values.health <= 0){
          if (bird.x <= player.x){
              bird.setVelocityX(-300)
              bird.anims.play('bird_hit_to_left', true);}
          else{
              bird.setVelocityX(300)
              bird.anims.play('bird_hit_to_right', true);}}
      if ((bird.y > ((this.scene.layers.layer.height*2)+100)) || (bird.x > ((this.scene.layers.layer.width*2)+500)) || (bird.x < -1000)) {
          bird.destroy()
          bird.setVisible(false)}

      if (bird.data){ // prevents TypeError
          // DROP
          if (bird.data.values.type == 'drop' && bird.data.values.health > 0) {
              if (this.scene.manual_time > (bird.data.values.last_throw + bird.data.values.throw_wait)) {
                  this.censor = new Censor(this.scene, bird, player)}} // Drop censorship
          // WAVE
          if (bird.data.values.type == 'wave' && bird.data.values.health > 0) {
              if (bird.y >= bird.data.values.y_origin+80){
                  bird.setVelocityY(-350)}
              else if (bird.y < bird.data.values.y_origin-80){
                  bird.setVelocityY(350)}}
          // DIVE
          if (bird.data.values.type == 'dive' && bird.data.values.health > 0) {
              if (bird.state == 'down'){
                  bird.anims.play('bird_fly_right', true);
                  bird.setAngle(90)
                  bird.setVelocity(0, 200)
                  if (bird.y >= player.y-10 && bird.y <= player.y+10){
                      if (bird.x >= player.x){
                          bird.state = 'charge_left'}
                      else {
                          bird.state = 'charge_right'}}}
              else if (bird.state == 'charge_left'){
                  bird.setAngle(0)
                  bird.anims.play('bird_fly_left', true);
                  bird.setVelocity(-800, 0)}
              else if (bird.state == 'charge_right'){
                  bird.setAngle(0)
                  bird.anims.play('bird_fly_right', true);
                  bird.setVelocity(800, 0)}}
          // DIAGONAL
          if (bird.data.values.type == 'diag' && bird.data.values.health > 0) {
              bird.state = 'diag'
              if (bird.data.values.x_origin <= bird.data.values.player_x_origin && bird.data.values.y_origin > bird.data.values.player_y_origin) {
                  bird.anims.play('bird_fly_right', true);
                  bird.setAngle(-35)
                  bird.setVelocity(bird.data.values.speed, -190)} // up right
              else if (bird.data.values.x_origin > bird.data.values.player_x_origin && bird.data.values.y_origin > bird.data.values.player_y_origin) {
                  bird.anims.play('bird_fly_left', true);
                  bird.setAngle(-325)
                  bird.setVelocity(-bird.data.values.speed, -190)} // up left
              else if (bird.data.values.x_origin <= bird.data.values.player_x_origin && bird.data.values.y_origin <= bird.data.values.player_y_origin) {
                  bird.anims.play('bird_fly_right', true);
                  bird.setAngle(35)
                  bird.setVelocity(bird.data.values.speed, 190)} // down right
              else if (bird.data.values.x_origin > bird.data.values.player_x_origin && bird.data.values.y_origin <= bird.data.values.player_y_origin) {
                  bird.anims.play('bird_fly_left', true);
                  bird.setAngle(325)
                  bird.setVelocity(-bird.data.values.speed, 190)} // down left
              }
          } // if bird.data exists

      }  // if bird active
    }, this); // looping through all birds
}

addBird(scene, x, y, type) {
		var bird = this.scene.birds.create(x,y, 'bird');
		bird.setActive(true).setVisible(true)
		bird.setData({'health': 1,
								 'throw_wait': Phaser.Math.Between(2000, 4000),
						 		 'last_throw': 0,
								 'speed': Phaser.Math.Between(300,375),
                 'type': type,
                 'x_origin': x,
                 'y_origin': y,
                 'player_x_origin': this.scene.player.player.x,
                 'player_y_origin': this.scene.player.player.y
							 });
		bird.setSize(80, 50, false)
		bird.setOffset(15,5)
    bird.setScale(1.25)
    bird.setDepth(500)
    bird.name = 'bird'
		bird.state = 'fly_right'
		this.scene.birds.add(bird)
		bird.body.setAllowGravity(false);
    if (bird.data.values.type == 'wave'){
        bird.setVelocityY(350)}
    if (bird.data.values.type == 'dive'){
        bird.state = 'down'}

    this.scene.enemies_added += 1;
}

}
