var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var group;

var caption;

var captionStyle = {
    fill: '#7fdbff',
    fontFamily: 'monospace',
    lineSpacing: 4
};

var captionTextFormat = (
    'Total:    %1\n' +
    'Max:      %2\n' +
    'Active:   %3\n' +
    'Inactive: %4\n' +
    'Used:     %5\n' +
    'Free:     %6\n' +
    'Full:     %7\n'
);

var game = new Phaser.Game(config);

function preload () {
    this.load.spritesheet('npc', 'assets/npc.png', { frameWidth: 64, frameHeight: 96 });
}

function create () {
    this.anims.create({
        key: 'creep',
        frames: this.anims.generateFrameNumbers('npc', { start: 0, end: 1 }),
        frameRate: 2,
        repeat: -1
    });


    group = this.add.group({
        defaultKey: 'npc',
        maxSize: 100,
        createCallback: function (npc) {
            npc.setName('npc' + this.getLength());
            console.log('Created', npc.name);
        },
        removeCallback: function (npc) {
            console.log('Removed', npc.name);
        }
    });


    this.time.addEvent({
        delay: 100,
        loop: true,
        callback: addnpc
    });
}

function update () {
    // Phaser.Actions.IncY(group.getChildren(), 1);
    //
    // group.children.iterate(function (npc) {
    //     if (npc.y > 600) {
    //         group.killAndHide(npc);
    //     }
    // });

}

function addnpc () {
    npc = group.create(Phaser.Math.Between(250, 800), Phaser.Math.Between(-64, 0));
    if (!npc) return; // None free

    npc
    // .setActive(true)
    .setVisible(true)
    // .setTint(Phaser.Display.Color.RandomRGB().color)
    // .play('creep');
}
