var config = {
    type: Phaser.CANVAS,
    parent: 'phaser-example',
    width: 800,
    height: 600,
    backgroundColor: '#7d7d7d',
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 600 },
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);

var anim;
var player;

function preload ()
{
    this.load.spritesheet('jeremy', 'assets/jeremy.png', { frameWidth: 66, frameHeight: 96 });
    this.load.spritesheet('npc', 'assets/NPC.png', { frameWidth: 66, frameHeight: 96 });
}

function create ()
{
    this.anims.create({
        key: 'throw_left',
        frames: this.anims.generateFrameNumbers('npc', { start: 4, frames: [3,4,5,3] }),
        frameRate: 10,
        repeat: -1
    });

    player = this.physics.add.sprite(400, 20, 'npc');
    // player2 = this.physics.add.sprite(500, 20, 'jeremy');

    player.anims.load('throw_left');

    // this.input.keyboard.on('keydown_SPACE', function (event) {
        player.anims.play('throw_left');

    // });
// player2.anims.play('throw_left');

}


function update ()
{
}
