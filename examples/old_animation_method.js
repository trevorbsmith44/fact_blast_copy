var config = {
		type: Phaser.CANVAS,
		width: 1200,
		height: 600,
    backgroundColor: '#272765',
		physics: {
				default: 'arcade',
				arcade: {
						gravity: { y: 600 },
						debug: false
				}
		},
		scene: {
				preload: preload,
				create: create,
				update: update
		}
};

var player;
var platforms;
var bomb;
var game = new Phaser.Game(config);
var score;
var scoreText;
var tileset;
var map;
var layer;
var cursors;


function preload (){
    this.load.image('star', 'assets/star.png');
    this.load.image('bomb', 'assets/bomb.png');
    this.load.spritesheet('jeremy', 'assets/jeremy.png', { frameWidth: 66, frameHeight: 96 });
    // this.load.spritesheet('dude', 'assets/bottle.png', { frameWidth: 32, frameHeight: 32 });
    this.load.tilemapTiledJSON("map", "assets/maps/level3.json")
    this.load.image("tiles", "assets/futureish.png");
    }

function create (){
    map = this.add.tilemap("map");
    tileset = map.addTilesetImage("tileset", "tiles");
    layer = map.createStaticLayer(0, tileset, 0, 0); // layer index, tileset, x, y
    layer.setCollisionByProperty({ collides: true }); // tiles w/ collides set to true (in Tiled) will collide
		// layer.setCollision([32, 12, 13, 14, 20, 21, 22, 23, 24, 28, 29, 30, 31])


    this.anims.create({
        key: 'run_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 10, frames: [10,11,12,13] }),
        frameRate: 10,
        repeat: -1 // tells the animation to loop
        });
    this.anims.create({
        key: 'jump_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 17, end: 17 }),
        });
		this.anims.create({
        key: 'jump_left_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 18, frames: [18,19] }),
        });
    this.anims.create({
        key: 'stand_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 3, end: 3 }),
        });
		this.anims.create({
        key: 'stand_left_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 4, frames: [4,5] }),
				frameRate: 10
        });
    this.anims.create({
        key: 'turn',
        frames: [ { key: 'jeremy', frame: 4 } ],
        frameRate: 20
        });
    this.anims.create({
        key: 'run_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 6, frames: [6,7,8,9] }),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'jump_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 14, end: 14 }),
        });
		this.anims.create({
        key: 'jump_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 15, frames: [15,16] }),
        });
    this.anims.create({
        key: 'stand_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 0, end: 0 }),
        });
		this.anims.create({
        key: 'stand_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 1, frames: [1,2] }),
				frameRate: 10,
				repeat: 0,
        });
    cursors = this.input.keyboard.createCursorKeys();
    key_left = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    key_right = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
    key_jump = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
		key_throw = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.L);
		player = this.physics.add.sprite(300, 10, 'jeremy').setScale(1.5); // character spawn point and size

    player.setBounce(0.2); //character will bounce slightly when hits the ground
    player.body.setGravityY(300)
    this.physics.add.collider(player, layer);
    this.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
    this.cameras.main.startFollow(player);
    stars = this.physics.add.group();

    stars.children.iterate(function (child) {
        child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8)); //gives stars a random bounce between  0.4 and 0.8
        });

    this.physics.add.collider(stars, layer); // stars collide with platforms
    this.physics.add.overlap(player, stars, collectStar, null, this); // when player collides with a star,

    score = 0                                                            // call collectStar()
    function collectStar (player, star){
        star.disableBody(true, true);
        score += 10;
        // scoreText.setText('Score: ' + score);
        if (stars.countActive(true) === 0){                     // if all the stars are gone,
            stars.children.iterate(function (child) {           // iterate over them all
                child.enableBody(true, child.x, 0, true, true);}); // reset their position to top
            var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);
            //fancy if statement: choose a random x coordinate on the opposite side of the screen from the player
            var bomb = bombs.create(100, 16, 'bomb');             // create bomb
            this.physics.add.collider(bomb, layer);
            bomb.setBounce(1);
            // bomb.setCollideWorldBounds(true);
            bomb.setVelocity(Phaser.Math.Between(-200, 200), 20); // bomb has random velocity
            }
        }

    // scoreText = this.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#000' });

    bombs = this.physics.add.group();
    this.physics.add.collider(player, bombs, hitBomb, null, this);

    function hitBomb (player, bomb){
        this.physics.pause();
        player.setTint(0xff0000);
        player.anims.play('turn');
        gameOver = true;
        }

				player.anims.load('stand_right_and_throw')
				this.input.keyboard.on('keydown_SPACE', function (event) {
						player.anims.play('stand_right_and_throw');
				});
}

function update (){

    if (key_left.isDown){ // isDown means if the key is being held down on the keyboard
        player.setVelocityX(-200);
        if (player.body.onFloor()){
						player.anims.play('run_left', true);
					}
        else { // player not on floor
            player.anims.play('jump_left', true);}}
    else if (key_right.isDown){
        player.setVelocityX(200);
        if (player.body.onFloor()){
						player.anims.play('run_right', true);
					}
        else{
            player.anims.play('jump_right', true);}}
    else{ //if (!(key_left.isDown || key_right.isDown)){ // neither left, nor right key is being held down
        player.setVelocityX(0);
        if (player.body.onFloor()){
            if (key_left.timeDown > key_right.timeDown){
                player.anims.play('stand_left', true);
						}
            else{
                player.anims.play('stand_right', true);}}
        else { // player not touching the floor
            if (key_left.timeDown > key_right.timeDown){
                player.anims.play('jump_left', true);}
            else{
                player.anims.play('jump_right', true);}}
              }

    if (Phaser.Input.Keyboard.JustDown(key_jump) && player.body.onFloor()){ //jump code
        if (key_jump.repeats == 1){  // so the player won't jump repeatedly by holding down the button
            player.setVelocityY(-500);
            }
        }

		// console.log(player.anims.currentAnim['key']);
		// console.log(player.anims.currentFrame);
		// console.log(key_throw.timeDown)

		// key_throw.onDown(test_onDown, this);

		// function throw_fact(anim){
		// 		throw_anim = anim + '_and_throw'
		// 		player.anims.play(throw_anim, true);
		// }
// if (Phaser.Input.Keyboard.JustDown(key_throw)){
// 		current_anim = player.anims.currentAnim['key'];
// 		throw_anim = current_anim + '_and_throw'
// 		player.anims.play(throw_anim);
// 		player.setVelocityX(0);
// }

	// key_throw_down = key_throw.on('down', function throw_fact(){
	// 		current_anim = player.anims.currentAnim['key'];
	// 		throw_anim = current_anim + '_and_throw'
	// 		player.anims.play(throw_anim);
	// 		player.setVelocityX(0);
	// });
	// key_throw.onDown(key_throw_down);
}
