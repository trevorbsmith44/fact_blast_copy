var config = {
		type: Phaser.AUTO,
		width: 1200,
		height: 600,
    backgroundColor: '#272765',
		input: {
				gamepad: true
		},
		physics: {
				default: 'arcade',
				arcade: {
						gravity: { y: 800 },
						debug: false
				}
		},
		scene: {
				preload: preload,
				create: create,
				update: update
		}
};

var player;
var npc;
var platforms;
// var fact;
var game = new Phaser.Game(config);
var score;
var scoreText;
var tileset;
var map;
var layer;
var currently_throwing;
var time_at_throw_start
var time_at_collision
var time_at_damage
var player_layer
var gamepad
var gamepad_A_last_pressed = 0
var gamepad_X_last_pressed = 0
var gamepad_left_last_pressed
var gamepad_right_last_pressed

function preload (){
    this.load.image('star', 'assets/star.png');
    this.load.image('fact', 'assets/fact.png');
    this.load.spritesheet('jeremy', 'assets/jeremy.png', { frameWidth: 66, frameHeight: 96 });
		this.load.spritesheet('npc', 'assets/npc.png', { frameWidth: 64, frameHeight: 96 });
		this.load.spritesheet('life_bar', 'assets/life_bar.png', { frameWidth: 280, frameHeight: 50 });
    this.load.tilemapTiledJSON("map", "assets/maps/level3.json")
    this.load.image("tiles", "assets/futureish.png");
    }

function create (){
    map = this.add.tilemap("map");
    tileset = map.addTilesetImage("tileset", "tiles");
    layer = map.createStaticLayer(0, tileset, 0, 0); // layer index, tileset, x, y
    layer.setCollisionByProperty({ collides: true }); // tiles w/ collides set to true (in Tiled) will collide
		player = this.physics.add.sprite(400, 20, 'jeremy').setScale(1.5);
		player.setData({'health': 4,
										'post_hit_invincibility': false,
										'knock_back_time': 200,
										'may_jump': true,
										'may_throw': true})
		player.state = 'jump_right'
		// player.setCircle(33, 1, 100)
		player.setSize(37, 95, false)
		player.setOffset(15,0)

		//LIFE BAR
		life_bar = this.physics.add.sprite(140, 30, 'life_bar').setDepth(1000);
		life_bar.body.setAllowGravity(false);
		life_bar.setScrollFactor(0,0)
		life_bar.setFrame(4)

		//GAMEPAD
		this.input.gamepad.once('down', function (pad, button, index) {
				gamepad = pad;
		}, this);

		frameView = this.add.graphics({ fillStyle: { color: 0xff00ff }, x: 32, y: 32 });

		//JEREMY ANIMATIONS
    this.anims.create({
        key: 'run_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 10, frames: [10,11,12,13] }),
        frameRate: 10,
        repeat: -1 // tells the animation to loop
        });
    this.anims.create({
        key: 'jump_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 17, end: 17 }),
        });
    this.anims.create({
        key: 'stand_left',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 3, end: 3 }),
        });
    this.anims.create({
        key: 'run_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 6, frames: [6,7,8,9] }),
        frameRate: 10,
        repeat: -1
        });
    this.anims.create({
        key: 'jump_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 14, end: 14 }),
        });
		this.anims.create({
        key: 'jump_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 15, frames: [15,16,16,16,14] }),
				frameRate: 10,
        });
		this.anims.create({
				key: 'jump_left_and_throw',
				frames: this.anims.generateFrameNumbers('jeremy', { start: 18, frames: [18,19,19,19,17] }),
				frameRate: 10,
				});
    this.anims.create({
        key: 'stand_right',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 0, end: 0 }),
        });
		this.anims.create({
        key: 'stand_right_and_throw',
        frames: this.anims.generateFrameNumbers('jeremy', { start: 1, frames: [1,2,2,2,0] }),
				frameRate: 10,
				repeat: 0,
        });
		this.anims.create({
				key: 'stand_left_and_throw',
				frames: this.anims.generateFrameNumbers('jeremy', { start: 4, frames: [4,5,5,5,3] }),
				frameRate: 10,
				repeat: 0
				});
		this.anims.create({
				key: 'hit_to_left',
				frames: this.anims.generateFrameNumbers('jeremy', { start: 20, end: 20 }),
				});
		this.anims.create({
				key: 'hit_to_right',
				frames: this.anims.generateFrameNumbers('jeremy', { start: 21, end: 21 }),
				});

		//////////////////////////////////////////
		// NPC ANIMATIONS
		this.anims.create({
        key: 'npc_walk_left',
        frames: this.anims.generateFrameNumbers('npc', { start: 4, frames: [3,4,5,4] }),
        frameRate: 6,
        repeat: -1
        });
		this.anims.create({
				key: 'npc_walk_right',
				frames: this.anims.generateFrameNumbers('npc', { start: 1, frames: [0,1,2,1] }),
				frameRate: 6,
				repeat: -1
				});
		this.anims.create({
				key: 'npc_stand_right',
				frames: this.anims.generateFrameNumbers('npc', { start: 1, end: 1 }),
				});

		npc = this.physics.add.sprite(4200, 20, 'npc').setScale(1.5).setDepth(500);
		this.physics.add.collider(npc, layer);
		npc.setSize(40, 95, false)
		npc.setOffset(15,0)
		// this.physics.add.overlap(npc, player, damagePlayer, checkRealOverlap, this);

    key_left = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    key_right = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
		key_up = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
    key_jump = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.L);
		key_throw = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.K);

    player_layer = this.physics.add.collider(player, layer);
    this.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
    this.cameras.main.startFollow(player);
    stars = this.physics.add.group();

    stars.children.iterate(function (child) {
        child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8)); //gives stars a random bounce between  0.4 and 0.8
        });

    this.physics.add.collider(stars, layer); // stars collide with platforms
    this.physics.add.overlap(player, stars, collectStar, null, this); // when player collides w/ star call collectStar()

    score = 0
    function collectStar (player, star){
        star.disableBody(true, true);
        score += 10;
        // scoreText.setText('Score: ' + score);
        if (stars.countActive(true) === 0){                     // if all the stars are gone,
            stars.children.iterate(function (child) {           // iterate over them all
                child.enableBody(true, child.x, 0, true, true);}); // reset their position to top
            var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);
            //fancy if statement: choose a random x coordinate on the opposite side of the screen from the player
            var bomb = bombs.create(100, 16, 'bomb').setDepth(1000);             // create bomb
            this.physics.add.collider(bomb, layer);
            bomb.setBounce(1);
            // bomb.setCollideWorldBounds(true);
            bomb.setVelocity(Phaser.Math.Between(-200, 200), 20); // bomb has random velocity
            }
        }

    // scoreText = this.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#000' });

    // this.physics.add.collider(player, bombs, hitBomb, null, this);

    // function hitBomb (player, bomb){
    //     this.physics.pause();
    //     player.setTint(0xff0000);
    //     player.anims.play('turn');
    //     gameOver = true;
    //     }

		facts = this.physics.add.group();
		this.physics.add.collider(layer, facts, dissolveFact, null, this);
		this.physics.add.overlap(facts, npc, hitNPC, null, this);
		// npc.setAngularVelocity(-400)  // SPINNING!


}

function dissolveFact (fact, target){
		fact.destroy()
		fact.setVisible(false)
		}

function hitNPC (target, fact){
		target.destroy()
		target.setVisible(false)
		}

function throwFactGamePad(x_spawn, x_velocity, y_velocity){
		console.log('in function')
		var fact = facts.create((player.x + x_spawn), player.y, 'fact');
		fact.setBounce(1);
		if (!gamepad.up && key_up.isUp){
				console.log('up button not pressed')
				fact.setVelocity(x_velocity, y_velocity);}
		else{
				if (player.state.includes('right')){
						fact.setVelocity(x_velocity-250, y_velocity-400);}
				else if (player.state.includes('left')){
						fact.setVelocity(x_velocity+250, y_velocity-400);}}
		fact.onCollide = true;


		}

function throwFactKeyboard(x_spawn, x_velocity, y_velocity){
		console.log('in function')
		var fact = facts.create((player.x + x_spawn), player.y, 'fact');
		fact.setBounce(1);
		if (key_up.isUp){
				console.log('up button not pressed')
				fact.setVelocity(x_velocity, y_velocity);}
		else{
				if (player.state.includes('right')){
						fact.setVelocity(x_velocity-250, y_velocity-400);}
				else if (player.state.includes('left')){
						fact.setVelocity(x_velocity+250, y_velocity-400);}}
		fact.onCollide = true;
		}

function damagePlayer(enemy, player, time){
		if (player.data.values.post_hit_invincibility){
				return}
		player.data.values.health -= 1;
		if (player.data.values.health > 0){
				player.setAlpha(0.5)}  // Opacity
		life_bar.setFrame(player.data.values.health)
		player.data.values.post_hit_invincibility = true;
		time_at_damage = time
		}

function knockPlayerBack(distance){
		player.setPosition(player.x+distance, player.y, player.z, player.w)
		}

function checkRealOverlap(layer, player){
		// overlap = (enemy.x == player.x);
		overlap = ((player.x+50) > layer.x && layer.x > (player.x-50))
		return overlap
		}

function resetPlayerState(){
		if (player.body.onFloor()){
			if (npc.x < player.x){
					player.state = 'stand_left'}
			else {
					player.state = 'stand_right'}}
		else{
			if (npc.x < player.x){
					player.state = 'jump_left'}
			else {
					player.state = 'jump_right'}}
		}

function setPostHitInvincibility(){

		}

function restart(fjsdkl){
		this.cameras.main.shake(100, 0.005)
		}

function endGame(){
		// show game over screen and 'continue' button
		}

function update (){ // FINITE STATE MACHINE //

		if(gamepad){
			// if (gamepad.A){
			// 		gamepad_A_last_pressed = this.time.now}
			// if (gamepad.X){
			// 		gamepad_X_last_pressed = this.time.now}

		// console.log(player.state)
		if (player.state == 'stand_right'){

				player.setVelocityX(0);
				player.anims.play('stand_right', true);
				if (!player.body.onFloor()){
						player.state = 'jump_right'}
				else if (Phaser.Input.Keyboard.JustDown(key_jump) || (gamepad.A && player.data.values.may_jump)) {
						player.setVelocityY(-550);
						player.state = 'jump_right';}
				else if (key_right.isDown || gamepad.right){
						player.state = 'run_right'}
				else if (key_left.isDown || gamepad.left){
						player.state = 'run_left'}
				else if (Phaser.Input.Keyboard.JustDown(key_throw) || (gamepad.X && player.data.values.may_throw)) {
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'stand_right_and_throw'}}

		if (player.state == 'stand_left'){
				player.setVelocityX(0);
				player.anims.play('stand_left', true);
				if (!player.body.onFloor()){
						player.state = 'jump_left'}
				else if (Phaser.Input.Keyboard.JustDown(key_jump) || (gamepad.A && player.data.values.may_jump)){
						player.setVelocityY(-550);
						player.state = 'jump_left';}
				else if (key_left.isDown || gamepad.left){
						player.state = 'run_left'}
				else if (key_right.isDown || gamepad.right){
						player.state = 'run_right'}
				else if (Phaser.Input.Keyboard.JustDown(key_throw) || (gamepad.X && player.data.values.may_throw)) {
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'stand_left_and_throw'}}

		if (player.state == 'run_right'){
				player.setVelocityX(200);
				player.anims.play('run_right', true);
				if (!player.body.onFloor()){
						player.state = 'jump_right'}
				else if (Phaser.Input.Keyboard.JustDown(key_jump) || (gamepad.A && player.data.values.may_jump)){
						player.setVelocityY(-550);
						player.state = 'jump_right'}
				else if ((key_right.isUp) && (!gamepad.right)){
						player.state = 'stand_right'}
				else if ((key_left.isDown && key_right.isUp) || gamepad.left){
						player.state = 'run_left'}
				else if (key_left.isDown && key_right.isDown){
						player.state = 'run_right'}
				else if (Phaser.Input.Keyboard.JustDown(key_throw) || (gamepad.X && player.data.values.may_throw)){
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'stand_right_and_throw'}}

		if (player.state == 'run_left'){
				player.setVelocityX(-200);
				player.anims.play('run_left', true);
				if (!player.body.onFloor()){
						player.state = 'jump_left'}
				else if (Phaser.Input.Keyboard.JustDown(key_jump) || (gamepad.A && player.data.values.may_jump)){
						player.setVelocityY(-550);
						player.state = 'jump_left'}
				else if (key_left.isUp && (!gamepad.left)){
						player.state = 'stand_left'}
				else if ((key_right.isDown && key_left.isUp) || gamepad.right){
						player.state = 'run_right'}
				else if (key_left.isDown && key_right.isDown){
						player.state = 'run_left'}
				else if (Phaser.Input.Keyboard.JustDown(key_throw) || (gamepad.X && player.data.values.may_throw)){
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'stand_left_and_throw'}}

		if (player.state == 'jump_right'){
				player.anims.play('jump_right', true);
				player.clearTint()
				if (player.body.onFloor()){
						player.state = 'stand_right'}
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){
						player.state = 'jump_right'}
				else if ((key_right.isDown && Phaser.Input.Keyboard.JustDown(key_throw)) || (gamepad.right && (gamepad.X && player.data.values.may_throw))) {
							time_at_throw_start = Math.trunc(this.time.now)
							player.state = 'jump_right_and_throw'
							player.setVelocityX(250);}
				else if (key_right.isDown || gamepad.right){
						player.setVelocityX(250);}
						else if (Phaser.Input.Keyboard.JustDown(key_left) || (gamepad.left)){
								player.state = 'jump_left'}
				else if ((key_right.isUp && Phaser.Input.Keyboard.JustDown(key_throw)) || ((!gamepad.right) && (gamepad.X && player.data.values.may_throw))) {
						player.setVelocityX(0);
						time_at_throw_start = Math.trunc(this.time.now)
						player.state = 'jump_right_and_throw';}
				else if (Phaser.Input.Keyboard.JustUp(key_right) || (!gamepad.right)){
						player.setVelocityX(0);}}

			if (player.state == 'jump_left'){
					player.anims.play('jump_left', true);
					player.clearTint()
					if (player.body.onFloor()){
							player.state = 'stand_left'}
					else if (Phaser.Input.Keyboard.JustDown(key_jump)){
							player.state = 'jump_left'}
					else if ((key_left.isDown && Phaser.Input.Keyboard.JustDown(key_throw)) || (gamepad.left && (gamepad.X && player.data.values.may_throw))) {
								time_at_throw_start = Math.trunc(this.time.now)
								player.state = 'jump_left_and_throw'
								player.setVelocityX(-250);}
					else if (key_left.isDown || gamepad.left){
							player.setVelocityX(-250);}
							else if (Phaser.Input.Keyboard.JustDown(key_right) || (gamepad.right)){
									player.state = 'jump_right'}
					else if ((key_right.isUp && Phaser.Input.Keyboard.JustDown(key_throw)) || ((!gamepad.left) && (gamepad.X && player.data.values.may_throw))) {
							player.setVelocityX(0);
							time_at_throw_start = Math.trunc(this.time.now)
							player.state = 'jump_left_and_throw';}
					else if (Phaser.Input.Keyboard.JustUp(key_left) || (!gamepad.left)){
							player.setVelocityX(0);}}

		if (player.state == 'stand_right_and_throw'){
				player.setVelocityX(0);
				player.anims.play('stand_right_and_throw', true);
				now = Math.trunc(this.time.now);
				if ((time_at_throw_start + 122) > now && now > (time_at_throw_start + 100)){ //so it doesn't throw multiple
						throwFactGamePad(70, 400, -200)}
				if (now >= (time_at_throw_start + 250)){ // gives 250 miliseconds so it plays the animation once
						player.state = 'stand_right'}}

		if (player.state == 'stand_left_and_throw'){
				player.setVelocityX(0);
				player.anims.play('stand_left_and_throw', true);
				now = Math.trunc(this.time.now);
				if ((time_at_throw_start + 122) > now && now > (time_at_throw_start + 100)){
						throwFactGamePad(-70, -400, -200)}
				if (now >= (time_at_throw_start + 250)){
						player.state = 'stand_left'}}

		if (player.state == 'jump_right_and_throw'){
				player.anims.play('jump_right_and_throw', true);
				now = Math.trunc(this.time.now);
				if ((time_at_throw_start + 122) > now && now > (time_at_throw_start + 100)){
						throwFactGamePad(70, 400, -200)}
				if (now >= (time_at_throw_start + 250)){
						player.state = 'jump_right'}}

		if (player.state == 'jump_left_and_throw'){
				player.anims.play('jump_left_and_throw', true);
				now = Math.trunc(this.time.now);
				if ((time_at_throw_start + 122) > now && now > (time_at_throw_start + 100)){
						throwFactGamePad(-70, -400, -200)}
				if (now >= (time_at_throw_start + 250)){
						player.state = 'jump_left'}}

		if (player.state == 'hit_to_left'){
				player.anims.play('hit_to_left');
				if (key_left.isDown || gamepad.left){
						player.setVelocityX(-250);}
				// else if (Phaser.Input.Keyboard.JustUp(key_left)){
				// 		player.setVelocityX(0);}
				else if ((key_right.isDown || gamepad.right) && (!player.body.onFloor())) {
						player.state = 'hit_to_right'}}

		if (player.state == 'hit_to_right'){
				player.anims.play('hit_to_right');
				if (key_right.isDown || gamepad.right){
						player.setVelocityX(250);}
				else if (Phaser.Input.Keyboard.JustUp(key_right)){
						player.setVelocityX(0);}
				else if (key_left.isDown || gamepad.left){
						player.state = 'hit_to_left'}}

		player.data.values.may_jump = ((gamepad.A) ? false : true);
		player.data.values.may_throw = ((gamepad.X) ? false : true);
}
else{ // KEYBOARD ONLY - NO GAMEPAD																////
	if (player.state == 'stand_right'){                                                         ////
				player.setVelocityX(0);                                                         ////
				player.anims.play('stand_right', true);                                         ////
				if (!player.body.onFloor()){                                                    ////
						player.state = 'jump_right'}                                            ////
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){                             ////
						player.setVelocityY(-550);                                              ////
						player.state = 'jump_right';}                                           ////
				else if (key_right.isDown){                                                     ////
						player.state = 'run_right'}                                             ////
				else if (key_left.isDown){                                                      ////
						player.state = 'run_left'}                                              ////
				else if (Phaser.Input.Keyboard.JustDown(key_throw)){                            ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'stand_right_and_throw'}}                                ////
                                                                                                ////
		if (player.state == 'stand_left'){                                                      ////
				player.setVelocityX(0);                                                         ////
				player.anims.play('stand_left', true);                                          ////
				if (!player.body.onFloor()){                                                    ////
						player.state = 'jump_left'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){                             ////
						player.setVelocityY(-550);                                              ////
						player.state = 'jump_left';}                                            ////
				else if (key_left.isDown){                                                      ////
						player.state = 'run_left'}                                              ////
				else if (key_right.isDown){                                                     ////
						player.state = 'run_right'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(key_throw)){                            ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'stand_left_and_throw'}}                                 ////
                                                                                                ////
		if (player.state == 'run_right'){                                                       ////
				player.setVelocityX(200);                                                       ////
				player.anims.play('run_right', true);                                           ////
				if (!player.body.onFloor()){                                                    ////
						player.state = 'jump_right'}                                            ////
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){                             ////
						player.setVelocityY(-550);                                              ////
						player.state = 'jump_right'}                                            ////
				else if (Phaser.Input.Keyboard.JustUp(key_right)){                              ////
						player.state = 'stand_right'}                                           ////
				else if (key_left.isDown && key_right.isUp){                                    ////
						player.state = 'run_left'}                                              ////
				else if (key_left.isDown && key_right.isDown){                                  ////
						player.state = 'run_right'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(key_throw)){                            ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'stand_right_and_throw'}}                                ////
                                                                                                ////
		if (player.state == 'run_left'){                                                        ////
				player.setVelocityX(-200);                                                      ////
				player.anims.play('run_left', true);                                            ////
				if (!player.body.onFloor()){                                                    ////
						player.state = 'jump_left'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){                             ////
						player.setVelocityY(-550);                                              ////
						player.state = 'jump_left'}                                             ////
				else if (Phaser.Input.Keyboard.JustUp(key_left)){                               ////
						player.state = 'stand_left'}                                            ////
				else if (key_right.isDown && key_left.isUp){                                    ////
						player.state = 'run_right'}                                             ////
				else if (key_left.isDown && key_right.isDown){                                  ////
						player.state = 'run_left'}                                              ////
				else if (Phaser.Input.Keyboard.JustDown(key_throw)){                            ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'stand_left_and_throw'}}                                 ////
                                                                                                ////
		if (player.state == 'jump_right'){                                                      ////
				player.anims.play('jump_right', true);                                          ////
				player.clearTint()                                                              ////
				if (player.body.onFloor()){                                                     ////
						player.state = 'stand_right'}                                           ////
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){                             ////
						player.state = 'jump_right'}                                            ////
				else if (key_right.isDown && Phaser.Input.Keyboard.JustDown(key_throw)){        ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'jump_right_and_throw'                                   ////
						player.setVelocityX(250);}                                              ////
				else if (key_right.isDown){                                                     ////
						player.setVelocityX(250);}                                              ////
				else if (Phaser.Input.Keyboard.JustUp(key_right)){                              ////
						player.setVelocityX(0);}                                                ////
				else if (Phaser.Input.Keyboard.JustDown(key_left)){                             ////
						player.state = 'jump_left'}                                             ////
				else if (Phaser.Input.Keyboard.JustDown(key_throw)){                            ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'jump_right_and_throw'}}                                 ////
                                                                                                ////
		if (player.state == 'jump_left'){                                                       ////
				player.anims.play('jump_left', true);                                           ////
				if (player.body.onFloor()){                                                     ////
						player.state = 'stand_left'}                                            ////
				else if (Phaser.Input.Keyboard.JustDown(key_jump)){                             ////
						player.state = 'jump_left'}                                             ////
				else if (key_left.isDown && Phaser.Input.Keyboard.JustDown(key_throw)){         ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'jump_left_and_throw'                                    ////
						player.setVelocityX(-250);}                                             ////
				else if (key_left.isDown){                                                      ////
						player.setVelocityX(-250);}                                             ////
				else if (Phaser.Input.Keyboard.JustUp(key_left)){                               ////
						player.setVelocityX(0);}                                                ////
				else if (Phaser.Input.Keyboard.JustDown(key_right)){                            ////
						player.state = 'jump_right'}                                            ////
				else if (Phaser.Input.Keyboard.JustDown(key_throw)){                            ////
						time_at_throw_start = Math.trunc(this.time.now)                         ////
						player.state = 'jump_left_and_throw'}}                                  ////
                                                                                                ////
		if (player.state == 'stand_right_and_throw'){                                           ////
				player.setVelocityX(0);                                                         ////
				player.anims.play('stand_right_and_throw', true);                               ////
				now = Math.trunc(this.time.now);                                                ////
				if ((time_at_throw_start + 122) > now && now > (time_at_throw_start + 100)){    ////
						throwFactKeyboard(70, 400, -200)}                                               ////
				if (now >= (time_at_throw_start + 250)){                                        ////
						player.state = 'stand_right'}}                                          ////
                                                                                                ////
		if (player.state == 'stand_left_and_throw'){                                            ////
				player.setVelocityX(0);                                                         ////
				player.anims.play('stand_left_and_throw', true);                                ////
				now = Math.trunc(this.time.now);                                                ////
				if ((time_at_throw_start + 122) > now && now > (time_at_throw_start + 100)){    ////
						throwFactKeyboard(-70, -400, -200)}                                             ////
				if (now >= (time_at_throw_start + 250)){                                        ////
						player.state = 'stand_left'}}                                           ////
                                                                                                ////
		if (player.state == 'jump_right_and_throw'){                                            ////
				player.anims.play('jump_right_and_throw', true);                                ////
				now = Math.trunc(this.time.now);                                                ////
				if ((time_at_throw_start + 122) > now && now > (time_at_throw_start + 100)){    ////
						throwFactKeyboard(70, 400, -200)}                                               ////
				if (now >= (time_at_throw_start + 250)){                                        ////
						player.state = 'jump_right'}}                                           ////
                                                                                                ////
		if (player.state == 'jump_left_and_throw'){                                             ////
				player.anims.play('jump_left_and_throw', true);                                 ////
				now = Math.trunc(this.time.now);                                                ////
				if ((time_at_throw_start + 122) > now && now > (time_at_throw_start + 100)){    ////
						throwFactKeyboard(-70, -400, -200)}                                             ////
				if (now >= (time_at_throw_start + 250)){                                        ////
						player.state = 'jump_left'}}                                            ////
                                                                                                ////
		if (player.state == 'hit_to_left'){                                                     ////
				player.anims.play('hit_to_left');                                               ////
				if (key_left.isDown){                                                           ////
						player.setVelocityX(-250);}                                             ////
				// else if (Phaser.Input.Keyboard.JustUp(key_left)){                            ////
				// 		player.setVelocityX(0);}                                                ////
				else if (key_right.isDown && (!player.body.onFloor())) {                        ////
						player.state = 'hit_to_right'}}                                         ////
                                                                                                ////
		if (player.state == 'hit_to_right'){                                                    ////
				player.anims.play('hit_to_right');                                              ////
				if (key_right.isDown){                                                          ////
						player.setVelocityX(250);}                                              ////
				else if (Phaser.Input.Keyboard.JustUp(key_right)){                              ////
						player.setVelocityX(0);}                                                ////
				else if (key_left.isDown){                                                      ////
						player.state = 'hit_to_left'}}                                          ////
}                                                                                               ////
		if (npc.active){
				// NPC ANIMATIONS
				if (npc.x > player.x+10){
						npc.setVelocityX(-50);
						npc.anims.play('npc_walk_left', true);}
				else if (npc.x < player.x-10){
						npc.setVelocityX(50);
						npc.anims.play('npc_walk_right', true);}
				else if ((player.x+10) > npc.x && npc.x > (player.x-10)){
						npc.setVelocityX(0);
						npc.anims.play('npc_stand_right', true)}
				// DAMAGE PLAYER AND SET INVINCIBILITY
				xOverlap = ((player.x+50) > npc.x && npc.x > (player.x-50))
				yOverlap = ((player.y+75) > npc.y && npc.y > (player.y-75))
				if (xOverlap && yOverlap){
						time_at_collision = this.time.now
						damagePlayer(npc, player, this.time.now)}
				if (player.data.values.health <= 0){
						player.data.values.knock_back_time = 5000}
				if (this.time.now < (time_at_collision + player.data.values.knock_back_time)) {
						// this.cameras.main.shake(100, 0.005)
						if (player.x <= npc.x){
								knockPlayerBack(-7);
								player.state = 'hit_to_left'}
						else{
								knockPlayerBack(7);
								player.state = 'hit_to_right'}}
				else if ((time_at_collision + 200) <= this.time.now  // 200 miliseconds after hit
									&& player.state.includes('hit')
									&& player.body.onFloor()) {
						resetPlayerState()}

				}
				if (Math.trunc(this.time.now) > (Math.trunc(time_at_damage) + 2000)) {
						player.data.values.post_hit_invincibility = false
						player.setAlpha(1)}

				if (player.data.values.health == 0){
						if (this.time.now < (time_at_collision + 5000)) {
								player.setPosition(player.x, player.y-5, player.z, player.w)
								player.setAngularVelocity(-400)}
						this.physics.world.removeCollider(player_layer);
						}

				if (player.y > (layer.height+100)){
						this.cameras.main.shake(500);
						this.time.delayedCall(400, function() { // wait 400 ms, then fade
							this.cameras.main.fade(300); // fade effect lasts 400 ms
						}, [], this);
						this.time.delayedCall(800, function() {
							this.scene.restart(); // restart game
						}, [], this);
						}
}
