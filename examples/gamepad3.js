var config = {
    type: Phaser.AUTO,
    parent: 'phaser-example',
    width: 800,
    height: 600,
    input: {
        gamepad: true
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var sprite;
var gamepad;

var game = new Phaser.Game(config);

function preload ()
{
    this.load.image('sky', 'assets/skies/lightblue.png');
    this.load.image('elephant', 'assets/sprites/elephant.png');
}

function create ()
{

    this.input.gamepad.once('down', function (pad, button, index) {
        gamepad = pad;
        sprite = this.add.image(400, 300, 'elephant');
    }, this);
}

function update ()
{
    if (gamepad){
        if (gamepad.left){
            sprite.x -= 4;
            sprite.flipX = false;}
        else if (gamepad.right){
            sprite.x += 4;
            sprite.flipX = true;}
        if (gamepad.up){
            sprite.y -= 4;}
        else if (gamepad.down){
            sprite.y += 4;}
    }
}
